#include "..\Headers\Mesh_Dynamic.h"
#include "HierarchyLoader.h"
#include "AnimationCtrl.h"
#include "MeshTexture.h"
CMesh_Dynamic::CMesh_Dynamic(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{

}

CMesh_Dynamic::CMesh_Dynamic(const CMesh_Dynamic & rhs)
	: CComponent(rhs)
	, m_vecMeshContainer(rhs.m_vecMeshContainer)
	, m_pLoader(rhs.m_pLoader)
	, m_pRootFrame(rhs.m_pRootFrame)
	, m_matPivot(rhs.m_matPivot)
	, m_pAnimationCtrl(rhs.m_pAnimationCtrl->Clone())
{

	if (rhs.m_pServeAnimationCtrl != nullptr)
	{
		m_pServeAnimationCtrl = rhs.m_pServeAnimationCtrl->Clone();
		m_pServeRootFrame = rhs.m_pServeRootFrame;
		m_pServeRootName = rhs.m_pServeRootName;
		m_pConnectFrame = rhs.m_pConnectFrame;
		m_eObject = rhs.m_eObject;
	}

	if (rhs.m_pTorsoAnimationCtrl != nullptr)
	{
		m_pTorsoAnimationCtrl = rhs.m_pTorsoAnimationCtrl->Clone();
		m_pTorsoFrame = rhs.m_pTorsoFrame;

	}

	if (rhs.m_pArmAnimationCtrl != nullptr)
	{
		m_pArmAnimationCtrl = rhs.m_pArmAnimationCtrl->Clone();
		m_pArmFrame = rhs.m_pArmFrame;
		m_pArmConnectFrame = rhs.m_pArmConnectFrame;
	}
	
	m_pLoader->AddRef();
}

_matrix CMesh_Dynamic::Get_LocalTransform(const _uint & iContainerIdx) const
{
	_matrix			matResult;
	D3DXMatrixIdentity(&matResult);

	if (m_vecMeshContainer.size() <= iContainerIdx)
		return matResult;

	_matrix			matScale, matTrans;

	D3DXMatrixScaling(&matScale, m_vecMeshContainer[iContainerIdx]->vMax.x - m_vecMeshContainer[iContainerIdx]->vMin.x, m_vecMeshContainer[iContainerIdx]->vMax.y - m_vecMeshContainer[iContainerIdx]->vMin.y, m_vecMeshContainer[iContainerIdx]->vMax.z - m_vecMeshContainer[iContainerIdx]->vMin.z);

	D3DXMatrixTranslation(&matTrans, (m_vecMeshContainer[iContainerIdx]->vMax.x + m_vecMeshContainer[iContainerIdx]->vMin.x) * 0.5f, (m_vecMeshContainer[iContainerIdx]->vMax.y + m_vecMeshContainer[iContainerIdx]->vMin.y) * 0.5f, (m_vecMeshContainer[iContainerIdx]->vMax.z + m_vecMeshContainer[iContainerIdx]->vMin.z) * 0.5f);

	return matResult = matScale * matTrans;
}

const _matrix * CMesh_Dynamic::Find_Frame(const char * pFrameName)
{
	D3DXFRAME_DERIVED*	pFindFrame = ((D3DXFRAME_DERIVED*)D3DXFrameFind(m_pRootFrame, pFrameName));

	if (nullptr != pFindFrame)
		return &pFindFrame->CombinedTransformationMatrix;
	else
	{
		pFindFrame = ((D3DXFRAME_DERIVED*)D3DXFrameFind(m_pServeRootFrame, pFrameName));
		return &pFindFrame->CombinedTransformationMatrix;
	}
}

_bool CMesh_Dynamic::IsOverTime(const _float& fCorrectionValue)
{
	//if (m_pServeAnimationCtrl != nullptr)
	//	return m_pServeAnimationCtrl->IsOverTime(fCorrectionValue);

	return m_pAnimationCtrl->IsOverTime(fCorrectionValue);
}

HRESULT CMesh_Dynamic::Ready_Mesh_Dynamic(const _tchar * pFilePath, const _tchar * pFileName, void* pArg)

{
	LPD3DXMESH		pMesh = nullptr;

	_tchar			szFullPath[MAX_PATH] = L"";

	lstrcpy(szFullPath, pFilePath);
	lstrcat(szFullPath, pFileName);

	m_pLoader = CHierarchyLoader::Create(m_pGraphic_Device, pFilePath);
	if (nullptr == m_pLoader)
		return E_FAIL;

	LPD3DXANIMATIONCONTROLLER			pAniCtrl = nullptr;

	if (FAILED(D3DXLoadMeshHierarchyFromX(szFullPath, D3DXMESH_MANAGED, m_pGraphic_Device, m_pLoader, nullptr, &m_pRootFrame, &pAniCtrl)))
		return E_FAIL;

	if (nullptr != pAniCtrl)
	{
		m_pAnimationCtrl = CAnimationCtrl::Create(pAniCtrl);

		if (nullptr == m_pAnimationCtrl)
			return E_FAIL;
	}

	Safe_Release(pAniCtrl);

	D3DXMatrixRotationY(&m_matPivot, D3DXToRadian(180.f));

	Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pRootFrame, &m_matPivot);
	SetUp_CombinedMatrixPointer((D3DXFRAME_DERIVED*)m_pRootFrame);

	if (pArg == nullptr)
		return NOERROR;

	OBJECT* eObject = (OBJECT*)&pArg;
	m_eObject = *eObject;
	if (*eObject == CAMPER)
	{
		m_pServeRootName = "joint_Pelvis_01";
		m_pServeAnimationCtrl = m_pAnimationCtrl->Clone();
		m_pArmAnimationCtrl = m_pAnimationCtrl->Clone();
		m_pTorsoAnimationCtrl = m_pAnimationCtrl->Clone();
		Separate_Bone((D3DXFRAME_DERIVED*)m_pRootFrame);
		m_pArmFrame = D3DXFrameFind(m_pServeRootFrame, "joint_ShoulderRT_01");
		m_pArmConnectFrame = D3DXFrameFind(m_pServeRootFrame, "joint_ClavicleRT_01");
	}
	else if (*eObject == WRAITH)
	{
		m_pServeRootName = "joint_Pelvis_01";
		m_pServeAnimationCtrl = m_pAnimationCtrl->Clone();
		m_pArmAnimationCtrl = m_pAnimationCtrl->Clone();
		Separate_Bone((D3DXFRAME_DERIVED*)m_pRootFrame);
		m_pArmFrame = D3DXFrameFind(m_pRootFrame, "joint_ShoulderRT_01");
		m_pArmConnectFrame = D3DXFrameFind(m_pRootFrame, "joint_ClavicleRT_01");
	}
	else if (*eObject == SPIRIT)
	{
		m_pServeRootName = "joint_Pelvis_01";
		m_pServeAnimationCtrl = m_pAnimationCtrl->Clone();
		Separate_Bone((D3DXFRAME_DERIVED*)m_pRootFrame);
	}

	return NOERROR;
}

_bool CMesh_Dynamic::Update_Skinning(const _uint& iMeshContainerIdx, const _uint& iAttributeID)
{
	if (m_vecMeshContainer.size() < iMeshContainerIdx ||
		m_vecMeshContainer[iMeshContainerIdx]->NumMaterials <= iAttributeID)
		return false;

	D3DXMESHCONTAINER_DERIVED*	pMeshContainer = m_vecMeshContainer[iMeshContainerIdx];
	if (nullptr == pMeshContainer)
		return false;

	if (pMeshContainer->pSkinInfo == NULL)
	{
		pMeshContainer->MeshData.pMesh->DrawSubset(iAttributeID);
		return true;
	}


	if (nullptr == pMeshContainer->pBoneCombinationBuf)
		return false;

	_uint Vindex = pMeshContainer->dwNumFrames;

	LPD3DXBONECOMBINATION pBoneComb = reinterpret_cast<LPD3DXBONECOMBINATION>(pMeshContainer->pBoneCombinationBuf->GetBufferPointer());

	for (_uint iPaletteEntry = 0; iPaletteEntry < Vindex; ++iPaletteEntry)
	{
		int iMatIndex = pBoneComb[iAttributeID].BoneId[iPaletteEntry];
		if (iMatIndex != UINT_MAX)
		{
			D3DXMatrixMultiply(&pMeshContainer->pRenderingMatrices[iPaletteEntry]
				, &pMeshContainer->pOffsetMatrices[iMatIndex], pMeshContainer->ppCombinedTransformationMatrices[iMatIndex]);
		}
	}
	return false;
}

void CMesh_Dynamic::Update_Hair(const _uint & iMeshContainerIdx, const _uint & iAttributeID)
{
	D3DXMESHCONTAINER_DERIVED*	pMeshContainer = m_vecMeshContainer[iMeshContainerIdx];
	if (nullptr == pMeshContainer)
		return;

	if (nullptr == pMeshContainer->pBoneCombinationBuf)
		return;

	_uint Vindex = pMeshContainer->dwNumFrames;
	_vec3 vNewPos;

	LPD3DXBONECOMBINATION pBoneComb = reinterpret_cast<LPD3DXBONECOMBINATION>(pMeshContainer->pBoneCombinationBuf->GetBufferPointer());

	for (_uint iPaletteEntry = 0; iPaletteEntry < Vindex; ++iPaletteEntry)
	{
		_int a = (rand() % 40) - 20;
		_int b = (rand() % 3) + 40;
		_int c = (rand() % 40) - 20;

		int iMatIndex = pBoneComb[iAttributeID].BoneId[iPaletteEntry];
		if (iMatIndex != UINT_MAX)
		{
			D3DXMatrixMultiply(&pMeshContainer->pRenderingMatrices[iPaletteEntry]
				, &pMeshContainer->pOffsetMatrices[iMatIndex], pMeshContainer->ppCombinedTransformationMatrices[iMatIndex]);

			memcpy(&vNewPos, &pMeshContainer->pRenderingMatrices[iPaletteEntry].m[3], sizeof(_vec3));
			vNewPos.x += a * 0.01f * iPaletteEntry;
			vNewPos.y += b * iPaletteEntry * 0.05f;
			vNewPos.z += c * 0.01f * iPaletteEntry;
			memcpy(&pMeshContainer->pRenderingMatrices[iPaletteEntry].m[3], &vNewPos, sizeof(_vec3));
		}
	}
	return;
}

void CMesh_Dynamic::Render_Mesh(const _uint& iMeshContainerIdx, const _uint& iAttributeID)
{
	if (m_vecMeshContainer.size() <= iMeshContainerIdx ||
		m_vecMeshContainer[iMeshContainerIdx]->NumMaterials <= iAttributeID)
		return;

	D3DXMESHCONTAINER_DERIVED*	pMeshContainer = m_vecMeshContainer[iMeshContainerIdx];
	if (nullptr == pMeshContainer)
		return;

	pMeshContainer->MeshData.pMesh->DrawSubset(iAttributeID);
}

HRESULT CMesh_Dynamic::Set_AnimationSet(const _uint& iAnimationID, _bool bIsInterrupt)
{
	return m_pAnimationCtrl->Set_AnimationSet(iAnimationID, bIsInterrupt);
}
HRESULT CMesh_Dynamic::Set_NoBleningAnimationSet(const _uint & iAnimationID)
{
	if (m_pTorsoAnimationCtrl != nullptr)
		m_pTorsoAnimationCtrl->Set_NoBleningAnimationSet(iAnimationID);

	if(m_pServeAnimationCtrl != nullptr)
		m_pServeAnimationCtrl->Set_NoBleningAnimationSet(iAnimationID);

	if (m_pArmAnimationCtrl != nullptr)
		m_pArmAnimationCtrl->Set_NoBleningAnimationSet(iAnimationID);

	
	return m_pAnimationCtrl->Set_NoBleningAnimationSet(iAnimationID);
}

HRESULT CMesh_Dynamic::Set_ServeAnimationSet(const _uint & iAnimationID, _bool bIsInterrupt)
{
	return m_pServeAnimationCtrl->Set_AnimationSet(iAnimationID, bIsInterrupt);
}

HRESULT CMesh_Dynamic::Set_TorsoAnimationSet(const _uint & iAnimationID, _bool bIsInterrupt)
{
	return m_pTorsoAnimationCtrl->Set_AnimationSet(iAnimationID, bIsInterrupt);
}

void CMesh_Dynamic::Play_Animation(const _float& fTimeDelta)
{

	m_pAnimationCtrl->Play_Animation(fTimeDelta);

	D3DXMatrixRotationY(&m_matPivot, D3DXToRadian(180.f));

	Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pRootFrame, &m_matPivot);
}

void CMesh_Dynamic::Play_ServeAnimation(const _float & fTimeDelta)
{
	m_pServeAnimationCtrl->Play_Animation(fTimeDelta);

	if(m_eObject == CAMPER)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pTorsoFrame->pFrameFirstChild, &((D3DXFRAME_DERIVED*)m_pTorsoFrame)->CombinedTransformationMatrix);
	else if (m_eObject == WRAITH)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pServeRootFrame, &((D3DXFRAME_DERIVED*)m_pConnectFrame)->CombinedTransformationMatrix);
	else
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pServeRootFrame, &((D3DXFRAME_DERIVED*)m_pConnectFrame)->CombinedTransformationMatrix);
}

void CMesh_Dynamic::Play_TorsoAnimation(const _float & fTImeDelta)
{
	m_pTorsoAnimationCtrl->Play_Animation(fTImeDelta);

	((D3DXFRAME_DERIVED*)m_pTorsoFrame)->CombinedTransformationMatrix = m_pTorsoFrame->TransformationMatrix *((D3DXFRAME_DERIVED*)m_pConnectFrame)->CombinedTransformationMatrix;
}
void CMesh_Dynamic::Setup_AnimationTime(const _uint& Track, const _float & fFixingTime)
{
	m_pAnimationCtrl->Setup_AnimationTime(Track, fFixingTime);
}

HRESULT CMesh_Dynamic::Set_SpecifyAnimationSet(ANIMSET tagAnimSet)
{
	return m_pAnimationCtrl->Set_SpecifyAnimationSet(tagAnimSet);
}

HRESULT CMesh_Dynamic::Set_SpecifyTorsoAnimationSet(ANIMSET tagAnimSet)
{
	return m_pTorsoAnimationCtrl->Set_SpecifyAnimationSet(tagAnimSet);
}

void CMesh_Dynamic::Set_ArmAnimationSet(const _uint & iAnimationID)
{
	m_pArmAnimationCtrl->Set_AnimationSet(iAnimationID);
}

void CMesh_Dynamic::Play_ArmAnimation(const _float & fTimeDelta)
{
	m_pArmAnimationCtrl->Play_Animation(fTimeDelta);

	Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED*)m_pArmFrame, &((D3DXFRAME_DERIVED*)m_pArmConnectFrame)->CombinedTransformationMatrix);
}

void CMesh_Dynamic::Stop_Animation()
{
	m_pAnimationCtrl->Stop_Animation();
	m_pServeAnimationCtrl->Stop_Animation();
}

_uint CMesh_Dynamic::Get_CurAnimation()
{
	return m_pAnimationCtrl->Get_CurAnimation();
}
_double CMesh_Dynamic::GetPeriod()
{
	return m_pAnimationCtrl->GetPeriod();
}
_double CMesh_Dynamic::GetPeriod(_uint AniNum)
{
	return m_pAnimationCtrl->GetPeriod(AniNum);
}
_double CMesh_Dynamic::GetTimeAcc()
{
	return m_pAnimationCtrl->GetTimeAcc();
}

void CMesh_Dynamic::SetTimeAcc(_double TimeAcc)
{
	m_pServeAnimationCtrl->SetTimeAcc(TimeAcc);
}

HRESULT CMesh_Dynamic::Set_ForceAnimationSet(const _uint & iAnimationID)
{
	return NOERROR;
}

void CMesh_Dynamic::Update_CombinedTransformationMatrices(D3DXFRAME_DERIVED * pFrame, const _matrix* pParentMatrix)
{
	pFrame->CombinedTransformationMatrix = pFrame->TransformationMatrix * *pParentMatrix;

	if (nullptr != pFrame->pFrameSibling)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED *)pFrame->pFrameSibling, pParentMatrix);

	if (nullptr != pFrame->pFrameFirstChild)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED *)pFrame->pFrameFirstChild, &pFrame->CombinedTransformationMatrix);
}

void CMesh_Dynamic::Update_CombinedTransformationMatrices(D3DXFRAME_DERIVED * pFrame, const _matrix * pParentMatrix, const char * pFrameName)
{
	if(strcmp(pFrame->Name, pFrameName))
		pFrame->CombinedTransformationMatrix = pFrame->TransformationMatrix * *pParentMatrix;

	if (nullptr != pFrame->pFrameSibling)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED *)pFrame->pFrameSibling, pParentMatrix);

	if (nullptr != pFrame->pFrameFirstChild)
		Update_CombinedTransformationMatrices((D3DXFRAME_DERIVED *)pFrame->pFrameFirstChild, &pFrame->CombinedTransformationMatrix);
}

HRESULT CMesh_Dynamic::SetUp_CombinedMatrixPointer(D3DXFRAME_DERIVED * pFrame)
{
	if (nullptr != pFrame->pMeshContainer)
	{
		D3DXMESHCONTAINER_DERIVED* pMeshContainer = (D3DXMESHCONTAINER_DERIVED*)pFrame->pMeshContainer;

		m_vecMeshContainer.push_back(pMeshContainer);

		for (_ulong i = 0; i < pMeshContainer->dwNumFrames; ++i)
		{
			const char* pBoneName = pMeshContainer->pSkinInfo->GetBoneName(i);
			if (nullptr == pBoneName)
				return E_FAIL;

			D3DXFRAME_DERIVED*	pFindFrame = (D3DXFRAME_DERIVED*)D3DXFrameFind(m_pRootFrame, pBoneName);
			if (nullptr == pFindFrame)
				return E_FAIL;

			pMeshContainer->ppCombinedTransformationMatrices[i] = &pFindFrame->CombinedTransformationMatrix;
		}
	}

	HRESULT			hr = 0;

	if (nullptr != pFrame->pFrameSibling)
	{
		if (FAILED(SetUp_CombinedMatrixPointer((D3DXFRAME_DERIVED *)pFrame->pFrameSibling)))
			return E_FAIL;
	}


	if (nullptr != pFrame->pFrameFirstChild)
	{
		if (FAILED(SetUp_CombinedMatrixPointer((D3DXFRAME_DERIVED *)pFrame->pFrameFirstChild)))
			return E_FAIL;
	}

	return NOERROR;
}

void CMesh_Dynamic::Separate_Bone(D3DXFRAME_DERIVED * pFrame)
{
	if (m_eObject == CAMPER)
	{
		if (pFrame->Name != nullptr)
		{
			if (!strcmp(pFrame->Name, m_pServeRootName))
			{
				m_pConnectFrame = pFrame;
				m_pServeRootFrame = pFrame->pFrameFirstChild->pFrameSibling;
				m_pTorsoFrame = pFrame->pFrameFirstChild->pFrameSibling;
				pFrame->pFrameFirstChild->pFrameSibling = nullptr;
			}
		}

	}
	else if (m_eObject == WRAITH)
	{
		if (pFrame->Name != nullptr)
		{
			if (!strcmp(pFrame->Name, m_pServeRootName))
			{
				m_pConnectFrame = pFrame;
				m_pServeRootFrame = pFrame->pFrameFirstChild;
				m_pTorsoFrame = pFrame->pFrameFirstChild->pFrameSibling;
				pFrame->pFrameFirstChild = m_pTorsoFrame;
				m_pServeRootFrame->pFrameSibling = nullptr;
				//m_pServeRootFrame = pFrame->pFrameFirstChild;
				//pFrame->pFrameFirstChild = pFrame->pFrameFirstChild->pFrameSibling;
				////m_pTorsoFrame = pFrame->pFrameFirstChild->pFrameSibling->pFrameSibling;
				//m_pServeRootFrame->pFrameSibling = nullptr;
			}
		}
	}
	else if (m_eObject == SPIRIT)
	{
		if (!strcmp(pFrame->Name, m_pServeRootName))
		{
			m_pConnectFrame = pFrame;
			m_pServeRootFrame = pFrame->pFrameFirstChild;
			m_pTorsoFrame = pFrame->pFrameFirstChild->pFrameSibling;
			//m_pTorsoFrame = pFrame->pFrameFirstChild->pFrameSibling;
			pFrame->pFrameFirstChild->pFrameSibling = nullptr;
			pFrame->pFrameFirstChild = m_pTorsoFrame;
		}
	}

	if (nullptr != pFrame->pFrameSibling)
		Separate_Bone((D3DXFRAME_DERIVED *)pFrame->pFrameSibling);

	if (nullptr != pFrame->pFrameFirstChild)
		Separate_Bone((D3DXFRAME_DERIVED *)pFrame->pFrameFirstChild);
}

CMesh_Dynamic * CMesh_Dynamic::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar* pFilePath, const _tchar* pFileName, void* pArg)
{
	CMesh_Dynamic*	pInstance = new CMesh_Dynamic(pGraphic_Device);

	if (FAILED(pInstance->Ready_Mesh_Dynamic(pFilePath, pFileName, pArg)))
	{
		MessageBox(0, L"CMesh_Dynamic Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CMesh_Dynamic::Clone_Component(void* pArg)
{
	return new CMesh_Dynamic(*this);
}

void CMesh_Dynamic::Free()
{
	if (false == m_isClone)
	{
		if (m_pServeRootFrame != nullptr)
		{
			if (m_eObject == CAMPER)
				m_pConnectFrame->pFrameFirstChild->pFrameSibling = m_pServeRootFrame;
			else if (m_eObject == WRAITH)
			{
				m_pConnectFrame->pFrameFirstChild = m_pServeRootFrame;
				m_pServeRootFrame->pFrameSibling = m_pTorsoFrame;
			}
			else if (m_eObject == SPIRIT)
			{
				m_pConnectFrame->pFrameFirstChild = m_pServeRootFrame;
				m_pServeRootFrame->pFrameSibling = m_pTorsoFrame;
			}
		}
		m_pLoader->DestroyFrame(m_pRootFrame);

		for (auto& pMeshContainer : m_vecMeshContainer)
		{
			m_pLoader->DestroyMeshContainer(pMeshContainer);
		}
	}

	Safe_Release(m_pServeAnimationCtrl);
	Safe_Release(m_pArmAnimationCtrl);
	Safe_Release(m_pTorsoAnimationCtrl);
	Safe_Release(m_pAnimationCtrl);

	m_vecMeshContainer.clear();

	Safe_Release(m_pLoader);

	CComponent::Free();
}

