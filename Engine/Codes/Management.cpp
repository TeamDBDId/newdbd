#include "Management.h"
#include "Scene.h"
#include "System.h"
#include "Timer_Manager.h"
#include "Frame_Manager.h"
#include "Input_Device.h"
#include "Graphic_Device.h"
#include "Light_Manager.h"
#include "Target_Manager.h"
#include "Coll_Manager.h"
#include "GameObject.h"
#include "MeshTexture.h"

_IMPLEMENT_SINGLETON(CManagement)

CManagement::CManagement()
	: m_pObject_Manager(CObject_Manager::GetInstance())
	, m_pComponent_Manager(CComponent_Manager::GetInstance())
{
	m_pObject_Manager->AddRef();
	m_pComponent_Manager->AddRef();
}

CComponent * CManagement::Get_ComponentPointer(const _uint & iSceneID, const _tchar * pLayerTag, const _tchar * pComponentTag, const _uint & iIndex)
{
	if (nullptr == m_pObject_Manager)
		return nullptr;

	return m_pObject_Manager->Get_ComponentPointer(iSceneID, pLayerTag, pComponentTag, iIndex);
}

list<CGameObject*>& CManagement::Get_ObjectList(const _uint & iSceneID, const _tchar * pLayerTag)
{
	if (nullptr == m_pObject_Manager)
	{
#if _DEBUG
		_MSG_BOX("Can't Find ObjectList");
#endif 
		return list<CGameObject*>();
	}

	return m_pObject_Manager->Get_ObjectList(iSceneID, pLayerTag);
}

CGameObject * CManagement::Get_GameObject(const _uint& iSceneID, const _tchar* pLayerTag, const _uint& iIndex)
{
	if (nullptr == m_pObject_Manager)
		return nullptr;
	return m_pObject_Manager->Get_GameObject(iSceneID, pLayerTag, iIndex);
}

HRESULT CManagement::Remove_Prototype_GameObject(const _tchar * pGameObjectTag)
{
	if (nullptr == m_pObject_Manager)
		return E_FAIL;

	return m_pObject_Manager->Remove_Prototype_GameObject(pGameObjectTag);
}

HRESULT CManagement::Remove_GameObjectFromLayer(const _uint & iSceneID, const _tchar * pLayerTag)
{
	if (nullptr == m_pObject_Manager)
		return E_FAIL;

	return m_pObject_Manager->Remove_GameObjectFromLayer(iSceneID, pLayerTag);
}

HRESULT CManagement::Remove_Prototype_Component(const _uint & iSceneID, const _tchar * pComponentTag)
{
	if (nullptr == m_pComponent_Manager)
		return E_FAIL;

	return m_pComponent_Manager->Remove_Prototype_Component(iSceneID, pComponentTag);
}

HRESULT CManagement::Ready_Management(const _uint & iNumScene)
{
	if (nullptr == m_pObject_Manager ||
		nullptr == m_pComponent_Manager)
		return E_FAIL;

	if (FAILED(m_pObject_Manager->Reserve_Object_Manager(iNumScene)))
		return E_FAIL;

	if (FAILED(m_pComponent_Manager->Reserve_Component_Manager(iNumScene)))
		return E_FAIL;



	return NOERROR;
}

HRESULT CManagement::Add_Prototype_GameObject(const _tchar * pGameObjectTag, CGameObject * pGameObject)
{
	if (nullptr == m_pObject_Manager)
		return E_FAIL;

	return m_pObject_Manager->Add_Prototype_GameObject(pGameObjectTag, pGameObject);
}

HRESULT CManagement::Add_Prototype_Component(const _uint & iSceneID, const _tchar * pComponentTag, CComponent * pComponent)
{
	if (nullptr == m_pComponent_Manager)
		return E_FAIL;

	return m_pComponent_Manager->Add_Prototype_Component(iSceneID, pComponentTag, pComponent);
}

HRESULT CManagement::Add_GameObjectToLayer(const _tchar * pProtoTag, const _uint & iSceneID, const _tchar * pLayerTag, CGameObject** ppCloneObject)
{
	if (nullptr == m_pObject_Manager)
		return E_FAIL;

	return m_pObject_Manager->Add_GameObjectToLayer(pProtoTag, iSceneID, pLayerTag, ppCloneObject);
}

CComponent * CManagement::Clone_Component(const _uint & iSceneID, const _tchar * pComponentTag, void* pArg)
{
	if (nullptr == m_pComponent_Manager)
		return nullptr;

	return m_pComponent_Manager->Clone_Component(iSceneID, pComponentTag, pArg);
}

HRESULT CManagement::SetUp_ScenePointer(CScene * pNewScenePointer)
{
	if (nullptr == pNewScenePointer)
		return E_FAIL;

	if (0 != Safe_Release(m_pScene))
		return E_FAIL;

	m_pScene = pNewScenePointer;

	m_pScene->AddRef();

	return NOERROR;
}

_int CManagement::Update_Management(const _float & fTimeDelta)
{
	if (nullptr == m_pScene)
		return -1;

	_int	iProcessCodes = 0;

	iProcessCodes = m_pScene->Update_Scene(fTimeDelta);
	if (iProcessCodes & 0x80000000)
		return iProcessCodes;

	iProcessCodes = m_pScene->LastUpdate_Scene(fTimeDelta);
	if (iProcessCodes & 0x80000000)
		return iProcessCodes;

	return _int(0);
}

void CManagement::Render_Management()
{
	if (nullptr == m_pScene)
		return;

	m_pScene->Render_Scene();
}

HRESULT CManagement::Clear_Layers(const _uint & iSceneID)
{
	if (nullptr == m_pObject_Manager)
		return E_FAIL;

	return m_pObject_Manager->Clear_Layers(iSceneID);
}

void CManagement::Release_Engine()
{
	_ulong			dwRefCnt = 0;

	if (dwRefCnt = CManagement::GetInstance()->DestroyInstance())
		_MSG_BOX("CManagement Release Failed");

	if (dwRefCnt = CMeshTexture::GetInstance()->DestroyInstance())
		_MSG_BOX("CMeshTexture Release Failed");

	if (dwRefCnt = CObject_Manager::GetInstance()->DestroyInstance())
		_MSG_BOX("CObject_Manager Release Failed");

	if (dwRefCnt = CComponent_Manager::GetInstance()->DestroyInstance())
		_MSG_BOX("CComponent_Manager Release Failed");

	if (dwRefCnt = CSystem::GetInstance()->DestroyInstance())
		_MSG_BOX("CSystem Release Failed");

	if (dwRefCnt = CTimer_Manager::GetInstance()->DestroyInstance())
		_MSG_BOX("CTimer_Manager Release Failed");

	if (dwRefCnt = CFrame_Manager::GetInstance()->DestroyInstance())
		_MSG_BOX("CTimer_Manager Release Failed");

	if (dwRefCnt = CLight_Manager::GetInstance()->DestroyInstance())
		_MSG_BOX("CLight_Manager Release Failed");

	if (dwRefCnt = CInput_Device::GetInstance()->DestroyInstance())
		_MSG_BOX("CInput_Device Release Failed");

	if (dwRefCnt = CTarget_Manager::GetInstance()->DestroyInstance())
		_MSG_BOX("CTarget_Manager Release Failed");

	if (dwRefCnt = CGraphic_Device::GetInstance()->DestroyInstance())
		_MSG_BOX("CGraphic_Device Release Failed");

	if (dwRefCnt = CColl_Manager::GetInstance()->DestroyInstance())
		_MSG_BOX("CColl_Manager Release Failed");
}

map<const _tchar*, CLayer*>*& CManagement::Get_mapLayers()
{
	return m_pObject_Manager->Get_mapLayers();
}

map<const _tchar*, CComponent*>*& CManagement::Get_mapComPrototype()
{
	return m_pComponent_Manager->Get_mapComPrototype();
}

map<const _tchar*, CGameObject*>& CManagement::Get_mapPrototype()
{
	return m_pObject_Manager->Get_mapPrototype();
}

list<CGameObject*>* CManagement::Get_ObjList(const _uint & iSceneID, const _tchar * pLayerTag)
{
	return m_pObject_Manager->Get_ObjList(iSceneID, pLayerTag);
}

CGameObject * CManagement::Get_GameObjectFromObjectID(const _uint & iSceneID, const _tchar * pLayerTag, const _uint & ObjectID)
{
	list<CGameObject*>* ObjectList = Get_ObjList(iSceneID, pLayerTag);
	if (ObjectList == nullptr)
		return nullptr;
	for (auto& iter : *ObjectList)
	{
		if (iter->GetID() == ObjectID)
			return iter;
	}
	return nullptr;
}

void CManagement::Free()
{
	Safe_Release(m_pComponent_Manager);
	Safe_Release(m_pObject_Manager);
	Safe_Release(m_pScene);
}
