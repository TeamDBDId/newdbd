#include "..\Headers\System.h"
#include "Timer_Manager.h"
#include "Frame_Manager.h"

_IMPLEMENT_SINGLETON(CSystem)

CSystem::CSystem()
	: m_pTimer_Manager(CTimer_Manager::GetInstance())
	, m_pFrame_Manager(CFrame_Manager::GetInstance())
{
	m_pTimer_Manager->AddRef();	
	m_pFrame_Manager->AddRef();
}

HRESULT CSystem::Add_Timer(const _tchar * pTimerTag)
{
	if (nullptr == m_pTimer_Manager)
		return E_FAIL;

	return m_pTimer_Manager->Add_Timer(pTimerTag);
}

_float CSystem::Get_TimeDelta(const _tchar * pTimerTag)
{
	// TODO: 여기에 반환 구문을 삽입합니다.
	if (nullptr == m_pTimer_Manager)
		return 0.0f;

	return m_pTimer_Manager->Get_TimeDelta(pTimerTag);
}

HRESULT CSystem::Add_Frame(const _tchar * pFrameTag, const _float & fCallCnt)
{
	if (nullptr == m_pFrame_Manager)
		return E_FAIL;

	return m_pFrame_Manager->Add_Frame(pFrameTag, fCallCnt);
}

_bool CSystem::Permit_Call(const _tchar * pFrameTag, const _float & fTimeDelta)
{
	if (nullptr == m_pFrame_Manager)
		return false;

	return m_pFrame_Manager->Permit_Call(pFrameTag, fTimeDelta);
}

void CSystem::Free()
{
	Safe_Release(m_pFrame_Manager);
	Safe_Release(m_pTimer_Manager);
}
