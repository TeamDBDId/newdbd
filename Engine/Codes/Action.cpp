#include "..\Headers\Action.h"

CAction::CAction()
{
}

CAction::STATE CAction::Check_Action(const _float& fTimeDelta)
{
	if (!m_bIsPlaying)
		return FAILED_ACTION;

	if (nullptr == m_pGameObject)
		return FAILED_ACTION;

	if (m_fCurrentTime > m_fDuration_Time)
		return END_ACTION;

	m_fCurrentTime += fTimeDelta;

	return UPDATE_ACTION;
}

void CAction::Free()
{
}