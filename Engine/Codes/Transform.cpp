#include "..\Headers\Transform.h"

CTransform::CTransform(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{

}

CTransform::CTransform(const CTransform & rhs)
	: CComponent(rhs)
	, m_matWorld(rhs.m_matWorld)
	, m_pParentMatrix(rhs.m_pParentMatrix)
{
}

const _vec3 * CTransform::Get_StateInfo(STATE eState) const
{
	return (_vec3*)&m_matWorld.m[eState][0];	
}

_vec3 CTransform::Get_Scale()
{
	_float		fScaleX = D3DXVec3Length(Get_StateInfo(CTransform::STATE_RIGHT));
	_float		fScaleY = D3DXVec3Length(Get_StateInfo(CTransform::STATE_UP));
	_float		fScaleZ = D3DXVec3Length(Get_StateInfo(CTransform::STATE_LOOK));

	return _vec3(fScaleX, fScaleY, fScaleZ);
}

_matrix CTransform::Get_Matrix_Inverse() const
{
	_matrix			matInverse;
	D3DXMatrixInverse(&matInverse, nullptr, &m_matWorld);

	return _matrix(matInverse);
}

void CTransform::Set_StateInfo(STATE eState, const _vec3* pInfo)
{
	memcpy(&m_matWorld.m[eState][0], pInfo, sizeof(_vec3));

}

void CTransform::Set_Parent(_matrix * pParentMatrix)
{
	m_pParentMatrix = pParentMatrix;
	
}

HRESULT CTransform::Ready_Transform()
{
	D3DXMatrixIdentity(&m_matWorld);

	return NOERROR;
}

HRESULT CTransform::SetUp_OnGraphicDev()
{
	if (nullptr == m_pGraphic_Device)
		return E_FAIL;

	m_pGraphic_Device->SetTransform(D3DTS_WORLD, &m_matWorld);

	return NOERROR;
}

HRESULT CTransform::SetUp_OnShader(LPD3DXEFFECT pEffect, const char * pConstantName)
{
	if(nullptr == m_pParentMatrix) // 본 * 플레이어 월드
		pEffect->SetMatrix(pConstantName, &m_matWorld);
	else
		pEffect->SetMatrix(pConstantName, &(m_matWorld * *m_pParentMatrix));
		

	return NOERROR;
}

void CTransform::SetUp_Speed(const _float & fMovePerSec, const _float & fRotationPerSec)
{
	m_fSpeed_Move = fMovePerSec;
	m_fSpeed_Rotation = fRotationPerSec;
}

void CTransform::Go_Straight(const _float & fTimeDelta)
{
	_vec3	vLook, vPosition;

	vLook = *Get_StateInfo(CTransform::STATE_LOOK);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vLook, &vLook) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_Left(const _float & fTimeDelta)
{
	_vec3	vRight, vPosition;

	vRight = *Get_StateInfo(CTransform::STATE_RIGHT);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vRight, &vRight) * m_fSpeed_Move * fTimeDelta;


	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_Right(const _float & fTimeDelta)
{
	_vec3	vRight, vPosition;

	vRight = *Get_StateInfo(CTransform::STATE_RIGHT);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vRight, &vRight) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_Up(const _float & fTimeDelta)
{
	_vec3	vUp, vPosition;

	vUp = *Get_StateInfo(CTransform::STATE_UP);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition += *D3DXVec3Normalize(&vUp, &vUp) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::BackWard(const _float & fTimeDelta)
{
		_vec3	vLook, vPosition;

	vLook = *Get_StateInfo(CTransform::STATE_LOOK);
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vPosition -= *D3DXVec3Normalize(&vLook, &vLook) * m_fSpeed_Move * fTimeDelta;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::SetUp_RotationX(const _float & fRadian)
{
	_vec3		vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix			matRot;
	D3DXMatrixRotationX(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	Set_StateInfo(CTransform::STATE_UP, &vUp);
	Set_StateInfo(CTransform::STATE_LOOK, &vLook);
}

void CTransform::SetUp_RotationY(const _float & fRadian)
{
	_vec3		vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix			matRot;
	D3DXMatrixRotationY(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	
	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	Set_StateInfo(CTransform::STATE_UP, &vUp);
	Set_StateInfo(CTransform::STATE_LOOK, &vLook);
}

void CTransform::SetUp_RotationZ(const _float & fRadian)
{
	_vec3		vRight(1.f, 0.f, 0.f), vUp(0.f, 1.f, 0.f), vLook(0.f, 0.f, 1.f);

	_matrix			matRot;
	D3DXMatrixRotationZ(&matRot, fRadian);

	vRight *= Get_Scale().x;
	vUp *= Get_Scale().y;
	vLook *= Get_Scale().z;

	D3DXVec3TransformNormal(&vRight, &vRight, &matRot);
	D3DXVec3TransformNormal(&vUp, &vUp, &matRot);
	D3DXVec3TransformNormal(&vLook, &vLook, &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	Set_StateInfo(CTransform::STATE_UP, &vUp);
	Set_StateInfo(CTransform::STATE_LOOK, &vLook);

}

void CTransform::SetUp_Rotation_Axis(const _float & fRadian, const _vec3* pAxis)
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix			matRot;
	D3DXMatrixRotationAxis(&matRot, pAxis, fRadian);

	for (size_t i = 0; i < 3; ++i)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Rotation_X(const _float & fTimeDelta)
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix			matRot;
	D3DXMatrixRotationX(&matRot, m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; ++i)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Rotation_Y(const _float & fTimeDelta)
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
		vDir[i] = *Get_StateInfo(STATE(i));

	/*vRight = *Get_StateInfo(CTransform::STATE_RIGHT);
	vUp = *Get_StateInfo(CTransform::STATE_UP);
	vLook = *Get_StateInfo(CTransform::STATE_LOOK);*/

	_matrix			matRot;
	D3DXMatrixRotationY(&matRot, m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; ++i)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Rotation_Z(const _float & fTimeDelta)
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix			matRot;
	D3DXMatrixRotationZ(&matRot, m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; ++i)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);



}

void CTransform::Reverse_RotationY()
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
		vDir[i] = *Get_StateInfo(STATE(i));

	Set_StateInfo(CTransform::STATE_RIGHT, &-vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_LOOK, &-vDir[STATE_LOOK]);
}

void CTransform::Rotation_Axis(const _float & fTimeDelta, const _vec3* pAxis)
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix			matRot;
	D3DXMatrixRotationAxis(&matRot, pAxis, m_fSpeed_Rotation * fTimeDelta);

	for (size_t i = 0; i < 3; ++i)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Rotation_Axis_Angle(const _float & fAngle, const _vec3 * pAxis)
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
		vDir[i] = *Get_StateInfo(STATE(i));

	_matrix			matRot;
	D3DXMatrixRotationAxis(&matRot, pAxis, fAngle);

	for (size_t i = 0; i < 3; ++i)
		D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Scaling(const _float & fX, const _float & fY, const _float & fZ)
{
	_vec3		vDir[3];

	for (size_t i = 0; i < 3; ++i)
	{
		vDir[i] = *Get_StateInfo(STATE(i));
		D3DXVec3Normalize(&vDir[i], &vDir[i]);
	}

	vDir[STATE_RIGHT] *= fX;
	vDir[STATE_UP] *= fY;
	vDir[STATE_LOOK] *= fZ;

	Set_StateInfo(CTransform::STATE_RIGHT, &vDir[STATE_RIGHT]);
	Set_StateInfo(CTransform::STATE_UP, &vDir[STATE_UP]);
	Set_StateInfo(CTransform::STATE_LOOK, &vDir[STATE_LOOK]);
}

void CTransform::Go_ToTarget(const _vec3 * pTargetPos, const _float & fTimeDelta)
{
	_vec3	vLook, vPosition;
	
	vPosition = *Get_StateInfo(CTransform::STATE_POSITION);

	vLook = *pTargetPos /*- vPosition*/;	

	vPosition += *D3DXVec3Normalize(&vLook, &vLook) * m_fSpeed_Move * fTimeDelta;


	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::SetUp_RotateXYZ(const _float * fAngleX, const _float * fAngleY, const _float * fAngleZ)
{
	if(nullptr != fAngleX)
		SetUp_RotationX(*fAngleX);

	_vec3 vDir[3];
	_matrix matRot;

	if (nullptr != fAngleY)
	{
		for (size_t i = 0; i < 3; ++i)
			vDir[i] = *Get_StateInfo(CTransform::STATE(i));
		
		D3DXMatrixRotationY(&matRot, *fAngleY);

		for (size_t i = 0; i < 3; ++i)
			D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

		Set_StateInfo(CTransform::STATE_RIGHT, &vDir[CTransform::STATE_RIGHT]);
		Set_StateInfo(CTransform::STATE_UP, &vDir[CTransform::STATE_UP]);
		Set_StateInfo(CTransform::STATE_LOOK, &vDir[CTransform::STATE_LOOK]);

	}

	if (nullptr != fAngleZ)
	{
		for (size_t i = 0; i < 3; ++i)
			vDir[i] = *Get_StateInfo(CTransform::STATE(i));

		D3DXMatrixRotationZ(&matRot, *fAngleZ);

		for (size_t i = 0; i < 3; ++i)
			D3DXVec3TransformNormal(&vDir[i], &vDir[i], &matRot);

		Set_StateInfo(CTransform::STATE_RIGHT, &vDir[CTransform::STATE_RIGHT]);
		Set_StateInfo(CTransform::STATE_UP, &vDir[CTransform::STATE_UP]);
		Set_StateInfo(CTransform::STATE_LOOK, &vDir[CTransform::STATE_LOOK]);
	}
}

void CTransform::Set_PosY(const _float& _fY)
{
	_vec3 vPos = *Get_StateInfo(CTransform::STATE_POSITION);
	vPos.y = _fY;


	Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}

void CTransform::Move_V3(const _vec3 & _vMove)
{
	if (isnan(_vMove.x) != 0 ||
		isnan(_vMove.y) != 0 ||
		isnan(_vMove.z) != 0)
		return;

	_vec3 vPos = *Get_StateInfo(CTransform::STATE_POSITION);
	vPos += _vMove;

	Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}

void CTransform::Move_V3Time(const _vec3 & _vMove, const _float& _fTime)
{
	if (isnan(_vMove.x) != 0 ||
		isnan(_vMove.y) != 0 ||
		isnan(_vMove.z) != 0)
		return;

	_vec3 vPos = *Get_StateInfo(CTransform::STATE_POSITION);
	vPos += _vMove*_fTime;

	Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}


void CTransform::Move_Gravity(const _float & _fTime)
{
	_vec3 vPos = *Get_StateInfo(CTransform::STATE_POSITION);


	if (m_fAcc > -500.f)
		m_fAcc -= 2.f;

	

	vPos += _vec3(0.f, 1.f, 0.f) * m_fAcc * _fTime;


	Set_StateInfo(CTransform::STATE_POSITION, &vPos);

}

void CTransform::Go_ToTarget_ChangeDirection(const _vec3 * pTargetPos, const _float & fTimeDelta, _bool bNotMove)
{
	_vec3 vLook, vAxis, vPosition, vDirection;
	_float fAngle = 0.f;

	vDirection = *pTargetPos;
	vDirection.y = 0.f;
	vLook = *(_vec3*)&m_matWorld.m[2][0];
	vPosition = *(_vec3*)&m_matWorld.m[3][0];
	D3DXVec3Normalize(&vLook, &vLook);
	D3DXVec3Normalize(&vDirection, &vDirection);
	fAngle = acosf(D3DXVec3Dot(&vLook, &vDirection));
	D3DXVec3Cross(&vAxis, &vLook, &vDirection);

	if (fabsf(D3DXToDegree(fAngle)) >= 5.5f)
	{
		if (vAxis.y <= 0.f)
			Rotation_Y((fTimeDelta * -fAngle * 14.f));
		else
			Rotation_Y((fTimeDelta * fAngle * 14.f));

	}
	if (bNotMove)
		return;

	vPosition += *D3DXVec3Normalize(&vLook, &vLook) * fTimeDelta * m_fSpeed_Move;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

void CTransform::Go_ToTarget_DIR(const _vec3 * pDirPos, const _float & fTimeDelta)
{
	_vec3 vPosition, vDir;
	vDir = *pDirPos;

	vPosition += *D3DXVec3Normalize(&vDir, &vDir) * fTimeDelta * m_fSpeed_Move;

	Set_StateInfo(CTransform::STATE_POSITION, &vPosition);
}

CTransform * CTransform::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTransform*	pInstance = new CTransform(pGraphic_Device);

	if (FAILED(pInstance->Ready_Transform()))
	{
		MessageBox(0, L"CTransform Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}


CComponent * CTransform::Clone_Component(void* pArg)
{
	return new CTransform(*this);
}

void CTransform::Free()
{
	

	CComponent::Free();
}
