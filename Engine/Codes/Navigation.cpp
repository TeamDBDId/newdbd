#include "..\Headers\Navigation.h"
#include "Cell.h"

CNavigation::CNavigation(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
{

}

CNavigation::CNavigation(const CNavigation & rhs)
	: CComponent(rhs)
	, m_vecCell(rhs.m_vecCell)
{
	for (auto& pCell : m_vecCell)
		pCell->AddRef();
}

HRESULT CNavigation::Ready_Navigation(const _tchar * pFileName)
{
	HANDLE			hFile = 0;
	_ulong			dwByte = 0;

	hFile = CreateFile(pFileName, GENERIC_READ, 0, nullptr, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	if (0 == hFile)
		return E_FAIL;

	_uint			iNumCells = 0;
	ReadFile(hFile, &iNumCells, sizeof(_uint), &dwByte, nullptr);

	m_vecCell.reserve(iNumCells);

	while (true)
	{
		_vec3		vPoints[3];

		ReadFile(hFile, vPoints, sizeof(_vec3) * 3, &dwByte, nullptr);
		if (0 == dwByte)
			break;

		CCell*		pCell = CCell::Create(m_pGraphic_Device, &vPoints[0], &vPoints[1], &vPoints[2], m_vecCell.size());
		if (nullptr == pCell)
			return E_FAIL;

		m_vecCell.push_back(pCell);
	}

	CloseHandle(hFile);

	if (FAILED(Ready_Neighbor()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CNavigation::Ready_Neighbor()
{
	_uint	iNumCells = m_vecCell.size();

	for (size_t i = 0; i < iNumCells; ++i)
	{
		for (size_t j = 0; j < iNumCells; j++)
		{
			if (i == j)
				continue;

			if (true == m_vecCell[j]->Compare_Point(m_vecCell[i]->Get_Point(CCell::POINT_A), m_vecCell[i]->Get_Point(CCell::POINT_B)))			
				m_vecCell[i]->Set_Neighbor(CCell::NEIGHBOR_AB, m_vecCell[j]);

			if (true == m_vecCell[j]->Compare_Point(m_vecCell[i]->Get_Point(CCell::POINT_B), m_vecCell[i]->Get_Point(CCell::POINT_C)))
				m_vecCell[i]->Set_Neighbor(CCell::NEIGHBOR_BC, m_vecCell[j]);

			if (true == m_vecCell[j]->Compare_Point(m_vecCell[i]->Get_Point(CCell::POINT_C), m_vecCell[i]->Get_Point(CCell::POINT_A)))
				m_vecCell[i]->Set_Neighbor(CCell::NEIGHBOR_CA, m_vecCell[j]);
		}
	}

	return NOERROR;
}

HRESULT CNavigation::Ready_Clone_Navigation(void * pArg)
{
	if (nullptr == pArg)
		return NOERROR;

	m_iCurrentIdx = *(_uint*)pArg;

	if (m_vecCell.size() <= m_iCurrentIdx)
		return E_FAIL;

	return NOERROR;
}

_bool CNavigation::Move_OnNavigation(const _vec3 * vPosition, const _vec3 * vDirectionPerSec)
{
	if (m_vecCell.size() <= m_iCurrentIdx)
		return false;

	CCell::LINE		eOutLine = CCell::LINE(-1);
	const CCell*	pNeighbor = nullptr;

	_bool	isIn = m_vecCell[m_iCurrentIdx]->is_InCell(*vPosition + *vDirectionPerSec, &eOutLine);

	if (false == isIn)
	{
		// �̿��� ������
		if (pNeighbor = m_vecCell[m_iCurrentIdx]->Get_Neighbor(CCell::NEIGHBOR(eOutLine)))
		{
			m_iCurrentIdx = pNeighbor->Get_CellIndex();
			return true;
		}
		// �̿��� ������
		else
		{

			return false;			
		}
	}
	else
		return true;
}

void CNavigation::Render_Navigation()
{
	for (auto& pCell : m_vecCell)
		pCell->Render_Cell();
}

CNavigation * CNavigation::Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar * pFileName)
{
	CNavigation*	pInstance = new CNavigation(pGraphic_Device);

	if (FAILED(pInstance->Ready_Navigation(pFileName)))
	{
		MessageBox(0, L"CNavigation Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CComponent * CNavigation::Clone_Component(void * pArg)
{
	CNavigation*	pInstance = new CNavigation(*this);

	if (FAILED(pInstance->Ready_Clone_Navigation(pArg)))
	{
		MessageBox(0, L"CNavigation Cloned Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CNavigation::Free()
{
	if (false == m_isClone)
	{
		for (auto& pCell : m_vecCell)
			pCell->Clear_Neighbor();

	}
	
	for (auto& pCell : m_vecCell)
		Safe_Release(pCell);
		
	m_vecCell.clear();

	CComponent::Free();
}
