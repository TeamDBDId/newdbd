#include "..\Headers\Renderer.h"
#include "GameObject.h"
#include "Target_Manager.h"
#include "Light_Manager.h"
#include "Shader.h"
#include "Mesh_Static.h"
#include "Instancing.h"

CRenderer::CRenderer(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CComponent(pGraphic_Device)
	, m_pTarget_Manager(CTarget_Manager::GetInstance())
{
	m_pTarget_Manager->AddRef();
}


HRESULT CRenderer::Ready_Renderer()
{
	if (nullptr == m_pTarget_Manager)
		return E_FAIL;

	//if (FAILED(D3DXCreateVolumeTextureFromFileEx(m_pGraphic_Device, L"../Bin/LUT_Greenish_G.dds"
	//	, 16, 16, 16, 0, D3DUSAGE_DYNAMIC, D3DFMT_UNKNOWN
	//	, D3DPOOL_DEFAULT, D3DX_FILTER_BOX, D3DX_DEFAULT, NULL, NULL,
	//	NULL, &m_pLutTex)))Bin/ShaderFiles/Shader_Back
	//	return E_FAIL;

	if (FAILED(D3DXCreateTextureFromFileEx(m_pGraphic_Device, L"../Bin/Resources/Textures/Etc/RandomNormal.jpg", 0, 0, 0, 0, D3DFMT_A8R8G8B8, D3DPOOL_MANAGED, D3DX_DEFAULT, D3DX_DEFAULT, 0, 0, NULL, (LPDIRECT3DTEXTURE9*)&m_pRandomTexture)))
		return E_FAIL;

	if (FAILED(m_pGraphic_Device->CreateTexture(128, 128, 1, 0, D3DFMT_A32B32G32R32F, D3DPOOL_MANAGED, &m_pSkinTex, nullptr)))
		return E_FAIL;

	//if (FAILED(D3DXCreateTextureFromFileW(m_pGraphic_Device, L"../Bin/LUT_Greenish.jpg", &m_pLutTex)))
	//	return E_FAIL;

	// For.Shader_Back
	m_pShader_Back = CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Back.fx");
	if (nullptr == m_pShader_Back)
		return E_FAIL;

	// For.Shader_LightAcc
	m_pShader_LightAcc = CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_LightAcc.fx");
	if (nullptr == m_pShader_LightAcc)
		return E_FAIL;

	// For.Shader_Blend
	m_pShader_Blend = CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Blend.fx");
	if (nullptr == m_pShader_Blend)
		return E_FAIL;

	//// For.Shader_Cube
	//m_pShader_Cube = CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_CubeMap.fx");
	//if (nullptr == m_pShader_Cube)
	//	return E_FAIL;

	if (FAILED(D3DXCreateCubeTexture(m_pGraphic_Device, 720, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pTxCbm)))
	{
		if (FAILED(D3DXCreateCubeTexture(m_pGraphic_Device, 720, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, &m_pTxCbm)))
			return E_FAIL;
	}

	D3DVIEWPORT9			ViewPort;
	m_pGraphic_Device->GetViewport(&ViewPort);

	// For.Target_BackBuffer
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_BackBuffer", ViewPort.Width, ViewPort.Height, D3DFMT_A16B16G16R16F, D3DXCOLOR(1.f, 1.f, 1.f, 1.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_BackBuffer", 0.0f, 400.f, 200.f, 200.f)))
		return E_FAIL;

	// For.Target_Cube
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Cube", 720, 720, D3DFMT_A8R8G8B8, D3DXCOLOR(1.f, 1.f, 1.f, 1.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Cube", 120.0f, 120.f, 120.f, 120.f)))
		return E_FAIL;

	// For.Target_LightStem
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_LightStem", ViewPort.Width, ViewPort.Height, D3DFMT_A8R8G8B8, D3DXCOLOR(1.f, 1.f, 1.f, 1.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_LightStem", 0.0f, 400.f, 200.f, 200.f)))
		return E_FAIL;

	// For.Target_RAOM
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_RAOM", ViewPort.Width, ViewPort.Height, D3DFMT_A8R8G8B8, D3DXCOLOR(0.f, 0.f, 0.f, 1.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_RAOM", 0.0f, 200.f, 200.f, 200.f)))
		return E_FAIL;

	// For.Target_Diffuse
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Diffuse", ViewPort.Width, ViewPort.Height, D3DFMT_A8R8G8B8, D3DXCOLOR(0.f, 0.f, 0.f, 0.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Diffuse", 0.0f, 400.f, 200.f, 200.f)))
		return E_FAIL;

	// For.Target_Normal
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Normal", ViewPort.Width, ViewPort.Height, D3DFMT_A16B16G16R16F, D3DXCOLOR(0.f, 0.f, 0.f, 1.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Normal", 0.0f, 600.f, 200.f, 200.f)))
		return E_FAIL;

	// For.Target_Depth
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Depth", ViewPort.Width, ViewPort.Height, D3DFMT_A32B32G32R32F, D3DXCOLOR(1.f, 1.f, 1.f, 1.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Depth", 0.0f, 800.f, 200.f, 200.f)))
		return E_FAIL;



	// For.Target_Shade
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Shade", ViewPort.Width, ViewPort.Height, D3DFMT_A16B16G16R16F, D3DXCOLOR(0.f, 0.f, 0.f, 1.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Shade", 200.0f, 0.f, 200.f, 200.f)))
		return E_FAIL;

	// For.Target_Specular
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Specular", ViewPort.Width, ViewPort.Height, D3DFMT_A16B16G16R16F, D3DXCOLOR(0.f, 0.f, 0.f, 0.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Specular", 200.0f, 200.f, 200.f, 200.f)))
		return E_FAIL;

	// For.Target_Ref
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Ref", ViewPort.Width, ViewPort.Height, D3DFMT_A16B16G16R16F, D3DXCOLOR(0.f, 0.f, 0.f, 0.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Ref", 0.0f, 480.f, 120.f, 120.f)))
		return E_FAIL;

	// For.Target_Etc
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_Etc", ViewPort.Width, ViewPort.Height, D3DFMT_A16B16G16R16F, D3DXCOLOR(0.f, 0.f, 0.f, 0.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_Etc", 0.0f, 480.f, 120.f, 120.f)))
		return E_FAIL;



	// For.Target_LightMap
	if (FAILED(m_pTarget_Manager->Add_Target(m_pGraphic_Device, L"Target_LightMap", ViewPort.Width, ViewPort.Height, D3DFMT_A16B16G16R16F, D3DXCOLOR(0.f, 0.f, 0.f, 0.f))))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Ready_Debug_Buffer(L"Target_LightMap", 1000.0f, 400.f, 200.f, 120.f)))
		return E_FAIL;

	// For.MRT_STEM
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_Stem", L"Target_LightStem")))
		return E_FAIL;

	// For.MRT_Cube
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_Cube", L"Target_Cube")))
		return E_FAIL;



	// For.MRT_Deferred	
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_Deferred", L"Target_Diffuse")))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_Deferred", L"Target_Normal")))
		return E_FAIL;	
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_Deferred", L"Target_Depth")))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_Deferred", L"Target_RAOM")))
		return E_FAIL;


	// For.MRT_LightAcc
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_LightAcc", L"Target_Shade")))
		return E_FAIL;	
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_LightAcc", L"Target_Specular")))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_LightAcc", L"Target_Ref")))
		return E_FAIL;
	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_LightAcc", L"Target_Etc")))
		return E_FAIL;

	if (FAILED(m_pTarget_Manager->Add_MRT(L"MRT_Back", L"Target_BackBuffer")))
		return E_FAIL;

	if (FAILED(m_pGraphic_Device->CreateVertexBuffer(sizeof(VTXVIEWPORT) * 4, 0, D3DFVF_XYZRHW | D3DFVF_TEX1, D3DPOOL_MANAGED, &m_pVB, nullptr)))
		return E_FAIL;

	VTXVIEWPORT*	pVertices = nullptr;

	m_pVB->Lock(0, 0, (void**)&pVertices, 0);

	pVertices[0].vPosition = _vec4(0.f - 0.5f, 0.0f - 0.5f, 0.0f, 1.f);
	pVertices[0].vTexUV = _vec2(0.0f, 0.f);

	pVertices[1].vPosition = _vec4(ViewPort.Width - 0.5f, 0.0f - 0.5f, 0.0f, 1.f);
	pVertices[1].vTexUV = _vec2(1.0f, 0.f);

	pVertices[2].vPosition = _vec4(ViewPort.Width - 0.5f, ViewPort.Height - 0.5f, 0.0f, 1.f);
	pVertices[2].vTexUV = _vec2(1.0f, 1.f);

	pVertices[3].vPosition = _vec4(0.0f - 0.5f, ViewPort.Height - 0.5f, 0.0f, 1.f);
	pVertices[3].vTexUV = _vec2(0.0f, 1.f);

	m_pVB->Unlock();

	if (FAILED(m_pGraphic_Device->CreateIndexBuffer(sizeof(POLYGON16) * 2, 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIB, nullptr)))
		return E_FAIL;
	POLYGON16*		pIndices = nullptr;

	m_pIB->Lock(0, 0, (void**)&pIndices, 0);

	pIndices[0]._0 = 0;
	pIndices[0]._1 = 1;
	pIndices[0]._2 = 2;

	pIndices[1]._0 = 0;
	pIndices[1]._1 = 2;
	pIndices[1]._2 = 3;

	m_pIB->Unlock();

	m_pInstancing = CInstancing::Create(m_pGraphic_Device);

	return NOERROR;
}

HRESULT CRenderer::Add_RenderGroup(RENDERGROUP eGroup, CGameObject * pGameObject)
{
	if (RENDER_END <= eGroup)
		return E_FAIL;

	if (nullptr == pGameObject)
		return E_FAIL;

	m_RenderList[eGroup].push_back(pGameObject);

	pGameObject->AddRef();

	return NOERROR;
}

HRESULT CRenderer::Add_CubeGroup(CGameObject * pGameObject)
{
	if (nullptr == pGameObject)
		return E_FAIL;

	m_CubemapList.push_back(pGameObject);

	//pGameObject->AddRef();

	return NOERROR;
}

HRESULT CRenderer::Add_StemGroup(CGameObject * pGameObject)
{
	if (nullptr == pGameObject)
		return E_FAIL;

	//m_StemmapList.push_back(pGameObject);

	return NOERROR;
}

HRESULT CRenderer::Add_StencilGroup(CGameObject * pGameObject)
{
	if (nullptr == pGameObject)
		return E_FAIL;

	m_StencilList.push_back(pGameObject);

	//pGameObject->AddRef();

	return NOERROR;
}

HRESULT CRenderer::Render_RenderGroup()
{
	if (nullptr == m_pTarget_Manager)
		return E_FAIL;

	m_iCount++;

	Flashed();
	//CLight_Manager::GetInstance()->Render_DynamicShadow();
	//Render_LightStem();
	Render_EnvMap();
	//Render_Priority();
	Render_Deferred();	
	Render_LightAcc();
	Render_Blend(); // 디퓨즈타겟 * 셰이드타겟 = 백버퍼에 찍는다.
	Render_Stencil();
	// 알파블렌딩.
	Render_Alpha();
	Render_UI();

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &m_matPreView);

	//m_pTarget_Manager->Render_Debug_Buffer(L"MRT_Deferred");
	//m_pTarget_Manager->Render_Debug_Buffer(L"MRT_LightAcc");

	//if (GetKeyState(VK_RETURN) < 0)
	//{
	//	D3DXSaveTextureToFile(L"../Bin/123.dds", D3DXIFF_DDS, m_pTxCbm, nullptr);
	//}

	if (m_iCount >= 60)
		m_iCount = 0;

	return NOERROR;
}

void CRenderer::Add_Instancing(CMesh_Static * pMeshCom, const _matrix & matWorld)
{
	if (m_pInstancing != nullptr)
		m_pInstancing->Add_Instancing(pMeshCom, matWorld);
}

HRESULT CRenderer::SetUp_OnShaderFromTarget(LPD3DXEFFECT pEffect, _tchar * TargetName, const char * pConstantName)
{
	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, TargetName, pConstantName)))
		return E_FAIL;

	return NOERROR;
}

LPDIRECT3DTEXTURE9 CRenderer::Get_Texture(_tchar * pTargetTag)
{

	return m_pTarget_Manager->GetTexture(pTargetTag);
}

void CRenderer::Render_Priority()
{
	for (auto& pGameObject : m_RenderList[RENDER_PRIORITY])
	{
		if (nullptr != pGameObject)
		{
			pGameObject->Render_GameObject();
			Safe_Release(pGameObject);
		}
	}
	m_RenderList[RENDER_PRIORITY].clear();
}

_bool Compare(CGameObject* pSour, CGameObject* pDest)
{
	return pSour->Get_CameraDistance() > pDest->Get_CameraDistance();
}

_bool Compare2(CGameObject* pSour, CGameObject* pDest)
{
	return pSour->Get_CameraDistance() < pDest->Get_CameraDistance();
}

void CRenderer::Render_NoneAlpha()
{
	m_RenderList[RENDER_NONEALPHA].sort(Compare2);

	for (auto& pGameObject : m_RenderList[RENDER_NONEALPHA])
	{
		if (nullptr != pGameObject)
		{
			pGameObject->Render_GameObject();
			Safe_Release(pGameObject);
		}
	}
	m_RenderList[RENDER_NONEALPHA].clear();
}

void CRenderer::Render_Alpha()
{
	m_RenderList[RENDER_ALPHA].sort(Compare);

	for (auto& pGameObject : m_RenderList[RENDER_ALPHA])
	{
		if (nullptr != pGameObject)
		{
			pGameObject->Render_GameObject();
			Safe_Release(pGameObject);
		}
	}
	m_RenderList[RENDER_ALPHA].clear();
}

_bool Compareui(CGameObject* Sour, CGameObject* Dest)
{
	return Sour->GetUIDist() < Dest->GetUIDist();
}

void CRenderer::Render_UI()
{
	m_RenderList[RENDER_UI].sort(Compareui);

	for (auto& pGameObject : m_RenderList[RENDER_UI])
	{
		if (nullptr != pGameObject)
		{
			pGameObject->Render_GameObject();
			Safe_Release(pGameObject);
		}
	}
	m_RenderList[RENDER_UI].clear();
}

void CRenderer::Render_LightStem()
{
	//if (nullptr == m_pTarget_Manager)
	//	return;

	//m_pTarget_Manager->AddRef();

	//if (FAILED(m_pTarget_Manager->Begin_MRT(L"MRT_Stem")))
	//	return;

	//for (auto& pGameObject : m_StemmapList)
	//{
	//	if (nullptr != pGameObject)
	//	{
	//		pGameObject->Render_GameObject();
	//	}
	//}
	//m_StemmapList.clear();

	//if (FAILED(m_pTarget_Manager->End_MRT(L"MRT_Stem")))
	//	return;

	//Safe_Release(m_pTarget_Manager);
}

void CRenderer::Render_Deferred()
{
	if (nullptr == m_pTarget_Manager)
		return;

	m_pTarget_Manager->AddRef();

	// 디퍼드 그룹에 들어가 있는 디퓨즈, 노멀타겟을 장치에 셋한다.
	if (FAILED(m_pTarget_Manager->Begin_MRT(L"MRT_Deferred")))
		return;

	Render_Priority();
	m_pInstancing->Rendering_Instancing();
	Render_NoneAlpha();
	
	// 두개의 타겟을 셋하기 이전의 상태로 돌린다.
	if (FAILED(m_pTarget_Manager->End_MRT(L"MRT_Deferred")))
		return;

	Safe_Release(m_pTarget_Manager);
}

void CRenderer::Render_LightAcc()
{
	if (nullptr == m_pTarget_Manager || 
		nullptr == m_pShader_LightAcc)
		return;
	// 여러개의 빛의 연산 결과를 누적시킨다. (어디에? : 셰이드타겟(명암))
	if (FAILED(m_pTarget_Manager->Begin_MRT(L"MRT_LightAcc", true)))
		return;

	LPD3DXEFFECT pEffect = m_pShader_LightAcc->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_Normal", "g_NormalTexture")))
		return ;
	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_Depth", "g_DepthTexture")))
		return;

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_RAOM", "g_RMAOTexture")))
		return;

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_LightStem", "g_StemTexture")))
		return;

	pEffect->SetTexture("g_TexCube", m_pTxCbm);

	pEffect->Begin(nullptr, 0);
	

	_matrix matProj, matPVP;
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);
	matPVP = m_matPreView * matProj;

	pEffect->SetMatrix("g_matPreViewProj", &matPVP);

	CLight_Manager::GetInstance()->Render_Light(pEffect);


	pEffect->End();

	// 백버퍼의 상태로 복구한다.
	if (FAILED(m_pTarget_Manager->End_MRT(L"MRT_LightAcc")))
		return;

	/*전역변수에 값 전달. : 노멀, 빛

	셰이더 시작.

	

	셰이더 끝.*/


	Safe_Release(pEffect);

}

void CRenderer::Render_Blend()
{
	if (nullptr == m_pGraphic_Device ||
		nullptr == m_pShader_Blend || 
		nullptr == m_pTarget_Manager)
		return;

	if (FAILED(m_pTarget_Manager->Begin_MRT(L"MRT_Back", true)))
		return;

	LPD3DXEFFECT pEffect = m_pShader_Blend->Get_EffectHandle();
	if (nullptr == pEffect)
		return;
	pEffect->AddRef();

	//
	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_Depth", "g_DepthTexture")))
		return;

	//pEffect->SetTexture("g_RandomTexture", m_pRandomTexture);
	//pEffect->SetVector("g_ssao", &m_ssaop);
	//pEffect->SetBool("g_bssao", m_bssao);

	if (GetAsyncKeyState(VK_TAB) < 0)
		m_bssao = true;
	if (GetAsyncKeyState(VK_BACK) < 0)
		m_bssao = false;
	//

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_LightMap", "g_LightTexture")))
		return;

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_Diffuse", "g_DiffuseTexture")))
		return;

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_Shade", "g_ShadeTexture")))
		return;

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_Specular", "g_SpecularTexture")))
		return;

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffect, L"Target_Etc", "g_EtcTexture")))
		return;

	//_vec4 vScr = { m_vScrPos.x, m_vScrPos.y, m_vScrPos.z, 1.f };
	//pEffect->SetVector("g_vScreenLightPos", &vScr);
	_uint iPass = 0;
	if (m_bShapeSkill)
		iPass = 1;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(iPass);

	if (iPass == 1)
	{
		if (nullptr != m_pMaskingTexture)
			pEffect->SetTexture("g_MaskingTex", m_pMaskingTexture);
	}

	if (GetAsyncKeyState(VK_F6) < 0)
		fFogUV -= 0.01f;
	else if (GetAsyncKeyState(VK_F7) < 0)
		fFogUV += 0.01f;

	if (fFogUV < 0.f)
		fFogUV = 0.f;
	else if (fFogUV > 1.f)
		fFogUV = 1.f;

	pEffect->SetFloat("g_W", fgW);
	pEffect->SetFloat("g_E", fgE);

	//if (GetAsyncKeyState(VK_F2) < 0)
	//	fgE -= 1.f;
	//else if (GetAsyncKeyState(VK_F3) < 0)
	//	fgE += 1.f;



	//if ((m_iCount % 5) == 0)
	//{
	//	//cout << "gE : ";
	//	//cout << fFogUV << endl;

	//	//cout << "gE : ";
	//	//cout << fgE << endl;

	//	//cout << "gW : ";
	//	//cout << fgW << endl;
	//}

	pEffect->SetFloat("g_R", g_R);
	pEffect->SetFloat("g_G", g_G);
	pEffect->SetFloat("g_B", g_B);

	//if (GetAsyncKeyState(VK_NUMPAD1) < 0)
	//	g_B -= 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD2) < 0)
	//	g_B += 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD4) < 0)
	//	g_G -= 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD5) < 0)
	//	g_G += 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD7) < 0)
	//	g_R -= 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD8) < 0)
	//	g_R += 0.01f;

	//if (GetAsyncKeyState(VK_NUMPAD1) < 0)
	//	m_ssaop.x -= 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD2) < 0)
	//	m_ssaop.x += 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD4) < 0)
	//	m_ssaop.y -= 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD5) < 0)
	//	m_ssaop.y += 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD7) < 0)
	//	m_ssaop.z -= 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD8) < 0)
	//	m_ssaop.z += 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD6) < 0)
	//	m_ssaop.w -= 0.01f;
	//if (GetAsyncKeyState(VK_NUMPAD9) < 0)
	//	m_ssaop.w += 0.01f;

	//if ((m_iCount % 5) == 0)
	//{
	//	/*cout << "R : ";
	//	cout << g_R << endl;

	//	cout << "G : ";
	//	cout << g_G << endl;

	//	cout << "B : ";
	//	cout << g_B << endl;*/

	//	cout << "x : " << m_ssaop.x << " y : " << m_ssaop.y << " z : " << m_ssaop.z << " w : " << m_ssaop.w << endl;
	//	cout << m_ssaop.x << endl;
	//}


	if (nullptr != m_pFogTexture)
	{
		pEffect->SetTexture("g_FogTex", m_pFogTexture);
		pEffect->SetFloat("g_foguv", fFogUV);
	}
	if(nullptr != m_pLutTex)
		pEffect->SetTexture("g_LutTex", m_pLutTex);


	// 임의의 버퍼를 렌더링한다. == 백버퍼에 그린다.
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, sizeof(VTXVIEWPORT));
	m_pGraphic_Device->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

	pEffect->EndPass();
	pEffect->End();

	if (FAILED(m_pTarget_Manager->End_MRT(L"MRT_Back")))
		return;

	//////////////////////////// back
	LPD3DXEFFECT pEffectBack = m_pShader_Back->Get_EffectHandle();
	if (nullptr == pEffectBack)
		return;

	pEffectBack->AddRef();

	if (FAILED(m_pTarget_Manager->SetUp_OnShader(pEffectBack, L"Target_BackBuffer", "g_DiffuseTexture")))
		return;

	LPDIRECT3DTEXTURE9 pLightInfoTex = CLight_Manager::GetInstance()->Get_LightInfoTexture();

	pEffectBack->SetTexture("g_LightInfoTex", pLightInfoTex);
	_uint iNumLight = CLight_Manager::GetInstance()->Get_SizeofLInfo();
	if (m_bShapeSkill)
	{
		iNumLight = 0;
	}
	if (GetAsyncKeyState(VK_F5) < 0)
		m_bSharp = !m_bSharp;
	
	pEffectBack->SetBool("g_bSharp", m_bSharp);

	pEffectBack->SetInt("g_iNumLight", iNumLight);

	pEffectBack->Begin(nullptr, 0);
	pEffectBack->BeginPass(1);

	// 임의의 버퍼를 렌더링한다. == 백버퍼에 그린다.
	m_pGraphic_Device->SetStreamSource(0, m_pVB, 0, sizeof(VTXVIEWPORT));
	m_pGraphic_Device->SetFVF(D3DFVF_XYZRHW | D3DFVF_TEX1);
	m_pGraphic_Device->SetIndices(m_pIB);
	m_pGraphic_Device->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);

	pEffectBack->EndPass();
	pEffectBack->End();


	Safe_Release(pEffect);
	Safe_Release(pEffectBack);
}

void CRenderer::Render_EnvMap()
{
	if (nullptr == m_pTarget_Manager)
		return;

	D3DXMATRIX mtViwCur;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &mtViwCur);

	D3DXMATRIX mtViw[6];

	D3DXMATRIX mtPrj;

	D3DXMatrixPerspectiveFovLH(&mtPrj, D3DX_PI / 2.f, 1.0F, 0.002F, 30000.0f);

	T_SetupCubeViewMatrix(&mtViw[m_iCount % 6], (D3DCUBEMAP_FACES)(m_iCount % 6));
	mtViw[m_iCount % 6] = mtViwCur * mtViw[m_iCount % 6];

	m_pTarget_Manager->Begin_MRT(L"MRT_Cube");

	for (auto& pGameObject : m_RenderList[RENDER_PRIORITY])
	{
		if (nullptr != pGameObject)
		{
			pGameObject->Render_CubeMap(&mtViw[m_iCount % 6], &mtPrj);
		}
	}

	Render_CubeMap(&mtViw[m_iCount % 6], &mtPrj);

	m_pTxCbm->GetCubeMapSurface((D3DCUBEMAP_FACES)(m_iCount % 6), 0, &pSrf);
	m_pGraphic_Device->StretchRect(m_pTarget_Manager->GetSurface(L"Target_Cube"), NULL, pSrf, NULL, D3DTEXF_LINEAR);

	m_pTarget_Manager->End_MRT(L"MRT_Cube");

	Safe_Release(pSrf);
}

void CRenderer::Render_Stencil()
{
	m_StencilList.sort(Compare2);

	for (auto& pGameObject : m_StencilList)
	{
		if (nullptr != pGameObject)
		{
			//pGameObject->Render_GameObject();
			pGameObject->Render_Stencil();
		}
	}
	m_StencilList.clear();
}

void CRenderer::Flashed()
{
	if (m_fFlashedCoolTime > 0.f)
	{
		m_bFlashed = false;
		m_iFlashedCondition = 0;
		m_fFlashedCoolTime -= m_fTimeDelta;
	}

	if (m_bFlashed)
	{
		if (m_iFlashedCondition == 1)
		{
			if (fgW > 0.001f)
				fgW -= m_fTimeDelta * 10.f;
			else if (fgW < 0.0f)
				fgW = 0.00001f;
			else
			{
				m_iFlashedCondition = 2;
			}
		}
		else if (m_iFlashedCondition == 2)
		{
			if(m_fFlashedTime < 2.f)
				m_fFlashedTime += m_fTimeDelta;
			else
			{
				if (fgW < 11.5f)
					fgW += m_fTimeDelta * 8.f;
				else
				{
					m_iFlashedCondition = 0;
					fgW = 11.5f;
					m_bFlashed = false;
					m_fFlashedTime = 0.f;
					m_fFlashedCoolTime = 5.f;
				}
			}
		}
	}
}

void CRenderer::Render_CubeMap(_matrix* View, _matrix* Proj)
{
	for (auto& pGameObject : m_CubemapList)
	{
		if (nullptr != pGameObject)
		{
			pGameObject->Render_CubeMap(View, Proj);
			//Safe_Release(pGameObject);
		}
	}
	m_CubemapList.clear();
}

void CRenderer::T_SetupCubeViewMatrix(_matrix * pmtViw, DWORD dwFace)
{
	_vec3 vcEye = _vec3(0.0f, 0.0f, 0.0f);

	_vec3 vcLook;

	_vec3 vcUp;


	switch (dwFace)

	{

	case D3DCUBEMAP_FACE_POSITIVE_X:

		vcLook = _vec3(1.0f, 0.0f, 0.0f);

		vcUp = _vec3(0.0f, 1.0f, 0.0f);

		break;

	case D3DCUBEMAP_FACE_NEGATIVE_X:

		vcLook = _vec3(-1.0f, 0.0f, 0.0f);

		vcUp = _vec3(0.0f, 1.0f, 0.0f);

		break;

	case D3DCUBEMAP_FACE_POSITIVE_Y:

		vcLook = _vec3(0.0f, 1.0f, 0.0f);

		vcUp = _vec3(0.0f, 0.0f, -1.0f);

		break;

	case D3DCUBEMAP_FACE_NEGATIVE_Y:

		vcLook = _vec3(0.0F, -1.0F, 0.0F);

		vcUp = _vec3(0.0F, 0.0F, 1.0F);

		break;

	case D3DCUBEMAP_FACE_POSITIVE_Z:

		vcLook = _vec3(0.0F, 0.0F, 1.0F);

		vcUp = _vec3(0.0F, 1.0F, 0.0F);

		break;

	case D3DCUBEMAP_FACE_NEGATIVE_Z:

		vcLook = _vec3(0.0F, 0.0F, -1.0F);

		vcUp = _vec3(0.0F, 1.0F, 0.0F);

		break;

	}

	D3DXMatrixLookAtLH(pmtViw, &vcEye, &vcLook, &vcUp);
}



CRenderer * CRenderer::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CRenderer*	pInstance = new CRenderer(pGraphic_Device);

	if (FAILED(pInstance->Ready_Renderer()))
	{
		MessageBox(0, L"CRenderer Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

// 원형객체가 호출하는 함수.
CComponent * CRenderer::Clone_Component(void* pArg)
{
	// this포인터는 멤버함수 안에 존재한다.
	// 멤버함수는 객체가 있어야 호출가능하다.
	// this는 멤버함수를 호출한 객체의 주소를 의미하낟.
	AddRef();
	 
	return this;
}

void CRenderer::Free()
{
	Safe_Release(m_pRandomTexture);
	Safe_Release(m_pSkinTex);
	Safe_Release(m_pLutTex);
	Safe_Release(m_pInstancing);
	Safe_Release(m_pVB);
	Safe_Release(m_pIB);
	Safe_Release(m_pTxCbm);
	Safe_Release(m_pRndEnv);

	Safe_Release(m_pShader_Blend);
	Safe_Release(m_pShader_LightAcc);
	Safe_Release(m_pShader_Back);
	Safe_Release(m_pTarget_Manager);
	CComponent::Free();
}
