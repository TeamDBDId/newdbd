matrix			g_matWVP;
texture			g_DiffuseTexture;
texture			g_LightInfoTex;

int				g_iNumLight;
bool			g_bSharp;

sampler	DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler	LightInfoSampler = sampler_state
{
	texture = g_LightInfoTex;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

float4 LightShaft(float2 texCoord, vector LightInfo)
{
	float decay = 0.94015;
	float exposure = 0.017f;
	float density = 0.966;
	float weight = 0.57767;
	int NUM_SAMPLES = 1;
	float2 tc = texCoord;
	float2 deltaTexCoord = (tc - LightInfo.xy);
	deltaTexCoord *= 1.0f / NUM_SAMPLES * density;
	float illuminationDecay = 1.0;
	float4 color = tex2D(DiffuseSampler, tc)*0.4f;
	float4 samp;

	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		tc -= deltaTexCoord;
		samp = tex2D(DiffuseSampler, tc)*0.4f;
		samp.w = 1.f;
		samp *= illuminationDecay * weight;
		color += samp;
		illuminationDecay *= decay;
	}

	return float4((float3(color.r, color.g, color.b) * exposure), 1);
}

struct PS_IN
{
	vector		vPosition : POSITION;
	float2		vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector		vColor : COLOR0;
};

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector vDiff = tex2D(DiffuseSampler, In.vTexUV);
	if (g_iNumLight == 0)
	{
		Out.vColor = vDiff;
		return Out;
	}

	vector vColor = vector(0,0,0,1);

	for (int i = 0; i < g_iNumLight; ++i)
	{
		vector vLightInfo = tex2D(LightInfoSampler, float2((i + 0.5f) / 64.f, 0.5f));
		vColor.xyz += LightShaft(In.vTexUV, vLightInfo).xyz;
	}
	vColor.xyz /= g_iNumLight;

	vColor = vDiff + vColor;

	Out.vColor = vColor;

	return Out;
}

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;
};

struct VS_OUT
{
	vector	vPosition : POSITION0;
	float2	vTexUV : TEXCOORD1;
};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	Out.vPosition = mul(vector(In.vPosition, 1.f), g_matWVP);
	Out.vTexUV = In.vTexUV;

	return Out;
}

struct PS_CUBEIN
{
	vector	vPosition : POSITION0;
	float2	vTexUV : TEXCOORD1;
};

struct PS_CUBEOUT
{
	vector		Tex : COLOR0;
};

PS_CUBEOUT PS_CUBE(PS_CUBEIN In)
{
	PS_CUBEOUT		Out = (PS_CUBEOUT)0;

	Out.Tex = tex2D(DiffuseSampler, In.vTexUV) * 0.8f;
	Out.Tex.a = 1.f;

	return Out;
}

technique	DefaultDevice
{
	pass Cube
	{
		ZEnable = true;
		ZWriteEnable = true;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass Render_Blend
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}


