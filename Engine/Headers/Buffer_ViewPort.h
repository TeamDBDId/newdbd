#pragma once

#include "VIBuffer.h"

_BEGIN(Engine)

class _ENGINE_DLL CBuffer_ViewPort final : public CVIBuffer
{
private:
	explicit CBuffer_ViewPort(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_ViewPort(const CBuffer_ViewPort& rhs);
	virtual ~CBuffer_ViewPort() = default;
public:
	HRESULT Ready_VIBuffer();
	void Render_VIBuffer(const _matrix* pWorldMatrix);
public:
	virtual _bool Picking_ToBuffer(_vec3* pOut, const CTransform* pTransformCom, const CPicking* pPickingCom);
private:
	VTXVIEWPORT*		m_pOriVertices = nullptr;
private:
	HRESULT Transform_Vertices(const _matrix* pWorldMatrix);
public:
	static CBuffer_ViewPort* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();


};

_END