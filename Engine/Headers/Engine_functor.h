#pragma once

// 함수 객체.

// 객체를 함수처럼 사용할 수 있게 한다.




namespace Engine
{
	class CFinder_Tag
	{
	public:
		CFinder_Tag(const wchar_t* pTag) : m_pTag(pTag) {}
		~CFinder_Tag() = default;
	public:
		template <typename T>
		bool operator ()  (T& Pair)
		{
			return !lstrcmp(Pair.first, m_pTag);
		}
	private:
		const wchar_t* m_pTag;
	};
	
	class CFinder_TagA
	{
	public:
		CFinder_TagA(const char* pTag) : m_pTag(pTag) {}
		~CFinder_TagA() = default;
	public:
		template <typename T>
		bool operator ()  (T& Pair)
		{
			return !strcmp(Pair.first, m_pTag);
		}
	private:
		const char* m_pTag;
	};

}