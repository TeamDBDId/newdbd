#pragma once

#include "Component.h"

// 충돌을 제어하기위한 메시. (구, 박스)

_BEGIN(Engine)

class CPicking;
class CShader;
class CMesh_Static;
class CTransform;
class _ENGINE_DLL CCollider final : public CComponent
{
public:
	enum TYPE { TYPE_BOX, TYPE_SPHERE, TYPE_END };
	enum BOXTYPE { BOXTYPE_AABB, BOXTYPE_OBB, BOXTYPE_END };
public:
	// 오비비충돌을 위한 정보.
	typedef struct tagOBB
	{
		_vec3			vPoint[8];
		_vec3			vCenter;
		_vec3			vAlignAxis[3]; // 박스에 평행한 축 세개.(투영받을 벡터)
		_vec3			vProjAxis[3]; // 박스의 중심에서 각 면을 바라보는 벡터 세개(투영한 길이를 더할 벡터)
	}OBB;


	typedef struct tagColliderInfo
	{
		BOXTYPE				eBoxType;
		_matrix				matLocalTransform;
		const _matrix*		pBoneMatrix;
		const _matrix*		pWorldMatrix;
		const _matrix*		pParentMatrix;

		tagColliderInfo() {	}
		// 1. 박스타입. 
		// 2. 로컬스페이스상에서의 추가적인 변환.(예:메시의 사이즈에 맞게 변환 or 내가 지정한 상태로 변환)
		// 3. 뼈의 행렬(콜라이더가 뼈에 붙어서 출력되야하는 경우.)
		// 4. 월드 행렬(콜라이더가 월드 스페이스상에서 어떻게 출력돼야하는 지)
		// 5. 부모 행렬(콜라이더가 어떤 부모를 사용하고 있는지)
		tagColliderInfo(BOXTYPE InBoxType, _matrix InLocalTransform
			, const _matrix* pInBoneMatrix, const _matrix* pInWorldMatrix
			, const _matrix* pInParentMatrix = nullptr)
			: eBoxType(InBoxType), matLocalTransform(InLocalTransform)
			, pBoneMatrix(pInBoneMatrix), pWorldMatrix(pInWorldMatrix)
			, pParentMatrix(pInParentMatrix)
		{ }
	}COLLIDERINFO;
private:
	explicit CCollider(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCollider(const CCollider& rhs);
	virtual ~CCollider() = default;
public:
	const OBB* Get_OBBDescPointer() const {
		return m_pOBB;
	}
	const _vec3 Get_Center();
	LPD3DXMESH GetMesh() { return m_pCollider; }
public:
	HRESULT Ready_Collider(TYPE eType);
	HRESULT Ready_Clone_Collider(void* pArg);
	HRESULT Ready_Clone_ColliderSphere(void* _pArg);

	HRESULT Ready_HullMesh(const _uint& _iScene , const _tchar* _pHull);
	HRESULT Ready_NaviMesh(const _uint& _iScene, const _tchar* _pHull);
	HRESULT Ready_RealMesh(CMesh_Static* _pRealMesh);

	_bool Collision_Sphere(const CCollider* pTargetCollider);
	_bool Collision_AABB(const CCollider* pTargetCollider, _vec3* _pGap = nullptr);
	//_bool Collision_OBB(const CCollider* pTargetCollider);
	_bool Collision_LAY(const CTransform* _pTrans, _vec3* _pGap, const _bool& _isReal, const _bool& _isHouse = false);
	_bool Collision_CameraLAY(const CTransform* _pTrans,const _vec3* _pCamPos,const _bool& _isReal,_float* _pDist);
	_bool Collision_CameraLAY_Sphere(_float* _pDist);
	_bool Collision_WeaponLAY(CCollider* pTargetCollider,CTransform* _pTrans, const _bool& _isReal = false);
	_bool Collision_HullMeshLAY(CTransform * _pTrans, const _vec3& vRay, _float* _fDist,const _bool & _isReal =false);
	//_bool Collision_LAY_Sub(const CTransform* _pTrans, _vec3* _pGap, const _bool& _isReal =false);

	_bool Collision_LAY_Terr(CTransform* _pTrans,_float* _fDist);

	_bool Collision_Circle(CCollider* pTargetCollide, _vec3* _pGap=nullptr);

	void Set_CircleRad(const _float& _fCircleRad) { m_fCircleRad = _fCircleRad; }

	_bool Picking_ToCollider(_vec4* pOut, const CPicking* pPickingCom); // OnTool
	void Render_Collider();
	_vec3 Get_Position() { return m_vCenter; }

	_vec3 Cal_Normal(const LPD3DXMESH &pMesh, const DWORD &dwNumFaces);
	//void Cal_Dist(const LPD3DXMESH &pMesh, const DWORD &dwNumFaces, const _vec3& _vPos,_float* _fDist);

	void Set_Dist(const _float& _fDist, const _float& _fSlsher = 1.f);
	const _float& Get_Dist() { return m_fDist; }
	const _float& Get_Slasher() { return m_fSlasher; }

	const _vec3* Get_CamPos() { return m_pCamPos; }
	void Set_CamPos(const _vec3* _pCamPos) { m_pCamPos = _pCamPos; }
private:
	LPD3DXMESH			m_pCollider;
	LPD3DXBUFFER		m_pAdjacency;
	//CShader*			m_pShader = nullptr;
	_bool				m_isColl = false;
	BOXTYPE				m_eBoxType = BOXTYPE(0);
	_vec3				m_vMin, m_vMax;
	COLLIDERINFO		m_ColliderInfo;
	OBB*				m_pOBB = nullptr;

	CMesh_Static*		m_pHull = nullptr;
	CMesh_Static*		m_pNavi = nullptr;
	CMesh_Static*		m_pReal = nullptr;


	_bool				m_isHullLoad = false;
	_bool				m_isNaviLoad = false;

	TYPE				m_eType = TYPE_END;
	_vec3				m_vCenter = _vec3(0.f,0.f,0.f);
	_float				m_fRad = 0.f;

	_float				m_fDist = 1.f;
	_float				m_fSlasher = 0.f;

	_bool				m_fRender = false;

	_float				m_fCircleRad = 0.f;
	
	const _vec3*		m_pCamPos = nullptr;
private:
	HRESULT Ready_BoundingBox();
	HRESULT Ready_BoundingSphere();
	_matrix Remove_Rotation(_matrix matTransform) const;
	_matrix Compute_WorldTransform() const;
	_matrix Compute_WorldTransformForMesh() const;
	void Compute_AlignAxis(OBB* pOBB);
	void Compute_ProjAxis(OBB* pOBB);


public:
	static CCollider* Create(LPDIRECT3DDEVICE9 pGraphic_Device, TYPE eType);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();
};

_END