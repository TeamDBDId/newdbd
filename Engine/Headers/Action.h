#pragma once

#include "Base.h"

_BEGIN(Engine)

class CGameObject;
class _ENGINE_DLL CAction abstract : public CBase
{
public:
	enum STATE { UPDATE_ACTION, END_ACTION, FAILED_ACTION};
protected:
	explicit CAction();
	virtual ~CAction() = default;
public:
	virtual HRESULT Ready_Action() = 0;
	virtual _int	Update_Action(const _float& fTimeDelta) = 0;
	virtual void	End_Action() = 0;
	virtual void	Send_ServerData() = 0;
public:
	_float				m_fDuration_Time = 0.f;			// 애니메이션 끝나는 시간
	_float				m_fCurrentTime = 0.f;			// 애니메이션 현재 시간
	CGameObject*		m_pGameObject = nullptr;
	_bool				m_bIsPlaying = false;
protected:
	STATE Check_Action(const _float& fTimeDelta);
protected:
	virtual void Free();
};

_END