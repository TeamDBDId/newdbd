#pragma once

// 1. 레이어들을 모아서 관리한다.
// 2. 원형 객체들을 모아놓는다.
// 3. 객체를 레어이에 추가하려고 할 시
// 원형객체가 존재하는지 검사. 
// 원형객체를 복재.
// 객체를 레이어에 추가.

#include "Base.h"

_BEGIN(Engine)

class CLayer;
class CGameObject;
class CComponent;
class CObject_Manager final : public CBase
{
	_DECLARE_SINGLETON(CObject_Manager)
private:
	explicit CObject_Manager();
	virtual ~CObject_Manager() = default;
public:
	CComponent* Get_ComponentPointer(const _uint& iSceneID, const _tchar* pLayerTag, const _tchar* pComponentTag, const _uint& iIndex);
public:
	HRESULT Reserve_Object_Manager(const _uint& iNumScene);
	HRESULT Add_Prototype_GameObject(const _tchar* pGameObjectTag, CGameObject* pGameObject);
	HRESULT Add_GameObjectToLayer(const _tchar* pProtoTag, const _uint& iSceneID, const _tchar* pLayerTag, CGameObject** ppCloneObject = nullptr);
	_int	Update_GameObject_Manager(const _float& fTimeDelta);
	_int	Update_Scene_GameObject_Manager(const _float& fTimeDelta, const _uint& iSceneID);
	_int	LastUpdate_GameObject_Manager(const _float& fTimeDelta);
	_int	LastUpdate_Scene_GameObject_Manager(const _float& fTimeDelta, const _uint& iSceneID);
	HRESULT Clear_Layers(const _uint& iSceneID);
public:
	list<CGameObject*>& Get_ObjectList(const _uint& iSceneID, const _tchar* pLayerTag);
	CGameObject* Get_GameObject(const _uint& iSceneID, const _tchar* pLayerTag, const _uint& iIndex);
	HRESULT	Remove_Prototype_GameObject(const _tchar* pGameObjectTag);
	HRESULT	Remove_GameObjectFromLayer(const _uint& iSceneID, const _tchar* pLayerTag);
public:
	map<const _tchar*, CGameObject*>& Get_mapPrototype() { return m_mapPrototype; }
	map<const _tchar*, CLayer*>*& Get_mapLayers() { return m_pMapLayers; }
	list<CGameObject*>* Get_ObjList(const _uint& iSceneID, const _tchar* pLayerTag);
private: // 원형객체들을 모아노흔다.
	map<const _tchar*, CGameObject*>			m_mapPrototype;
	typedef map<const _tchar*, CGameObject*>	MAPPROTOTYPE;

private: // 원형객체들을 복제한 객체를 레이어에 보관한다.
	map<const _tchar*, CLayer*>*		m_pMapLayers = nullptr;
	typedef map<const _tchar*, CLayer*>	MAPLAYERS;
private:
	_uint								m_iNumScene = 0;
private:
	CGameObject* Find_Prototype(const _tchar* pGameObjectTag);
	CLayer* Find_Layer(const _uint& iSceneID, const _tchar* pLayerTag);
protected:
	virtual void Free();
};

_END