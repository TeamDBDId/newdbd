#pragma once


// 컴포넌트 원형객체들만 모아서 관리한다.
#include "Base.h"
#include "Transform.h"
#include "Renderer.h"
#include "Buffer_TriCol.h"
#include "Buffer_RcTex.h"
#include "Buffer_CubeTex.h"
#include "Buffer_Terrain.h"
#include "Buffer_ViewPort.h"
#include "Texture.h"
#include "Picking.h"
#include "Shader.h"
#include "Mesh_Static.h"
#include "Mesh_Dynamic.h"
#include "Collider.h"
#include "Navigation.h"
#include "Frustum.h"

_BEGIN(Engine)

class CComponent_Manager final : public CBase
{
	_DECLARE_SINGLETON(CComponent_Manager)
public:
	explicit CComponent_Manager();
	virtual ~CComponent_Manager() = default;
public:
	HRESULT Reserve_Component_Manager(const _uint& iNumScene);
	HRESULT Add_Prototype_Component(const _uint& iSceneID, const _tchar* pComponentTag, CComponent* pComponent);
	CComponent* Clone_Component(const _uint& iSceneID, const _tchar* pComponentTag, void* pArg);

	map<const _tchar*, CComponent*>*& Get_mapComPrototype() { return m_pMapPrototype; }
	HRESULT Remove_Prototype_Component(const _uint& iSceneID, const _tchar* pComponentTag);
private:
	map<const _tchar*, CComponent*>*		m_pMapPrototype = nullptr;
	typedef map<const _tchar*, CComponent*>	MAPPROTOTYPE;
private:
	_uint		m_iNumScene = 0;
private:
	CComponent* Find_Component(const _uint& iSceneID, const _tchar* pComponentTag);
protected:
	virtual void Free();
};

_END