#pragma once

#include "VIBuffer.h"

_BEGIN(Engine)

class CFrustum;
//class CQuadTree;
class _ENGINE_DLL CBuffer_Terrain final : public CVIBuffer
{
private:
	explicit CBuffer_Terrain(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CBuffer_Terrain(const CBuffer_Terrain& rhs);
	virtual ~CBuffer_Terrain() = default;
public:
	HRESULT Ready_VIBuffer(const _uint& iNumVerticesX, const _uint& iNumVerticesZ, const _float& fInterval);
	HRESULT Ready_VIBuffer(const _tchar* pHeightMap, const _float& fInterval);
	void Render_VIBuffer();
	_float Compute_HeightOnTerrain(const _vec3* pPosition);
	//HRESULT Culling_ToFrustum(CFrustum* pFrustum, const _matrix& matWorld);
private:
	_uint				m_iNumVerticesX = 0;
	_uint				m_iNumVerticesZ = 0;
	_float				m_fInterval = 0.f;
	BITMAPFILEHEADER	m_fh;
	BITMAPINFOHEADER	m_ih;
	_ulong*				m_pPixel = nullptr;
private:
	POLYGON32*			m_pPolygonVertexIndex = nullptr;
	//CQuadTree*			m_pQuadTree = nullptr;

public:
	static CBuffer_Terrain* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _uint& iNumVerticesX, const _uint& iNumVerticesZ, const _float& fInterval = 1.f);
	static CBuffer_Terrain* Create(LPDIRECT3DDEVICE9 pGraphic_Device, const _tchar* pHeightMap, const _float& fInterval = 1.f);
	virtual CComponent* Clone_Component(void* pArg = nullptr);
protected:
	virtual void Free();


};

_END