#pragma once

#include "Engine_Defines.h"

// 모든 클래스들의 부모. 
// 참조 갯수(레퍼런스카운트)를 관리한다.
_BEGIN(Engine)

class _ENGINE_DLL CBase abstract
{
protected:
	explicit CBase();
	virtual ~CBase() = default;
public:
	// _ulong : 증가시키고 난 이후의 레퍼런스 카운트를 리턴한다.
	_ulong AddRef(); // 레퍼런스 카운트를 증가시키낟. 

	// _ulong : 감소시키기 이전의 레퍼런스 카운트를 리턴한다.
	_ulong Release(); // 레퍼런스 카운트를 감소시킨다.
private:
	_ulong			m_dwRefCnt = 0;
protected:
	virtual void Free() = 0;
	
};

_END