#pragma once

#include "Base.h"


_BEGIN(Engine)

class CShader;
class CLight;
class _ENGINE_DLL CLight_Manager final : public CBase
{
	_DECLARE_SINGLETON(CLight_Manager)
public:
	explicit CLight_Manager();
	virtual ~CLight_Manager() = default;
public:
	D3DLIGHT9* Get_LightInfo(const _uint& iIndex = 0);
	_vec3	GetProjPos(const _uint& iIndex = 0);
	_uint	GetSize() { return m_LightList.size(); }
public:
	HRESULT Ready_LightManager(LPDIRECT3DDEVICE9 pGraphicDev);
	HRESULT Add_LightInfo(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9& LightInfo);
	CLight* Add_LightInfo2(LPDIRECT3DDEVICE9 pGraphic_Device, const D3DLIGHT9& LightInfo);
	void Render_Light(LPD3DXEFFECT pEffect);
	void Ready_ShadowMap(CLight* pLight);
	void Cal_ShaftList();
	void Delete_AllLight();
	void Set_ShadowCube(CShader* pShader) { m_pShader_ShadowCube = pShader; }
	LPDIRECT3DTEXTURE9 Get_LightInfoTexture();
	_int Get_SizeofLInfo() { return m_iSize; }
	//void Render_DynamicShadow();
private:
	CShader*				m_pShader_ShadowCube = nullptr;
	_int					m_iSize = 0;
	LPDIRECT3DDEVICE9		m_pGraphicDev = nullptr;
	LPDIRECT3DTEXTURE9		m_pLightInfoTex = nullptr;
	list<CLight*>			m_LightList;
	//list<CLight*>			m_ShaftList;
	_vec4					m_LightInfo[64];
	typedef list<CLight*>	LIGHTLIST;
protected:
	virtual void Free();
};

_END