#pragma once
#include "Base.h"
#include "GameObject.h"

_BEGIN(Client)
class CToolManager : public CBase
{
	_DECLARE_SINGLETON(CToolManager)
private:
	explicit CToolManager();
	virtual ~CToolManager() = default;

public:
	void AddPickingPosition(CGameObject* Object, _vec4 vPickingPoint);
	void SetPickingTerrain(CGameObject* PickingTerrain) { m_pPickingTerrain = PickingTerrain; }
	void ReSetPickingObject() { m_pPickingObject = nullptr; }
	void SetCameraMatrix(const _matrix* matrix) { m_CameraMatrix = matrix; }
	CGameObject* Get_PickingObject() { return m_pPickingObject; }
	_vec3 Get_PickingPoint() { return _vec3(m_vPickingPoint.x, m_vPickingPoint.y, m_vPickingPoint.z); }
	POINT Get_MousePoint() { return m_MousePoint; }
	const _matrix* Get_CameraMatrix() { return m_CameraMatrix; }
private:
	_vec4 m_vPickingPoint = { -1.f, -1.f, -1.f, -1.f };
	POINT m_MousePoint = { -1, -1 };
	const _matrix* m_CameraMatrix = nullptr;
	CGameObject* m_pPickingObject = nullptr;
	CGameObject* m_pPickingTerrain = nullptr;

private:
	virtual void Free() override;

};
_END