#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CPicking;
class CFrustum;
_END

_BEGIN(Client)

class CMeatHook final : public CGameObject
{
public:
	enum STATE {
		Stand, SpiderStruggle_MIN, SpiderStruggle_MAX, SpiderStruggle2Sacrifice, SpiderStabOut, SpiderStabLoop, SpiderStabIN, SpiderReaction_OUT,
		SpiderReaction_Loop, SpiderReaction_IN,	Spider_Struggle, Idle, HookedFree, Broken_Idle, BreakEnd, Struggle, GetHookedOut, GetHookedIn };
private:
	explicit CMeatHook(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMeatHook(const CMeatHook& rhs);
	virtual ~CMeatHook() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();

public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
private:
	void State_Check();
	void OnTool();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CPicking*			m_pPickingCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	wstring m_Key;
	_int m_iOtherOption = 0;
	STATE m_OldState = Idle;
	STATE m_CurState = Idle;
	_float m_fDeltaTime = 0.f;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CMeatHook* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END