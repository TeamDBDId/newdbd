
texture			g_DiffuseTexture;

sampler	DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_ShadeTexture;

sampler	ShadeSampler = sampler_state
{
	texture = g_ShadeTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};


texture			g_SpecularTexture;

sampler	SpecularSampler = sampler_state
{
	texture = g_SpecularTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture			g_RefTexture;

sampler	RefSampler = sampler_state
{
	texture = g_RefTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};


struct PS_IN
{
	vector		vPosition : POSITION;
	float2		vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector		vColor : COLOR0;
};

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector			vDiffuse = tex2D(DiffuseSampler, In.vTexUV);
	vector			vShade = tex2D(ShadeSampler, In.vTexUV);
	vector			vSpecular = tex2D(SpecularSampler, In.vTexUV);	
	vector			vRef = tex2D(RefSampler, In.vTexUV);

	vector vColor = vDiffuse * vShade + vRef;

	//float2 vVelosity = vSpecular.zw;

	//In.vTexUV += vVelosity;
	//for (int i = 1; i <6; ++i, In.vTexUV += vVelosity)
	//{
	//	float4 currentColor = tex2D(ShadeSampler, In.vTexUV) * tex2D(DiffuseSampler, In.vTexUV) /*+ tex2D(RefSampler, In.vTexUV);*/;
	//	vColor += currentColor;
	//}

	//float4 finalColor = vColor / 6.f;


	//if (g_bBlur)
	//{
	//	float2 vCenter = float2(0.5f, 0.5f);
	//	float2 vDir = vCenter - In.vTexUV;


	//	float4 col;
	//	for (int x = -6; x <= 6; ++x)
	//	{
	//		float2 T = In.vTexUV;
	//		T.x += (2.f * x) / 1024.f;
	//		col += tex2D(DiffuseSampler, T) * exp((-x*x) / 12.f);
	//	}
	//	col /= 12.f;

	//	//float factor = max(((0.5f - length(vDir)) / 0.5f), 0.f);
	//	float factor = (length(vDir) * 1.9f)/* * (length(vDir) * 1.8f);*/;
	//	Out.vColor = lerp(finalColor, col, /*(1.f - factor) * (1.f - factor)*/factor);
	//	Out.vColor *= 1.18f;
	//}
	//else
	
	//Out.vColor = finalColor;
	Out.vColor = vColor;

	return Out;
}

technique	DefaultDevice
{
	pass Render_Blend
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}


