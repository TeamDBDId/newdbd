
// MapTool.h : MapTool 응용 프로그램에 대한 주 헤더 파일
//
#pragma once

#ifndef __AFXWIN_H__
	#error "PCH에 대해 이 파일을 포함하기 전에 'stdafx.h'를 포함합니다."
#endif

#include "resource.h"       // 주 기호입니다.
#include "Engine_Defines.h"

// CMapToolApp:
// 이 클래스의 구현에 대해서는 MapTool.cpp을 참조하십시오.
//

_BEGIN(Engine)
class CSystem;
_END

_BEGIN(Client)

class CMainApp;
class CMapToolApp : public CWinAppEx
{
public:
	CMapToolApp();


// 재정의입니다.
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// 구현입니다.
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
	virtual int Run();


private:
	CMainApp*	m_pMainApp = nullptr;
	CSystem*	m_pSystem = nullptr;
private:
	_tchar		m_szFPS[MAX_PATH] = L"";
	_ulong		m_dwRenderCnt = 0;
	_float		m_fTimeAcc = 0.f;
};

extern CMapToolApp theApp;

_END