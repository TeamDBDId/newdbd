#include "stdafx.h"
#include "..\Headers\Tree.h"
#include "Management.h"
#include "ToolManager.h"
#include "Defines.h"
_USING(Client)

CTree::CTree(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CTree::CTree(const CTree & rhs)
	: CGameObject(rhs)
{
}

HRESULT CTree::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CTree::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (nullptr != m_pMeshCom && m_Max < 0.001f)
	{
		_vec3 vMax = m_pMeshCom->GetMax();
		_vec3 vMin = m_pMeshCom->GetMin();
		_vec3 vResult = (vMax - vMin)*0.51f;
		m_Max = max(vResult.x, max(vResult.y, vResult.z));
	}

	return NOERROR;
}

_int CTree::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	//OnTool();
	return _int();
}

_int CTree::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;
	return 0;
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	//if (m_fCameraDistance > 3000.f)
	//	return 0;

	m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this);

	return _int();
}

void CTree::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);


	m_pColliderCom->Render_Collider();
}

void CTree::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	_matrix mat = matWorld;
	mat._42 = 0.f;
	m_pTransformCom->Set_Matrix(mat);


	m_iTreeNum = OtherOption;
	if (m_iTreeNum >= 3)
		m_iTreeNum = 0;
	if (m_iTreeNum == 1)
		m_iTreeNum = 2;


	if (Key == nullptr)
		return;

	m_Key = Key;

	_tchar MeshTag[256];

	swprintf_s(MeshTag, L"Mesh_Tree_0%d", m_iTreeNum + 1);

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, MeshTag);
	Add_Component(L"Com_Mesh", m_pMeshCom);

	//For.Com_Collider
	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Box", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_AABB, m_pMeshCom->Get_LocalTransform(), nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);
}

_matrix CTree::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CTree::Get_Key()
{
	return m_Key.c_str();
}

_int CTree::Get_OtherOption()
{
	return m_iTreeNum;
}

void CTree::OnTool()
{
	if (nullptr != m_pPickingCom)
		m_pPickingCom->Update_Ray();
	_bool bIsPick = false;
	_vec4 vPickingPoint;
	bIsPick = m_pColliderCom->Picking_ToCollider(&vPickingPoint, m_pPickingCom);
	if (bIsPick)
		GET_INSTANCE(CToolManager)->AddPickingPosition(this, vPickingPoint);
}

HRESULT CTree::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Picking
	m_pPickingCom = (CPicking*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Picking");
	if (FAILED(Add_Component(L"Com_Picking", m_pPickingCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CTree::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	pEffect->SetMatrix("g_matView", &matView);
	pEffect->SetMatrix("g_matProj", &matProj);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	D3DXMatrixInverse(&matView, nullptr, &matView);
	pEffect->SetVector("g_vCamPosition", (_vec4*)&matView.m[3][0]);

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);


	return NOERROR;
}

CTree * CTree::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CTree*	pInstance = new CTree(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CTree Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CTree::Clone_GameObject()
{
	CTree*	pInstance = new CTree(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CTree::Free()
{
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pPickingCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
