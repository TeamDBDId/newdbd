#include "stdafx.h"
#include "..\Headers\Scene_Logo.h"
#include "Management.h"
#include "Scene_Stage.h"

_USING(Client)

CScene_Logo::CScene_Logo(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{

}

HRESULT CScene_Logo::Ready_Scene()
{
	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;

	return NOERROR;
}

_int CScene_Logo::Update_Scene(const _float & fTimeDelta)
{
	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Logo::LastUpdate_Scene(const _float & fTimeDelta)
{
	if (GetKeyState(VK_SPACE) & 0x8000)
	{
		CManagement*		pManagement = CManagement::GetInstance();
		if (nullptr == pManagement)
			return -1;
		pManagement->AddRef();

		CScene_Stage*	pNewScene = CScene_Stage::Create(m_pGraphic_Device);
		if (nullptr == pNewScene)
			return - 1;

		if (FAILED(pManagement->SetUp_ScenePointer(pNewScene)))
			return -1;

		Safe_Release(pNewScene);
		Safe_Release(pManagement);

		return 0;

	}


	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Logo::Render_Scene()
{
	int a = 10;
}

// 원형객체를 생성해 놓는다.
HRESULT CScene_Logo::Ready_Prototype_GameObject()
{

	return NOERROR;
}

HRESULT CScene_Logo::Ready_Prototype_Component()
{

	return NOERROR;
}

CScene_Logo * CScene_Logo::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Logo*	pInstance = new CScene_Logo(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Logo Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);		
	}
	return pInstance;
}

void CScene_Logo::Free()
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return;

	pManagement->AddRef();

	pManagement->Clear_Layers(SCENE_LOGO);
	
	Safe_Release(pManagement);

	CScene::Free();
}
