#include "stdafx.h"
#include "..\Headers\Scene_Stage.h"
#include "Management.h"

#include "Camera_Debug.h"
#include "Light_Manager.h"
#include "LoadManager.h"

_USING(Client)

CScene_Stage::CScene_Stage(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CScene(pGraphic_Device)
{

}

HRESULT CScene_Stage::Ready_Scene()
{
	if (FAILED(Ready_Prototype_Component()))
		return E_FAIL;

	if (FAILED(Ready_LightInfo()))
		return E_FAIL;
	if (FAILED(Ready_Prototype_GameObject()))
		return E_FAIL;

	if (FAILED(Ready_Layer_Camera(L"Layer_Camera")))
		return E_FAIL;

	if (FAILED(Ready_Layer_BackGround(L"Layer_BackGround")))
		return E_FAIL;


	return NOERROR;
}

_int CScene_Stage::Update_Scene(const _float & fTimeDelta)
{

	return CScene::Update_Scene(fTimeDelta);
}

_int CScene_Stage::LastUpdate_Scene(const _float & fTimeDelta)
{
	return CScene::LastUpdate_Scene(fTimeDelta);
}

void CScene_Stage::Render_Scene()
{

}

HRESULT CScene_Stage::Ready_LightInfo()
{
	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return E_FAIL;

	pLight_Manager->AddRef();

	D3DLIGHT9			LightInfo;
	ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	LightInfo.Type = D3DLIGHT_DIRECTIONAL;
	LightInfo.Diffuse = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	LightInfo.Specular = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);
	LightInfo.Ambient = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);

	LightInfo.Direction = _vec3(1.f, -1.f, 1.f);

	if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
		return E_FAIL;


	ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	LightInfo.Type = D3DLIGHT_POINT;
	LightInfo.Diffuse = D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f);
	LightInfo.Specular = LightInfo.Diffuse;
	LightInfo.Ambient = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);

	LightInfo.Position = _vec3(5.f, 5.f, 5.f);
	LightInfo.Range = 10.0f;

	if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
		return E_FAIL;

	ZeroMemory(&LightInfo, sizeof(D3DLIGHT9));

	LightInfo.Type = D3DLIGHT_POINT;
	LightInfo.Diffuse = D3DXCOLOR(0.2f, 1.f, 0.2f, 1.f);
	LightInfo.Specular = LightInfo.Diffuse;
	LightInfo.Ambient = D3DXCOLOR(1.f, 1.f, 1.f, 1.f);

	LightInfo.Position = _vec3(15.f, 5.f, 5.f);
	LightInfo.Range = 10.0f;

	if (FAILED(pLight_Manager->Add_LightInfo(m_pGraphic_Device, LightInfo)))
		return E_FAIL;

	Safe_Release(pLight_Manager);

	return NOERROR;
}

// 원형객체를 생성해 놓는다.
HRESULT CScene_Stage::Ready_Prototype_GameObject()
{
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_GameObject()))
		return E_FAIL;

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Prototype_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Component_Buffer_Terrain	
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_Terrain", CBuffer_Terrain::Create(m_pGraphic_Device, 100, 100, 100.f))))
		return E_FAIL;

	// For.Component_Shader_Terrain
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Shader_Terrain", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Terrain.fx"))))
		return E_FAIL;

	// For.Component_Shader_Mesh
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Shader_Mesh", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Mesh.fx"))))
		return E_FAIL;

	// For.Component_Shader_Effect
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Shader_Effect", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Effect.fx"))))
		return E_FAIL;

	// For.Component_Shader_Sky
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Shader_Sky", CShader::Create(m_pGraphic_Device, L"../Bin/ShaderFiles/Shader_Sky.fx"))))
		return E_FAIL;

	// For.Component_Buffer_CubeTex
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_CubeTex", CBuffer_CubeTex::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Buffer_ViewPort
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Buffer_ViewPort", CBuffer_ViewPort::Create(m_pGraphic_Device))))
		return E_FAIL;

	// For.Component_Texture_Terrain
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Terrain", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Terrain/T_DirtTerrain_BC.tga"))))
		return E_FAIL;	

	// For.Component_Texture_Sky
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Texture_Sky", CTexture::Create(m_pGraphic_Device, CTexture::TYPE_CUBE, L"../Bin/Resources/Textures/SkyBox/Burger%d.dds", 4))))
		return E_FAIL;

	// For.Component_Picking
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Picking", CPicking::Create(m_pGraphic_Device, g_hWnd))))
		return E_FAIL;

	// For.Componet_Collider_Box
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Collider_Box", CCollider::Create(m_pGraphic_Device, CCollider::TYPE_BOX))))
		return E_FAIL;

	// For.Componet_Collider_Sphere
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Componet_Collider_Sphere", CCollider::Create(m_pGraphic_Device, CCollider::TYPE_SPHERE))))
		return E_FAIL;

	// For.Component_Navigation
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Navigation", CNavigation::Create(m_pGraphic_Device, L"../Bin/Data/Navi.dat"))))
		return E_FAIL;

	// For.Component_Frustum
	if (FAILED(pManagement->Add_Prototype_Component(SCENE_STAGE, L"Component_Frustum", CFrustum::Create(m_pGraphic_Device))))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Add_ReplacedName()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_StaticMesh()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_S1_DynamicMesh()))
		return E_FAIL;
	if (FAILED(GET_INSTANCE(CLoadManager)->Prototype_L_StaticMesh()))
		return E_FAIL;

	Safe_Release(pManagement);	

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_Camera(const _tchar * pLayerTag)
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return E_FAIL;

	pManagement->AddRef();

	// For.Camera_Debug
	CCamera_Debug*		pCameraObject = nullptr;

	// 원형카메라를 복제해서 레이어에 추가할(실제 사용할)객체를 생성한다.레이어에 추가한다ㅏ.
	// 그렇게 복제된 객체를 건져온다.
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Camera_Debug", SCENE_LOGO, pLayerTag, (CGameObject**)&pCameraObject)))
		return E_FAIL;

	CAMERADESC		CameraDesc;
	ZeroMemory(&CameraDesc, sizeof(CAMERADESC));
	CameraDesc.vEye = _vec3(0.f, 3000.f, -10.f);
	CameraDesc.vAt = _vec3(0.f, 0.f, 0.f);
	CameraDesc.vAxisY = _vec3(0.f, 1.f, 0.f);
		
	PROJDESC		ProjDesc;
	ZeroMemory(&ProjDesc, sizeof(PROJDESC));
	ProjDesc.fFovY = D3DXToRadian(60.0f);
	ProjDesc.fAspect = _float(g_iBackCX) / g_iBackCY;
	ProjDesc.fNear = 0.2f;
	ProjDesc.fFar = 30000.f;

	if (FAILED(pCameraObject->SetUp_CameraProjDesc(CameraDesc, ProjDesc)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CScene_Stage::Ready_Layer_BackGround(const _tchar * pLayerTag)
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return E_FAIL;

	pManagement->AddRef();

	//// For.Terrain
	//if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Terrain", SCENE_STAGE, pLayerTag)))
	//	return E_FAIL;

	// For.PickingTerrain
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_PickingTerrain", SCENE_LOGO, pLayerTag)))
		return E_FAIL;




	// For.Sky
	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_Sky", SCENE_LOGO, pLayerTag)))
		return E_FAIL;



	Safe_Release(pManagement);

	return NOERROR;
}

CScene_Stage * CScene_Stage::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CScene_Stage*	pInstance = new CScene_Stage(pGraphic_Device);

	if (FAILED(pInstance->Ready_Scene()))
	{
		MessageBox(0, L"CScene_Stage Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);		
	}
	return pInstance;
}

void CScene_Stage::Free()
{
	CManagement*		pManagement = CManagement::GetInstance();

	if (nullptr == pManagement)
		return;

	pManagement->AddRef();

	pManagement->Clear_Layers(SCENE_STAGE);
	
	Safe_Release(pManagement);

	CScene::Free();
}
