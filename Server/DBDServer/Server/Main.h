#pragma once

#define MAX_Client 4

// IOCP 관련 모드 설정
#define Recv_Mode 0
#define Send_Mode 1
#define No_Connected FALSE

//Perk
#define HOOK_BEING				0x00000080
#define BEING_HEAL				0x00000100
#define HEALED					0x00000200
#define END_HEAL				0x00000400
#define ClOSET_OCCUPIED			0x00000800
#define WIGGLE_RIGHT			0x00001000
#define WIGGLE_LEFT				0x00002000
#define FLASHBANG				0x00004000
#define HOOK_BEINGBORROWED		0x00008000

//
#define BORROWEDTIME			0x00000040

// CURSE
#define CSWALLOWEDHOPE			0x00000001
#define CRUIN					0x00000002
#define CINCANTATION			0x00000004

#define INCANTATION				0x00000080

//
#define SABOTAGEING				100
#define BROKENING				101
#define SABOTAGETIME			30.0f
#define HOOKREPAIRTIME			60.0f

#define CLEAR_PACKET			0X00200000

typedef struct tagCamperData //Camper
{
	int				Padding = 0;
	int				Number = 0;
	bool			bConnect = false;
	bool			bLive = false;

	D3DXVECTOR3 vRight = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vUp = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vLook = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vPos = { 0.f, 0.f, 0.f };

	D3DXVECTOR3 vCamRight = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vCamUp = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vCamLook = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vCamPos = { 0.f, 0.f, 0.f };

	int				InterationObject = 0;
	int				InterationObjAnimation = 0;
	int				SecondInterationObject = 0;
	int				SecondInterationObjAnimation = 0;
	int				Character = 0;
	int				iState = 0;

	unsigned int	iItem = 0;
	unsigned int	iCondition = 0;
	unsigned int	iOldCondition = 0;
	unsigned int	iHookStage = 0;
	float			fEnergy = 0.f;

	int				Packet = 0;
	float			fHeal = 0.f;
	int				RecvPacket = 0;

	int				Score[4] = { 0,0,0,0 };
	int				Perk = 0;

	bool			bUseItem = false;
}CamperData;

typedef struct tagSlasherData //Slasher
{
	int				Padding = 0;
	int				Number = 5;

	unsigned int	iCharacter = 0;
	int				iState = 0;
	int				iLowerState = 0;
	int				iSkill = 0;

	int				InterationObject = 0;
	int				InterationObjAnimation = 0;
	int				SecondInterationObject = 0;
	int				SecondInterationObjAnimation = 0;

	D3DXVECTOR3		vCamLook = { 0.f, 0.f, 0.f };

	D3DXVECTOR3 vRight = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vUp = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vLook = { 0.f, 0.f, 0.f };
	D3DXVECTOR3 vPos = { 0.f, 0.f, 0.f };

	int				RecvPacket = 0;
	int				Perk = 0;
	int				Packet = 0;
	int				Score[4] = { 0,0,0,0 };
	bool			bConnect = false;
}SlasherData;

typedef struct tagGameData
{
	int		Packet = 0;
	int		WoodBoard[7];

	float	GRP[7];	// GeneratorRepairProgress
	float	ItemBox[9];
	int		ItemBoxAnim[9];
	int		Item[9];

	float	Totem[5];
	int		Curse;

	float	ExitDoor[2];
	int		iHatch;

	int		Locker[27];
	int		Hook[12];
	float	HookBroken[12];
	int		Spider[12];
	int		iPlayerAction[5];
}GameData;

enum OBJECT_ID { CAMPER = 0x100, SLASHER = 0x200, CLOSET = 0x400, HATCH = 0x800, HOOK = 0x1000, PLANK = 0x2000, WINDOW = 0x4000, CHEST = 0x8000, GENERATOR = 0x10000, EXITDOOR = 0x20000, TOTEM = 0x40000, ITEM = 0x80000, OBJECT_ID_END };
enum HOOKSTATE {
	Stand, SpiderStruggle_MIN, SpiderStruggle_MAX, SpiderStruggle2Sacrifice, SpiderStabOut, SpiderStabLoop, SpiderStabIN, SpiderReaction_OUT,
	SpiderReaction_Loop, SpiderReaction_IN, Spider_Struggle, Idle, HookedFree, Broken_Idle, BreakEnd, Struggle, GetHookedOut, GetHookedIn
};

enum SPIDERSTATE { Struggle_MIN, Struggle_MAX, StruggleToSacrifice, Stuggle, StabOut, StabLoop, StabIN, Reaction_Out, Reaction_Loop, Reaction_In, Invisible };
enum ITEMTYPE { I_NULL, MEDIKIT, TOOLBOX, FLASHLIGHT, KEY, ITEMEND };

typedef struct tagServerData
{
	CamperData	Campers[MAX_Client];
	SlasherData	Slasher;
	GameData	Game_Data;
}Server_Data;



//-------------------------------------------------------------------------------------
// IOCP 관련 소켓 정보 구조체
struct SOCKETINFO {
	OVERLAPPED overlapped;
	SOCKET sock;
	char buf[BUFSIZE + 1];
	bool Incoming_data;
	WSABUF wsabuf;
};

// 클라이언트 고유번호, 팀번호 구조체
typedef struct ClientData {
	int client_imei = -1;
	int Number = -1;
	int Character = -1;
}ClientData;

// IOCP 관련 구조체
typedef struct {
	//소켓정보를구조체화.
	SOCKET hClntSock;
	SOCKADDR_IN clntAddr;
	int client_imei = 0;
} PER_HANDLE_DATA, *LPPER_HANDLE_DATA;

typedef struct {
	// 소켓의버퍼정보를구조체화.
	OVERLAPPED overlapped;
	char buffer[BUFSIZE + 1];
	bool Incoming_data;
	WSABUF wsaBuf;
} PER_IO_DATA, *LPPER_IO_DATA;

//typedef struct tagClientData {
//	int client_imei;
//	bool team;
//}ClientData;