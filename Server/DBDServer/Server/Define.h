#pragma once



typedef unsigned char			_ubyte;
typedef signed char				_byte;

typedef unsigned short			_ushort;
typedef signed short			_short;

typedef unsigned int			_uint;
typedef signed int				_int;

typedef unsigned long			_ulong;
typedef signed long				_long;

typedef float					_float;
typedef double					_double;

typedef bool					_bool;

typedef wchar_t					_tchar;

typedef D3DXVECTOR2				_vec2;
typedef D3DXVECTOR3				_vec3;
typedef D3DXVECTOR4				_vec4;

typedef D3DXMATRIX				_matrix;

// Timer
LARGE_INTEGER			m_FrameTime;
LARGE_INTEGER			m_LastTime;
LARGE_INTEGER			m_FixTime;
LARGE_INTEGER			m_CpuTick;

// 네트워크 기본정보

#define SERVERPORT 10102
#define BUFSIZE   	10240

// IOCP 관련 모드 설정

#define Recv_Mode 	0
#define Send_Mode 	1


// CAMERA

#define CAMERA_NONE		0
#define CAMERA_FPS		1
#define CAMERA_TPS		2
#define	CAMERA_SCOPE	3

// OBJKEY

// 상태 관련
#define GSKILLCHECKFAIL				10
#define GSKILLCHECKGREATSUCCESS		11
#define SUCCESS_CHECK				12