#include "stdafx.h"
#include "..\Headers\Bell.h"
#include "Management.h"
#include "Defines.h"
#include "Slasher.h"
#include "MeshTexture.h"

_USING(Client)

CBell::CBell(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CBell::CBell(const CBell & rhs)
	: CGameObject(rhs)
{
}

HRESULT CBell::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CBell::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	D3DXMatrixIdentity(&m_matParent);
	m_pTransformCom->Set_Parent(&m_matParent);
	Approach_Slasher();

	//m_pTransformCom->Set_StateInfo(CT)
	_float fX = D3DXToRadian(90.f);
	_float fY = D3DXToRadian(0.f);
	_float fZ = D3DXToRadian(0.f);

	m_pTransformCom->SetUp_RotateXYZ(&fX, &fY, &fZ);
	return NOERROR;

	m_isColl = false;
}

_int CBell::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	SkillTime_Check();

	return _int();
}

_int CBell::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	if (server_data.Slasher.iSkill == 0)
		return 0;

	if (nullptr != m_pLHandMatrix)
		m_matParent = *m_pLHandMatrix * m_pSlasherTransform->Get_Matrix();

	if (SkillTime_Check())
		return 0;

	_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	D3DXVec3TransformCoord(&vPos, &vPos, &m_matParent);

	if (server_data.Slasher.iCharacter == 0 && m_bSkillUse)
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
			return -1;
	}
	else
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

		if (FAILED(m_pRendererCom->Add_StemGroup(this)))
			return -1;
	}

	return _int();
}

void CBell::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	_uint iPass = 9;
	if (m_bSkillUse)
		iPass = 7;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(iPass);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
			return;

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CBell::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(0);

	pEffect->SetVector("g_vLightPos", &vLightPos);
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matW");
	pEffect->SetMatrix("g_matVP", VP);

	pEffect->CommitChanges();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();
}

void CBell::Render_Stemp()
{
	LPD3DXEFFECT	pEffect = m_pRendererCom->GetCubeEffectHandle()->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(13);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();
	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(i);
		if (nullptr == pSubSet)
			return;

		m_pMeshCom->Render_Mesh(i);

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

_int CBell::Do_Coll(CGameObject * _pObj, const _vec3 & _vPos)
{
	return 0;
}

HRESULT CBell::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	// For.Com_Mesh
	m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Weapon_Bell");
	if (FAILED(Add_Component(L"Com_Mesh", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CBell::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);

	if (m_bSkillUse)
	{
		pEffect->SetFloat("g_fFloorHeight", m_fHeight);
		pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
		pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_SmokeTile.tga")));
		pEffect->SetFloat("g_fTimeAcc", m_fDissolveTime);
	}
	pEffect->SetBool("g_isFootPrint", false);
	return NOERROR;
}

void CBell::Approach_Slasher()
{
	GET_INSTANCE_MANAGEMENT;

	if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size() == 0)
	{
		Safe_Release(pManagement);
		return;
	}

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	m_pLHandMatrix = pSlasher->Get_LHandMatrix();
	m_pSlasherTransform = (CTransform*)pSlasher->Get_ComponentPointer(L"Com_Transform");

	Safe_Release(pManagement);
}

_bool CBell::SkillTime_Check()
{
	_bool bRend = false;

	GET_INSTANCE_MANAGEMENTR(false);

	if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size() == 0)
	{
		Safe_Release(pManagement);
		return false;
	}

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front();
	Safe_Release(pManagement);
	if (nullptr == pSlasher)
		return false;

	m_bSkillUse = pSlasher->Get_UseSkill();
	m_fDissolveTime = pSlasher->Get_DissolveTime();
	m_fHeight = pSlasher->Get_Height();

	if (pSlasher->Get_Stealth() && !m_bSkillUse)
		bRend = true;

	return bRend;
}

CBell * CBell::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CBell*	pInstance = new CBell(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CBell Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CBell::Clone_GameObject()
{
	CBell*	pInstance = new CBell(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CArt_Static Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CBell::Free()
{
	//CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_WEAPON, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);

	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
