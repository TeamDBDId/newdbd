#include "stdafx.h"
#include "Effect_Generator.h"
#include "Management.h"

#include "Effect_Smoke.h"
#include "Effect_Flash.h"

#include "Math_Manager.h"
_USING(Client)

CEffect_Generator::CEffect_Generator(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_Generator::CEffect_Generator(const CEffect_Generator & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Generator::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Generator::Ready_GameObject()
{

	return NOERROR;
}

_int CEffect_Generator::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;

	m_fTick += _fTick;
	if (2.5f < m_fTick)
		m_isDead = true;






	return _int();
}

_int CEffect_Generator::LastUpdate_GameObject(const _float & _fTick)
{



	return _int();
}

void CEffect_Generator::Render_GameObject()
{


}

void CEffect_Generator::Set_Param(const _vec3 & _vPos)
{
	m_vPos = _vPos;

	Make_Smoke();
	Make_Flash();
}





void CEffect_Generator::Make_Smoke()
{
	GET_INSTANCE_MANAGEMENT;

	for (size_t i = 0; i < SMOKE_CNT; ++i)
	{
		CEffect_Smoke* pEffect = nullptr;
		if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_Smoke", SCENE_STAGE, L"Layer_ESmoke", (CGameObject**)&pEffect)))
			_MSG_BOX("Fail Make_Smoke");
		pEffect->Set_Param(m_vPos);
	}

	Safe_Release(pManagement);
}

void CEffect_Generator::Make_Flash()
{

	GET_INSTANCE_MANAGEMENT;
	
	CEffect_Flash* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_Flash", SCENE_STAGE, L"Layer_EFlash", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Flash");
	pEffect->Set_Param(m_vPos);
	

	Safe_Release(pManagement);

}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Generator * CEffect_Generator::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Generator*	pInst = new CEffect_Generator(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Generator::Clone_GameObject()
{
	CEffect_Generator*	pInst = new CEffect_Generator(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_Generator Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Generator::Free()
{

	CGameObject::Free();
}