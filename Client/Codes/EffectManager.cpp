#include "stdafx.h"
#include "EffectManager.h"
#include "Management.h"

#include "Effect_Generator.h"
#include "Effect_Smoke.h"
#include "Effect_Flash.h"

#include "Effect_Fire.h"
#include "Effect_FireCore.h"
#include "Effect_FireSmoke.h"

#include "Effect_Mist.h"

#include "Effect_Fail.h"
#include "Effect_Exit.h"
#include "Effect_Leaf.h"

#include "Effect_PlayerChange.h"
#include "Effect_SmokeBall.h"

#include "Effect_ExitOpen.h"
#include "Effect_Particle.h"

#include "Effect_WindowJump.h"

#include "Effect_Death.h"
#include "Effect_DeathSmoke.h"

#include "Effect_BG.h"

#include "Effect_FootPrint.h"
#include "Effect_Blood.h"

#include "Effect_BloodUI.h"
#include "Effect_BloodAnim.h"

#include "Effect_BloodSpread.h"
#include "Effect_BloodBall.h"

#include "Effect_Hook.h"

#include "Effect_HookBG.h"
#include "Effect_HookParticle.h"
#include "Effect_HookDust.h"

#include "Math_Manager.h"

_USING(Client)

_IMPLEMENT_SINGLETON(CEffectManager)

CEffectManager::CEffectManager()
{
	m_pManagement = CManagement::GetInstance();
	m_pManagement->AddRef();
}


HRESULT CEffectManager::Ready_Effect(LPDIRECT3DDEVICE9 _pGDevice)
{
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Effect", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Shader_Effect.fx"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_FootPrint", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Shader_FootPrint.fx"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Blood", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Shader_Blood.fx"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_HookEff", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Shader_HookEff.fx"))))
		return E_FAIL;

	//���ʷ�����
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Generator", CEffect_Generator::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Smoke", CEffect_Smoke::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Flash", CEffect_Flash::Create(_pGDevice))))
		return E_FAIL;
	//�̽�Ʈ
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Mist", CEffect_Mist::Create(_pGDevice))))
		return E_FAIL;
	//��ں�
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Fire", CEffect_Fire::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_FireCore", CEffect_FireCore::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_FireSmoke", CEffect_FireSmoke::Create(_pGDevice))))
		return E_FAIL;
	//exit
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Exit", CEffect_Exit::Create(_pGDevice))))
		return E_FAIL;
	//����
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Fail", CEffect_Fail::Create(_pGDevice))))
		return E_FAIL;
	//����
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Leaf", CEffect_Leaf::Create(_pGDevice))))
		return E_FAIL;
	//�÷��̾�ü����
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_PlayerChange", CEffect_PlayerChange::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_SmokeBall", CEffect_SmokeBall::Create(_pGDevice))))
		return E_FAIL;
	//exitOpen
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_ExitOpen", CEffect_ExitOpen::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Particle", CEffect_Particle::Create(_pGDevice))))
		return E_FAIL;

	//jump
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_WindowJump", CEffect_WindowJump::Create(_pGDevice))))
		return E_FAIL;

	//Death
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Death", CEffect_Death::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_DeathSmoke", CEffect_DeathSmoke::Create(_pGDevice))))
		return E_FAIL;

	//BG
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_BG", CEffect_BG::Create(_pGDevice))))
		return E_FAIL;
	// For.GameObject_FootPrint
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_FootPrint", CEffect_FootPrint::Create(_pGDevice))))
		return E_FAIL;

	//�ٴ���
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Blood", CEffect_Blood::Create(_pGDevice))))
		return E_FAIL;

	//ui��
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_BloodUI", CEffect_BloodUI::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_BloodAnim", CEffect_BloodAnim::Create(_pGDevice))))
		return E_FAIL;



	//�ѷ�������
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_BloodSpread", CEffect_BloodSpread::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_BloodBall", CEffect_BloodBall::Create(_pGDevice))))
		return E_FAIL;

	//��
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_Hook", CEffect_Hook::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_HookBG", CEffect_HookBG::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_HookParticle", CEffect_HookParticle::Create(_pGDevice))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_GameObject(L"Obj_Effect_HookDust", CEffect_HookDust::Create(_pGDevice))))
		return E_FAIL;

	//���ؽ���
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Hook_BG0", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Hook/Hook_BG0.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Hook_BG1", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Hook/Hook_BG1.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Hook_Spark", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Hook/Hook_Spark.tga"))))
		return E_FAIL;

	//uvTest
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_uvTest", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Mist/uvTest.png"))))
		return E_FAIL;

	//�̽�Ʈ
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Mist_SubUV", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Mist/Mist_SubUV.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Mist_Cloud", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Mist/Mist_Cloud.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Mist_Noise", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Mist/Mist_Noise.tga"))))
		return E_FAIL;

	//���ʷ�����
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Generator_Flash", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Generator/Generator_Flash.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Generator_Flash_Grad", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Generator/Generator_Flash_Grad.png"))))
		return E_FAIL;


	//��ں�
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fire_Noise", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fire/Fire_Noise.png"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fire_SubUV", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fire/Fire_SubUV%d.png",28))))
		return E_FAIL;
	
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Cone", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Cone.x"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Ribbon0", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Ribbon0.x"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Ribbon1", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Ribbon1.x"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_Ribbon2", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Fire/", L"Fire_Ribbon2.x"))))
		return E_FAIL;

	//��������Ʈ
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fail_TendrilMask", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fail/TendrilMask.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fail_Gradiant", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fail/Gradiant.png"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fail_Radical", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fail/Radical.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Fail_Noise", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Fail/Noise.tga"))))
		return E_FAIL;

	//exit����Ʈ
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_BG_Smoke", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Exit/Exit_Smoke/Exit_Smoke%d.png",30))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Exit_Switch", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Exit/Exit_Switch.png"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Exit_Round", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Exit/Exit_Round.tga"))))
		return E_FAIL;

	//Leaf����Ʈ
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Leaf", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Leaf/Leaf.tga"))))
		return E_FAIL;

	//PlayerChange����Ʈ
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_SmokeBall", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/PlayerChange/SmokeBall.tga"))))
		return E_FAIL;

	//ExitOpen
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Particle", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Particle/Particle.tga"))))
		return E_FAIL;

	//Death
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_SmokeParticle", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Death/SmokeParticle.tga"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_DeathGradient", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Death/DeathGradient.tga"))))
		return E_FAIL;

	//��
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_Blood", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/Blood/Blood%d.tga",5))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_BloodAnim", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/BloodAnim/Blood_Anim%d.tga", 6))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Tex_BloodBall", CTexture::Create(_pGDevice, CTexture::TYPE_GENERAL, L"../Bin/Resources/Textures/Effect/BloodBall/Blood_Ball.tga"))))
		return E_FAIL;

	//���ʷ����͵�������
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_GenOn", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Generator/", L"Generator_OutLine.x"))))
		return E_FAIL;
	if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STAGE, L"Com_Mesh_HookOn", CMesh_Static::Create(_pGDevice, L"../Bin/Resources/Meshes/EffectMesh/Hook/", L"Hook_OutLine.x"))))
		return E_FAIL;

	

	//���̴�����
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Mist", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Mist.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Fire", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Fire.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Generator", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Generator.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Fail", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Fail.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Exit", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Exit.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Leaf", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Leaf.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_PlayerChange", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_PlayerChange.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Particle", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Particle.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Jump", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Jump.fx"))))
	//	return E_FAIL;
	//if (FAILED(m_pManagement->Add_Prototype_Component(SCENE_STATIC, L"Component_Shader_Death", CShader::Create(_pGDevice, L"../Bin/ShaderFiles/Effect/Shader_Death.fx"))))
	//	return E_FAIL;

	return NOERROR;
}

HRESULT CEffectManager::Ready_PlayerPos(const _vec3 * _pPos)
{
	if(nullptr == m_pPos)
		m_pPos = _pPos;
	

	return NOERROR;
}

void CEffectManager::Make_Effect(const eEffect& _eEffect, const _vec3 & _vPos, const _uint& _iType)
{
	switch (_eEffect)
	{
	case E_Fire:
		if (_vec3(0.f, 0.f, 0.f) == _vPos)
			_MSG_BOX("Fail Effect_Fire Pos")
		else
			Make_Fire(_vPos + _vec3(0.f, -30.f, 0.f), _iType);
		break;
	case E_Generator:
		if (_vec3(0.f, 0.f, 0.f) == _vPos)
			_MSG_BOX("Fail Effect_Generator Pos")
		else
			Make_Generator(_vPos);
		break;
	case E_Fail:
		Make_Fail(_vPos);
		break;
	case E_Exit:
		Make_Exit(_vPos);
		break;
	case E_Leaf:
		Make_Leaf(_vPos);
		break;
	case E_Change:
		Make_Change(_vPos);
		break;
	case E_ExitOpen:
		Make_ExitOpen(_vPos);
		break;
	case E_Jump:
		Make_Jump(_vPos);
		break;
	case E_Death:
		Make_Death(_vPos);
		break;
	//case E_GeneratorOn:
	//	Make_GeneratorOn(_vPos);
	//	break;
	case E_FootPrint:
		Make_FootPrint(_vPos);
		break;
	case E_Blood:
		Make_Blood(_vPos);
		break;
	case E_BloodUI:
		Make_BloodUI();
		break;
	case E_HookDeath:
		Make_Hook(_vPos);
		break;
	}


}

void CEffectManager::Make_Fire(const _vec3 & _vPos, const _uint& _fSize)
{
	

	CEffect_Fire* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Fire", SCENE_STAGE, L"Layer_EFire", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Fire");
	pEffect->Set_Param(_vPos, _fSize);

}


void CEffectManager::Make_Mist(const _float& _fTime)
{
	if (nullptr == m_pPos)
		return;

	m_fMist += _fTime;
	if (m_fMist < 0.1f)
		return;
	m_fMist = 0.f;


	_vec3 vPos = *m_pPos;
	vPos.x += Math_Manager::CalRandIntFromTo(-2000, 2000);
	vPos.z += Math_Manager::CalRandIntFromTo(-2000, 2000);
	vPos.y = 0.f;

	if (vPos.x < 1000.f || 10000.f < vPos.x)
		return;
	if (vPos.z < 1000.f || 10000.f < vPos.z)
		return;



	CEffect_Mist* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Mist", SCENE_STAGE, L"Layer_EMist", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Mist");
	pEffect->Set_Param(vPos);
}

void CEffectManager::Make_BG(const eEffect& _eEffect, const _matrix & _matWorld, void* _pCamper)
{
	_vec3 vPos = *(_vec3*)&_matWorld._41;
	vPos.y += 250.f;

	CEffect_BG* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_BG", SCENE_STAGE, L"Layer_BG", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_BG");
	pEffect->Set_Kind(CEffect_BG::eKind(_eEffect-12), _pCamper);
	pEffect->Set_Param(vPos,&_matWorld);
}

void CEffectManager::Make_BloodSpread(const _vec3 & _vPos, const _vec3 & _vDir)
{
	CEffect_BloodSpread* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_BloodSpread", SCENE_STAGE, L"Layer_BloodSpread", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_BloodSpread");
	pEffect->Set_Param(_vPos,_vDir);

}

void CEffectManager::Make_Generator(const _vec3 & _vPos)
{
	_vec3 vPos = _vPos;
	vPos.y += 100.f;

	CEffect_Generator* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Generator", SCENE_STAGE, L"Layer_EGenerator", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Generator");
	pEffect->Set_Param(vPos);

}

void CEffectManager::Make_Fail(const _vec3 & _vPos)
{

	_vec3 vPos = _vPos;
	vPos.y += 50.f;

	CEffect_Fail* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Fail", SCENE_STAGE, L"Layer_EFail", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Fail");
	pEffect->Set_Param(vPos);

}

void CEffectManager::Make_Exit(const _vec3 & _vPos)
{
	_vec3 vPos = _vPos;
	vPos.y += 50.f;

	CEffect_Exit* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Exit", SCENE_STAGE, L"Layer_EExit", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Exit");
	pEffect->Set_Param(vPos);
}

void CEffectManager::Make_ExitOpen(const _vec3 & _vPos)
{

	CEffect_ExitOpen* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_ExitOpen", SCENE_STAGE, L"Layer_EExitOpen", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Open");
	//pEffect->Set_Param(&_vPos, m_pPos);
	pEffect->Set_Param(&_vPos);
}

void CEffectManager::Make_Leaf(const _vec3 & _vPos)
{
	_vec3 vPos = _vPos;
	vPos.y += 250.f;

	CEffect_Leaf* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Leaf", SCENE_STAGE, L"Layer_ELeaf", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Leaf");
	pEffect->Set_Param(vPos);

}

void CEffectManager::Make_Change(const _vec3 & _vPos)
{
	_vec3 vPos = _vPos;
	vPos.y += 50.f;

	CEffect_PlayerChange* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_PlayerChange", SCENE_STAGE, L"Layer_EPlayerChange", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Change");
	pEffect->Set_Param(vPos);

}

void CEffectManager::Make_Jump(const _vec3 & _vPos)
{
	//_vec3 vPos = _vPos;
	//vPos.y += 200.f;

	CEffect_WindowJump* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_WindowJump", SCENE_STAGE, L"Layer_EWindowJump", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Jump");
	pEffect->Set_Param(_vPos  );

}

void CEffectManager::Make_Death(const _vec3 & _vPos)
{
	_vec3 vPos = _vPos;
	vPos.y += 800.f;

	CEffect_Death* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Death", SCENE_STAGE, L"Layer_EDeath", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Death");
	pEffect->Set_Param(vPos);
}



void CEffectManager::Make_FootPrint(const _vec3 & _vPos)
{
	CEffect_FootPrint* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_FootPrint", SCENE_STAGE, L"Layer_FootPrint", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_FootPrint");
	pEffect->Set_Param(_vPos);

}

void CEffectManager::Make_Blood(const _vec3 & _vPos)
{
	CEffect_Blood* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Blood", SCENE_STAGE, L"Layer_Blood", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Blood");
	pEffect->Set_Param(_vPos);
}

void CEffectManager::Make_BloodUI()
{

	CEffect_BloodUI* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_BloodUI", SCENE_STAGE, L"Layer_BloodUI", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_BloodUI");

}

void CEffectManager::Make_Hook(const _vec3 & _vPos)
{
	CEffect_Hook* pEffect = nullptr;
	if (FAILED(m_pManagement->Add_GameObjectToLayer(L"Obj_Effect_Hook", SCENE_STAGE, L"Layer_EHook", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail Make_Hook");
	pEffect->Set_Param(_vPos);
}




void CEffectManager::Free()
{
	Safe_Release(m_pManagement);
}

