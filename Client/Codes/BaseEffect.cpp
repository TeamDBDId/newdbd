#include "stdafx.h"
#include "BaseEffect.h"
#include "Management.h"

#include "Target_Manager.h"
#include "Math_Manager.h"

_USING(Client)

CBaseEffect::CBaseEffect(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CBaseEffect::CBaseEffect(const CBaseEffect & _rhs)
	: CGameObject(_rhs)
{

}


HRESULT CBaseEffect::Ready_Prototype()
{

	return NOERROR;
}


HRESULT CBaseEffect::Ready_GameObject()
{



	return NOERROR;
}

_int CBaseEffect::Update_GameObject(const _float & _fTick)
{
	return _int();
}

_int CBaseEffect::LastUpdate_GameObject(const _float & _fTick)
{
	return _int();
}

void CBaseEffect::Render_GameObject()
{
	
}



void CBaseEffect::PlaySound(const _tchar * pLayerTag)
{
}




HRESULT CBaseEffect::Ready_Component(const _tchar* _shaderTag)
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);



	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STATIC, _shaderTag);
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;


	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CBaseEffect::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	_matrix			matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	_pEffect->SetMatrix("g_matView", &matView);

	_matrix			matProj;
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);
	_pEffect->SetMatrix("g_matProj", &matProj);


	_matrix			matVInv;
	D3DXMatrixInverse(&matVInv, nullptr, &matView);
	_pEffect->SetMatrix("g_matVInv", &matVInv);


	CTarget_Manager::GetInstance()->SetUp_OnShader(_pEffect, L"Target_Depth", "g_DepthTexture");

	return NOERROR;
}



void CBaseEffect::Free()
{
	
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);


	CGameObject::Free();
}
