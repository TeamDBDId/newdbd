#include "stdafx.h"
#include "Effect_BloodSpread.h"
#include "Management.h"

#include "Effect_Hook.h"

#include "Effect_HookBG.h"
#include "Effect_HookDust.h"
#include "Effect_HookParticle.h"

#include "Math_Manager.h"
_USING(Client)

CEffect_Hook::CEffect_Hook(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_Hook::CEffect_Hook(const CEffect_Hook & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Hook::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Hook::Ready_GameObject()
{

	return NOERROR;
}

_int CEffect_Hook::Update_GameObject(const _float & _fTick)
{
	if (true == m_isDead)
		return DEAD_OBJ;

	m_fLifeTime += _fTick;
	if (10.f < m_fLifeTime)
		m_isDead = true;


	Make_BG(_fTick);
	Make_Paticle(_fTick);
	Make_EX(_fTick);

	return _int();
}

_int CEffect_Hook::LastUpdate_GameObject(const _float & _fTick)
{

	return _int();
}

void CEffect_Hook::Render_GameObject()
{

}

void CEffect_Hook::Set_Param(const _vec3 & _vPos)
{
	m_vPos = _vPos;
}

void CEffect_Hook::Make_BG(const _float & _fTick)
{
	if (true == m_isBGInit)
		return;

	if (m_fLifeTime < BGINIT)
		return;

	m_isBGInit = true;
	

	GET_INSTANCE_MANAGEMENT;

	CEffect_HookBG* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_HookBG", SCENE_STAGE, L"Layer_HookBG", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_HookBG");
	pEffect->Set_Param(m_vPos);

	Safe_Release(pManagement);
}

void CEffect_Hook::Make_Paticle(const _float & _fTick)
{
	if (m_fLifeTime < PARICLESTART)
		return;

	m_fParticle += _fTick;
	if (m_fParticle < PARTICLEDELAY)
		return;
	m_fParticle = 0.f;




	GET_INSTANCE_MANAGEMENT;

	CEffect_HookParticle* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_HookParticle", SCENE_STAGE, L"Layer_HookParticle", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_HookParticle");
	pEffect->Set_Param(m_vPos);

	Safe_Release(pManagement);



}

void CEffect_Hook::Make_EX(const _float & _fTick)
{

	if (true == m_isEXInit)
		return;

	if (m_fLifeTime < EXINIT)
		return;

	m_isEXInit = true;
	cout << "dd" << endl;

	GET_INSTANCE_MANAGEMENT;

	CEffect_HookParticle* pEffect = nullptr;

	for (size_t i = 0; i < 100; ++i)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_HookParticle", SCENE_STAGE, L"Layer_HookParticle", (CGameObject**)&pEffect)))
			_MSG_BOX("Fail CEffect_HookParticle");
		pEffect->Set_Param2(m_vPos);
	}

	Make_Dust(_fTick);

	Safe_Release(pManagement);

}


void CEffect_Hook::Make_Dust(const _float & _fTick)
{

	GET_INSTANCE_MANAGEMENT;

	CEffect_HookDust* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_HookDust", SCENE_STAGE, L"Layer_Effect_HookDust", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_HookDust");
	pEffect->Set_Param(m_vPos, nullptr);

	Safe_Release(pManagement);

}



// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Hook * CEffect_Hook::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Hook*	pInst = new CEffect_Hook(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Hook::Clone_GameObject()
{
	CEffect_Hook*	pInst = new CEffect_Hook(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_Hook Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Hook::Free()
{

	CGameObject::Free();
}