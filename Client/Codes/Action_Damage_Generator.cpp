#include "stdafx.h"
#include "..\Headers\Action_Damage_Generator.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Slasher.h"
#include "Generator.h"

_USING(Client)

CAction_Damage_Generator::CAction_Damage_Generator()
{
}

HRESULT CAction_Damage_Generator::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	m_fIndex = 0.f;

	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	m_bIsPlaying = true;
	m_bIsFinished = false;
	pSlasher->IsLockKey(true);

	pSlasher->Set_State(AS::Damage_Generator);
	
	return NOERROR;
}

_int CAction_Damage_Generator::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (!KEYMGR->KeyPressing(DIK_SPACE))
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_State(AS::Idle);
		return END_ACTION;
	}

	if (pMeshCom->IsOverTime(0.3f))
	{
		m_bIsFinished = true;
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_State(AS::Idle);
	}

	if (m_fIndex > 81.f)
		return END_ACTION;

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_Damage_Generator::End_Action()
{
	m_bIsPlaying = false;
	CSlasher* pSlasher = (CSlasher*)m_pGameObject;
	pSlasher->IsLockKey(false);
	if (m_bIsFinished)
	{
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Brutality, wstring(L"발전기 손상"), 100);
		m_pGenerator->Set_CurProgressTimeForSlasher(m_pGenerator->Get_ProgressTime());
	}
	m_pGenerator = nullptr;
	Send_ServerData();
}

void CAction_Damage_Generator::Send_ServerData()
{
	if (m_pGenerator != nullptr)
		slasher_data.InterationObject = m_pGenerator->GetID();
	else
		slasher_data.InterationObject = 0;
}

void CAction_Damage_Generator::Free()
{
	CAction::Free();
}
