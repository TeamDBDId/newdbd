#include "stdafx.h"
#include "..\Headers\Action_Generator.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Generator.h"
#include "Item.h"

_USING(Client)

CAction_Generator::CAction_Generator()
{
}

HRESULT CAction_Generator::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->Set_State(AC::GeneratorActivation);
	m_iState = AC::GeneratorActivation;
	m_fRepairTime = 0.f;
	m_fCoopTime = 0.f;
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Generator);
	GET_INSTANCE(CUIManager)->Set_SkillCheckState(0);
	return NOERROR;
}

_int CAction_Generator::Update_Action(const _float & fTimeDelta)
{
	//if (m_fFailCoolTime > 0.01f)
	//{
	//	m_fFailCoolTime -= fTimeDelta;
	//	return END_ACTION;
	//}
	//else
	//	m_fFailCoolTime = 0.00f;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	if (nullptr == pCamper)
		return END_ACTION;

	if (nullptr != pCamper->GetCurItem() && pCamper->GetCurItem()->Get_ItemType() == CItem::TOOLBOX)
	{
		if(KEYMGR->MousePressing(1))
			pCamper->GetCurItem()->Use_Item(fTimeDelta, nullptr, nullptr);
	}

	if (m_pGenerator->Get_ProgressTime() > m_pGenerator->Get_MaxProgressTime())
	{
		CTransform* pGenerTrans = (CTransform*)m_pGenerator->Get_ComponentPointer(L"Com_Transform");
		EFFMGR->Make_BG(CEffectManager::E_BG_GENON,pGenerTrans->Get_Matrix());
		return END_ACTION;
	}
		
	m_fRepairTime += fTimeDelta;
	Coop_Check(fTimeDelta);
	
	_int SkillCheck = GET_INSTANCE(CUIManager)->Get_SkillCheckState();

	if (SkillCheck == FAILED_SKILL_CHECK)
	{
		camper_data.Packet = FAILED_SKILL_CHECK;   // 실패
		pCamper->Set_State(AC::GeneratorFail);
		m_iState = AC::GeneratorFail;
		//CTransform* pGenerTrans = (CTransform*)m_pGenerator->Get_ComponentPointer(L"Com_Transform");
		//EFFMGR->Make_Effect(CEffectManager::E_Generator, *pGenerTrans->Get_StateInfo(CTransform::STATE_POSITION));
	}
	else if (SkillCheck == SUCCESS_SKILL_CHECK)
	{
		camper_data.Packet = SUCCESS_SKILL_CHECK; // 대성공
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Objective, L"스킬 체크(훌륭함)", 150);
	}
	else if (SkillCheck == SUCCESS_CHECK)
	{
		camper_data.Packet = SUCCESS_CHECK;
		GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Objective, L"스킬 체크(좋음)", 50);
		if (server_data.Game_Data.Curse & CRUIN)
		{
			CTransform* pGenerTrans = (CTransform*)m_pGenerator->Get_ComponentPointer(L"Com_Transform");
			EFFMGR->Make_Effect(CEffectManager::E_Fail, *pGenerTrans->Get_StateInfo(CTransform::STATE_POSITION));
		}
			
	}
		
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	if (m_iState == AC::GeneratorFail && pMeshCom->IsOverTime(0.3f))
	{
		pCamper->Set_State(AC::GeneratorActivation);
		m_iState = AC::GeneratorActivation;
	}

	if (!KEYMGR->MousePressing(0) && !KEYMGR->MousePressing(1))
		return END_ACTION;

	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_Generator::End_Action()
{
	_int SkillCheckState = GET_INSTANCE(CUIManager)->Get_SkillCheckState();
	if ((SkillCheckState == 1 || SkillCheckState == 2) && m_pGenerator->Get_ProgressTime() < 80.f)
	{
		camper_data.Packet = FAILED_SKILL_CHECK;
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->Set_State(AC::GeneratorFail);
		m_iState = AC::GeneratorFail;
		CTransform* pGenerTrans = (CTransform*)m_pGenerator->Get_ComponentPointer(L"Com_Transform");
		EFFMGR->Make_Effect(CEffectManager::E_Generator, *pGenerTrans->Get_StateInfo(CTransform::STATE_POSITION));
	}
	GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Objective, L"협동", _int(m_fCoopTime * 20.f));
	GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Objective, L"수리", _int(m_fRepairTime * 15.625f));
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->IsLockKey(false);
	m_pGenerator = nullptr;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	Send_ServerData();
}

void CAction_Generator::Send_ServerData()
{
	if (m_pGenerator != nullptr)
	{
		camper_data.InterationObject = m_pGenerator->GetID();
		camper_data.InterationObjAnimation = m_pGenerator->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_Generator::Coop_Check(const _float & fTimeDelta)
{
	_int iCount = 0;
	_uint GeneratorID = m_pGenerator->GetID();
	for (_int i = 0; i < 4; i++)
	{
		if (server_data.Campers[i].InterationObject == GeneratorID)
			iCount++;
	}

	if (iCount <= 1)
		return;
	m_fCoopTime += fTimeDelta *(iCount - 1);
}

void CAction_Generator::Free()
{
	CAction::Free();
}
