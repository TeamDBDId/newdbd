#include "stdafx.h"
#include "..\Headers\Action_UnhideCloset.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Closet.h"

_USING(Client)

CAction_UnhideCloset::CAction_UnhideCloset()
{
}

HRESULT CAction_UnhideCloset::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	m_bIsPlaying = true;
	m_fIndex = 0.f;

	if (!m_bIsInit)
		SetVectorPos();

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	if (KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		pCamper->Set_State(AC::ClosetUnHideFast);
		m_iState = AC::ClosetUnHideFast;
		if (m_pCloset != nullptr)
			m_pCloset->SetState(CCloset::DoorUnhideFast);

		if (server_data.Campers[exPlayerNumber - 1].Perk & PALOMA && pCamper->Get_PerkTime(L"PALOMA") <= 0.01f)
		{
			_vec3 vMyPos = server_data.Campers[exPlayerNumber - 1].vPos;
			_vec3 vSlasherPos = server_data.Slasher.vPos;		

			_vec3 vDirection = vSlasherPos - vMyPos;
			_float fDist = D3DXVec3Length(&vDirection);
			if (fDist < 150.f)
			{
				_vec3 vMyLook = server_data.Campers[exPlayerNumber - 1].vLook;
				D3DXVec3Normalize(&vDirection, &vDirection);
				_float fAngle = acosf(vDirection * vMyLook);
				if (fAngle <= D3DXToRadian(60.f) && pCamper->Get_PerkTime(L"INCLOSET") <= 0.01f)
				{
					camper_data.SecondInterationObject = SLASHER;
					camper_data.SecondInterationObjAnimation = XXPALOMA;
					pCamper->Set_PerkTime(L"PALOMA", 40.f);
				}
			}
		}
	}
	else
	{
		pCamper->Set_State(AC::ClosetUnHide);
		m_iState = AC::ClosetUnHide;
		if (m_pCloset != nullptr)
			m_pCloset->SetState(CCloset::DoorUnhide);
	}

	Send_ServerData();
	return NOERROR;
}

_int CAction_UnhideCloset::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;

	size_t iIndex = (size_t)m_fIndex;

	if (m_pCloset == nullptr)
		return END_ACTION;

	if (m_iState == AC::ClosetUnHide)
	{
		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.65f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::Idle);
			m_pCloset->SetState(CCloset::DoorIdleClosed);
		}

		if (m_vecPos.size() <= iIndex)
			return END_ACTION;

		if (m_fIndex >= 76.f)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	}
	else
	{
		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.4f))
		{
			CCamper* pCamper = (CCamper*)m_pGameObject;
			pCamper->Set_State(AC::Idle);
			m_pCloset->SetState(CCloset::DoorIdleClosed);
		}

		if (m_vecPosFast.size() <= iIndex)
			return END_ACTION;

		if (m_fIndex >= 26.f)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPosFast[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}

  	Send_ServerData();
	return UPDATE_ACTION;
}

void CAction_UnhideCloset::End_Action()
{
	m_bIsPlaying = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->SetColl(true);
	pCamper->IsLockKey(false);
	m_pCloset = nullptr;
	Send_ServerData();
}

void CAction_UnhideCloset::Send_ServerData()
{
	if (m_pCloset != nullptr)
	{
		camper_data.InterationObject = m_pCloset->GetID();
		camper_data.InterationObjAnimation = m_pCloset->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_UnhideCloset::SetVectorPos()
{
	m_vecPos.reserve(76);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.435f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 1.697f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 3.724f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 6.45f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 9.813f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 13.749f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 18.194f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 23.084f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 28.357f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 33.947f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 39.793f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 45.829f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 51.992f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 58.22f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 64.447f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 70.61f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 76.647f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 82.492f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 88.083f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 93.355f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 98.246f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 102.691f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 106.026f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 109.989f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 112.716f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 114.742f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.004f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 116.439f));

	m_vecPosFast.reserve(26);

	m_vecPosFast.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 1.549f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 5.906f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 12.64f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 21.319f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 31.51f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 42.782f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 54.702f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 66.838f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 78.757f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 90.629f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 100.22f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 108.899f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 115.633f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 119.991f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 121.539f));

	m_bIsInit = true;
}

void CAction_UnhideCloset::Free()
{
	CAction::Free();
}
