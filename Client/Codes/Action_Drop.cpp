#include "stdafx.h"
#include "..\Headers\Action_Drop.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client)

CAction_Drop::CAction_Drop()
{
}

HRESULT CAction_Drop::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	m_fIndex = 0.f;
	if (m_pGameObject->GetID() & SLASHER)
	{
		m_pSlasher = (CSlasher*)m_pGameObject;
		m_pSlasher->IsLockKey(true);
		m_pSlasher->Set_Carry(false);
		m_pSlasher->Set_State(AS::Drop);
		m_pCamper = (CCamper*)m_pSlasher->Get_CarriedCamper();
		m_pSlasher->Set_CarriedCamper(nullptr);
	}
	else
	{
		m_pCamper = (CCamper*)m_pGameObject;
		if (server_data.Slasher.iCharacter == 0)
			m_pCamper->Set_State(AC::TT_Drop);
		else
			m_pCamper->Set_State(AC::WI_Drop);
		m_pCamper->IsLockKey(true);
		m_pCamper->SetCarry(false);
		GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_WiggleEnd); //내릴때
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None); //내릴때  
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
	}
	m_bIsPlaying = true;

	Send_ServerData();
	return NOERROR;
}

_int CAction_Drop::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	m_fIndex += fTimeDelta * 30.f;
	size_t iIndex = (size_t)m_fIndex;

	if (m_pSlasher == nullptr)
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			m_pCamper->Set_State(AC::CrawlFT);
			m_pCamper->SetCurCondition(CCamper::DYING);
			return END_ACTION;
		}

		if (m_fIndex >= 68.f)
			return END_ACTION;

		if (m_vecPos.size() <= iIndex)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}
	else
	{
		if (pMeshCom->IsOverTime(0.3f))
			return END_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_Drop::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;
	slasher_data.SecondInterationObject = 0;
	if (m_pSlasher == nullptr)
	{
		m_pCamper->SetCarry(false);
		m_pCamper->IsLockKey(false);
		m_pCamper->SetColl(true);
		m_pCamper->Set_SlasherColl(true);
		m_pCamper->SetCurCondition(CCamper::DYING);
		m_pCamper->SetEnergy(m_pCamper->GetDyingEnergy());
	}
	else
	{
		m_pSlasher->IsLockKey(false);
		m_pCamper->SetColl(true);
	}

	m_pCamper = nullptr;
	m_pSlasher = nullptr;
}

void CAction_Drop::Send_ServerData()
{
	if (nullptr == m_pCamper)
		return;

	slasher_data.SecondInterationObject = m_pCamper->GetID();
	slasher_data.SecondInterationObjAnimation = DROP;
}

void CAction_Drop::SetVectorPos()
{
	m_vecPos.reserve(68);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(2.268f, 0.f, 0.254f));
	m_vecPos.push_back(_vec3(4.535f, 0.f, 0.507f));
	m_vecPos.push_back(_vec3(6.803f, 0.f, 0.761f));
	m_vecPos.push_back(_vec3(9.071f, 0.f, 1.015f));
	m_vecPos.push_back(_vec3(11.338f, 0.f, 1.269f));
	m_vecPos.push_back(_vec3(13.666f, 0.f, 1.522f));
	m_vecPos.push_back(_vec3(15.874f, 0.f, 1.776f));
	m_vecPos.push_back(_vec3(18.141f, 0.f, 2.03f));
	m_vecPos.push_back(_vec3(20.409f, 0.f, 2.283f));
	m_vecPos.push_back(_vec3(22.677f, 0.f, 2.537f));
	m_vecPos.push_back(_vec3(29.944f, 0.f, 2.791f));
	m_vecPos.push_back(_vec3(27.212f, 0.f, 3.045f));
	m_vecPos.push_back(_vec3(29.48f, 0.f, 3.298f));
	m_vecPos.push_back(_vec3(31.747f, 0.f, 3.552f));
	m_vecPos.push_back(_vec3(34.015f, 0.f, 3.806f));
	m_vecPos.push_back(_vec3(36.283f, 0.f, 4.06f));
	m_vecPos.push_back(_vec3(38.55f, 0.f, 4.313f));
	m_vecPos.push_back(_vec3(40.818f, 0.f, 4.567f));
	m_vecPos.push_back(_vec3(43.086f, 0.f, 4.821f));
	m_vecPos.push_back(_vec3(45.353f, 0.f, 5.074f));
	m_vecPos.push_back(_vec3(47.621f, 0.f, 5.328f));
	m_vecPos.push_back(_vec3(49.889f, 0.f, 5.582f));
	m_vecPos.push_back(_vec3(52.156f, 0.f, 5.836f));
	m_vecPos.push_back(_vec3(54.424f, 0.f, 6.089f));
	m_vecPos.push_back(_vec3(56.692f, 0.f, 6.343f));
	m_vecPos.push_back(_vec3(58.059f, 0.f, 6.597f));
	m_vecPos.push_back(_vec3(61.227f, 0.f, 6.85f));
	m_vecPos.push_back(_vec3(63.495f, 0.f, 7.104f));
	m_vecPos.push_back(_vec3(65.762f, 0.f, 7.358f));
	m_vecPos.push_back(_vec3(68.03f, 0.f, 7.612f));
	m_vecPos.push_back(_vec3(70.298f, 0.f, 7.865f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));
	m_vecPos.push_back(_vec3(72.565f, 0.f, 8.119f));

	m_bIsInit = true;
}

void CAction_Drop::Free()
{
	CAction::Free();
}
