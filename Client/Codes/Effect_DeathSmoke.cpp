#include "stdafx.h"
#include "Effect_DeathSmoke.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_DeathSmoke::CEffect_DeathSmoke(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_DeathSmoke::CEffect_DeathSmoke(const CEffect_DeathSmoke & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_DeathSmoke::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_DeathSmoke::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;



	m_fRad = Math_Manager::CalRandFloatFromTo(0.f, 6.28f);
	m_fSize = 200.f;

	return NOERROR;
}

_int CEffect_DeathSmoke::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (2.f < m_fTime)
		m_isDead = true;


	m_pTransformCom->Move_V3(m_vDir*_fTick);


	return _int();
}

_int CEffect_DeathSmoke::LastUpdate_GameObject(const _float & _fTick)
{
	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;


	vRight = *(_vec3*)&matView.m[0][0] * m_fSize;
	vUp = *(_vec3*)&matView.m[1][0] * m_fSize;
	vLook = *(_vec3*)&matView.m[2][0];



	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
	m_pTransformCom->Rotation_Axis_Angle(m_fRad, &vLook);



	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_DeathSmoke::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(_E_DEATH);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CEffect_DeathSmoke::Set_Param(const _vec3 & _vPos, const void * _pVoid)
{
	m_vDir = *(_vec3*)_pVoid *1.2f;
	//m_vDir *= 4.5f;


	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vPos);
}




HRESULT CEffect_DeathSmoke::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Effect");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom[0] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_SmokeBall");
	if (FAILED(Add_Component(L"Com_Texture0", m_pTextureCom[0])))
		return E_FAIL;
	m_pTextureCom[1] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_SmokeParticle");
	if (FAILED(Add_Component(L"Com_Texture1", m_pTextureCom[1])))
		return E_FAIL;
	m_pTextureCom[2] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_DeathGradient");
	if (FAILED(Add_Component(L"Com_Texture2", m_pTextureCom[2])))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_DeathSmoke::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);



	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");
	//_pEffect->SetMatrix("g_matWorld", &m_matWorld);

	m_pTextureCom[0]->SetUp_OnShader(_pEffect, "g_Tex0");
	m_pTextureCom[1]->SetUp_OnShader(_pEffect, "g_Tex1");
	m_pTextureCom[2]->SetUp_OnShader(_pEffect, "g_Tex2");

	_pEffect->SetInt("g_iColumn", _uint(m_fTime*20.f) / 8);
	_pEffect->SetInt("g_iRow", _uint(m_fTime*20.f) % 8);

	_pEffect->SetFloat("g_fTime",m_fTime);
	_pEffect->SetFloat("g_fDot", 0.125f);
	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_DeathSmoke * CEffect_DeathSmoke::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_DeathSmoke*	pInst = new CEffect_DeathSmoke(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_DeathSmoke::Clone_GameObject()
{
	CEffect_DeathSmoke*	pInst = new CEffect_DeathSmoke(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_DeathSmoke Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_DeathSmoke::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	for(size_t i =0;i<3;++i)
		Safe_Release(m_pTextureCom[i]);

	//지울것들



	CBaseEffect::Free();
}
