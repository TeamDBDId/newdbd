#include "stdafx.h"
#include "..\Headers\Action_HideCloset.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Closet.h"

_USING(Client)

CAction_HideCloset::CAction_HideCloset()
{
}

HRESULT CAction_HideCloset::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (!m_bIsInit)
		SetVectorPos();

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	pCamper->IsLockKey(true);
	m_bIsOccupied = false;
	m_bIsPlaying = true;
	m_fIndex = 0.f;

	CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
	m_vOriginPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

	CManagement* pManagement = CManagement::GetInstance();
	if (pManagement == nullptr)
		return END_ACTION;

	pManagement->AddRef();
	list<CGameObject*>* pObjList = pManagement->Get_ObjList(SCENE_STAGE, L"Layer_Camper");
	Safe_Release(pManagement);
	if (nullptr != pObjList)
	{
		for (auto pObj : *pObjList)
		{
			if (pObj->GetID() == m_pGameObject->GetID())
				continue;

			CCamper* pTargetCamper = (CCamper*)pObj;
			if (pTargetCamper->GetState() == AC::ClosetIdle)
			{
				CTransform* pTargetTransform = (CTransform*)pObj->Get_ComponentPointer(L"Com_Transform");
				_vec3 vTargetPos = *pTargetTransform->Get_StateInfo(CTransform::STATE_POSITION);

				CTransform* pMyTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
				_vec3 vMyPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);

				_float fDist = D3DXVec3Length(&(vTargetPos - vMyPos));

				if (fDist < 150.f)
				{
					m_pCamper = pTargetCamper;
					pCamper->Set_State(AC::ClosetFull_Entering);
					m_iState = AC::ClosetFull_Entering;
					m_pCloset->SetState(CCloset::DoorFull);
					Send_ServerData_Camper();
					Send_ServerData_Closet();
					return NOERROR;
				}
			}
		}
	}

	if (KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		pCamper->SetColl(false);
		pCamper->Set_State(AC::ClosetHideFast);
		m_iState = AC::ClosetHideFast;
		if (m_pCloset != nullptr)
			m_pCloset->SetState(CCloset::DoorHideFast);
	}
	else
	{
		pCamper->SetColl(false);
		pCamper->Set_State(AC::ClosetHide);
		m_iState = AC::ClosetHide;
		if (m_pCloset != nullptr)
			m_pCloset->SetState(CCloset::DoorHide);
	}

	Send_ServerData_Closet();
	return NOERROR;
}

_int CAction_HideCloset::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;
	
	m_bIsOccupied = true;
	
	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_fIndex += fTimeDelta * 30.f;

	size_t iIndex = (size_t)m_fIndex;
	if (m_iState == AC::ClosetFull_Entering)
	{
		if (m_fIndex >= 49.f)
		{
			pCamper->Set_State(AC::Idle);
			m_pCloset->SetState(CCloset::DoorIdleClosed);
		}

		if (m_fIndex >= 61.f)
			return END_ACTION;
	}
	else if (m_iState == AC::ClosetHide)
	{
		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
		if (pMeshCom->IsOverTime(0.3f))
		{
			if (!pCamper->IsColl())
			{
				pCamper->Set_State(AC::ClosetIdle);
				m_pCloset->SetState(CCloset::DoorIdleClosed);
			}
			else
			{
				pCamper->Set_State(AC::Idle);
				m_pCloset->SetState(CCloset::DoorIdleClosed);
			}
		}

		if (m_vecPos.size() <= iIndex)
			return END_ACTION;

		if (m_fIndex >= 61.f)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPos[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}
	else if (m_iState == AC::ClosetHideFast)
	{
		CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pCloset->Get_ComponentPointer(L"Com_Mesh");
		if (m_fIndex > 5.f)
			m_pCloset->SetState(CCloset::DoorIdleClosed);
		
		if (m_fIndex > 7.f)
		{
			if (!pCamper->IsColl())
				pCamper->Set_State(AC::ClosetIdle);
			else
				pCamper->Set_State(AC::Idle);
		}
		
		if (m_vecPosFast.size() <= iIndex)
			return END_ACTION;

		if (m_fIndex >= 15.f)
			return END_ACTION;

		_vec3 vLocalPos = m_vecPosFast[iIndex];
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");

		_matrix mat = pTransform->Get_Matrix();
		D3DXVec3TransformNormal(&vLocalPos, &vLocalPos, &mat);

		_vec3 vPos = m_vOriginPos + vLocalPos;
		pTransform->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	}

	Send_ServerData_Closet();
	return UPDATE_ACTION;
}

void CAction_HideCloset::End_Action()
{
	camper_data.InterationObject = 0;
	m_bIsPlaying = false;
	m_bIsOccupied = false;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	
	if (!pCamper->IsColl())
	{
		CTransform* pTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		pTransform->Reverse_RotationY();
		pCamper->Set_State(AC::ClosetIdle);
		m_pCloset->SetState(CCloset::DoorIdleClosed);
		pCamper->SetCurCondition(pCamper->GetOldCondition());
		pCamper->Set_PerkTime(L"INCLOSET", 3.f);
	}
	else
	{
		pCamper->SetCurCondition(pCamper->GetOldCondition());
		m_pCloset->SetState(CCloset::DoorIdleClosed);
		pCamper->IsLockKey(false);
	}
	m_pCloset = nullptr;
	m_pCamper = nullptr;
	Send_ServerData_Closet();
}

void CAction_HideCloset::Send_ServerData()
{

}

void CAction_HideCloset::Send_ServerData_Closet()
{
	if (m_pCloset != nullptr)
	{
		camper_data.InterationObject = m_pCloset->GetID();
		camper_data.InterationObjAnimation = m_pCloset->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_HideCloset::Send_ServerData_Camper()
{
	if (m_pCamper != nullptr && !m_bIsOccupied)
	{
		camper_data.SecondInterationObject = m_pCamper->GetID();
		camper_data.SecondInterationObjAnimation = ClOSET_OCCUPIED;
	}
}

void CAction_HideCloset::SetVectorPos()
{
	m_vecPos.reserve(61);

	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 0.497f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 1.923f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 4.181f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 7.175f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 10.808f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 14.981f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 19.6f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 24.566f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 29.783f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 35.154f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 40.583f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 45.971f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 51.222f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 56.24f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 60.927f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 65.187f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 68.922f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 72.036f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 74.697f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 77.141f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 79.374f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 81.4f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 83.224f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 84.851f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 86.286f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 87.534f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 88.599f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 89.487f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 90.203f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 90.751f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.137f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.364f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPos.push_back(_vec3(0.f, 0.f, 91.439f));

	m_vecPosFast.reserve(15);

	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 0.f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 0.f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 0.f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 1.933f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 7.203f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 15.013f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 24.566f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 38.023f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 55.421f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 72.777f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 86.11f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	//m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 1.933f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 7.203f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 15.013f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 24.566f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 38.023f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 55.421f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 72.777f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 86.11f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_vecPosFast.push_back(_vec3(0.f, 0.f, 91.439f));
	m_bIsInit = true;
}

void CAction_HideCloset::Free()
{
	CAction::Free();
}
