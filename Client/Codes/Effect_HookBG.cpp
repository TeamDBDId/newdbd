#include "stdafx.h"
#include "Effect_HookBG.h"
#include "Management.h"


#include "Target_Manager.h"
#include "Math_Manager.h"
_USING(Client)

CEffect_HookBG::CEffect_HookBG(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_HookBG::CEffect_HookBG(const CEffect_HookBG & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_HookBG::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_HookBG::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;

	m_pTransformCom->Scaling(350.f, 500.f, 0.f);

	

	return NOERROR;
}

_int CEffect_HookBG::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (10.f < m_fTime)
		m_isDead = true;

	

	return _int();
}

_int CEffect_HookBG::LastUpdate_GameObject(const _float & _fTick)
{

	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;


	vRight = *(_vec3*)&matView.m[0][0] * m_pTransformCom->Get_Scale().x;
	//vUp = *(_vec3*)&matView.m[1][0] * m_pTransformCom->Get_Scale().y;
	vLook = *(_vec3*)&matView.m[2][0] * m_pTransformCom->Get_Scale().z;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	//m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);



	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_HookBG::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(0);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CEffect_HookBG::Set_Param(const _vec3 & _vPos, const void * _pVoid)
{
	_vec3 vPos = _vPos;
	vPos.y += 300.f;
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}





HRESULT CEffect_HookBG::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_HookEff");


	GET_INSTANCE_MANAGEMENTR(E_FAIL);


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom[0] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Hook_BG0");
	if (FAILED(Add_Component(L"Com_Texture0", m_pTextureCom[0])))
		return E_FAIL;
	m_pTextureCom[1] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Hook_BG1");
	if (FAILED(Add_Component(L"Com_Texture1", m_pTextureCom[1])))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_HookBG::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);

	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");


	m_pTextureCom[0]->SetUp_OnShader(_pEffect, "g_Tex0");
	m_pTextureCom[1]->SetUp_OnShader(_pEffect, "g_Tex1");

	_pEffect->SetFloat("g_fTime", m_fTime);


	_float fAlpha = 1.f;
	if (m_fTime < 2.f)
		fAlpha = m_fTime*0.5f;
	else if(8.f<m_fTime)
		fAlpha = (10.f-m_fTime)*0.5f;


	_pEffect->SetFloat("g_fAlpha", fAlpha);

	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_HookBG * CEffect_HookBG::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_HookBG*	pInst = new CEffect_HookBG(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_HookBG::Clone_GameObject()
{
	CEffect_HookBG*	pInst = new CEffect_HookBG(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_HookBG Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_HookBG::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	for(size_t i =0;i<2;++i)
		Safe_Release(m_pTextureCom[i]);

	//지울것들



	CBaseEffect::Free();
}
