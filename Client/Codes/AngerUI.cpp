#include "stdafx.h"
#include "..\Headers\AngerUI.h"
#include "Management.h"
#include "MeshTexture.h"
#include "Slasher.h"

_USING(Client)

CAngerUI::CAngerUI(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CAngerUI::CAngerUI(const CAngerUI & rhs)
	: CGameObject(rhs)
{
}

HRESULT CAngerUI::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CAngerUI::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	return NOERROR;
}

_int CAngerUI::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	Check_Anger(fTimeDelta);

	m_fTime += fTimeDelta * 0.2f;
	if (m_fTime > 1.f)
		m_fTime = 0.f;

	return _int();
}

_int CAngerUI::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (m_fAnger < 0.01f)
		return 0;

	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;

	return _int();
}

void CAngerUI::Render_GameObject()
{
	if (nullptr == m_pBufferCom)
		return;

	LPD3DXEFFECT pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();
	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	SetUp_ConstantTable(pEffect);
	pEffect->CommitChanges();

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CAngerUI::Check_Anger(const _float& fTimeDelta)
{
	if (5 != exPlayerNumber)
		return;

	GET_INSTANCE_MANAGEMENT;

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front();
	if (nullptr == pSlasher)
	{
		Safe_Release(pManagement);
		return;
	}

	_float fAngerGauge = pSlasher->Get_AngerGauge();

	if (fAngerGauge >= 30.f && m_fAnger <= 1.0f)
		m_fAnger += fTimeDelta * 0.5f;
	else if (fAngerGauge < 30.f && m_fAnger >= 0.0f)
		m_fAnger -= fTimeDelta * 0.5f;

	


	Safe_Release(pManagement);
}

HRESULT CAngerUI::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);
	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Buffer
	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Shader_Default");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CAngerUI::SetUp_ConstantTable(LPD3DXEFFECT pEffect)
{
	if (nullptr == m_pRendererCom)
		return E_FAIL;

	_matrix			matWorld;
	D3DXMatrixIdentity(&matWorld);

	matWorld._11 = g_iBackCX;
	matWorld._22 = g_iBackCY;

	matWorld._41 = /*(g_iBackCX * 0.5f) - (g_iBackCX >> 1);*/0.f;
	matWorld._42 = /*-(g_iBackCY * 0.5f) + (g_iBackCY >> 1);*/ 0.f;

	pEffect->SetMatrix("g_matWorld", &matWorld);

	_matrix			matView;
	D3DXMatrixIdentity(&matView);
	pEffect->SetMatrix("g_matView", &matView);

	_matrix			matProj;
	D3DXMatrixOrthoLH(&matProj, (_float)g_iBackCX, (_float)g_iBackCY, 0.0f, 1.f);
	pEffect->SetMatrix("g_matProj", &matProj);

	pEffect->SetFloat("g_fTime", m_fTime);
	pEffect->SetFloat("g_fTime2", m_fAnger);
	m_pRendererCom->SetUp_OnShaderFromTarget(pEffect, L"Target_BackBuffer", "g_DiffuseTexture");

	pEffect->SetTexture("g_DistortionTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Noise02.tga")));
	pEffect->SetTexture("g_OffsetTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Noise01.tga")));
	pEffect->SetTexture("g_FlowTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"FlowMap01.tga")));

	return NOERROR;
}

CAngerUI * CAngerUI::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CAngerUI*	pInstance = new CAngerUI(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CAngerUI Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CAngerUI::Clone_GameObject()
{
	CAngerUI*	pInstance = new CAngerUI(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CAngerUI Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CAngerUI::Free()
{
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pShaderCom);

	CGameObject::Free();
}
