﻿#include "stdafx.h"
#include "..\Headers\Camper.h"
#include "Management.h"
#include "Light_Manager.h"
#include "Camera_Camper.h"
#include "CustomLight.h"
#include "Slasher.h"
#include "Action_BeingHeal.h"
#include "Action_HealCamper.h"
#include "Action_HealingSelf.h"
#include "Action_HookBeing.h"
#include "Action_BeingKilledBySpider.h"
#include "Action_Generator.h"
#include "Action_JumpInHatch.h"
#include "Action_LootChest.h"
#include "Action_CleansTotem.h"
#include "Action_PickItemOnGround.h"
#include "Action_Sabotage.h"
#include "Action_UnlockExit.h"
#include "Action_HideCloset.h"
#include "Action_JumpPlank.h"
#include "Action_PassWindow.h"
#include "Action_PullDownPlank.h"
#include "Action_UnhideCloset.h"
#include "Action_CrawlToHit.h"
#include "Action_HookFree.h"
#include "Action_MoveLerp.h"
#include "Action_Occupied.h"
#include "Action_StunDrop.h"
#include "Action_Grab_Generic_Fast.h"
#include "Action_Grab_Obstacles.h"
#include "Action_Search_Locker.h"
#include "Action_Drop.h"
#include "Action_PickUp.h"
#include "Action_HookIn.h"
#include "Action_Mori.h"
#include "EffectManager.h"
#include "Action_Jesture.h"
#include "Action_SpiderReaction.h"
#include "Action_HealingSelf.h"
#include "Totem.h"
#include "Generator.h"
#include "Chest.h"
#include "ExitDoor.h"
#include "Hatch.h"
#include "Item.h"
#include "MeshTexture.h"
#include "Math_Manager.h"
#include <string>

_USING(Client)

CCamper::CCamper(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CCamper::CCamper(const CCamper & rhs)
	: CGameObject(rhs)
	, m_FSoundList(rhs.m_FSoundList)
	, m_MSoundList(rhs.m_MSoundList)
{

}

HRESULT CCamper::Ready_Prototype()
{
	SetUp_Sound();

	return NOERROR;
}

HRESULT CCamper::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (FAILED(Ready_Actions()))
		return E_FAIL;

	m_pTransformCom->SetUp_Speed(385.f, D3DXToRadian(45.f));
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(3200.f, 0.f, 3200.f));

	m_eCurState = AC::Idle;
	//m_pMeshCom->Set_AnimationSet(AC::Idle);
	//m_pMeshCom->Set_ServeAnimationSet(AC::Idle);
	m_fEnergy = 240.f;
	m_fDyingEnergy = 240.f;
	m_fHookEnergy = 120.f;
	m_fMaxEnergy = 240.f;
	m_BreathTime = 0.f;
	m_fHeal = 0.f;
	m_fMaxHeal = 30.f;
	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 11.f;



	m_mapCoolTime[L"HOOK_BEINGBORROWED"] = 0.0f;
	m_mapCoolTime[L"HOOK_BEINGBORROWEDHIT"] = 0.0f;
	m_mapCoolTime[L"DEADHARD"] = 0.f;
	m_mapCoolTime[L"DEADHARDTime"] = 0.f;
	m_mapCoolTime[L"PREMONITION"] = 0.f;
	m_mapCoolTime[L"PALOMA"] = 0.f;
	m_mapCoolTime[L"ADRENALINE"] = 0.f;
	m_mapCoolTime[L"BREAKING"] = 0.f;
	m_mapCoolTime[L"INCLOSET"] = 0.f;
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CCamper::Update_GameObject(const _float & fTimeDelta)
{

	if (nullptr != m_pTransformCom)
	{
		_float fMyZ = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->z;
		if (fMyZ >= 11880.f)
		{
			GET_INSTANCE_MANAGEMENTR(0);

			CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
			if (nullptr == pCamera)
			{
				Safe_Release(pManagement);
				return 0;
			}

			if (exPlayerNumber == m_iIndex)
				pCamera->Set_IsLockCamera(true);

			m_pTransformCom->Go_ToTarget_ChangeDirection(&_vec3(0.f, 0.f, 1.f), fTimeDelta);
			m_eCurCondition = ESCAPE;
		}
		else if (fMyZ <= -650.f)
		{
			GET_INSTANCE_MANAGEMENTR(0);

			CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
			if (nullptr == pCamera)
			{
				Safe_Release(pManagement);
				return 0;
			}

			if(exPlayerNumber == m_iIndex)
				pCamera->Set_IsLockCamera(true);

			m_pTransformCom->Go_ToTarget_ChangeDirection(&_vec3(0.f, 0.f, -1.f), fTimeDelta);
			m_eCurCondition = ESCAPE;
		}
	}

	if (AC::BeingKilledBySpiderLoop == m_eCurState)
	{
		m_fFailDelay += fTimeDelta;
		if (m_fFailDelay > 5.f)
		{
			

			camper_data.bLive = false;
			m_isDead = true;
		}
	}

	//if (m_isLook)
	//{
	//	if (m_fDissolveTime >= 3.f)
	//	{
	//		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vec3(300000.f, 0.f, 300000.f));
	//		m_isLook = false;
	//	}		

	//	GET_INSTANCE_MANAGEMENTR(0);

	//	CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
	//	if (nullptr == pCamera)
	//	{
	//		Safe_Release(pManagement);
	//		return 0;
	//	}

	//	m_fDissolveTime += fTimeDelta;

	//	pCamera->Set_IsLockCamera(true);
	//	m_eCurCondition = ESCAPE;

	//	Safe_Release(pManagement);
	//	m_pTransformCom->Go_ToTarget_ChangeDirection(&_vec3(0, 0, 1), fTimeDelta);
	//	return 0;
	//}

	//if (nullptr != m_pTransformCom)
	//{
	//	_vec3 mypos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	//	cout << mypos.x << "," << mypos.y << "," << mypos.z << endl;
	//}

	m_bPenetration = false;

	if (m_eCurCondition == HOOKED || m_eCurCondition == DYING)
		m_isAnger = false;
	else
		m_isAnger = true;

	if (!m_bInitEffect)
	{
		if (m_fDissolveTime < 1.f)
			m_fDissolveTime += fTimeDelta * 2.f;
		else
		{
			m_bInitEffect = true;
			m_fDissolveTime = 0.f;
		}
	}

	if (KEYMGR->KeyDown(DIK_4))
		Set_Action(L"Action_HookBeing", 5.f);
	if (KEYMGR->KeyDown(DIK_5))
		Set_Action(L"Action_SpiderReaction", 200.f);
	if (m_isDead)
	{

		camper_data.bLive = false;

		return DEAD_OBJ;
	}	

	if (!bUseServer)
	{
		if (KEYMGR->KeyPressing(DIK_UP))
			m_pTransformCom->Go_ToTarget_ChangeDirection(&_vec3(0, 0, 1), m_fTimeDelta);
		if (KEYMGR->KeyPressing(DIK_RIGHT))
			m_pTransformCom->Go_ToTarget_ChangeDirection(&_vec3(1, 0, 0), m_fTimeDelta);
		if (KEYMGR->KeyPressing(DIK_DOWN))
			m_pTransformCom->Go_ToTarget_ChangeDirection(&_vec3(0, 0, -1), m_fTimeDelta);
		if (KEYMGR->KeyPressing(DIK_LEFT))
			m_pTransformCom->Go_ToTarget_ChangeDirection(&_vec3(-1, 0, 0), m_fTimeDelta);
		m_fTimeDelta = fTimeDelta;

		return 0;
	}

	Init_Camera();
	Collision_Check();
	Key_Input(fTimeDelta);

	CommunicationWithServer(fTimeDelta);
	Check_RGB();
	Check_Perk(fTimeDelta);
	State_Check();
	Check_FootStep();
	//Check_OtherCamperState();
	MoveSpeed_Check();
	Compute_Map_Index();
	Processing_Others(fTimeDelta);
	Update_Actions(fTimeDelta);
	Update_StateSound(fTimeDelta);
	SetBright(fTimeDelta);
	if (m_fPenetrationTime > 0.f)
	{
		m_bPenetration = true;
		m_fPenetrationTime -= fTimeDelta;
	}
	else
		m_fPenetrationTime = 0.0f;

	return _int();
}

_int CCamper::LastUpdate_GameObject(const _float & fTimeDelta)
{

	if(m_bIsCarried)
		IsCarried(fTimeDelta);
	
	if (nullptr == m_pRendererCom)
		return -1;

	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance < 1000.f)
		m_pRendererCom->Add_CubeGroup(this);

	m_bSpiritDisapear = false;

	if (!m_bInitEffect || m_isDead)
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
			return -1;

		return 0;
	}

	if (exPlayerNumber == 5)
	{
		if (server_data.Slasher.iSkill == 1
			&& server_data.Slasher.iCharacter == CSlasher::C_SPIRIT)
		{
			m_bSpiritDisapear = true;

			if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
				return -1;

			return 0;
		}
		else
		{
			if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
				return -1;
		}
	}
	else
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;
	}

	if (FAILED(m_pRendererCom->Add_StemGroup(this)))
		return -1;

	if (m_bPenetration)
	{
		m_pRendererCom->Add_StencilGroup(this);
	}

	return _int();
}

void CCamper::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;
	if (m_pItem)
		m_pMeshCom->Set_Mask(true);
	else
		m_pMeshCom->Set_Mask(false);

	if (exPlayerNumber == 5 && server_data.Slasher.iCharacter == 1 && server_data.Slasher.iSkill == 1)
		m_bPenetration = false;

	if (!m_bPenetration)
	{
		_bool bShape = false;
		_uint iPass = 2;
		if (m_isDead || !m_bInitEffect || m_bSpiritDisapear)
			iPass = 7;

		m_pMeshCom->Play_Animation(m_fTimeDelta);
		m_pMeshCom->Play_TorsoAnimation(m_fTimeDelta);
		m_pMeshCom->Play_ServeAnimation(m_fTimeDelta);

		if(m_pItem != nullptr && !m_bIsLockKey)
			m_pMeshCom->Play_ArmAnimation(m_fTimeDelta);
		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(iPass);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);
				m_matRHand = *m_pRHandMatrix;
				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();

		if (bShape)
		{
			pEffect->BeginPass(16);

			GET_INSTANCE_MANAGEMENT;
			_float SkillTime = ((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front())->Get_DissolveTime();
			Safe_Release(pManagement);

			pEffect->SetFloat("g_fTimeAcc", SkillTime);

			Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
			
			_float fovx = atan(tan(D3DX_PI / 2.f));

			_float Thicness = 15.f * m_fCameraDistance * fovx / g_iBackCX;

			//pEffect->SetFloat("g_Thicness", Thicness);

			for (size_t i = 0; i < dwNumMeshContainer; i++)
			{
				const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

				for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
				{
					m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);
					m_matRHand = *m_pRHandMatrix;
					D3DLOCKED_RECT lock_Rect = { 0, };
					if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
						return;

					memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

					m_pSkinTextures->UnlockRect(0);

					if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
						return;
					pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

					pEffect->CommitChanges();

					m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
				}

			}
			pEffect->EndPass();
		}

		pEffect->End();

		Safe_Release(pEffect);
	}
	else
	{
		m_pMeshCom->Play_Animation(m_fTimeDelta);
		m_pMeshCom->Play_TorsoAnimation(m_fTimeDelta);
		m_pMeshCom->Play_ServeAnimation(m_fTimeDelta);

		if (m_pItem != nullptr && !m_bIsLockKey)
			m_pMeshCom->Play_ArmAnimation(m_fTimeDelta);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(4);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);
				m_matRHand = *m_pRHandMatrix;
				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);
				//m_vecStencil.push_back(m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices);
				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();

		pEffect->End();

		Safe_Release(pEffect);
	}
}

void CCamper::Render_Stencil()
{
	if (nullptr == m_pSkinTextures)
		return;

	m_pMeshCom->Play_Animation(0.f);
	m_pMeshCom->Play_TorsoAnimation(0.f);
	m_pMeshCom->Play_ServeAnimation(0.f);

	if (m_pItem != nullptr && !m_bIsLockKey)
		m_pMeshCom->Play_ArmAnimation(0.f);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(5);

	//_uint iCount = 0;

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetInt("numBoneInf", 4);
			pEffect->SetVector("g_DiffuseColor", (_vec4*)&m_Color);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);

			//++iCount;
		}

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CCamper::Render_ShadowMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetInt("numBoneInf", 4);
			pEffect->SetMatrix("g_matVP", VP);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CCamper::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			pEffect->SetInt("numBoneInf", 4);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CCamper::Render_CubeMap(_matrix * View, _matrix * Proj)
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);
			pEffect->SetInt("numBoneInf", 4);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			_matrix matWVP = m_pTransformCom->Get_Matrix() * (*View) * (*Proj);

			pEffect->SetMatrix("g_matWVP", &matWVP);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CCamper::Render_UIImage()
{
	if (nullptr == m_pSkinTextures)
		return;

	m_pMeshCom->Play_Animation(0.0f);
	m_pMeshCom->Play_TorsoAnimation(0.0f);
	m_pMeshCom->Play_ServeAnimation(0.0f);

	if (m_pItem != nullptr && !m_bIsLockKey)
		m_pMeshCom->Play_ArmAnimation(0.0f);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(24);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);
			m_matRHand = *m_pRHandMatrix;
			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			pEffect->SetInt("numBoneInf", 4);
			pEffect->SetVector("g_DiffuseColor", &_vec4(0.7f, 0.0f, 0.0f, 0.7f));

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();

	pEffect->End();

	Safe_Release(pEffect);
}

void CCamper::SetBright(const _float& fTimeDelta)
{
	if (m_iIndex != exPlayerNumber)
		return;

	if (m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y < -10.f)
	{
		if (m_pRendererCom->Get_Brightness() > 17.f)
			m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() - fTimeDelta * 40.f);
		else
			m_pRendererCom->Set_Brightness(17.f);
	
		_vec3 vRGB = m_pRendererCom->Get_RGB();
		_vec3 vCalRGB = vRGB;
		if (vRGB.x < 3.5f)
			vCalRGB.x += fTimeDelta * 0.35f;
		else
			vCalRGB.x = 2.5f;
		if (vRGB.y > 0.1f)
			vCalRGB.y -= fTimeDelta * 0.1f;
		else
			vCalRGB.y = 0.1f;
		if (vRGB.z > 0.1f)
			vCalRGB.z -= fTimeDelta * 0.1f;
		else
			vCalRGB.z = 0.1f;

		m_pRendererCom->Set_RGB(vCalRGB);
	}
	else
	{
		if (m_pRendererCom->Get_Brightness() < 35.f)
			m_pRendererCom->Set_Brightness(m_pRendererCom->Get_Brightness() + fTimeDelta * 40.f);
		else
			m_pRendererCom->Set_Brightness(35.f);

		_vec3 vRGB = m_pRendererCom->Get_RGB();
		_vec3 vCalRGB = vRGB;
		if (vRGB.x > 0.5f)
			vCalRGB.x -= fTimeDelta * 0.35f;
		else
			vCalRGB.x = 0.5f;
		if (vRGB.y < 0.5f)
			vCalRGB.y += fTimeDelta * 0.1f;
		else
			vCalRGB.y = 0.5f;
		if (vRGB.z < 0.5f)
			vCalRGB.z += fTimeDelta * 0.1f;
		else
			vCalRGB.z = 0.5f;

		m_pRendererCom->Set_RGB(vCalRGB);
	}
}

_int CCamper::Do_Coll(CGameObject * _pObj, const _vec3& _vPos)
{
	if (exPlayerNumber != m_iIndex)
		return 0;



	//if (_pObj->GetID() & HATCH)
	//	cout << "dd" << endl;

	if (_pObj->GetID() & HOOK)
	{
		if (server_data.Game_Data.Hook[_pObj->GetID() - HOOK] == 13
			|| server_data.Game_Data.Hook[_pObj->GetID() - HOOK] == 14)
			return 0;
	}

	else if ((_pObj->GetID() & TOTEM) || (_pObj->GetID() & CHEST))
	{
		for (int i = 0; i < 4; ++i)
		{
			if (i == (exPlayerNumber - 1))
				continue;

			if (!server_data.Campers[i].bConnect || !server_data.Campers[i].bLive)
				continue;

			if (server_data.Campers[i].InterationObject != _pObj->GetID())
				return 0;
		}
	}
	else if (_pObj->GetID() & HATCH)
	{
		if (((CHatch*)_pObj)->Get_CurState() != CHatch::IdleOpen)
		{
			return 0;
		}
		else
		{
			GET_INSTANCE(CUIManager)->Add_Button(L"M1", L"탈출");
		}
	}
	else if (_pObj->GetID() & EXITDOOR)
	{
		if (!bCanGoOut)
			return 0;
	}

	m_pTargetOfInteraction = _pObj;
	m_vTargetPos = _vPos;



	return _int();
}

_int CCamper::Do_Coll_Player(CGameObject * _pObj, const _vec3 & _vPos)
{
	if (exPlayerNumber != m_iIndex)
		return 0;

	if (_pObj->GetID() & CAMPER)
	{
		m_pCamperOfInteraction = _pObj;
		m_vTargetCamperPos = _vPos;
		return 0;
	}

	if (_pObj->GetID() & SLASHER)
		return 0;

	return _int();
}

void CCamper::PickUpItem()
{
	if (nullptr == m_pItem)
	{
		if (nullptr == m_pColledItem)
			return;
		m_pColledItem->Pick_Item(m_pTransformCom, &m_matRHand);
		//camper_data.InterationObject = m_pColledItem->GetID();
		//camper_data.InterationObjAnimation = 1;
		m_pItem = m_pColledItem;
		

		GET_INSTANCE_MANAGEMENT;

		if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").size() == 0)
		{
			Safe_Release(pManagement);
			return;
		}

		for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
		{
			if (pCamper->Get_IsDead())
				continue;

			if (pCamper == this)
				continue;

			if (((CCamper*)pCamper)->GetColledItem() == m_pColledItem)
				((CCamper*)pCamper)->SetColledItem(nullptr);
		}

		m_pColledItem = nullptr;

		Safe_Release(pManagement);
	}
	else if (nullptr != m_pItem)
	{
		if (m_pItem->Get_IsDead())
		{
			m_pItem = nullptr;
			return;
		}
		if (m_pItem->Get_ItemType() < 1 || m_pItem->Get_ItemType() > 5)
		{
			m_pItem = nullptr;
			return;
		}

		if (m_pColledItem != nullptr)
		{
			_vec3 vCurPos = m_pColledItem->Get_Position();
			m_pItem->Drop_Item(&vCurPos);
			m_pColledItem->Pick_Item(m_pTransformCom, &m_matRHand);
			//camper_data.InterationObject = m_pColledItem->GetID();
			//camper_data.InterationObjAnimation = 3;
			m_pItem = m_pColledItem;

			GET_INSTANCE_MANAGEMENT;

			if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").size() == 0)
			{
				Safe_Release(pManagement);
				return;
			}

			for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
			{
				if (pCamper->Get_IsDead())
					continue;

				if (pCamper == this)
					continue;

				if (((CCamper*)pCamper)->GetColledItem() == m_pColledItem)
					((CCamper*)pCamper)->SetColledItem(nullptr);
			}

			m_pColledItem = nullptr;

			Safe_Release(pManagement);
		}
		else
		{
			//camper_data.InterationObject = m_pItem->GetID();
			//camper_data.InterationObjAnimation = 2;
			camper_data.iItem = 0;
			_vec3 vCurPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
			vCurPos.y += 30.f;
			m_pItem->Drop_Item(&vCurPos);
			m_pItem = nullptr;
		}
	}
}

HRESULT CCamper::Set_Index(_uint index)
{
	m_iIndex = index; m_eObjectID = (CAMPER + index - 1);

	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_uint iCharacter = 0;

	if (bUseServer)
	{
		if (m_iIndex == exPlayerNumber)
		{
			iCharacter = camper_data.Character;
		}
		else
		{
			iCharacter = server_data.Campers[m_iIndex - 1].Character;
		}
	}
	else
	{
		_int PlayerIndex = -1;
		for (_int i = 0; i < 4; i++)
		{
			if (lserver_data.Number[i] == index)
				PlayerIndex = i;
		}
		iCharacter = lserver_data.playerdat[PlayerIndex].character;
	}

	m_Character = (CHARACTER)iCharacter;

	switch (iCharacter)
	{

	case CH_DWIGHT:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Dwight");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;
		break;
	}
	break;
	case CH_BILL:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Bill");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;
		break;
	}
	case CH_NEA:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Nea");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;

		m_bWoman = true;
		break;
	}
	case CH_MEG:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Meg");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return E_FAIL;

		m_bWoman = true;
		break;
	}
	default:
		break;
	}

	m_pMatCamera = m_pMeshCom->Find_Frame("joint_TorsoC_01");

	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, 150.f, 150.f, 10.f);
	matLocalTransform._42 = 60.f;

	m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		return E_FAIL;

	m_pRHandMatrix = m_pMeshCom->Find_Frame("joint_WeaponRT_01");
	m_pTorsoMatrix = m_pMeshCom->Find_Frame("joint_TorsoC_01");

	m_pMeshCom->Set_NoBleningAnimationSet(AC::Idle);
	m_pMeshCom->Set_TorsoAnimationSet(AC::Idle);

	m_pColliderCom->Set_CircleRad(90.f);

	if (bUseServer)
	{
	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_PLAYER, this);
	EFFMGR->Ready_PlayerPos(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

		m_pFootMatrix[0] = m_pMeshCom->Find_Frame("joint_Foot_LT_01_IK");
		m_pFootMatrix[1] = m_pMeshCom->Find_Frame("joint_Foot_RT_01_IK");

		D3DXMatrixIdentity(&matLocalTransform);
		D3DXMatrixScaling(&matLocalTransform, 50.f, 50.f, 50.f);

		m_pFootCollider[0] = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, m_pFootMatrix[0], m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_ColliderFoot1", m_pFootCollider[0])))
			return E_FAIL;

		D3DXMatrixIdentity(&matLocalTransform);
		D3DXMatrixScaling(&matLocalTransform, 50.f, 50.f, 50.f);

		m_pFootCollider[1] = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, m_pFootMatrix[1], m_pTransformCom->Get_Matrix_Pointer()));
		if (FAILED(Add_Component(L"Com_ColliderFoot2", m_pFootCollider[1])))
			return E_FAIL;
	}
	Safe_Release(pManagement);
	Set_StartPosition();
	return NOERROR;
}

void CCamper::SetMaxEnergyAndMaxHeal()
{
	if (m_eCurCondition == INJURED)
	{
		m_fMaxHeal = 30.f;
	}
	else if (m_eCurCondition == DYING)
	{
		m_fMaxEnergy = 240.f;
		m_fMaxHeal = 30.f;
	}
	else if (m_eCurCondition == HOOKED)
	{
		m_fMaxEnergy = 120.f;
	}
	else
		return;
}

void CCamper::Change_Mesh(_uint iNum)
{
	Safe_Release(m_pMeshCom);

	GET_INSTANCE_MANAGEMENT;

	switch (iNum)
	{
	case CH_DWIGHT:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Dwight");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return;
	}
	break;
	case CH_BILL:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Bill");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return;
	}
	break;
	case CH_MEG:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Meg");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return;
	}
	break;
	case CH_NEA:
	{
		// For.Com_Mesh
		m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Mesh_Camper_Nea");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
			return;
	}
	break;
	default:
		break;
	}

	if (nullptr != m_pMeshCom)
		m_pMeshCom->Set_AnimationSet(AC::Idle);

	m_pTransformCom->SetUp_RotationY(D3DXToRadian(270.f));
	m_eCurState = AC::Idle;

	Safe_Release(pManagement);
}

void CCamper::Set_SlasherColl(_bool IsColl)
{
	CManagement* pManagement = CManagement::GetInstance();
	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	pSlasher->SetColl(IsColl);
}

void CCamper::BloodSpread(_int index)// 0:up 1:down 2:left 3:right 4:front 5:back
{
	if (nullptr == m_pTorsoMatrix)
		return;

	_matrix matTorso = *m_pTorsoMatrix * m_pTransformCom->Get_Matrix();
	_vec3 vPos = _vec3();
	_vec3 vDir = _vec3();
	memcpy(&vPos, &matTorso.m[3], sizeof(_vec3));

	switch (index)
	{
	case 0:
		memcpy(&vDir, &matTorso.m[0], sizeof(_vec3));
		break;
	case 1:
		memcpy(&vDir, &matTorso.m[0], sizeof(_vec3));
		vDir *= -1.f;
		break;
	case 2:
		memcpy(&vDir, &matTorso.m[1], sizeof(_vec3));
		break;
	case 3:
		memcpy(&vDir, &matTorso.m[1], sizeof(_vec3));
		vDir *= -1.f;
		break;
	case 4:
		memcpy(&vDir, &matTorso.m[2], sizeof(_vec3));
		break;
	case 5:
		memcpy(&vDir, &matTorso.m[2], sizeof(_vec3));
		vDir *= -1.f;
		break;
	default:
		vDir = { 0,0,0 };
		break;
	}

	EFFMGR->Make_BloodSpread(vPos, vDir);
}

void CCamper::Update_StateSound(const _float & fTimeDelta)
{
	_int Random = 0;
	_float Distance = 1700.f;
	string FileName = "";
	_float Volume = 1.f;
	if (m_eCurCondition == INJURED && !m_IsInjured)
	{
		m_IsInjured = true;

		if (!m_bWoman)
		{
			FileName = "Male_Hurt_Hard/Male_Hurt_Hard";
			Random = Math_Manager::CalRandIntFromTo(0, 8);
		}
		else
		{
			FileName = "Female_Hurt_Hard/Female_Hurt_Hard";
			Random = Math_Manager::CalRandIntFromTo(0, 31);
		}
		GET_INSTANCE(CSoundManager)->PlaySound(FileName + to_string(Random) + string(".ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), Distance, 1.f);

		if (!m_bIsCarried)
		{
			if (server_data.Slasher.iCharacter == 0)
			{
				FileName = "Slasher/Body_Hit/Body_Hit";
				Random = Math_Manager::CalRandIntFromTo(0, 7);
			}
			else
			{
				FileName = "Slasher/Body_Cut/Body_Cut";
				Random = Math_Manager::CalRandIntFromTo(0, 8);
			}
			GET_INSTANCE(CSoundManager)->PlaySound(FileName + to_string(Random) + string(".ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 850.f, 1.f);
		}
	}
	else if (m_eCurCondition == HEALTHY)
		m_IsInjured = false;
	if (m_eCurCondition == DYING && !m_IsScream)
	{
		m_IsScream = true;
		if (!m_bWoman)
			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/Male_Scream.ogg"), _vec3(), 0, 1.f);
		else
			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/Female_Scream.ogg"), _vec3(), 0, 1.f);
		GET_INSTANCE(CSoundManager)->PlaySound(FileName + to_string(Random) + string(".ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), Distance, 1.f);
		if (!m_bIsCarried)
		{
			if (server_data.Slasher.iCharacter == 0)
			{
				FileName = "Slasher/Body_Hit/Body_Hit";
				Random = Math_Manager::CalRandIntFromTo(0, 7);
			}
			else
			{
				FileName = "Slasher/Body_Cut/Body_Cut";
				Random = Math_Manager::CalRandIntFromTo(0, 8);
			}
			GET_INSTANCE(CSoundManager)->PlaySound(FileName + to_string(Random) + string(".ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 850.f, 1.f);
		}
	}
	else if (m_eCurCondition != DYING)
		m_IsScream = false;
	if (m_eCurCondition == HOOKED && !m_IsHookScream)
	{
		m_IsHookScream = true;
		if (!m_bWoman)
			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/Male_LongScream.ogg"), _vec3(), 0, 1.f);
		else
			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/Female_LongScream.ogg"), _vec3(), 0, 1.f);
	}
	else if (m_eCurCondition != HOOKED)
		m_IsHookScream = false;

	m_BreathTime += fTimeDelta;

	if (m_BreathTime <= 2.f)
		return;
	m_BreathTime = 0.f;

	Random = 0;
	Distance = 1700.f;
	FileName = "";

	if (m_eCurCondition == HEALTHY && m_eCurState == AC::Run)
	{
		if (!m_bWoman)
		{
			FileName = "Male_Running/Male_Running";
			Random = Math_Manager::CalRandIntFromTo(0, 19);
		}
		else 
		{
			FileName = "Female_Running/Female_Running";
			Random = Math_Manager::CalRandIntFromTo(0, 20);
		}
		Distance = 850.f;
	}
	else if(m_eCurCondition == HEALTHY)
	{
		if (!m_bWoman)
		{
			FileName = "Male_Normal/Male_Normal";
			Random = Math_Manager::CalRandIntFromTo(0, 18);
		}
		else
		{
			FileName = "Female_Normal/Female_Normal";
			Random = Math_Manager::CalRandIntFromTo(0, 21);
		}
		Distance = 450.f;
	}
	else if (m_eCurCondition == INJURED)
	{
		if (!m_bWoman)
		{
			FileName = "Male_Injured/Male_Injured";
			Random = Math_Manager::CalRandIntFromTo(0, 29);
		}
		else
		{
			FileName = "Female_Injured/Female_Injured";
			Random = Math_Manager::CalRandIntFromTo(0, 28);
		}
		Volume = 0.6f;
	}
	else if (m_eCurCondition == DYING)
	{
		if (!m_bWoman)
		{
			FileName = "Male_Dying/Male_Dying";
			Random = Math_Manager::CalRandIntFromTo(0, 53);
		}
		else
		{
			FileName = "Female_Dying/Female_Dying";
			Random = Math_Manager::CalRandIntFromTo(0, 33);
		}
	}
	else if (m_eCurCondition == HOOKED)
	{
		if (!m_bWoman)
		{
			FileName = "Male_Hooked/Male_Hooked";
			Random = Math_Manager::CalRandIntFromTo(0, 44);
		}
		else
		{
			FileName = "Female_Hooked/Female_Hooked";
			Random = Math_Manager::CalRandIntFromTo(0, 31);
		}
	}
	GET_INSTANCE(CSoundManager)->PlaySound(string("breath"), FileName + to_string(Random) + string(".ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), Distance, Volume);
}

void CCamper::Check_InteractionOfObject(CGameObject * pGameObject)
{
	if (m_iIndex != exPlayerNumber)
		return;

	if (m_bIsCarried)
		return;

	if (nullptr != m_pItem && !m_pItem->Get_IsDead() && !KEYMGR->MousePressing(1))
	{
		if (m_pItem->Get_ItemType() == CItem::FLASHLIGHT
			|| m_pItem->Get_ItemType() == CItem::KEY)
		{
			GET_INSTANCE(CUIManager)->Add_Button(L"M2", L"아이템 사용");
		}

		if (nullptr != m_pTargetOfInteraction)
		{
			if (m_pItem->Get_ItemType() == CItem::TOOLBOX)
			{
				if (m_pTargetOfInteraction->GetID() & HOOK)
					GET_INSTANCE(CUIManager)->Add_Button(L"M2", L"파괴공작");

				if (m_pTargetOfInteraction->GetID() & GENERATOR)
					GET_INSTANCE(CUIManager)->Add_Button(L"M2", L"수리");
			}
		}

		if (nullptr != m_pCamperOfInteraction)
		{
			if (m_pItem->Get_ItemType() == CItem::MEDIKIT)
			{
				GET_INSTANCE(CUIManager)->Add_Button(L"M2", L"치료");
			}
		}
		
	}

	if (nullptr != m_pItem && !m_pItem->Get_IsDead())
	{
		if (m_pItem->Get_Durability() <= 0.0f)
		{
			camper_data.bUseItem = false;

			camper_data.InterationObject = m_pItem->GetID();
			camper_data.InterationObjAnimation = 4;
			camper_data.iItem = 0;
			m_pItem->SetDead();
			m_pItem = nullptr;
		}
	}

	if (m_eCurState == AC::ClosetIdle && KEYMGR->KeyDown(DIK_SPACE))
	{
		CAction_HideCloset* pAction_HideCloset = (CAction_HideCloset*)Find_Action(L"Action_HideCloset");
		if (pAction_HideCloset->m_bIsPlaying)
			return;

		Set_Action(L"Action_UnhideCloset", 2.f);
		return;
	}

	if (m_bIsLockKey)
		return;
	
	if (KEYMGR->MouseDown(1) && nullptr == m_pCamperOfInteraction && m_eCurCondition == INJURED && m_pItem != nullptr && ((CItem*)m_pItem)->Get_ItemType() == CItem::MEDIKIT)
	{
		Set_Action(L"Action_HealingSelf", 50.f);
		return;
	}

	if (KEYMGR->MousePressing(1) && nullptr == m_pCamperOfInteraction)
	{
		if (nullptr == m_pItem && server_data.Campers[exPlayerNumber - 1].Perk & SELFHEAL && m_eCurCondition == INJURED)
		{
			Set_Action(L"Action_HealingSelf", 100.f);
			return;
		}
	}

		if (KEYMGR->MousePressing(1))
		{
			if (nullptr == m_pItem)
				return;

			if (m_pItem->Get_IsDead())
				return;

			if (nullptr != m_pCamperOfInteraction && m_pItem->Get_ItemType() == CItem::MEDIKIT)
			{
				_int CamperID = m_pCamperOfInteraction->GetID();

				if (server_data.Campers[CamperID].iCondition != INJURED)
					return;
						
				CAction_HealCamper* pAction_HealCamper = (CAction_HealCamper*)Find_Action(L"Action_HealCamper");
				if(nullptr != m_pCamperOfInteraction)
					pAction_HealCamper->SetCamper(m_pCamperOfInteraction);
				Set_Action(L"Action_HealCamper", 100.f);
				return;		
			}
			else if (nullptr == m_pCamperOfInteraction && m_pItem->Get_ItemType() == CItem::MEDIKIT)
			{
				if (m_eCurCondition != INJURED)
					return;

				Set_Action(L"Action_HealingSelf", 100.f);
			}
			else if (m_pItem->Get_ItemType() == CItem::KEY)
			{
				camper_data.bUseItem = true;
			}
			else if (m_pItem->Get_ItemType() == CItem::FLASHLIGHT)
			{
				m_bUseFlashLight = true;
				//GameObject_Camera_Camper
				GET_INSTANCE_MANAGEMENT;

				CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
				pCamera->Set_FlashCamera(true);

				_matrix matView;
				m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
				D3DXMatrixInverse(&matView, nullptr, &matView);
				_vec3 vCamLook;
				memcpy(&vCamLook, &matView.m[2], sizeof(_vec3));
				vCamLook.y = 0.0f;

				D3DXVec3Normalize(&vCamLook, &vCamLook);
				m_pTransformCom->Go_ToTarget_ChangeDirection(&vCamLook, m_fTimeDelta, true);		

				_float fDist = 0.f;

				if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size() != 0)
				{
					_vec3 vSlasherLook = -server_data.Slasher.vLook;
					vSlasherLook.y = 0.f;
					D3DXVec3Normalize(&vSlasherLook, &vSlasherLook);
					
					_float fAngSlasher = D3DXToDegree(acosf(vSlasherLook * vCamLook));
					if (fAngSlasher < 15.f)
					{		
						CCollider* pSlasherHead = (CCollider*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front()->Get_ComponentPointer(L"Com_HeadCollider");
						pSlasherHead->Collision_CameraLAY_Sphere(&fDist);
						if (fDist < 2000.f)
						{
							m_fFacedSlasherHeadTime += m_fTimeDelta;
							if (m_fFacedSlasherHeadTime > 0.7f)
							{
								camper_data.Packet = FLASHBANG;
								m_fFacedSlasherHeadTime = 0.f;
							}
						}
					}
				}
				
				camper_data.bUseItem = true;
				_bool bOverDurability = false;
				Use_FlashLight(m_fTimeDelta);
				bOverDurability = m_pItem->Use_Item(m_fTimeDelta, nullptr, nullptr);
				m_pItem->Set_UseNow(true);
				if (bOverDurability)
				{
					CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
					pCamera->Set_FlashCamera(false);
				}
				Safe_Release(pManagement);
			}
		}

	//}

	if (m_eCurCondition != HEALTHY && m_eCurCondition != INJURED)
		return;

	else if (KEYMGR->KeyDown(DIK_SPACE))
	{
		if (m_pTargetOfInteraction == nullptr)
			return;
		_uint eID = m_pTargetOfInteraction->GetID();

		if (eID & CLOSET || eID & PLANK || eID & WINDOW)
		{
			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);
			return;
		}
	}
	else if (KEYMGR->MousePressing(1))
	{
		if (nullptr == m_pItem)
			return;
		if (m_pTargetOfInteraction == nullptr)
			return;
		_uint eID = m_pTargetOfInteraction->GetID();

		if (eID & CAMPER || eID & CLOSET || eID & HATCH || eID & PLANK || eID & WINDOW || eID & CHEST || eID & EXITDOOR || eID & TOTEM)
			return;
		else if (eID & HOOK)
		{
			if (m_pItem->Get_ItemType() != CItem::TOOLBOX)
				return;

			if (m_pCamperOfInteraction != nullptr)
				return;

			camper_data.bUseItem = true;
			_bool bOverDurability = false;

			CGameObject* pGameObj = nullptr;

			bOverDurability = m_pItem->Use_Item(m_fTimeDelta, pGameObj, nullptr);

			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);

			return;
		}
		else if (eID & GENERATOR)
		{
			if (m_pItem->Get_ItemType() != CItem::TOOLBOX)
				return;

			_float fProgressTime = m_pTargetOfInteraction->Get_ProgressTime();
			_float fMaxProgressTime = m_pTargetOfInteraction->Get_MaxProgressTime();

			if (fProgressTime >= fMaxProgressTime)
				return;

			camper_data.bUseItem = true;
			_bool bOverDurability = false;

			CGameObject* pGameObj = nullptr;

			bOverDurability = m_pItem->Use_Item(m_fTimeDelta, nullptr, nullptr);

			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);

			return;
		}
	} 

	else if (m_bUseFlashLight && !KEYMGR->MousePressing(1) && bUseServer)
	{
		m_bUseFlashLight = false;
		m_fFacedSlasherHeadTime = 0.f;
		GET_INSTANCE_MANAGEMENT;

		CCamera_Camper* pCamera = (CCamera_Camper*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camera").front();
		if (nullptr == pCamera)
		{
			Safe_Release(pManagement);
			return;
		}
		pCamera->Set_FlashCamera(false);

		Safe_Release(pManagement);
		camper_data.bUseItem = false;
		if (nullptr != m_pItem && m_pItem->Get_ItemType() == CItem::FLASHLIGHT)
		{
			m_pItem->Set_UseNow(false);
		}
		m_eOldState = AC::Flash_Stand_FT;
		m_eCurState = AC::Idle;
	}
}

void CCamper::Init_Camera()
{
	if (!LateInit && exPlayerNumber == m_iIndex && bUseServer)
	{
		LateInit = true;

		GET_INSTANCE_MANAGEMENT;

		((CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0))->Set_OrbitMatrix();

		Safe_Release(pManagement);
	}
}

void CCamper::CommunicationWithServer(const _float& fTimeDelta)
{
	if (!bUseServer)
		return;

	if (exPlayerNumber == m_iIndex)	// 본인
	{
		//camper_data.Character =

		if (server_data.Campers[m_iIndex - 1].iCondition == INJURED || server_data.Campers[m_iIndex - 1].iCondition == DYING)
		{
			m_fBloodEffectTime += fTimeDelta;
			if (m_fBloodEffectTime > 5.f)
			{
				m_fBloodEffectTime = 0.0f;
				EFFMGR->Make_Effect(CEffectManager::E_Blood, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
			}
		}
		
		if (HOOKDEAD == m_eCurCondition || BLOODDEAD == m_eCurCondition || ESCAPE == m_eCurCondition)
			camper_data.bLive = false;
		else
			camper_data.bLive = true;


		camper_data.bConnect = true;

		camper_data.vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
		camper_data.vUp = *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
		camper_data.vLook = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
		camper_data.vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

		if (m_eCurState < 0)
			camper_data.iState = 0;
		if (m_eCurCondition < 0)
			camper_data.iCondition = 0;
		if (m_eHookStage < 0)
			camper_data.iHookStage = 0;
		if (m_fHeal < 0.0f)
			m_fHeal = 0.0f;
		if (m_fEnergy < 0.0f)
			m_fEnergy = 0.0f;

		camper_data.iState = (_int)m_eCurState;
		camper_data.iCondition = m_eCurCondition;
		camper_data.iOldCondition = m_eOldCondition;
		camper_data.iHookStage = m_eHookStage;
	
		camper_data.fHeal = m_fHeal;
		camper_data.fEnergy = m_fEnergy;

		if (nullptr != m_pItem && !m_pItem->Get_IsDead())
		{
			camper_data.iItem = m_pItem->GetID() - ITEM;
		}
		else if (nullptr == m_pItem)
		{
			camper_data.iItem = 100;
			/*if (server_data.Campers[exPlayerNumber - 1].iItem != 0)
			{
				for (int i = 0; i < 10; ++i)
				{
					if (_int(server_data.Game_Data.Item[i] / 10) == exPlayerNumber)
					{
						camper_data.InterationObject = i + ITEM;
						camper_data.InterationObjAnimation = 4;
						camper_data.iItem = 0;
					}
				}
			}*/		
		}

		if (nullptr != m_pItem && m_pItem->Get_IsDead())
		{
			camper_data.InterationObject = m_pItem->GetID();
			camper_data.InterationObjAnimation = 4;
			camper_data.iItem = 100;
		}		

		if(server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] != 0)
		{
			_tchar ActionTag[150] = L"";

			if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & PLAYERATTACK)
			{
				Check_Attacked(fTimeDelta);

				camper_data.RecvPacket = PLAYERATTACK;

				return;
			}
			else if(server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & PICK_UP)
			{
				m_isColl = false;
				lstrcpy(ActionTag, L"Action_PickUp");

				camper_data.RecvPacket = PICK_UP;
			}
			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & HOOK_IN)
			{
				lstrcpy(ActionTag, L"Action_HookIn");

				camper_data.RecvPacket = HOOK_IN;
			}
			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & DROP)
			{
				lstrcpy(ActionTag, L"Action_Drop");

				camper_data.RecvPacket = DROP;
			}
			else if(server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & GRAB_LOCKER)
			{
				m_isColl = false;
				lstrcpy(ActionTag, L"Action_SearchLocker");
				camper_data.RecvPacket = GRAB_LOCKER;
			}		
			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & GRAB_OBSTACLES)
			{
				m_isColl = false;
				Assert_EndAllAction(L"Action_Grab_Obstacles");
				lstrcpy(ActionTag, L"Action_Grab_Obstacles");

				camper_data.RecvPacket = GRAB_OBSTACLES;
			}
			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & GRAB_GENERIC_FAST)
			{
				m_isColl = false;
				Assert_EndAllAction(L"Action_Grab_Generic_Fast");
				lstrcpy(ActionTag, L"Action_Grab_Generic_Fast");

				camper_data.RecvPacket = GRAB_GENERIC_FAST;
			}

			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & HOOK_BEING)
			{
				if(m_eCurState == AC::Spider_Reaction_In || m_eCurState == AC::Spider_Reaction_Loop
					|| m_eCurState == AC::Spider_Reaction_Out || m_eCurState == AC::Spider_Struggle || m_eCurState == AC::HookedIdle)
					Assert_EndAllAction();

				if (nullptr != m_pTargetOfInteraction && m_pTargetOfInteraction->GetID() & HOOK)
				{
					CAction_HookBeing* pAction = (CAction_HookBeing*)Find_Action(L"Action_HookBeing");
					pAction->SetHook(m_pTargetOfInteraction);
				}
				lstrcpy(ActionTag, L"Action_HookBeing");

				camper_data.RecvPacket = HOOK_BEING;
			}

			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & HOOK_BEINGBORROWED)
			{
				if (m_eCurState == AC::Spider_Reaction_In || m_eCurState == AC::Spider_Reaction_Loop
					|| m_eCurState == AC::Spider_Reaction_Out || m_eCurState == AC::Spider_Struggle || m_eCurState == AC::HookedIdle)
					Assert_EndAllAction();

				if (nullptr != m_pTargetOfInteraction && m_pTargetOfInteraction->GetID() & HOOK)
				{
					CAction_HookBeing* pAction = (CAction_HookBeing*)Find_Action(L"Action_HookBeing");
					pAction->SetHook(m_pTargetOfInteraction);
				}
				lstrcpy(ActionTag, L"Action_HookBeing");

				m_mapCoolTime[L"HOOK_BEINGBORROWED"] = 17.f;

				camper_data.RecvPacket = HOOK_BEINGBORROWED;
			}
			else if(server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & BEING_HEAL)
			{
				Assert_EndAllAction();
				lstrcpy(ActionTag, L"Action_BeingHeal");

				camper_data.RecvPacket = BEING_HEAL;
			}
			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & END_HEAL)
			{
				Assert_EndAllAction();

				camper_data.InterationObject = 0;
				camper_data.InterationObjAnimation = 0;
				camper_data.SecondInterationObject = 0;
				camper_data.SecondInterationObjAnimation = 0;

				camper_data.RecvPacket = END_HEAL;
			}
			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & ClOSET_OCCUPIED)
			{
				lstrcpy(ActionTag, L"Action_ClosetOccupied");

				camper_data.RecvPacket = ClOSET_OCCUPIED;
			}
			else if (server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & MORICAMPER)
			{
				lstrcpy(ActionTag, L"Action_Mori");

				camper_data.RecvPacket = MORICAMPER;
			}

			Set_Action(ActionTag, 100.f);
		}

			//_tchar ActionTag[150] = L"";

		
	}
	else
	{
		if (!server_data.Campers[m_iIndex - 1].bConnect)
			m_isDead = true;

		_vec3 vRight = server_data.Campers[m_iIndex - 1].vRight;
		_vec3 vUp = server_data.Campers[m_iIndex - 1].vUp;
		_vec3 vLook = server_data.Campers[m_iIndex - 1].vLook;
		_vec3 vPos = server_data.Campers[m_iIndex - 1].vPos;

		m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);
		m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

		m_eCurState = (AC::ANIMATION_CAMPER)server_data.Campers[m_iIndex - 1].iState;
		m_eCurCondition = (CONDITION)server_data.Campers[m_iIndex - 1].iCondition;
		m_eOldCondition = (CONDITION)server_data.Campers[m_iIndex - 1].iOldCondition;
		m_eHookStage = (HOOKSTAGE)server_data.Campers[m_iIndex - 1].iHookStage;
		m_fHeal = server_data.Campers[m_iIndex - 1].fHeal;
		m_fEnergy = server_data.Campers[m_iIndex - 1].fEnergy;

		if (nullptr != m_pItem)
		{
			if (m_pItem->Get_IsDead())
			{
				m_pItem = nullptr;
				return;
			}		

			if (server_data.Campers[m_iIndex - 1].bUseItem)
			{
				m_pItem->Set_UseNow(true);
				if (m_pItem->Get_ItemType() == CItem::FLASHLIGHT)
				{
					Use_FlashLight(fTimeDelta);
				}
				m_pItem->Use_Item(fTimeDelta * 0.8f, nullptr, nullptr);
			}
			else
				m_pItem->Set_UseNow(false);

			//if(server_data.Campers[m_iIndex].)

			//if (server_data.Campers[m_iIndex - 1].InterationObject & ITEM)
			//{
			//	if (m_pItem != nullptr && (server_data.Campers[m_iIndex - 1].InterationObjAnimation == 2) && server_data.Campers[m_iIndex - 1].iItem == 0)
			//	{
			//		_vec3 vItemPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
			//		vItemPos.y += 5.f;
			//		m_pItem->Drop_Item(&vItemPos);
			//		m_pItem = nullptr;
			//	}
			//}]

			if (server_data.Campers[m_iIndex - 1].iItem == 100)
			{
				_vec3 vItemPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
				vItemPos.y += 30.f;
				m_pItem->Drop_Item(&vItemPos);
				m_pItem = nullptr;
			}
		}
		else
		{
			if (server_data.Campers[m_iIndex - 1].iItem != 100)
			{
				GET_INSTANCE_MANAGEMENT;

				for (auto& pItem : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Item"))
				{
					if (!pItem->Get_IsDead() && (pItem->GetID() - ITEM) == server_data.Campers[m_iIndex - 1].iItem)
					{
						((CItem*)pItem)->Pick_Item(m_pTransformCom, m_pRHandMatrix);
						m_pItem = (CItem*)pItem;
						Safe_Release(pManagement);
						return;
					}
				}

				Safe_Release(pManagement);
			}
		}
		
		if (exPlayerNumber != 5)
		{

			if (server_data.Campers[m_iIndex - 1].iCondition == INJURED || server_data.Campers[m_iIndex - 1].iCondition == DYING)
			{
				m_fBloodEffectTime += fTimeDelta;
				if (m_fBloodEffectTime > 5.f)
				{
					m_fBloodEffectTime = 0.0f;
					EFFMGR->Make_Effect(CEffectManager::E_Blood, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
				}
			}

			GET_INSTANCE_MANAGEMENT;
			
			for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
			{
				if (nullptr == pCamper)
					continue;
				if ((pCamper->GetID() - CAMPER) != (exPlayerNumber - 1))
					continue;

				CTransform* pCamperTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");

				_vec3 vMyCamperPos = *pCamperTransform->Get_StateInfo(CTransform::STATE_POSITION);
				_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
				_float fLength = D3DXVec3Length(&(vMyCamperPos - vMyPos));

				if (fLength < 500.f)
				{
					m_bPenetration = false;
					Safe_Release(pManagement);
					return;
				}
			}

			Safe_Release(pManagement);

			if (_int(server_data.Campers[exPlayerNumber - 1].iItem % 10) == CItem::KEY
				&& server_data.Campers[exPlayerNumber - 1].bUseItem)
			{
				m_bPenetration = true;
				m_Color = { 0.7f, 0.7f, 0.1f, 1.f };
			}

			if (CONDITION::DYING == m_eCurCondition ||
				CONDITION::HOOKED == m_eCurCondition)
			{
				m_bPenetration = true;
				m_Color = { 0.6f, 0.1f, 0.1f, 1.f };
			}
			
			if (CONDITION::HOOKED == server_data.Campers[exPlayerNumber - 1].iCondition)
			{
				m_bPenetration = true;
				m_Color = { 0.7f, 0.7f, 0.1f, 1.f };
			}

			if (server_data.Campers[exPlayerNumber - 1].Perk & BOND)
			{
				_vec3 vPlayerPos = server_data.Campers[exPlayerNumber - 1].vPos;
				_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
				_float fDist = D3DXVec3Length(&(vPlayerPos - vMyPos));

				if (fDist <= BOND_RANGE)
				{
					m_bPenetration = true;
					m_Color = { 0.7f, 0.7f, 0.1f, 1.f };
				}
			}

			if ((server_data.Campers[exPlayerNumber - 1].Perk & SYMPATHY) && CONDITION::INJURED == m_eCurCondition)
			{
				_vec3 vPlayerPos = server_data.Campers[exPlayerNumber - 1].vPos;
				_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
				_float fDist = D3DXVec3Length(&(vPlayerPos - vMyPos));

				if (fDist <= SYMPATHY_RANGE)
				{
					m_bPenetration = true;
					m_Color = { 0.7f, 0.7f, 0.1f, 1.f };
				}
			}
		}
		else if (exPlayerNumber == 5)
		{
			if (server_data.Campers[m_iIndex - 1].iState == AC::Run
				|| server_data.Campers[m_iIndex - 1].iState == AC::Injured_Run)
			{
				m_fFootPrintTime += fTimeDelta;

				if (m_fFootPrintTime > 1.5f)
				{
					m_fFootPrintTime = 0.f;
					EFFMGR->Make_Effect(CEffectManager::E_FootPrint, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
				}
			}

			if (server_data.Campers[m_iIndex - 1].iCondition == INJURED || server_data.Campers[m_iIndex - 1].iCondition == DYING)
			{
				m_fBloodEffectTime += fTimeDelta;
				if (m_fBloodEffectTime > 5.f)
				{
					m_fBloodEffectTime = 0.0f;
					EFFMGR->Make_Effect(CEffectManager::E_Blood, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
				}
			}
				
			GET_INSTANCE_MANAGEMENT;
			
			CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
			Safe_Release(pManagement);
			if (nullptr == pSlasher)
				return;

			if (server_data.Campers[m_iIndex - 1].Packet == FLASHBANG)
			{
				m_pRendererCom->Set_Flashed(true);
			}

			CTransform* pSlasherTransform = (CTransform*)pSlasher->Get_ComponentPointer(L"Com_Transform");

			_vec3 vSlasherPos = *pSlasherTransform->Get_StateInfo(CTransform::STATE_POSITION);
			_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
			_float fLength = D3DXVec3Length(&(vSlasherPos - vMyPos));



			if (CONDITION::HOOKED == m_eCurCondition)
			{
				m_bPenetration = true;
				m_Color = { 0.6f, 0.1f, 0.1f, 1.f };
			}
			if (server_data.Slasher.Perk & DISSONANCE)
			{
				if (bCanGoOut && !m_bCanOut)
				{
					m_bCanOut = true;
					m_fPenetrationTime = 10.f;
					m_Color = { 0.6f, 0.1f, 0.1f, 1.f };
				}
			}
			if (server_data.Slasher.Perk & NURSECALLING)
			{
				if (fLength < NURSECALLING_RANGE)
				{
					if ((m_eCurState == AC::HealCamper) || (m_eCurState == AC::HealCamperFail)
						|| (m_eCurState == AC::HealingSelf) || (m_eCurState == AC::BeingHeal) || (m_eCurState == AC::BeingHealFail))
					{
						m_fPenetrationTime = 1.f;
						m_Color = { 0.6f, 0.1f, 0.1f, 1.f };
					}
				}
			}
			if (server_data.Slasher.Perk & DEERSTALKER)
			{
				if (fLength < DEER_RANGE)
				{
					if (m_eCurCondition == CONDITION::DYING)
					{
						m_fPenetrationTime = 1.f;
						m_Color = { 0.6f, 0.1f, 0.1f, 1.f };
					}
				}
			}
				
			if (fLength < 500.f)
			{
				m_bPenetration = false;
				return;
			}
		}

	}
}

void CCamper::Compute_Map_Index()
{
	const _vec3* pPos = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	_int IndexX = (_int)(pPos->x / _MAP_INTERVAL);
	_int IndexZ = (_int)(pPos->z / _MAP_INTERVAL);

	m_iMapIndex = (IndexX * 14) + IndexZ;

}

void CCamper::Collision_Check()
{
	//if (exPlayerNumber == 5)
	//{
	//	if (m_eCurState == AC::CrawlFT)
	//		m_isColl = true;
	//}
}

void CCamper::Check_Perk(const _float& fTimeDelta)
{
	if (exPlayerNumber != m_iIndex)
		return;
	
	if (m_bHealUp)
	{
		switch (m_eCurCondition)
		{
		case CCamper::INJURED:
			m_eCurCondition = CCamper::HEALTHY;
			m_bHealUp = false;
			break;
		case CCamper::DYING:
			m_eCurCondition = CCamper::INJURED;
			m_bHealUp = false;
			break;
		default:
			break;
		}
	}

	for (auto& fTime : m_mapCoolTime)
	{
		if(fTime.second >= -0.01f)
			fTime.second -= fTimeDelta;
	}
		
	if (server_data.Campers[exPlayerNumber - 1].Perk & DEADHARD)
	{
		if (KEYMGR->KeyDown(DIK_E) && INJURED == m_eCurCondition && m_mapCoolTime[L"DEADHARD"] < 0.01f)
		{
			//auto& iter = m_mapCoolTime.find(L"DEADHARDTime");
			//iter->second = 0.15f;
			//iter = m_mapCoolTime.find(L"DEADHARD");
			//iter->second = 40.f;
			m_mapCoolTime[L"DEADHARDTime"] = 0.15f;
			m_mapCoolTime[L"DEADHARD"] = 40.f;
			m_eCurState = AC::M_InjuredDeadHard;
		}
	}
	if (server_data.Campers[exPlayerNumber - 1].Perk & PREMONITION)
	{
		if (m_mapCoolTime[L"PREMONITION"] < 0.01f)
		{
			if (server_data.Slasher.bConnect)
			{
				_vec3 vSlasherPos = server_data.Slasher.vPos;
				_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
				_vec3 vDirection = vSlasherPos - vMyPos;

				_float fDistance = D3DXVec3Length(&vDirection);

				if (fDistance <= PREMONITION_RANGE)
				{
					_matrix matCamLook;
					m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matCamLook);
					D3DXMatrixInverse(&matCamLook, nullptr, &matCamLook);
					
					_vec3 vMyLook;
					memcpy(&vMyLook, &matCamLook.m[2], sizeof(_vec3));
					vMyLook.y = 0.f;

					D3DXVec3Normalize(&vMyLook, &vMyLook);
					D3DXVec3Normalize(&vDirection, &vDirection);

					_float fAngle = acosf((vMyLook * vDirection));

					if (fAngle <= D3DXToRadian(22.5f))
					{
						// 감지 소리
						GET_INSTANCE(CSoundManager)->PlaySound(string("Ingame/perks_premonition.ogg"), _vec3(), 0, 1.5f);
						m_mapCoolTime[L"PREMONITION"] = 30.f;
					}					
				}
			}
		}
	}
	if (server_data.Campers[exPlayerNumber - 1].Perk & ADRENALINE)
	{
		if (bCanGoOut && !m_bAdrenaline)
		{
			m_bAdrenaline = true;
			m_mapCoolTime[L"ADRENALINE"] = 5.f;
			m_bHealUp = true;
		}
	}
	
	for (int i = 0; i < 4; ++i)
	{
		if (i == (exPlayerNumber - 1))
			continue;
		if (!server_data.Campers[i].bConnect)
			continue;

		if ((server_data.Campers[i].Perk & HOMOGENEITY) && server_data.Campers[i].iCondition == HOOKED)
		{
			GET_INSTANCE_MANAGEMENT;

			for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
			{
				if ((((CCamper*)pCamper)->GetID() - CAMPER) == (exPlayerNumber - 1))
					continue;

				((CCamper*)pCamper)->Set_Penetraition(1.f);
				((CCamper*)pCamper)->Set_PColor(_vec4(0.7f, 0.7f, 0.f, 1.f));
			}
			
			_vec3 vHookedCamper = server_data.Campers[i].vPos;
			_vec3 vSlasherPos = server_data.Slasher.vPos;

			_float fLength = D3DXVec3Length(&(vHookedCamper - vSlasherPos));
			if (fLength < HOMOGENEITY_RANGE)
				if(0 != pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size())
					((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front())->Set_Penetration(1.f);

			Safe_Release(pManagement);
		}
	}
}

void CCamper::Check_FootStep()
{
	//if (nullptr != m_pFootCollider[0])
	//{
	//	_vec3 vLPos = m_pFootCollider[0]->Get_Center();

	//	_float fHeight = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y;
	//	
	//	if (!m_bFootColl[0] && (fHeight + 8.47f) >= vLPos.y)
	//	{
	//		m_bFootColl[0] = true;

	//		//switch (m_eCurState)
	//		//{
	//		//case AC::CrouchWalk:
	//			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_walk_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 300.f, 10.f);
	//		//	break;
	//		//case AC::Injured_CrouchWalk:
	//		//	GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_walk_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 600.f, 15.f);
	//		//	break;
	//		//case AC::Injured_Walk:
	//		//	GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_walk_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1200.f, 20.f);
	//		//	break;
	//		//case AC::Run:
	//		//	GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_run_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1200.f, 20.f);
	//		//	break;
	//		//case AC::Injured_Run:
	//		//	GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_run_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1200.f, 20.f);
	//		//	break;
	//		//default:
	//		//	break;
	//		//}
	//	}
	//	else if(m_bFootColl[0])
	//		m_bFootColl[0] = false;
	//}

	//if (nullptr != m_pFootCollider[1])
	//{
	//	_vec3 vLPos = m_pFootCollider[1]->Get_Center();

	//	_float fHeight = m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION)->y;

	//	if (!m_bFootColl[1] && (fHeight + 8.47f) >= vLPos.y)
	//	{
	//		m_bFootColl[1] = true;

	//		switch (m_eCurState)
	//		{
	//		case AC::CrouchWalk:
	//			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_walk_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 300.f, 10.f);
	//			break;
	//		case AC::Injured_CrouchWalk:
	//			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_walk_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 600.f, 15.f);
	//			break;
	//		case AC::Injured_Walk:
	//			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_walk_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1200.f, 20.f);
	//			break;
	//		case AC::Run:
	//			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_run_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1200.f, 20.f);
	//			break;
	//		case AC::Injured_Run:
	//			GET_INSTANCE(CSoundManager)->PlaySound(string("Camper/footsteps_barefoot_dirt_run_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1200.f, 20.f);
	//			break;
	//		default:
	//			break;
	//		}
	//	}
	//	else if(m_bFootColl[1])
	//		m_bFootColl[1] = false;
	//}
}

void CCamper::Check_RGB()
{
	if (exPlayerNumber != m_iIndex)
		return;

	if (nullptr == m_pRendererCom)
		return;

	_bool bRed = false;

	//if (m_eCurCondition == CONDITION::INJURED || m_eOldCondition == CONDITION::INJURED)
	//{
	//	m_pRendererCom->Set_RGB(_vec3(0.7f, 0.2f, 0.2f));
	//}
	//else if (m_eCurCondition == CONDITION::DYING || m_eOldCondition == CONDITION::DYING)
	//{
	//	m_pRendererCom->Set_RGB(_vec3(0.9f, 0.1f, 0.1f));
	//}
	//else
	//{
	//	m_pRendererCom->Set_RGB(_vec3(0.5f, 0.5f, 0.5f));
	//}
}

void CCamper::Set_StartPosition()
{
	if (exPlayerNumber - 1 != m_eObjectID - CAMPER)
		return;

	_int Random = Math_Manager::CalRandIntFromTo(0, 1);
	_vec3 vPos = { 0.f,0.f,0.f };
	if (exPlayerNumber == 1)
	{
		if (Random == 0)
			vPos = { 3200.f, 0.f,  3200.f };
		else
			vPos = { 1321.f, 0.f,7068.f };
	}
	if (exPlayerNumber == 2)
	{
		if (Random == 0)
			vPos = { 1105.f, 0.f, 10254.f };
		else
			vPos = { 4933.f, 0.f,4343.f };
	}
	if (exPlayerNumber == 3)
	{
		if (Random == 0)
			vPos = { 5293.f, 689.57f, 7201.f };
		else
			vPos = { 7029.f, 0.f,2741.f };
	}
	if (exPlayerNumber == 4)
	{
		if (Random == 0)
			vPos = { 10121.f, 0.f,  1178.f };
		else
			vPos = { 6919.f, 0.f, 10954.f };
	}

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
}

void CCamper::SetUp_Sound()
{
	Add_SoundList(AC::Injured_CrouchWalk, 6.0f / 24.0f, "Camper/footsteps_barefoot_dirt_walk_01.ogg", 850.f);
	Add_SoundList(AC::Injured_CrouchWalk, 19.0f / 24.0f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 850.f);
	Add_SoundList(AC::CrouchWalk, 0.11f, "Camper/footsteps_barefoot_dirt_walk_01.ogg", 850.f);
	Add_SoundList(AC::CrouchWalk, 0.626781f / 24.0f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 850.f);
	Add_SoundList(AC::Walk, 5.8f/24.f, "Camper/footsteps_barefoot_dirt_walk_01.ogg",1200.f);
	Add_SoundList(AC::Walk, 16.f/24.f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 1200.f);
	Add_SoundList(AC::Injured_Walk, 8.f / 27.0f, "Camper/footsteps_barefoot_dirt_walk_01.ogg", 1200.f);
	Add_SoundList(AC::Injured_Walk, 21.2f / 27.0f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 1200.f);
	Add_SoundList(AC::Run, 3.4f / 18.0f, "Camper/footsteps_barefoot_dirt_run_01.ogg");
	Add_SoundList(AC::Run, 13.4f / 18.0f, "Camper/footsteps_barefoot_dirt_run_02.ogg");
	Add_SoundList(AC::Injured_Run, 6.4f / 20.0f, "Camper/footsteps_barefoot_dirt_run_01.ogg");
	Add_SoundList(AC::Injured_Run, 17.4f / 20.0f, "Camper/footsteps_barefoot_dirt_run_02.ogg");

	Add_SoundListFemale(AC::Injured_CrouchWalk, 6.0f / 24.0f, "Camper/footsteps_barefoot_dirt_walk_01.ogg", 850.f);
	Add_SoundListFemale(AC::Injured_CrouchWalk, 19.0f / 24.0f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 850.f);
	Add_SoundListFemale(AC::CrouchWalk, 6.0f / 24.0f, "Camper/footsteps_barefoot_dirt_walk_01.ogg", 850.f);
	Add_SoundListFemale(AC::CrouchWalk, 19.0f / 24.0f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 850.f);
	Add_SoundListFemale(AC::Walk, 5.8f / 24.f, "Camper/footsteps_barefoot_dirt_walk_01.ogg",1200.f);
	Add_SoundListFemale(AC::Walk, 16.f / 24.f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 1200.f);
	Add_SoundListFemale(AC::Injured_Walk, 8.f / 27.0f, "Camper/footsteps_barefoot_dirt_walk_01.ogg", 1200.f);
	Add_SoundListFemale(AC::Injured_Walk, 21.2f / 27.0f, "Camper/footsteps_barefoot_dirt_walk_02.ogg", 1200.f);
	Add_SoundListFemale(AC::Run, 3.4f / 18.0f, "Camper/footsteps_barefoot_dirt_run_01.ogg");
	Add_SoundListFemale(AC::Run, 13.4f / 18.0f, "Camper/footsteps_barefoot_dirt_run_02.ogg");
	Add_SoundListFemale(AC::Injured_Run, 6.4f / 20.0f, "Camper/footsteps_barefoot_dirt_run_01.ogg");
	Add_SoundListFemale(AC::Injured_Run, 17.4f / 20.0f, "Camper/footsteps_barefoot_dirt_run_02.ogg");



	Add_SoundList(AC::GeneratorActivation, 10.0f / 210.0f, "Generator/generator_repair_crank_01.ogg");
	Add_SoundList(AC::GeneratorActivation, 30.0f / 210.0f, "Generator/generator_repair_pull_01.ogg");
	Add_SoundList(AC::GeneratorActivation, 45.0f / 210.0f, "Generator/generator_repair_push_05.ogg");
	Add_SoundList(AC::GeneratorActivation, 60.0f / 210.0f, "Generator/generator_repair_pull_05.ogg");
	Add_SoundList(AC::GeneratorActivation, 65.0f / 210.0f, "Generator/generator_repair_crank_02.ogg");
	Add_SoundList(AC::GeneratorActivation, 74.0f / 210.0f, "Generator/generator_repair_pull_02.ogg");
	Add_SoundList(AC::GeneratorActivation, 88.0f / 210.0f, "Generator/generator_repair_push_01.ogg");
	Add_SoundList(AC::GeneratorActivation, 100.0f / 210.0f, "Generator/generator_repair_pull_03.ogg");
	Add_SoundList(AC::GeneratorActivation, 121.0f / 210.0f, "Generator/generator_repair_push_02.ogg");
	Add_SoundList(AC::GeneratorActivation, 137.0f / 210.0f, "Generator/generator_repair_pull_04.ogg");
	Add_SoundList(AC::GeneratorActivation, 146.0f / 210.0f, "Generator/generator_repair_crank_03.ogg");
	Add_SoundList(AC::GeneratorActivation, 162.0f / 210.0f, "Generator/generator_repair_pull_05.ogg");
	Add_SoundList(AC::GeneratorActivation, 180.0f / 210.0f, "Generator/generator_repair_push_03.ogg");
	Add_SoundList(AC::GeneratorActivation, 195.0f / 210.0f, "Generator/generator_repair_crank_04.ogg");
	Add_SoundList(AC::GeneratorActivation, 208.0f / 210.0f, "Generator/generator_repair_push_04.ogg");

	Add_SoundListFemale(AC::GeneratorActivation, 10.0f / 210.0f, "Generator/generator_repair_crank_01.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 30.0f / 210.0f, "Generator/generator_repair_pull_01.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 45.0f / 210.0f, "Generator/generator_repair_push_05.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 60.0f / 210.0f, "Generator/generator_repair_pull_05.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 65.0f / 210.0f, "Generator/generator_repair_crank_02.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 74.0f / 210.0f, "Generator/generator_repair_pull_02.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 88.0f / 210.0f, "Generator/generator_repair_push_01.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 100.0f / 210.0f, "Generator/generator_repair_pull_03.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 121.0f / 210.0f, "Generator/generator_repair_push_02.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 137.0f / 210.0f, "Generator/generator_repair_pull_04.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 146.0f / 210.0f, "Generator/generator_repair_crank_03.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 162.0f / 210.0f, "Generator/generator_repair_pull_05.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 180.0f / 210.0f, "Generator/generator_repair_push_03.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 195.0f / 210.0f, "Generator/generator_repair_crank_04.ogg");
	Add_SoundListFemale(AC::GeneratorActivation, 208.0f / 210.0f, "Generator/generator_repair_push_04.ogg");


	Add_SoundList(AC::JumpOverAngle60LT, 3.f / 55.0f, "Camper/body_rush_slide_wood_02.ogg", 850.f,0.6f);
	Add_SoundList(AC::JumpOverAngle60LTFast, 1.0f / 31.0f, "Camper/body_rush_slide_wood_03.ogg");

	Add_SoundListFemale(AC::JumpOverAngle60LT, 3.0f / 55.0f, "Camper/body_rush_slide_wood_02.ogg", 850.f, 0.6f);
	Add_SoundListFemale(AC::JumpOverAngle60LTFast, 1.0f / 30.0f, "Camper/body_rush_slide_wood_03.ogg");

	Add_SoundList(AC::JumpOverAngle60RT, 3.0f / 55.0f, "Camper/body_rush_slide_wood_02.ogg", 850.f, 0.6f);
	Add_SoundList(AC::JumpOverAngle60RTFast, 1.0f / 31.0f, "Camper/body_rush_slide_wood_03.ogg");

	Add_SoundListFemale(AC::JumpOverAngle60RT, 3.0f / 55.0f, "Camper/body_rush_slide_wood_02.ogg", 850.f, 0.6f);
	Add_SoundListFemale(AC::JumpOverAngle60RTFast, 1.0f / 30.0f, "Camper/body_rush_slide_wood_03.ogg");



	// WindowVault_Mid
	Add_SoundList(AC::WindowVault_Mid, 15.0f / 30.0f, "Window/windows_bloc_start_01.ogg", 850.0f, 0.6f);
	// WindowVault_Fast

	Add_SoundList(AC::WindowVault_Fast, 5.0f / 22.0f, "Window/windows_hit_01.ogg");

	// WindowVault_Mid F
	Add_SoundListFemale(AC::WindowVault_Mid, 15.0f / 30.0f, "Window/windows_bloc_start_01.ogg", 850.0f, 0.6f);
	// WindowVault_Fast F
	Add_SoundListFemale(AC::WindowVault_Fast, 5.0f / 22.0f, "Window/windows_hit_01.ogg");

	// Camper_Male



	Add_SoundList(AC::TT_PickUp, 17.0f / 82.0f, "Camper/M_HookOut.ogg", 850.f);

	//Wiggle
	Add_SoundList(AC::TT_Wiggle, 11.0f / 60.0f, "Camper/M_Wiggle.ogg", 850.f);
										  
	Add_SoundList(AC::TT_Wiggle, 21.0f / 60.0f, "Camper/M_WiggleBreathing.ogg", 450.f);
										  
	Add_SoundList(AC::TT_Wiggle, 32.0f / 60.0f, "Camper/M_Wiggle.ogg", 850.f);
										  
	Add_SoundList(AC::TT_Wiggle, 42.0f / 60.0f, "Camper/M_WiggleBreathing.ogg", 450.f);

	// Drop
	Add_SoundList(AC::TT_Drop, 36.0f / 67.0f, "Camper/M_DeadHard.ogg", 850.f);

	// GrapLocker

	Add_SoundList(AC::TT_Grab_Locker, 30.2f / 150.0f, "Camper/M_Detectched.ogg");

	Add_SoundList(AC::TT_Grab_Locker, 42.2f / 150.0f, "Camper/M_Crying.ogg");

	// GrapObstacle_BK
	Add_SoundList(AC::TT_Grab_Obstacles_BK, 11.2f / 80.0f, "Camper/M_Detectched.ogg");

	// GrapObstacle_FT

	Add_SoundList(AC::TT_Grab_Obstacles_FT, 20.2f / 81.0f, "Camper/M_Detectched.ogg");

	// GrapObstacle_FT

	Add_SoundList(AC::TT_Hook_In, 27.0f / 45.0f, "Camper/M_HookedScream.ogg");

	Add_SoundList(AC::TT_Hook_In, 10.0f / 45.0f, "Camper/HookedSound.ogg");

	// StunDrop
	Add_SoundList(AC::TT_Stun_Drop, 1.0f / 32.0f, "Camper/M_Suprised.ogg", 850.f);

	// HitToCrawlBk

	Add_SoundList(AC::HitToCrawlBk, 2.0f / 70.0f, "Camper/M_Attacked2D.ogg");

	// HitToCrawlFk
	Add_SoundList(AC::HitToCrawlFk, 2.0f / 98.0f, "Camper/M_Attacked2D.ogg");

	// HealingSelf
	Add_SoundList(AC::HealingSelf, 2.2f / 95.0f, "Camper/M_BeingHeal.ogg", 850.f);
	Add_SoundList(AC::HealingSelf, 49.8f / 95.0f, "Camper/M_BeingHeal.ogg", 850.f);

	// HookResecue
	Add_SoundList(AC::HookBeingCamperEnd, 0.2f / 42.0f, "Camper/M_SpiderReaction.ogg", 850.f);
	// HookOut Land
	Add_SoundList(AC::HookBeingRescuredEnd, 8.5f / 73.0f, "Camper/M_SpiderReaction.ogg");

	// DeadHard
	Add_SoundList(AC::M_InjuredDeadHard, 0.5f / 15.0f, "Camper/M_DeadHard.ogg");

	// HookFree

	Add_SoundList(AC::HookBeingRescuredEnd, 8.5f / 73.0f, "Camper/M_DeadHard.ogg");

	Add_SoundList(AC::HookBeingRescuredEnd, 21.f / 73.0f, "Camper/M_Suprised.ogg");
	Add_SoundList(AC::HookBeingRescuredEnd, 36.f / 73.0f, "Camper/M_HookOutLand.ogg");
	
	//Resecue End
	Add_SoundList(AC::HookBeingRescuredEnd, 9.0f / 42.0f, "Camper/M_DeadHard.ogg");

	// HealFailed
	Add_SoundList(AC::HealCamperFail, 1.0f / 30.0f, "Camper/M_HookOutLand.ogg", 850.f);

	// BeingHealFailed

	Add_SoundList(AC::BeingHealFail, 2.0f / 31.0f, "Camper/M_HealFailed.ogg");

	Add_SoundList(AC::BeingHealFail, 30.0f / 31.0f, "Camper/M_Crying.ogg");

	// HealFailed

	Add_SoundList(AC::GeneratorFail, 20.0f / 78.0f, "Camper/M_Suprised.ogg", 850.f);

	// SpiderReaction
	Add_SoundList(AC::Spider_Reaction_In, 1.0f / 26.0f, "Camper/M_Suprised.ogg");
	Add_SoundList(AC::Spider_Reaction_Out, 23.0f / 44.0f, "Camper/M_SpiderReaction.ogg");

	Add_SoundList(AC::Spider_Struggle, 30.0f / 98.0f, "Camper/M_SpiderReaction.ogg");
	Add_SoundList(AC::Spider_Struggle, 60.0f / 98.0f, "Camper/M_SpiderReaction.ogg");

	if (exPlayerNumber == 5)
	{

		// Generator_Fail
		Add_SoundList(AC::GeneratorFail, 1.0f / 78.0f, "Camper/FailedSound.ogg");
		Add_SoundList(AC::HookBeingRescuredEnd, 1.f / 73.0f, "Camper/FailedSound.ogg");

		// Generator_Fail Female
		Add_SoundListFemale(AC::GeneratorFail, 1.0f / 78.0f, "Camper/FailedSound.ogg", 0.f);
		Add_SoundListFemale(AC::HookBeingRescuredEnd, 1.f / 73.0f, "Camper/FailedSound.ogg", 0.f);
	}
	

	Add_SoundListFemale(AC::TT_PickUp, 17.0f / 82.0f, "Camper/F_HookOut.ogg", 850.f);

	//Wiggle
	Add_SoundListFemale(AC::TT_Wiggle, 11.0f / 60.0f, "Camper/F_Wiggle.ogg", 850.f);

	Add_SoundListFemale(AC::TT_Wiggle, 21.0f / 60.0f, "Camper/F_DeadHard.ogg", 450.f);

	
	Add_SoundListFemale(AC::TT_Wiggle, 32.0f / 60.0f, "Camper/F_Wiggle.ogg", 850.f);
	
	Add_SoundListFemale(AC::TT_Wiggle, 42.0f / 60.0f, "Camper/F_DeadHard.ogg", 450.f);

	// Drop
	Add_SoundListFemale(AC::TT_Drop, 36.0f / 67.0f, "Camper/F_DeadHard.ogg", 850.f);

	// GrapLocker
	Add_SoundListFemale(AC::TT_Grab_Locker, 30.2f / 150.0f, "Camper/F_Suprised.ogg");

	Add_SoundListFemale(AC::TT_Grab_Locker, 42.2f / 150.0f, "Camper/F_Crying.ogg");

	// GrapObstacle_BK
	Add_SoundListFemale(AC::TT_Grab_Obstacles_BK, 11.2f / 80.0f, "Camper/F_Attacked.ogg");

	// GrapObstacle_FT
	Add_SoundListFemale(AC::TT_Grab_Obstacles_FT, 20.2f / 80.0f, "Camper/F_Attacked.ogg");

	// GrapObstacle_FT
	Add_SoundListFemale(AC::TT_Hook_In, 27.0f / 45.0f, "Camper/F_HookIn.ogg");
	Add_SoundListFemale(AC::WI_Hook_In, 10.0f / 45.0f, "Camper/HookedSound.ogg");

	// StunDrop
	Add_SoundListFemale(AC::WI_Stun_Drop, 1.0f / 32.0f, "Camper/F_DeadHard.ogg", 850.f);

	// HitToCrawlBk
	Add_SoundListFemale(AC::HitToCrawlBk, 2.0f / 70.0f, "Camper/F_Attacked2D.ogg");

	// HitToCrawlFk
	Add_SoundListFemale(AC::HitToCrawlFk, 2.0f / 98.0f, "Camper/F_Attacked2D.ogg");

	// HealingSelf
	Add_SoundListFemale(AC::HealingSelf, 2.2f / 95.0f, "Camper/F_BreathIn.ogg", 850.f);
	Add_SoundListFemale(AC::HealingSelf, 49.8f / 95.0f, "Camper/F_DeepBreathing.ogg", 850.f);

	// HookResecue
	Add_SoundListFemale(AC::HookBeingCamperEnd, 0.2f / 42.0f, "Camper/F_SpiderReaction.ogg", 850.f);
	// HookOut Land
	Add_SoundListFemale(AC::HookBeingRescuredEnd, 8.5f / 73.0f, "Camper/F_SpiderReaction.ogg");

	// DeadHard
	Add_SoundListFemale(AC::M_InjuredDeadHard, 0.5f / 15.0f, "Camper/F_DeadHard.ogg");

	// HookFree
	Add_SoundListFemale(AC::HookBeingRescuredEnd, 8.5f / 73.0f, "Camper/F_DeadHard.ogg");
	Add_SoundListFemale(AC::HookBeingRescuredEnd, 21.f / 73.0f, "Camper/F_BreathIn.ogg");
	Add_SoundListFemale(AC::HookBeingRescuredEnd, 36.f / 73.0f, "Camper/F_HookOutLand.ogg");

	//Resecue End
	Add_SoundListFemale(AC::HookBeingRescuredEnd, 9.0f / 42.0f, "Camper/F_DeadHard.ogg");

	// HealFailed
	Add_SoundListFemale(AC::HealCamperFail, 1.0f / 30.0f, "Camper/F_HookOutLand.ogg", 850.f);

	// BeingHealFailed
	Add_SoundListFemale(AC::BeingHealFail, 2.0f / 31.0f, "Camper/F_HealFailed.ogg");
	Add_SoundListFemale(AC::BeingHealFail, 30.0f / 31.0f, "Camper/F_Crying.ogg");

	// HealFailed
	Add_SoundListFemale(AC::GeneratorFail, 20.0f / 78.0f, "Camper/F_WiggleBreathing.ogg", 850.f);
}

void CCamper::Update_Sound()
{
	_float TimeAcc = (_float)m_pMeshCom->GetTimeAcc();
	_float Period = (_float)m_pMeshCom->GetPeriod(m_eCurState);
	_float Ratio = TimeAcc / Period;
	Ratio = (Ratio - (int)Ratio);
	cout << Ratio << endl;

	if (m_fRatio > Ratio || m_eCurState != m_eOldState)
	{
		m_iSoundCheck = 0;	//중복체크
		m_fRatio = Ratio;
	}
	else if (m_fRatio < Ratio)
	{
		m_fRatio = Ratio;
		_int SoundCount = 0;
		list<SoundData> SoundList;
		if (!m_bWoman)
			SoundList = m_MSoundList;
		else
			SoundList = m_FSoundList;
		for (auto& iter : SoundList)
		{
			if (m_eCurState == iter.m_iState)
			{
				if (Ratio >= iter.Ratio && SoundCount == m_iSoundCheck)
				{
					string SoundName = iter.SoundName;
					_float Volume = iter.SoundValue;
					if (m_eCurState == AC::Walk || m_eCurState == AC::Injured_CrouchWalk || m_eCurState == AC::Injured_Walk ||
						m_eCurState == AC::Run || m_eCurState == AC::Injured_Run || m_eCurState == AC::CrouchWalk)
					{
						if (m_iStep == 1)
						{
							_int Random = Math_Manager::CalRandIntFromTo(1, 9);
							SoundName = string("Camper/footsteps_barefoot_bush_0")+to_string(Random)+string(".ogg");
							Volume = 0.3f;
						}
						else if (m_iStep == 2)
						{
							SoundName.replace(26, 4, "wood");
						}
					}
					GET_INSTANCE(CSoundManager)->PlaySound(SoundName, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), iter.SoundDistance, Volume);
					m_iSoundCheck++;
					return;
				}
				SoundCount++;
			}
		}
	}
	m_iStep = 0;
}

void CCamper::Add_SoundListFemale(const _uint & m_iState, const _float Ratio, const char * SoundName, const _float & SoundDistance, const _float & SoundValue, const _int & OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundDistance = SoundDistance;
	Data.SoundValue = SoundValue;
	Data.OtherOption = OtherOption;

	m_FSoundList.push_back(Data);
}

void CCamper::Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance, const _float& SoundValue, const _int& OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundDistance = SoundDistance;
	Data.SoundValue = SoundValue;
	Data.OtherOption = OtherOption;

	m_MSoundList.push_back(Data);
}

HRESULT CCamper::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CCamper::Ready_Actions()
{
	//CamperInteractions;
	CAction* pAction = new CAction_BeingHeal;
	m_mapActions.insert(MAPACTION::value_type(L"Action_BeingHeal", pAction));

	pAction = new CAction_HealCamper;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HealCamper", pAction));

	pAction = new CAction_HealingSelf;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HealingSelf", pAction));

	pAction = new CAction_HookBeing;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookBeing", pAction));

	pAction = new CAction_ClosetOccupied;
	m_mapActions.insert(MAPACTION::value_type(L"Action_ClosetOccupied", pAction));
	// ObjectInteractions
	pAction = new CAction_BeingKilledBySpider;
	m_mapActions.insert(MAPACTION::value_type(L"Action_BeingKilledBySpider", pAction));

	pAction = new CAction_Generator;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Generator", pAction));

	pAction = new CAction_JumpInHatch;
	m_mapActions.insert(MAPACTION::value_type(L"Action_JumpInHatch", pAction));

	pAction = new CAction_LootChest;
	m_mapActions.insert(MAPACTION::value_type(L"Action_LootChest", pAction));

	pAction = new CAction_CleansTotem;
	m_mapActions.insert(MAPACTION::value_type(L"Action_CleansTotem", pAction));

	pAction = new CAction_PickItemOnGround;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PickItemOnGround", pAction));

	pAction = new CAction_Sabotage;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Sabotage", pAction));

	pAction = new CAction_UnlockExit;
	m_mapActions.insert(MAPACTION::value_type(L"Action_UnlockExit", pAction));

	pAction = new CAction_SpiderReaction;
	m_mapActions.insert(MAPACTION::value_type(L"Action_SpiderReaction", pAction));

	// ObastcleInteractions
	pAction = new CAction_HideCloset;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HideCloset", pAction));

	pAction = new CAction_JumpPlank;
	m_mapActions.insert(MAPACTION::value_type(L"Action_JumpPlank", pAction));

	pAction = new CAction_PassWindow;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PassWindow", pAction));

	pAction = new CAction_PullDownPlank;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PullDownPlank", pAction));

	pAction = new CAction_UnhideCloset;
	m_mapActions.insert(MAPACTION::value_type(L"Action_UnhideCloset", pAction));

	// SlasherInteractions
	pAction = new CAction_CrawlToHit;
	m_mapActions.insert(MAPACTION::value_type(L"Action_CrawlToHit", pAction));

	pAction = new CAction_HookFree;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookFree", pAction));

	pAction = new CAction_Drop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Drop", pAction));

	pAction = new CAction_Grab_Generic_Fast;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Generic_Fast", pAction));

	pAction = new CAction_Grab_Obstacles;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Grab_Obstacles", pAction));

	pAction = new CAction_HookIn;
	m_mapActions.insert(MAPACTION::value_type(L"Action_HookIn", pAction));

	pAction = new CAction_PickUp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_PickUp", pAction));

	pAction = new CAction_SearchLocker;
	m_mapActions.insert(MAPACTION::value_type(L"Action_SearchLocker", pAction));

	pAction = new CAction_StunDrop;
	m_mapActions.insert(MAPACTION::value_type(L"Action_StunDrop", pAction));

	// public
	pAction = new CAction_MoveLerp;
	m_mapActions.insert(MAPACTION::value_type(L"Action_MoveLerp", pAction));

	pAction = new CAction_Jesture;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Jesture", pAction));

	pAction = new CAction_Mori;
	m_mapActions.insert(MAPACTION::value_type(L"Action_Mori", pAction));

	return NOERROR;
}

HRESULT CCamper::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED * pMeshContainer, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetInt("numBoneInf", 4);

	if (m_eCurCondition == HEALTHY)
	{
		pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
		pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
		pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	}
	else
	{
		if((m_eCurCondition == SPECIAL || m_eCurCondition == OBSTACLE) && m_eOldCondition == HEALTHY)
		{
			pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
			pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
			pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
		}
		else
		{
			if (nullptr != pSubSet->MeshTexture.pExtendsTexture1)
				pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pExtendsTexture1);
			else
				pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
			if (nullptr != pSubSet->MeshTexture.pExtendsTexture2)
				pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pExtendsTexture2);
			else
				pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
			if (nullptr != pSubSet->MeshTexture.pExtendsTexture3)
				pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pExtendsTexture3);
			else
				pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
		}
	}
	
	if (m_isDead || !m_bInitEffect || m_bSpiritDisapear)
	{
		_float fTimeAcc = m_fDissolveTime;
		if (!m_bInitEffect)
			fTimeAcc = 1.f - fTimeAcc;
		pEffect->SetTexture("g_DissolveEffect", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));
		pEffect->SetTexture("g_DissolveTexture", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"BurnedEmbers.tga")));

		pEffect->SetFloat("g_fTimeAcc", fTimeAcc);
	}
		
	//pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetVector("g_DiffuseColor", (_vec4*)&m_Color);

	pEffect->SetBool("g_isFootPrint", false);
	return NOERROR;
}

void CCamper::Key_Input(const _float& fTimeDelta)
{
	if (exPlayerNumber != m_iIndex)
		return;

	if (!bCameraReady)
		return;

	Check_InteractionOfObject(nullptr);

	if (m_bIsCarried || m_bIsLockKey || m_eCurState == AC::Flash_Stand_FT)
		return;

	_int	iCount = 0;
	_vec3	vPosAcc = { 0.f, 0.f, 0.f };
	_matrix	matCam;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matCam);
	D3DXMatrixInverse(&matCam, nullptr, &matCam);
	_vec3 vRight = (_vec3)&matCam.m[CTransform::STATE_RIGHT][0];
	_vec3 vLook = (_vec3)&matCam.m[CTransform::STATE_LOOK][0];

	if (!m_bIsLockKey && KEYMGR->MousePressing(0) && m_eCurCondition == DYING && m_fHeal < 29.f)
	{
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_DyingHeal);
		m_fHeal += fTimeDelta * 0.5f;
		return;
	}
	else if (m_eCurCondition == DYING && m_fHeal < 29.f && !m_bIsCarried)
	{
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Dying);
		GET_INSTANCE(CUIManager)->Add_Button(wstring(L"M1"), wstring(L"회복"));
	}
	if (!m_bIsLockKey && KEYMGR->MouseDown(0))
	{
		if(nullptr != m_pColledItem)
			Set_Action(L"Action_PickItemOnGround", 2.f);
	}

	if (!m_bIsLockKey && KEYMGR->MousePressing(0))
	{
		_uint TargetID = 0;
		_uint CamperID = 0;
		if (m_pTargetOfInteraction != nullptr)
			TargetID = m_pTargetOfInteraction->GetID();
		if (m_pCamperOfInteraction != nullptr)
			CamperID = m_pCamperOfInteraction->GetID();

		if (m_pCamperOfInteraction != nullptr && (((CCamper*)m_pCamperOfInteraction)->GetCurCondition() == INJURED ||
			((CCamper*)m_pCamperOfInteraction)->GetCurCondition() == DYING || ((CCamper*)m_pCamperOfInteraction)->GetCurCondition() == HOOKED))
		{
			CCamper* pCamper = (CCamper*)m_pCamperOfInteraction;
			CONDITION ConditionOfCamper = pCamper->GetCurCondition();
			//if (pCamper->GetEnergy() <= -0.01f)
			//	return;

			if (ConditionOfCamper != HOOKED)
			{
				CAction_HealCamper* pAction_HealCamper = (CAction_HealCamper*)Find_Action(L"Action_HealCamper");
				if(nullptr != m_pCamperOfInteraction)
					pAction_HealCamper->SetCamper(m_pCamperOfInteraction);
				Set_Action(L"Action_HealCamper", 100.f);
				return;
			}

			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetCamperPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pCamperOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);
			return;
		}
		else if (TargetID & CHEST || TargetID & TOTEM || TargetID & EXITDOOR || TargetID & GENERATOR)
		{
			_float fProgressTime = m_pTargetOfInteraction->Get_ProgressTime();
			_float fMaxProgressTime = m_pTargetOfInteraction->Get_MaxProgressTime();

			if (fProgressTime >= fMaxProgressTime)
				return;

			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);
			return;
		}
		else if (TargetID & HATCH)
		{
			CAction_MoveLerp* pAction_MoveLerp = (CAction_MoveLerp*)Find_Action(L"Action_MoveLerp");
			pAction_MoveLerp->SetPos(m_vTargetPos);
			pAction_MoveLerp->SetTargetOfInteraction(m_pTargetOfInteraction);
			Set_Action(L"Action_MoveLerp", 0.2f);
			return;
		}
	}

	if (!m_bIsLockKey && KEYMGR->KeyDown(DIK_R) && nullptr != m_pItem && m_pColledItem == nullptr)
	{
		Set_Action(L"Action_PickItemOnGround", 2.f);
		return;
	}

	if (!m_bIsLockKey && (KEYMGR->KeyPressing(DIK_1) || KEYMGR->KeyPressing(DIK_2)))
	{
		if (m_eCurCondition != HEALTHY && m_eCurCondition != INJURED)
			return;

		Set_Action(L"Action_Jesture", 2.f);
		return;
	}

	if (KEYMGR->KeyDown(DIK_9))
		m_eCurCondition = INJURED;
	if (KEYMGR->KeyDown(DIK_8))
		m_eCurCondition = DYING;

	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_W))
	{
		iCount++;
		D3DXVec3Normalize(&vLook, &vLook);
		vPosAcc += vLook;
	}
	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_S))
	{
		iCount++;
		D3DXVec3Normalize(&vLook, &vLook);
		vPosAcc += -vLook;
	}
	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_A))
	{
		iCount++;
		D3DXVec3Normalize(&vRight, &vRight);
		vPosAcc += -vRight;
	}
	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_D))
	{
		iCount++;
		D3DXVec3Normalize(&vRight, &vRight);
		vPosAcc += vRight;
	}

	if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_LCONTROL))
	{
		if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D))
		{
			vPosAcc = (vPosAcc / (_float)iCount);
			D3DXVec3Normalize(&vPosAcc, &vPosAcc);
			m_pTransformCom->Go_ToTarget_ChangeDirection(&vPosAcc, fTimeDelta * 0.3f * m_fMoveSpeed);

			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::CrouchWalk;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_CrouchWalk;
		}
		else
		{
			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::CrouchStand;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_CrouchIdle;
			else if (m_eCurCondition == DYING)
				m_eCurState = AC::CrawlToStand;
		}
	}
	else if (!m_bIsLockKey && KEYMGR->KeyPressing(DIK_LSHIFT))
	{
		if (m_eCurCondition != DYING && KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D))
		{
			vPosAcc = (vPosAcc / (_float)iCount);
			D3DXVec3Normalize(&vPosAcc, &vPosAcc);
			m_pTransformCom->Go_ToTarget_ChangeDirection(&vPosAcc, fTimeDelta * m_fMoveSpeed);
			if (m_mapCoolTime[L"DEADHARDTime"] > 0.f)
			{
				m_pTransformCom->Go_ToTarget_ChangeDirection(&vPosAcc, fTimeDelta * 3.f * m_fMoveSpeed);
				m_eCurState = AC::M_InjuredDeadHard;
				return;
			}	

			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::Run;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_Run;
			else if (m_eCurCondition == DYING)
				m_eCurState = AC::CrawlFT;
		}
		else
		{
			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::Idle;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_Idle;
			else if (m_eCurCondition == DYING)
				m_eCurState = AC::CrawlToStand;
		}
	}
	else if(!m_bIsLockKey)
	{
		if (m_eCurCondition == HEALTHY)
			m_eCurState = AC::Walk;
		else if (m_eCurCondition == INJURED)
			m_eCurState = AC::Injured_Walk;
		else if (m_eCurCondition == DYING)
			m_eCurState = AC::CrawlFT;

		if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D))
		{
			vPosAcc = (vPosAcc / (_float)iCount);
			D3DXVec3Normalize(&vPosAcc, &vPosAcc);
			m_pTransformCom->Go_ToTarget_ChangeDirection(&vPosAcc, fTimeDelta * 0.5f * m_fMoveSpeed);
		}
		else
		{
			if (m_eCurCondition == HEALTHY)
				m_eCurState = AC::Idle;
			else if (m_eCurCondition == INJURED)
				m_eCurState = AC::Injured_Idle;
			else if (m_eCurCondition == DYING)
				m_eCurState = AC::CrawlToStand;
		}
	}

	if (m_isBool)
		m_eCurState = AC::M_Fall;

	//_matrix mat;
	//D3DXMatrixIdentity(&mat);
	//mat._41 = 3258.f;
	//mat._43 = 5157.f;
	//mat._44 = 1.f;

	/*if (KEYMGR->KeyDown(DIK_Z))
		EFFMGR->Make_Effect(CEffectManager::E_Fail, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));*/
	//if (KEYMGR->KeyDown(DIK_X))
	//	EFFMGR->Make_Effect(CEffectManager::E_Exit,*m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	//if (KEYMGR->KeyDown(DIK_C))
	//	EFFMGR->Make_Effect(CEffectManager::E_Change, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	//if (KEYMGR->KeyDown(DIK_V))
	//	EFFMGR->Make_Effect(CEffectManager::E_Jump, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	//
	//if (KEYMGR->KeyDown(DIK_B))
	//	EFFMGR->Make_Effect(CEffectManager::E_FootPrint, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	//if (KEYMGR->KeyDown(DIK_N))
	//	EFFMGR->Make_BG(CEffectManager::E_BG_GENON,mat);
	//if (KEYMGR->KeyDown(DIK_M))
	//	EFFMGR->Make_Effect(CEffectManager::E_Death, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	//if (KEYMGR->KeyDown(DIK_L))
	//	EFFMGR->Make_Effect(CEffectManager::E_Blood, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	//if (KEYMGR->KeyDown(DIK_K))
	//	EFFMGR->Make_Effect(CEffectManager::E_ExitOpen, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	
	/*if (KEYMGR->KeyDown(DIK_J))
			EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
	_vec3 vDir = _vec3(1.f, 1.f, 0.f);	
	if (KEYMGR->KeyDown(DIK_K))
		EFFMGR->Make_BloodSpread(_vec3(3200.f,150.f,3200.f), vDir);

	if (KEYMGR->KeyDown(DIK_L))
		EFFMGR->Make_Effect(CEffectManager::E_HookDeath,*m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));*/
}

void CCamper::State_Check()
{
	Update_Sound();
	//if (m_fHitedTime > 1.f)
	//{
	//	ANIMSET Anim;
	//	if ((m_fHitedTime - m_fTimeDelta) < 1.f)
	//	{
	//		m_fHitedTime -= m_fTimeDelta;
	//		Anim.iOffsetAnimationID = m_eCurState;
	//		Anim.iBlendAnimationID = m_eCurState;
	//		Anim.fOffsetWeight = 0.5f;
	//		Anim.fBlendWeight = 0.5f;
	//		m_pMeshCom->Set_SpecifyTorsoAnimationSet(Anim);
	//	}
	//	else
	//	{
	//		_float fWeight = (m_fHitedTime - 1.f) * 2.f;
	//		ANIMSET Anim;
	//		Anim.iOffsetAnimationID = AC::Flash_Stand_DOWN;
	//		Anim.iBlendAnimationID = AC::Flash_Stand_UP;
	//		Anim.fOffsetWeight = m_fHitedTime > 1.5f ? min(max(fWeight - 1.f, 0.f), 1.f) : min(max(1.f - fWeight, 0.f), 1.f);
	//		Anim.fBlendWeight = min(max(1.f - Anim.fOffsetWeight, 0.f), 1.f);
	//		m_pMeshCom->Set_SpecifyTorsoAnimationSet(Anim);
	//	}
	//}
	//if (m_eCurState == AC::GeneratorFail
	//	|| m_eCurState == AC::BeingHealFail
	//	|| m_eCurState == AC::HealCamperFail)
	//{
	//	if(!m_pMeshCom->IsOverTime(0.28f))
	//		return;
	//}

	if (m_eCurState == m_eOldState)
		return;

	/*if (m_eCurState == AC::Flash_Stand_FT)
		return;
	*/
	if (m_eCurState == AC::ClosetHideFast || m_eOldState == AC::ClosetHideFast || m_eCurState == AC::DeathGround || m_eCurState == AC::TT_Grab_Locker || m_eCurState == AC::WI_Grab_Locker
		|| m_eCurState == AC::TT_Grab_Obstacles_BK || m_eCurState == AC::WI_Grab_Obstacle_BK || m_eCurState == AC::TT_Grab_Obstacles_FT || m_eCurState == AC::WI_Grab_Obstacle_FT ||
		m_eCurState == AC::TT_Grab_Generic_Fast || m_eCurState == AC::WI_Grab_Generic_Fast)
		m_pMeshCom->Set_NoBleningAnimationSet(m_eCurState);
	else
	{
		m_pMeshCom->Set_AnimationSet(m_eCurState);
		m_pMeshCom->Set_TorsoAnimationSet(m_eCurState);
		m_pMeshCom->Set_ServeAnimationSet(m_eCurState);

		if (m_pItem != nullptr && !m_bIsLockKey)
		{
			if (KEYMGR->KeyPressing(DIK_LSHIFT))
			{
				if (m_eCurCondition != HEALTHY && m_eCurCondition != INJURED)
				{
					if (KEYMGR->KeyPressing(DIK_W) || KEYMGR->KeyPressing(DIK_A) || KEYMGR->KeyPressing(DIK_S) || KEYMGR->KeyPressing(DIK_D))
						m_pMeshCom->Set_ArmAnimationSet(AC::Toolbox_WalkArmOverride);
					else
						m_pMeshCom->Set_ArmAnimationSet(AC::Toolbox_RunArmOverride);
				}
				else
					m_pMeshCom->Set_ArmAnimationSet(AC::Toolbox_IdleArmOverride);
			}
			else
				m_pMeshCom->Set_ArmAnimationSet(AC::Toolbox_IdleArmOverride);
		}

	}

	if (exPlayerNumber == 5)
	{
		if (m_eOldState != m_eCurState && (m_eCurState == AC::GeneratorFail || m_eCurState == AC::HookedFree))
		{
			//_vec3 vSlasherPos = server_data.Slasher.vPos;
			//_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

			//_float fDistance = D3DXVec3Length(&(vSlasherPos - vMyPos));
			//if(fDistance < 1000.f)
				EFFMGR->Make_Effect(CEffectManager::E_Fail, *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
		}
	}
	else if (exPlayerNumber != 5 && exPlayerNumber != m_iIndex)
	{
	
		if (m_eOldState != m_eCurState && m_eCurState == AC::HookBeingCamperEnd)
		{
			


			m_bPenetration = true;
			m_fPenetrationTime = 5.f;
			m_Color = { 0.6f, 0.f, 0.f, 1.f };

			
		}

		//if (m_eOldState != m_eCurState && m_eCurState == AC::BeingKilledBySpiderLoop)
		//{
		//	cout << "dd" << endl;
		//}

		
		if (m_eOldState != m_eCurState && (m_eCurState == AC::TT_Hook_In || m_eCurState == AC::WI_Hook_In))
		{
			
			CManagement* pManagement = CManagement::GetInstance();
			if (nullptr == pManagement)
				return;
			pManagement->AddRef();

			CTransform* curTrans = nullptr;
			_float		fDist = 10000.f;

			list<CGameObject*> pList = pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject");

			//for(pList.begin(); pList.end)
			for (list<CGameObject*>::iterator it = pList.begin(); it != pList.end(); it++)
			{
				if ((*it)->GetID()&HOOK)
				{
					CTransform* pTrans = (CTransform*)(*it)->Get_ComponentPointer(L"Com_Transform");
					_vec3 vDist = *pTrans->Get_StateInfo(CTransform::STATE_POSITION) - *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
					if (D3DXVec3Length(&vDist) < fDist)
					{
						fDist = D3DXVec3Length(&vDist);
						curTrans = pTrans;
					}
				}
			}


		/*	_vec3 vFinalPos = server_data.Campers[m_eObjectID - CAMPER].vPos - (server_data.Campers[m_eObjectID - CAMPER].vLook * 50.f);
			vFinalPos.y = 0.0f;

			memcpy(&matOut.m[0], &server_data.Campers[m_eObjectID - CAMPER].vRight, sizeof(_vec3));
			memcpy(&matOut.m[1], &server_data.Campers[m_eObjectID - CAMPER].vUp, sizeof(_vec3));
			memcpy(&matOut.m[2], &server_data.Campers[m_eObjectID - CAMPER].vLook, sizeof(_vec3));
			memcpy(&matOut.m[3], &vFinalPos, sizeof(_vec3));*/

			EFFMGR->Make_BG(CEffectManager::E_BG_HOOKON, curTrans->Get_Matrix(), this);
			Safe_Release(pManagement);
		}
	}

	if(exPlayerNumber != 5)
	{
		if (m_eOldState != m_eCurState && m_eCurState == AC::BeingKilledBySpiderIN)
		{
			//CTransform* pTrans = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
			_vec3 vPos1 = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
			_vec3 vPos2 = vPos1 + *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK)*200.f;
			vPos1.y = 0.f;
			vPos2.y = 0.f;
			EFFMGR->Make_Effect(CEffectManager::E_HookDeath, vPos1);
			EFFMGR->Make_Effect(CEffectManager::E_Death, vPos2);
		}



		if (m_eOldState != m_eCurState && m_eCurState == AC::GeneratorFail)
		{


			CManagement* pManagement = CManagement::GetInstance();
			if (nullptr == pManagement)
				return;
			pManagement->AddRef();

			CTransform* curTrans = nullptr;
			_float		fDist = 10000.f;

			list<CGameObject*> pList = pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject");

			//for(pList.begin(); pList.end)
			for (list<CGameObject*>::iterator it = pList.begin(); it != pList.end(); it++)
			{
				if ((*it)->GetID()&GENERATOR)
				{
					CTransform* pTrans = (CTransform*)(*it)->Get_ComponentPointer(L"Com_Transform");
					_vec3 vDist = *pTrans->Get_StateInfo(CTransform::STATE_POSITION) - *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
					if (D3DXVec3Length(&vDist) < fDist)
					{
						fDist = D3DXVec3Length(&vDist);
						curTrans = pTrans;
					}
				}
			}


			EFFMGR->Make_Effect(CEffectManager::E_Generator, *curTrans->Get_StateInfo(CTransform::STATE_POSITION));

			Safe_Release(pManagement);
		}


		
	}

	m_eOldState = m_eCurState;
}

void CCamper::Check_Attacked(const _float& fTimeDelta)
{
	if (m_fAttackedTime > 0.f)
		return;

	if (m_mapCoolTime[L"HOOK_BEINGBORROWEDHIT"] > 0.01f)
		return;

	if (m_mapCoolTime[L"HOOK_BEINGBORROWED"] >= 0.01f)
	{
		m_fAttackedTime = 2.f;
		m_mapCoolTime[L"HOOK_BEINGBORROWED"] = 0.f;
		m_mapCoolTime[L"HOOK_BEINGBORROWEDHIT"] = 20.f;
		return;
	}

	if (m_eCurCondition != HEALTHY && m_eCurCondition != INJURED
		&& m_eCurCondition != SPECIAL && m_eCurCondition != OBSTACLE && m_eCurCondition != HOOKED)
		return;

	if (m_bIsCarried)
		return;

	if (AC::ClosetIdle == m_eCurState)
		return;

	server_data.Slasher.SecondInterationObject = 0;
	server_data.Slasher.SecondInterationObjAnimation = 0;

	if (server_data.Game_Data.Curse & CINCANTATION)
	{
		Assert_EndAllAction(L"Action_CrawlToHit");

		BloodSpread(LEFT);
		BloodSpread(TOP);
		BloodSpread(RIGHT);
		BloodSpread(BACK);
		BloodSpread(FRONT);
		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		Set_Action(L"Action_CrawlToHit", 10.f);
		m_fHeal = 0.0f;
		return;
	}

	switch (m_eCurCondition)
	{
	case HEALTHY:
		m_fAttackedTime = 2.f;
		BloodSpread(TOP);
		m_eCurCondition = INJURED;
		m_fHeal = 0.0f;
		break;
	case INJURED:
		m_fAttackedTime = 2.f;
		m_fHeal = 0.0f;
		Assert_EndAllAction(L"Action_CrawlToHit");
		BloodSpread(LEFT);
		BloodSpread(TOP);
		BloodSpread(RIGHT);
		BloodSpread(BACK);
		BloodSpread(FRONT);
		EFFMGR->Make_Effect(CEffectManager::E_BloodUI);
		Set_Action(L"Action_CrawlToHit", 10.f);
		break;
	case OBSTACLE:
	case SPECIAL:
		m_fAttackedTime = 2.f;
		if (m_eOldCondition == HEALTHY)
		{
			BloodSpread(TOP);
			m_eOldCondition = m_eCurCondition = INJURED;
			m_fHeal = 0.f;
			m_fMaxHeal = 30.f;
		}
		else if (m_eOldCondition == INJURED)
		{
			BloodSpread(TOP);
			m_eOldCondition = m_eCurCondition = DYING;
			m_fHeal = 0.f;
			m_fMaxHeal = 30.f;
			m_fEnergy = m_fDyingEnergy;
		}
		break;
	case HOOKED:
		break;
	}

}

void CCamper::MoveSpeed_Check()
{
	if (m_fAttackedTime > 0.f)
	{
		m_fMoveSpeed = 1.8f;
		return;
	}

	switch (m_eCurCondition)
	{
	case HEALTHY:
		m_bPlayAnimation = true;
		m_fMoveSpeed = 1.f;
		break;
	case INJURED:
		m_bPlayAnimation = true;
		m_fMoveSpeed = 1.f;
		break;
	case DYING:
		m_fMoveSpeed = 0.1f;
		break;
	default:
		m_bPlayAnimation = true;
		m_fMoveSpeed = 1.f;
		break;
	}
}

void CCamper::IsCarried(const _float& fTimeDelta)
{
	if (exPlayerNumber != m_iIndex)
		return;


	m_isColl = false;
	CManagement* pManagement = CManagement::GetInstance();

	//_matrix matSlasher;
	//D3DXMatrixIdentity(&matSlasher);
	//memcpy(&matSlasher.m[0], &server_data.Slasher.vRight, sizeof(_vec3));
	//memcpy(&matSlasher.m[1], &server_data.Slasher.vUp, sizeof(_vec3));
	//memcpy(&matSlasher.m[2], &server_data.Slasher.vLook, sizeof(_vec3));
	//memcpy(&matSlasher.m[3], &server_data.Slasher.vPos, sizeof(_vec3));

	CTransform* pTransform = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Transform");
	if (nullptr == pTransform)
		return;
	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	if (pSlasher == nullptr)
		return;

	_matrix matCamperAttach;
	
	memcpy(&matCamperAttach.m[0], &-*(_vec4*)&pSlasher->Get_CarryMatrix()->m[0], sizeof(_vec4));
	memcpy(&matCamperAttach.m[1], &*(_vec4*)&pSlasher->Get_CarryMatrix()->m[2], sizeof(_vec4));
	memcpy(&matCamperAttach.m[2], &*(_vec4*)&pSlasher->Get_CarryMatrix()->m[1], sizeof(_vec4));
	memcpy(&matCamperAttach.m[3], &pSlasher->Get_CarryMatrix()->m[3], sizeof(_vec4));

	_matrix matSlasher = pTransform->Get_Matrix();
	
	matSlasher = matCamperAttach * matSlasher;

	if(m_bIsCarried)
		m_pTransformCom->Set_Matrix(matSlasher);

	if (m_bIsLockKey)
		return;

	if (server_data.Slasher.iState == AS::Stun_Drop_In)
	{
		Set_Action(L"Action_StunDrop", 5.f);
		m_bIsCarried = false;
		return;
	}

	if ((server_data.Campers[m_iIndex - 1].Perk & CRUCIALBLOW) && !m_bUseCalZZi && m_fProgressTime >= m_fMaxProgressTime * 0.3f)
	{
		camper_data.InterationObject = 1;

		if (!m_bCalZZi)
		{
			m_bCalZZi = true;
			GET_INSTANCE(CUIManager)->Set_SkillCheckState(0);
			GET_INSTANCE(CUIManager)->Set_CalZZi();
		}
		
		_int SkillCheck = GET_INSTANCE(CUIManager)->Get_SkillCheckState();

		if (SkillCheck == SUCCESS_SKILL_CHECK || SkillCheck == SUCCESS_CHECK)
		{
			m_bUseCalZZi = true;
			camper_data.SecondInterationObject = SLASHER;
			camper_data.SecondInterationObjAnimation = CALZZI;
			Set_Action(L"Action_StunDrop", 5.f);
		}
	}

	if (m_fProgressTime > m_fMaxProgressTime)
	{
		GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_WiggleEnd); //내릴때
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None); //내릴때  
		camper_data.SecondInterationObject = SLASHER;
		camper_data.SecondInterationObjAnimation = STUNDROP;
		Set_Action(L"Action_StunDrop", 5.f);
		m_bIsCarried = false;
		bCalZZi = false;
		m_bCalZZi = false;
		return;
	}

	if (GET_INSTANCE(CUIManager)->Get_KeyEvent() == CUIManager::EV_LeftWiggle) // 좌로 위글
	{		
		camper_data.Packet = WIGGLE_LEFT;
		pTransform->Go_Left(fTimeDelta);
		server_data.Slasher.iCharacter == CSlasher::C_WRAITH ? m_eCurState = AC::TT_Wiggle : m_eCurState = AC::WI_Wiggle;
		m_fProgressTime += fTimeDelta;
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fProgressTime, m_fMaxProgressTime);
	}
	else if (GET_INSTANCE(CUIManager)->Get_KeyEvent() == CUIManager::EV_RightWiggle) // 우로 위글
	{
		camper_data.Packet = WIGGLE_RIGHT;
		pTransform->Go_Left(fTimeDelta);
		server_data.Slasher.iCharacter == CSlasher::C_WRAITH ? m_eCurState = AC::TT_Wiggle : m_eCurState = AC::WI_Wiggle;
		m_fProgressTime += fTimeDelta;
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(m_fProgressTime, m_fMaxProgressTime);
	}
	else 
	{
		if ((m_eCurState == AC::TT_Wiggle || m_eCurState == AC::WI_Wiggle) && m_pMeshCom->IsOverTime(0.3f))
			server_data.Slasher.iCharacter == CSlasher::C_WRAITH ? m_eCurState = AC::TT_Carry_Idle : m_eCurState = AC::WI_Carry_Idle;
	}
}

void CCamper::Processing_Others(const _float & fTimeDelta)
{
	m_iCollState = 0;										// 모름
	m_fTimeDelta = fTimeDelta;								// For_Player_Animation fTimeDelta
	m_pTargetOfInteraction = nullptr;						// For_Init_Intersect_Object
	m_pCamperOfInteraction = nullptr;						// For_Init_Intersect_Camper
	m_vTargetPos = { 0.f, 0.f, 0.f };						// For_Init_TargetPos
	m_vTargetCamperPos = { 0.f, 0.f, 0.f };					// For_Init_TargetCamperPos
	if (m_pItem != nullptr)									// For_Visible_Item
	{
		if (m_bIsLockKey)
			m_pItem->SetVisible(false);
		else
			m_pItem->SetVisible(true);
	}

	if (!m_bIsCarried && m_eCurCondition == DYING)			// For_Decrease_Energy_Of_Dying
		m_fEnergy -= fTimeDelta;

	if (m_fEnergy <= 0.f && m_eCurCondition == DYING)
	{
		m_bIsLockKey = true;
		m_eCurState = AC::DeathGround;
	}

	if (m_bSpiritDisapear && m_bInitEffect)
	{
		if (m_fDissolveTime < 1.f)
			m_fDissolveTime += fTimeDelta * 2.f;
		else
		{
			m_fDissolveTime = 1.f;
		}
	}
	else if(m_bInitEffect)
	{
		m_fDissolveTime = 0.f;
	}

	if (m_fAttackedTime > 0.f)
		m_fAttackedTime -= fTimeDelta;

	if (!m_bIsCarried)
		bCalZZi = false;
}

void CCamper::Use_FlashLight(const _float & fTimeDelta)
{
	CManagement* pManagement = CManagement::GetInstance();

	_vec3 vCamLook; 
	_vec3 vMyUp;

	if (exPlayerNumber == m_iIndex)
	{
		CCamera_Camper* pCamOfCamper = (CCamera_Camper*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Camera", 0);
		CTransform* pCamTransform = (CTransform*)pCamOfCamper->Get_ComponentPointer(L"Com_Transform");
		vCamLook = *pCamTransform->Get_StateInfo(CTransform::STATE_LOOK);
		vMyUp = *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
	}
	else
	{
		vCamLook = server_data.Campers[m_iIndex - 1].vCamLook;
		vMyUp = *m_pTransformCom->Get_StateInfo(CTransform::STATE_UP);
	}

	_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vCamLook, &vMyUp)));
	_float fUpWeight, fDownWeight;
	
	if (fAngle <= 45.f)
	{
		fUpWeight = 1.f;
		fDownWeight = 0.f;
	}
	else if (fAngle >= 135.f)
	{
		fUpWeight = 0.f;
		fDownWeight = 1.f;
	}
	else
	{	// 45 ~ 135 -> 0.f ~ 1.f
		fDownWeight = (fAngle - 45.f) / 90.f;
		fUpWeight = 1.f - fDownWeight;
	}

	ANIMSET Anim;
	Anim.iOffsetAnimationID = AC::Flash_Stand_UP;
	Anim.iBlendAnimationID = AC::Flash_Stand_DOWN;
	Anim.fOffsetWeight = fUpWeight;
	Anim.fBlendWeight = fDownWeight;
	m_eCurState = AC::Flash_Stand_FT;
	m_pMeshCom->Set_ServeAnimationSet(AC::Flash_Stand_FT);
	m_pMeshCom->Set_SpecifyTorsoAnimationSet(Anim);
	m_pMeshCom->Set_AnimationSet(AC::Flash_Stand_FT);
}

CCamper * CCamper::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCamper*	pInstance = new CCamper(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCamper Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCamper::Clone_GameObject()
{
	CCamper*	pInstance = new CCamper(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCamper Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CCamper::Free()
{
	m_mapCoolTime.clear();
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_PLAYER, this);

	Safe_Release(m_pFootCollider[0]);
	Safe_Release(m_pFootCollider[1]);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pColliderCom);

	Safe_Release(m_pCustomLight);

	CGameObject::Free();
	
}