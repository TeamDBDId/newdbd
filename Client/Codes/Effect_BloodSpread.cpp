#include "stdafx.h"
#include "Effect_BloodSpread.h"
#include "Management.h"

#include "Effect_BloodBall.h"

#include "Math_Manager.h"
_USING(Client)

CEffect_BloodSpread::CEffect_BloodSpread(LPDIRECT3DDEVICE9 _pGDevice)
	: CGameObject(_pGDevice)
{
}

CEffect_BloodSpread::CEffect_BloodSpread(const CEffect_BloodSpread & _rhs)
	: CGameObject(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_BloodSpread::Ready_Prototype()
{


	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_BloodSpread::Ready_GameObject()
{

	return NOERROR;
}

_int CEffect_BloodSpread::Update_GameObject(const _float & _fTick)
{
	if (true == m_isDead)
		return DEAD_OBJ;

	m_fLifeTime += _fTick;
	if (0.3f < m_fLifeTime)
		m_isDead = true;


	Make_BloodBall(_fTick);


	return _int();
}

_int CEffect_BloodSpread::LastUpdate_GameObject(const _float & _fTick)
{



	return _int();
}

void CEffect_BloodSpread::Render_GameObject()
{

}

void CEffect_BloodSpread::Set_Param(const _vec3 & _vPos, const _vec3 & _vDir)
{
	m_vPos = _vPos;
	D3DXVec3Normalize(&m_vDir, &_vDir);
}


void CEffect_BloodSpread::Make_BloodBall(const _float & _fTick)
{
	m_fBloodTime += _fTick;
	if (m_fBloodTime < BALLDELAY)
		return;
	m_fBloodTime = 0.f;

	//m_vDir *= -1.f;

	GET_INSTANCE_MANAGEMENT;

	CEffect_BloodBall* pEffect = nullptr;
	if (FAILED(pManagement->Add_GameObjectToLayer(L"Obj_Effect_BloodBall", SCENE_STAGE, L"Layer_BloodBall", (CGameObject**)&pEffect)))
		_MSG_BOX("Fail CEffect_BloodBall");
	pEffect->Set_Param(m_vPos, m_vDir);

	Safe_Release(pManagement);
}



// 원형객체를 생성하기위해 만들어진 함수.
CEffect_BloodSpread * CEffect_BloodSpread::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_BloodSpread*	pInst = new CEffect_BloodSpread(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_BloodSpread::Clone_GameObject()
{
	CEffect_BloodSpread*	pInst = new CEffect_BloodSpread(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_BloodSpread Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_BloodSpread::Free()
{

	CGameObject::Free();
}