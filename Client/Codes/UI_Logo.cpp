#include "stdafx.h"
#include "..\Headers\UI_Logo.h"
#include "Management.h"
#include "UI_Texture.h"

_USING(Client);




CUI_Logo::CUI_Logo(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_Logo::CUI_Logo(const CUI_Logo & rhs)
	: CGameObject(rhs)
{
}


HRESULT CUI_Logo::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_Logo::Ready_GameObject()
{
	Set_BaseTexture();
	Set_Sound();
	return NOERROR;
}

_int CUI_Logo::Update_GameObject(const _float & fTimeDelta)
{
	Check_LogoFadeEnd();
	Check_StateTexture();
	return _int();
}

_int CUI_Logo::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CUI_Logo::Set_Sound()
{
	GET_INSTANCE(CSoundManager)->PlaySound(string("Logo_BGM"),string("Logo/Logo.ogg"));
}

void CUI_Logo::Set_BaseTexture()
{
	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pUI_Texture = nullptr;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOGO, L"Layer_UI", (CGameObject**)&pUI_Texture);
	pUI_Texture->Set_TexName(L"Black.tga");
	pUI_Texture->Set_Scale(_vec2((_float)g_iBackCX, (_float)g_iBackCY),false);
	pUI_Texture->Set_Pos(_vec2(0.5f * g_iBackCX, 0.5f * g_iBackCY));

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOGO, L"Layer_UI", (CGameObject**)&m_pLogoTexture);
	m_pLogoTexture->Set_TexName(L"DBD_Logo.tga");
	m_pLogoTexture->Set_Scale(_vec2(0.3f, 0.3f));
	m_pLogoTexture->Set_Pos(_vec2(0.5f* g_iBackCX, 0.45f*g_iBackCY));
	m_pLogoTexture->Set_FadeIn(2.f);
	m_LogoFadeEnd = false;
	Safe_Release(pManagement);
}

void CUI_Logo::Check_LogoFadeEnd()
{
	if (m_pLogoTexture == nullptr || m_LogoFadeEnd)
		return;

	if (m_pLogoTexture->Get_FadeTime() != 0.f)
		return;


	GET_INSTANCE_MANAGEMENT;

	CUI_Texture* pUI_Texture = nullptr;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOGO, L"Layer_UI", (CGameObject**)&pUI_Texture);
	pUI_Texture->Set_Font(L"DEAD BY DAYLIGHT", _vec2(18.f, 30.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f),2);
	pUI_Texture->Set_Pos(_vec2(0.488454f * g_iBackCX, 0.86f * g_iBackCY));
	pUI_Texture->Set_FadeIn(0.3f);

	pUI_Texture = nullptr;
	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOGO, L"Layer_UI", (CGameObject**)&pUI_Texture);
	pUI_Texture->Set_Font(L"죽음은 탈출구가 아니다", _vec2(15.f, 17.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	pUI_Texture->Set_Pos(_vec2(0.488454f * g_iBackCX, 0.903568f * g_iBackCY));
	pUI_Texture->Set_FadeIn(0.3f);

	pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_LOGO, L"Layer_UI", (CGameObject**)&m_pStateTexture);
	m_pStateTexture->Set_Font(L"스페이스바를 눌러 계속", _vec2(16.f, 20.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f),1);
	m_pStateTexture->Set_Pos(_vec2(0.488454f * g_iBackCX, 0.745313f * g_iBackCY));
	m_pStateTexture->Set_FadeIn(1.5f);
	m_LogoFadeEnd = true;
	Safe_Release(pManagement);
}

void CUI_Logo::Check_StateTexture()
{
	if (m_pStateTexture == nullptr)
		return;

	_uint State;
	if (0.f == m_pStateTexture->Get_FadeTime(&State))
	{
		if (State == 1)
			m_pStateTexture->Set_FadeOut(2.f);
		else if (State == 2)
			m_pStateTexture->Set_FadeIn(0.5f);
	}
}

CUI_Logo * CUI_Logo::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_Logo*	pInstance = new CUI_Logo(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_Logo Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_Logo::Clone_GameObject()
{
	CUI_Logo*	pInstance = new CUI_Logo(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_Logo Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_Logo::Free()
{
	CGameObject::Free();
}
