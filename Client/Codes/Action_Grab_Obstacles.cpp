#include "stdafx.h"
#include "..\Headers\Action_Grab_Obstacles.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "Slasher.h"

_USING(Client)

CAction_Grab_Obstacles::CAction_Grab_Obstacles()
{
}

HRESULT CAction_Grab_Obstacles::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	if (m_fDelay > 0.f)
		return NOERROR;

	m_bIsPlaying = true;
	m_fIndex = 0.f;
	if (m_pGameObject->GetID() & SLASHER)
	{
		CTransform* pSlasherTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		if (pSlasherTransform == nullptr)
			return E_FAIL;

		_vec3 vSlasherLook = *pSlasherTransform->Get_StateInfo(CTransform::STATE_LOOK);
		
		CTransform* pCamperTransform = (CTransform*)m_pCamper->Get_ComponentPointer(L"Com_Transform");
		if (pCamperTransform == nullptr)
			return E_FAIL;
		_vec3 vCamperLook = *pCamperTransform->Get_StateInfo(CTransform::STATE_LOOK);

		_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vSlasherLook, &vCamperLook)));
		m_pCamper->SetColl(false);

		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->IsLockKey(true);
		pSlasher->Set_CarriedCamper(m_pCamper);
		pSlasher->Set_Carry(true);
		if (fAngle <= 90.f)
		{
			pSlasher->Set_State(AS::Grab_Obstacle_BK);
			m_iState = AS::Grab_Obstacle_BK;
		}
		else
		{
			pSlasher->Set_State(AS::Grab_Obstacle_FT);
			m_iState = AS::Grab_Obstacle_FT;
		}
		Send_ServerData();
	}
	else
	{
		CManagement* pManagement = CManagement::GetInstance();
		CTransform* pSlasherTransform = (CTransform*)pManagement->Get_ComponentPointer(SCENE_STAGE, L"Layer_Slasher", L"Com_Transform", 0);
		if (pSlasherTransform == nullptr)
			return E_FAIL;
		_vec3 vSlasherLook = *pSlasherTransform->Get_StateInfo(CTransform::STATE_LOOK);

		CTransform* pCamperTransform = (CTransform*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		_vec3 vCamperLook = *pCamperTransform->Get_StateInfo(CTransform::STATE_LOOK);
		if (pCamperTransform == nullptr)
			return E_FAIL;
		_float fAngle = D3DXToDegree(acosf(D3DXVec3Dot(&vSlasherLook, &vCamperLook)));
		
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->SetCurCondition(CCamper::DYING);
		pCamper->SetColl(false);
		pCamper->IsLockKey(true);
		pCamper->SetCarry(true);

		if (fAngle <= 90.f)
		{
			if (server_data.Slasher.iCharacter == 0)
				pCamper->Set_State(AC::TT_Grab_Obstacles_BK);
			else
				pCamper->Set_State(AC::WI_Grab_Obstacle_BK);
			m_iState = AC::TT_Grab_Obstacles_FT;
		}
		else
		{
			if (server_data.Slasher.iCharacter == 0)
				pCamper->Set_State(AC::TT_Grab_Obstacles_FT);
			else
				pCamper->Set_State(AC::WI_Grab_Obstacle_FT);
			m_iState = AC::TT_Grab_Obstacles_FT;
		}
	}

	return NOERROR;
}

_int CAction_Grab_Obstacles::Update_Action(const _float & fTimeDelta)
{
	if (m_fDelay > 0.0f)
		m_fDelay -= fTimeDelta;

	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	m_fIndex += fTimeDelta * 30.f;
	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");
	size_t iIndex = (size_t)m_fIndex;
	if (m_iState == AC::TT_Grab_Obstacles_FT)
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		if (pMeshCom->IsOverTime(0.3f) || iIndex >= 71)
		{
			server_data.Slasher.iCharacter == 0 ? pCamper->Set_State(AC::TT_Carry_Idle) : pCamper->Set_State(AC::WI_Carry_Idle);
			return END_ACTION;
		}

	}
	else
	{
		if (pMeshCom->IsOverTime(0.3f))
		{
			CSlasher* pSlasher = (CSlasher*)m_pGameObject;
			pSlasher->Set_State(AS::Carry_Idle);
			return END_ACTION;
		}
	}


	return UPDATE_ACTION;
}

void CAction_Grab_Obstacles::End_Action()
{
	m_fDelay = 1.f;
	m_bIsPlaying = false;

	if (m_pGameObject->GetID() & SLASHER)
	{
		CSlasher* pSlasher = (CSlasher*)m_pGameObject;
		pSlasher->Set_Carry(true);
		pSlasher->Set_CarriedCamper(m_pCamper);
		pSlasher->IsLockKey(false);
	}
	else
	{
		CCamper* pCamper = (CCamper*)m_pGameObject;
		pCamper->SetHeal(0.f);
		pCamper->SetEnergy(pCamper->GetDyingEnergy());
		pCamper->SetMaxEnergyAndMaxHeal();
		pCamper->IsLockKey(false);
		pCamper->SetColl(false);
		pCamper->SetCarry(true);
		GET_INSTANCE(CUIManager)->Set_KeyEvent(CUIManager::EV_SetWiggle); // 들었을때
		GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_Wiggle); // 들었을때
		GET_INSTANCE(CUIManager)->Set_ProgressPoint(pCamper->Get_ProgressTime(), pCamper->Get_MaxProgressTime());
		//server_data.Slasher.iCharacter == 0 ? pCamper->Set_State(AC::TT_Carry_Idle) : pCamper->Set_State(AC::WI_Carry_Idle);
	}

	m_pCamper = nullptr;
}

void CAction_Grab_Obstacles::Send_ServerData()
{
	if (m_pGameObject->GetID() & SLASHER)
	{
		slasher_data.SecondInterationObject = m_pCamper->GetID();
		slasher_data.SecondInterationObjAnimation = GRAB_OBSTACLES;
	}
}

void CAction_Grab_Obstacles::SetVectorPos()
{
	m_vecPosBK.reserve(81);

	m_vecPosBK.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.036f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.051f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.062f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.07f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.08f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.094f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.114f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.144f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.32f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.443f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.564f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.71f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -0.872f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.049f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.243f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.44f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -1.691f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -2.164f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -6.696f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -16.269f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -29.003f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -43.018f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -56.435f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -67.374f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -75.647f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -81.526f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.827f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -83.866f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -85.256f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -88.573f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -92.743f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -96.314f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -98.804f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.63f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.934f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.977f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.976f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.974f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.972f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.969f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.967f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.964f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.961f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.958f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.955f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.952f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.948f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.945f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.942f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.938f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.935f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.931f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.928f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.924f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.921f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.918f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.915f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.912f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.909f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.906f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.904f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.901f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.899f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.897f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.895f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.894f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.893f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.892f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.891f));
	m_vecPosBK.push_back(_vec3(0.f, 0.f, -99.891f));

	m_vecSpiritPosBK.reserve(81);

	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.031f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.067f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.082f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.092f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.101f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.111f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.124f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.145f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.175f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.351f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.474f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.595f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.741f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -0.903f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -1.08f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -1.274f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -1.47f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -1.721f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -2.194f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -6.726f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -16.299f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -29.033f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -43.049f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -56.466f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -67.405f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -75.677f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -81.557f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.858f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -83.896f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -85.287f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -88.604f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -92.774f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -96.345f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -98.835f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.661f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.965f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -100.008f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -100.006f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -100.004f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -100.002f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -100.0f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.997f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.995f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.992f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.989f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.986f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.982f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.979f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.976f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.972f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.969f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.965f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.962f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.959f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.955f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.952f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.949f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.946f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.942f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.94f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.937f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.934f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.932f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.93f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.928f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.926f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.925f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.924f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.923f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.922f));
	m_vecSpiritPosBK.push_back(_vec3(0.f, 0.f, -99.922f));

	m_vecPosFT.reserve(86);

	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 0.802f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 3.091f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 6.692f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 11.43f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 17.131f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 23.619f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 30.719f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 38.256f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 46.056f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 53.944f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 61.744f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 69.281f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 76.381f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 82.869f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 88.57f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 93.308f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 96.909f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 99.198f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));
	m_vecPosFT.push_back(_vec3(0.f, 0.f, 100.f));



	m_bIsInit = true;
}

void CAction_Grab_Obstacles::Free()
{
	CAction::Free();
}
