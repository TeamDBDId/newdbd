#include "stdafx.h"
#include "..\Headers\ServerManager.h"
#include <fstream>
#include <string>

_USING(Client)

_IMPLEMENT_SINGLETON(CServerManager)
IN_ADDR GetDefaultMyIP();
CServerManager::CServerManager()
{

}

void CServerManager::ErrorHandling(char *message) {
	LPVOID lpMsgBuf;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf, 0, NULL);

	fputs(message, stderr);
	printf("%s", (char *)lpMsgBuf);
	fputc('\n', stderr);
	exit(1);
}

void CServerManager::LoadIPAddr()
{
	ifstream in("../bin/IPAdress.txt");
	getline(in, ServerIpAddr);

	in.close();
}

SOCKET CServerManager::Ready_RobbyServer()
{
	LoadIPAddr();
	ZeroMemory(&dataBuf, sizeof(SOCKETINFO));

	WSADATA wsaData;
	SOCKET hSocket;
	SOCKADDR_IN recvAddr;

	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) /* Load Winsock 2.2 DLL */
		ErrorHandling("WSAStartup() error!");

	IN_ADDR myIp = GetDefaultMyIP();
	strcpy_s(MyIPAdress, inet_ntoa(myIp));
	
	hSocket = WSASocket(PF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (hSocket == INVALID_SOCKET)
		ErrorHandling("socket() error");

	memset(&recvAddr, 0, sizeof(recvAddr));
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_addr.s_addr = inet_addr(ServerIpAddr.c_str());
	recvAddr.sin_port = htons(atoi("10004"));

	strcpy_s(lobby_data.IPAdress, MyIPAdress);

	if (connect(hSocket, (SOCKADDR*)&recvAddr, sizeof(recvAddr)) == SOCKET_ERROR)
		ErrorHandling("connect() error!");

	event = WSACreateEvent();
	memset(&overlapped, 0, sizeof(overlapped));

	overlapped.hEvent = event;
	dataBuf.Incoming_data = Send_Mode;

	return hSocket;
}

SOCKET CServerManager::Ready_Server() {
	ZeroMemory(&GdataBuf, sizeof(SOCKETINFOGAME));
	WSADATA wsaData;
	SOCKET hSocket;
	SOCKADDR_IN recvAddr;
	
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) /* Load Winsock 2.2 DLL */
		_MSG_BOX("WinSetUp Error");

	hSocket = WSASocket(PF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	if (hSocket == INVALID_SOCKET)
		_MSG_BOX("Socket Error");

	memset(&recvAddr, 0, sizeof(recvAddr));
	recvAddr.sin_family = AF_INET;
	recvAddr.sin_addr.s_addr = inet_addr(ServerIpAddr.c_str());
	recvAddr.sin_port = htons(atoi("10102"));
	
	if (connect(hSocket, (SOCKADDR*)&recvAddr, sizeof(recvAddr)) == SOCKET_ERROR)
		_MSG_BOX("Invalid IP");

	event = WSACreateEvent();
	memset(&overlapped, 0, sizeof(overlapped));

	overlapped.hEvent = event;

	GdataBuf.Incoming_data = Send_Mode;
	client_data.client_imei = exPlayerNumber;

	return hSocket;
}

void CServerManager::Data_ExchangeLobby(SOCKET sock, LobbyData &lPlayer, Client_Data &cd, LServer_Data &lserver_data)
{
	recvBytes = 0;
	sendBytes = 0;
	if (dataBuf.Incoming_data == Send_Mode) {
		dataBuf.wsabuf.len = sizeof(LobbyData);
		dataBuf.wsabuf.buf = (char*)&lPlayer;
		dataBuf.Incoming_data = Recv_Mode;

		if (WSASend(sock, &dataBuf.wsabuf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR) {
			if (WSAGetLastError() != WSA_IO_PENDING) {
				Close_Lobby();
				Lobbysock = Ready_RobbyServer();
				return;
			}
		}
	}

	if (dataBuf.Incoming_data == Recv_Mode) {

		memset(&(overlapped), 0, sizeof(OVERLAPPED));
		dataBuf.wsabuf.len = BUFSIZE;
		dataBuf.wsabuf.buf = dataBuf.buf;
		dataBuf.Incoming_data = Send_Mode;

		if (WSARecv(sock, &dataBuf.wsabuf, 1, (LPDWORD)&recvBytes, (LPDWORD)&flags, &overlapped, NULL) == SOCKET_ERROR) {
			if (WSAGetLastError() != WSA_IO_PENDING) {
				Close_Lobby();
				Lobbysock = Ready_RobbyServer();
				return;
			}
		}

		dataBuf.wsabuf.buf[recvBytes] = '\0';
		rspData = (LServer_Data*)dataBuf.wsabuf.buf;
		lserver_data = *rspData;
	}

	if (cd.client_imei == -1) {
		for (int i = 0; i < 5; ++i) {
			if (!strcmp(lserver_data.playerdat[i].IPAdress , MyIPAdress)) {
 				cd.client_imei = i;
				client_data.client_imei = i;
				break;
			}
		}
	}
}

void CServerManager::Data_Exchange(SOCKET sock, CamperData &player, Client_Data &cd, Server_Data &server_data, SlasherData slasher)
{
	recvBytes = 0;
	sendBytes = 0;
	if (exPlayerNumber <= 4)
	{
		if (camper_data.InterationObject < 0 || camper_data.InterationObject > 0x200000)
			camper_data.InterationObject = 0;
		if (camper_data.SecondInterationObject < 0 || camper_data.SecondInterationObject > 0x200000)
			camper_data.SecondInterationObject = 0;

		if (GdataBuf.Incoming_data == Send_Mode) {
			GdataBuf.wsabuf.len = sizeof(CamperData);
			GdataBuf.wsabuf.buf = (char*)&player;
			GdataBuf.Incoming_data = Recv_Mode;
			if (WSASend(sock, &GdataBuf.wsabuf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR) {
				if (WSAGetLastError() != WSA_IO_PENDING) {
					Close_Game();
					Gamesock = Ready_Server();
					return;
				}
			}

			camper_data.Packet = 0;
		}
	}
	else
	{
		if (slasher_data.InterationObject < 0 || slasher_data.InterationObject > 0x200000)
			slasher_data.InterationObject = 0;
		if (slasher_data.SecondInterationObject < 0 || slasher_data.SecondInterationObject > 0x200000)
			slasher_data.SecondInterationObject = 0;

		if (GdataBuf.Incoming_data == Send_Mode) {
			GdataBuf.wsabuf.len = sizeof(SlasherData);
			GdataBuf.wsabuf.buf = (char*)&slasher;
			GdataBuf.Incoming_data = Recv_Mode;

			if (WSASend(sock, &GdataBuf.wsabuf, 1, (LPDWORD)&sendBytes, 0, &overlapped, NULL) == SOCKET_ERROR) {
				if (WSAGetLastError() != WSA_IO_PENDING) {
					//_MSG_BOX("Slasher Send Error");
					//exit(1);
					Close_Game();
					Gamesock = Ready_Server();
					return;
				}
			}
		}
	}
	if (GdataBuf.Incoming_data == Recv_Mode) {

		memset(&(overlapped), 0, sizeof(OVERLAPPED));
		GdataBuf.wsabuf.len = BUFSIZE;
		GdataBuf.wsabuf.buf = GdataBuf.buf;
		GdataBuf.Incoming_data = Send_Mode;

		if (WSARecv(sock, &GdataBuf.wsabuf, 1, (LPDWORD)&recvBytes, (LPDWORD)&flags, &overlapped, NULL) == SOCKET_ERROR) {
			if (WSAGetLastError() != WSA_IO_PENDING) {
				//_MSG_BOX("Invalid Data");
				//exit(1);
				Close_Game();
				Gamesock = Ready_Server();
				return;
			}
		}

		GdataBuf.wsabuf.buf[recvBytes] = '\0';
		spData = (Server_Data*)GdataBuf.wsabuf.buf;
		server_data = *spData;

		if (exPlayerNumber < 5)
		{
			if (server_data.Campers[exPlayerNumber - 1].SecondInterationObject != 0)
				camper_data.SecondInterationObject = 0;
			if ((server_data.Campers[exPlayerNumber - 1].RecvPacket != 0) && !(server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & server_data.Campers[exPlayerNumber - 1].RecvPacket))
				camper_data.RecvPacket = 0;
		}
		else
		{
			if (server_data.Slasher.SecondInterationObject != 0)
				slasher_data.SecondInterationObject = 0;
			if ((server_data.Slasher.RecvPacket != 0) && !(server_data.Game_Data.iPlayerAction[exPlayerNumber - 1] & server_data.Slasher.RecvPacket))
				slasher_data.RecvPacket = 0;
		}

		//if (cd.client_imei == -1) {
		//	for (int i = 0; i < MAX_Client; ++i) {
		//		if (server_data.player_imei[i] != -1) {
		//			cd.client_imei = i;
		//			client_data.client_imei = i;
		//			break;
		//		}
		//	}
		//}

		//if (cd.client_imei == -1) {
		//	for (int i = 0; i < 5; ++i) {
		//		if (server_data.PlaIp[i] == PlayerIP) {
		//			cd.client_imei = i;
		//			client_data.client_imei = i;
		//			break;
		//		}
		//	}
		//}
	}
}

HRESULT CServerManager::Close_Lobby()
{
	memset(&(overlapped), 0, sizeof(OVERLAPPED));
	dataBuf.wsabuf.len = BUFSIZE;
	dataBuf.wsabuf.buf = dataBuf.buf;
	dataBuf.Incoming_data = Recv_Mode;

	if (WSARecv(Lobbysock, &dataBuf.wsabuf, 1, (LPDWORD)&recvBytes, (LPDWORD)&flags, &overlapped, NULL) == SOCKET_ERROR) {
		if (WSAGetLastError() != WSA_IO_PENDING) {
			return E_FAIL;
		}
	}

	dataBuf.wsabuf.buf[recvBytes] = '\0';
	rspData = (LServer_Data*)dataBuf.wsabuf.buf;
	lserver_data = *rspData;

	shutdown(Lobbysock, SD_BOTH);
	closesocket(Lobbysock);

	return NOERROR;
}

HRESULT CServerManager::Close_Game()
{
	memset(&(overlapped), 0, sizeof(OVERLAPPED));
	GdataBuf.wsabuf.len = BUFSIZE;
	GdataBuf.wsabuf.buf = GdataBuf.buf;
	GdataBuf.Incoming_data = Recv_Mode;

	if (WSARecv(Gamesock, &GdataBuf.wsabuf, 1, (LPDWORD)&recvBytes, (LPDWORD)&flags, &overlapped, NULL) == SOCKET_ERROR) {
		if (WSAGetLastError() != WSA_IO_PENDING) {
			return E_FAIL;
		}
	}

	GdataBuf.wsabuf.buf[recvBytes] = '\0';
	spData = (Server_Data*)GdataBuf.wsabuf.buf;
	server_data = *spData;

	shutdown(Gamesock, SD_BOTH);
	closesocket(Gamesock);
	return NOERROR;
}

IN_ADDR GetDefaultMyIP()
{
	char localhostname[MAX_PATH];
	IN_ADDR addr = { 0, };

	if (gethostname(localhostname, MAX_PATH) == SOCKET_ERROR)//호스트 이름 얻어오기
	{
		return addr;
	}
	HOSTENT *ptr = gethostbyname(localhostname);//호스트 엔트리 얻어오기
	while (ptr && ptr->h_name)
	{
		if (ptr->h_addrtype == PF_INET)//IPv4 주소 타입일 때
		{
			memcpy(&addr, ptr->h_addr_list[0], ptr->h_length);//메모리 복사
			break;//반복문 탈출
		}
		ptr++;
	}
	return addr;
}

void CServerManager::Free()
{
	rspData = nullptr;
	spData = nullptr;
	cdData = nullptr;
}

