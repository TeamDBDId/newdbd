#include "stdafx.h"
#include "Closet.h"
#include "Management.h"
#include "Light_Manager.h"
_USING(Client)

CCloset::CCloset(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CCloset::CCloset(const CCloset & rhs)
	: CGameObject(rhs)
	,m_SoundList(rhs.m_SoundList)
{

}


HRESULT CCloset::Ready_Prototype()
{
	SetUp_Sound();

	return NOERROR;
}

HRESULT CCloset::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_OldState = DoorFull;
	m_CurState = DoorIdleClosed;
	m_pMeshCom->Set_AnimationSet(DoorIdleClosed);
	m_IsReciveServer = true;
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();

	return NOERROR;
}

_int CCloset::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	//m_iCurAnimation = m_CurState;
	m_fDeltaTime = fTimeDelta;
	ComunicateWithServer();
	State_Check();

	return _int();
}

_int CCloset::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;
	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	Compute_CameraDistance(&vPosition);

	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);
	if (m_fCameraDistance >= 5000.f)
		return 0;
	if ((vPosition.y < -100.f && matView._42 > -100.f) ||
		(vPosition.y > 100.f && matView._42 < 100.f))
		return 0;

	_matrix matWorld = m_pTransformCom->Get_Matrix();
	vPosition.y += 126.f;
	matWorld._42 = vPosition.y;
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);
	if (m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, 126.f))
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;

		if (FAILED(m_pRendererCom->Add_StemGroup(this)))
			return -1;
	}

	return _int();
}

void CCloset::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	m_pMeshCom->Play_Animation(m_fDeltaTime);

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	//m_pColliderCom->Render_Collider();
	Safe_Release(pEffect);
}

void CCloset::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(2);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);
			pEffect->SetMatrix("g_matVP", VP);
			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CCloset::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CCloset::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;

	_vec3 vFront = -*m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vFront, &vFront);

	m_vPosArr = new _vec3[1];
	m_vPosArr[DIR::FRONT] = vFront;
	m_pColliderCom->Set_Dist(60.f,1.f);

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_CLOSET, this);
}

_matrix CCloset::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CCloset::Get_Key()
{
	return m_Key.c_str();
}

_int CCloset::Get_OtherOption()
{
	return m_iOtherOption;
}

void CCloset::State_Check()
{
	Update_Sound();
	if (m_CurState == m_OldState)
		return;

	if (m_CurState == DoorHideFast)
		m_pMeshCom->Set_NoBleningAnimationSet(m_CurState);
	else
		m_pMeshCom->Set_AnimationSet(m_CurState);

	m_OldState = m_CurState;
}

void CCloset::Update_Sound()
{
	if (m_CurState != m_OldState)
	{
		m_SoundCheck = 0;	//�ߺ�üũ
		for (auto& iter : m_SoundList)
		{
			if((iter.OtherOption == 1 && iter.m_iState == m_CurState) || (iter.OtherOption == 2 && iter.m_iState == m_OldState))
				GET_INSTANCE(CSoundManager)->PlaySound(string(iter.SoundName), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION),1700.f, iter.SoundValue);
		}
	}
	else
	{
		_float TimeAcc = (_float)m_pMeshCom->GetTimeAcc();
		_float Period = (_float)m_pMeshCom->GetPeriod();
		_float Ratio = TimeAcc / Period;
		_int SoundCount = 0;
		for(auto& iter : m_SoundList)
		{
			if (m_CurState == iter.m_iState)
			{
				if (iter.OtherOption != 0)
					continue;
				if (Ratio >= iter.Ratio && SoundCount == m_SoundCheck)
				{
					GET_INSTANCE(CSoundManager)->PlaySound(string(iter.SoundName), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 1700.f, iter.SoundValue);
					m_SoundCheck++;
					return;
				}
				SoundCount++;
			}
		}
	}
}

void CCloset::ComunicateWithServer()
{	
	if(m_IsReciveServer)
		m_CurState = (CCloset::STATE)server_data.Game_Data.Locker[m_eObjectID - CLOSET];
	else
		m_IsReciveServer = true;
}

void CCloset::SetUp_Sound()
{
	Add_SoundList(DoorOpenEmpty, 13.f / 71.f, "Closet/closet_close_slow_01.ogg", 1.f);
	Add_SoundList(DoorOpenEmpty, 34.2f / 71.f, "Closet/closet_open_fast_01.ogg", 1.f);
	Add_SoundList(DoorOpenEmpty, 36.6f / 71.f, "Closet/closet_close_fast_01.ogg", 1.f);
	Add_SoundList(DoorHide, 0.f, "Closet/closet_squeak_01.ogg", 1.f,2);
	Add_SoundList(DoorHide, 0.f, "Closet/closet_impact_wood_hit_01.ogg", 1.f, 1);
	Add_SoundList(DoorUnhide, 0.f, "Closet/closet_squeak_01.ogg", 1.f, 2);
	Add_SoundList(DoorHideFast, 0.f, "Closet/closet_squeak_01.ogg", 3.f, 2);
	Add_SoundList(DoorUnhideFast, 0.f, "Closet/closet_squeak_01.ogg", 3.f, 2);

	Add_SoundList(DoorHide, 0.25f, "Closet/closet_open_slow_01.ogg");
	Add_SoundList(DoorUnhide, 0.3f, "Closet/closet_open_slow_02.ogg");
	Add_SoundList(DoorHideFast, 0.3f, "Closet/closet_open_slow_02.ogg");
}

void CCloset::Add_SoundList(const _uint & m_iState, const _float Ratio, const char * SoundName, const _float & SoundValue, const _int& OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundValue = SoundValue;
	Data.OtherOption = OtherOption;
	m_SoundList.push_back(Data);
}

HRESULT CCloset::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Static
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Closet");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	m_fRad = 100.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
	matLocalTransform._43 = -30.f;


	m_pColliderCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		_MSG_BOX("111");
	if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, L"Mesh_Closet")))
		_MSG_BOX("111");

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CCloset::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);


	return NOERROR;
}


CCloset * CCloset::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CCloset*	pInstance = new CCloset(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CCloset Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CCloset::Clone_GameObject()
{
	CCloset*	pInstance = new CCloset(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CCloset Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CCloset::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_CLOSET, this);

	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
