#include "stdafx.h"
#include "Generator.h"
#include "Management.h"
#include "Light_Manager.h"
#include "CustomLight.h"
#include "Slasher.h"
#include "MeshTexture.h"
#include "Camper.h"
#include <string>

_USING(Client)

CGenerator::CGenerator(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CGenerator::CGenerator(const CGenerator & rhs)
	: CGameObject(rhs)
	, m_SoundList(rhs.m_SoundList)
{

}


HRESULT CGenerator::Ready_Prototype()
{
	SetUp_Sound();
	return NOERROR;
}

HRESULT CGenerator::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;

	if (exPlayerNumber == 5)
		m_bPenetration = true;

	m_fProgressTime = 0.f;
	m_fMaxProgressTime = 80.f;
	m_fCurProgressTimeForSlasher = 0.1f;
	m_Color = { 0.7f, 0.f, 0.f, 0.5f };
	m_LateInit = false;
	m_pSkinTextures = m_pRendererCom->Get_SkinTex();
	return NOERROR;
}

_int CGenerator::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;

	if (m_fFailTime >= 0.00f)
		m_fFailTime -= fTimeDelta;

	Init_Light();
	ComunicateWithServer();

	Progress_Check();

	m_fDeltaTime = fTimeDelta;
	State_Check();

	if (exPlayerNumber == 5)
	{
		if (m_fProgressTime > m_fMaxProgressTime)
			m_bPenetration = false;
		else
			m_bPenetration = true;
	}

	//
	if (m_fProgressTime > 0.5f)
	{
		swprintf_s(sztt, sizeof(sztt) / sizeof(wchar_t), L"%f", m_fProgressTime);

		//SetWindowText(g_hWnd, sztt);
	}
	Penetration_Check();

	if (m_fMaxProgressTime < m_fProgressTime)
		m_isBool = true;

	Camper_Check();

	return _int();
}

_int CGenerator::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));

	if (m_fCameraDistance >= 1500.f)
		m_Color.w = 0.5f;
	else if (m_fCameraDistance < 500.f)
	{
		m_Color.w = 0.f;
		m_pRendererCom->Add_CubeGroup(this);
	}		
	else
		m_Color.w = (m_fCameraDistance - 500.f) * 0.0005f;

	if (!m_bPenetration)
	{
		if (m_fCameraDistance > 6000.f)
		{
			m_pMeshCom->Play_Animation(m_fDeltaTime);
			return 0;
		}
	}

	_vec3 vPosition = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
	_matrix matWorld = m_pTransformCom->Get_Matrix();
	vPosition.y += 240.f;
	matWorld._42 = vPosition.y;
	D3DXMatrixInverse(&matWorld, nullptr, &matWorld);
	if (m_pFrustumCom->Culling_Frustum(&vPosition, matWorld, 240.f))
	{
		if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
			return -1;
		if (m_bPenetration && !bCanGoOut)
			m_pRendererCom->Add_StencilGroup(this);

		if (FAILED(m_pRendererCom->Add_StemGroup(this)))
			return -1;
	}

	return _int();
}

void CGenerator::Render_GameObject()
{
	if (nullptr == m_pSkinTextures)
		return;

	if (!m_bPenetration || bCanGoOut)
	{
		if (nullptr == m_pMeshCom ||
			nullptr == m_pShaderCom)
			return;

		_uint iPass = 18;
		if (m_fProgressTime >= 80.f)
			iPass = 17;

		m_pMeshCom->Play_Animation(m_fDeltaTime);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(iPass);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		pEffect->EndPass();
		pEffect->End();

		Safe_Release(pEffect);
	}
	else
	{
		if (nullptr == m_pMeshCom ||
			nullptr == m_pShaderCom)
			return;

		m_pMeshCom->Play_Animation(m_fDeltaTime);

		LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
		if (nullptr == pEffect)
			return;

		pEffect->AddRef();

		_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

		pEffect->Begin(nullptr, 0);

		pEffect->BeginPass(4);

		for (size_t i = 0; i < dwNumMeshContainer; i++)
		{
			const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

			for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
			{
				m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

				D3DLOCKED_RECT lock_Rect = { 0, };
				if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, D3DLOCK_NOSYSLOCK | D3DLOCK_DISCARD | D3DLOCK_NOOVERWRITE)))
					return;

				memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

				m_pSkinTextures->UnlockRect(0);

				if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
					return;
				pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

				pEffect->CommitChanges();

				m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
			}

		}

		//pEffect->EndPass();

		//pEffect->BeginPass(5);

		//for (size_t i = 0; i < dwNumMeshContainer; i++)
		//{
		//	const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		//	for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		//	{

		//		if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
		//			return;
		//		pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

		//		pEffect->CommitChanges();

		//		m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		//	}

		//}

		pEffect->EndPass();
		pEffect->End();

		/////////////////////


		Safe_Release(pEffect);
	}
}

void CGenerator::Render_Stencil()
{
	if (nullptr == m_pSkinTextures)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(5);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			
			pEffect->SetVector("g_DiffuseColor", (_vec4*)&m_Color);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CGenerator::Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos)
{
	if (nullptr == m_pMeshCom ||
		nullptr == pEffect)
		return;

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(18);

	pEffect->CommitChanges();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			m_pMeshCom->Update_Skinning((_uint)i, (_uint)j);

			D3DLOCKED_RECT lock_Rect = { 0, };
			if (FAILED(m_pSkinTextures->LockRect(0, &lock_Rect, NULL, 0)))
				return;

			memcpy(lock_Rect.pBits, m_pMeshCom->Get_MeshContainer(i)->pRenderingMatrices, sizeof(_matrix) * pMeshContainer->dwNumFrames);

			m_pSkinTextures->UnlockRect(0);

			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;
			pEffect->SetMatrix("g_matVP", VP);
			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CGenerator::Render_CubeMap(_matrix * View, _matrix * Proj)
{
	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(18);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{
			if (FAILED(SetUp_ConstantTable(pEffect, pMeshContainer, (_uint)j)))
				return;

			_matrix matWVP = m_pTransformCom->Get_Matrix() * (*View) * (*Proj);

			pEffect->SetMatrix("g_matWVP", &matWVP);

			pEffect->SetVector("g_DiffuseColor", (_vec4*)&m_Color);

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

void CGenerator::Render_Stemp()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMeshContainer = m_pMeshCom->Get_NumMeshContainer();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(14);

	_matrix matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	for (size_t i = 0; i < dwNumMeshContainer; i++)
	{
		const D3DXMESHCONTAINER_DERIVED* pMeshContainer = m_pMeshCom->Get_MeshContainer((_uint)i);

		for (size_t j = 0; j < pMeshContainer->NumMaterials; ++j)
		{

			pEffect->SetTexture("g_SkinTexture", m_pSkinTextures);

			pEffect->CommitChanges();

			m_pMeshCom->Render_Mesh((_uint)i, (_uint)j);
		}

	}

	pEffect->EndPass();
	pEffect->End();
}

void CGenerator::Set_Base(const _tchar * Key, const _matrix & matWorld, const _int & OtherOption)
{
	m_pTransformCom->Set_Matrix(matWorld);

	if (Key == nullptr)
		return;

	m_Key = Key;


	_vec3 vFront = *m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK);
	D3DXVec3Normalize(&vFront, &vFront);

	_vec3 vRight = *m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT);
	D3DXVec3Normalize(&vRight, &vRight);

	m_vPosArr = new _vec3[4];
	m_vPosArr[DIR::FRONT] = vFront*1.2f;
	m_vPosArr[DIR::BACK] = -vFront*1.5f;
	m_vPosArr[DIR::LEFT] = -vRight;
	m_vPosArr[DIR::RIGHT] = vRight;


	m_pColliderCom->Set_Dist(80.f,1.5f);

	CColl_Manager::GetInstance()->Add_CollGroup(CColl_Manager::C_GENER, this);
}

_matrix CGenerator::Get_Matrix()
{
	return m_pTransformCom->Get_Matrix();
}

const _tchar * CGenerator::Get_Key()
{
	return m_Key.c_str();
}

_int CGenerator::Get_OtherOption()
{
	return m_iOtherOption;
}


void CGenerator::Update_Sound()
{
	_float TimeAcc = (_float)m_pMeshCom->GetTimeAcc();
	_float Period = (_float)m_pMeshCom->GetPeriod();
	_float Ratio = TimeAcc / Period;
	Ratio = (Ratio - (int)Ratio);
	if (m_fRatio > Ratio)
	{
		m_SoundCheck = 0;	//중복체크
		m_fRatio = Ratio;
	}
	else if(m_fRatio < Ratio)
	{
		m_fRatio = Ratio;
		_int SoundCount = 0;
		for (auto& iter : m_SoundList)
		{
			if (m_CurState == iter.m_iState)
			{
				if (iter.OtherOption != 0)
					continue;
				if (Ratio >= iter.Ratio && SoundCount == m_SoundCheck)
				{
					GET_INSTANCE(CSoundManager)->PlaySound(to_string(m_eObjectID),string(iter.SoundName), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), iter.SoundDistance, iter.SoundValue);
					m_SoundCheck++;
					return;
				}
				SoundCount++;
			}
		}
	}
}

void CGenerator::SetUp_Sound()
{
	Add_SoundList(PistonsIdleS1, 0.f, "Generator/Generator_1.mp3",2600.f, 1.f);
	Add_SoundList(PistonsIdleS2, 0.f, "Generator/Generator_2.mp3", 2600.f, 1.15f);
	Add_SoundList(PistonsIdleS3, 0.f, "Generator/Generator_3.mp3", 2600.f, 1.3f);
	Add_SoundList(PistonsIdle, 0.f, "Generator/Generator_4.mp3", 2600.f, 1.5f);
	Add_SoundList(On, 0.f, "Generator/Generator_On.mp3", 2600.f, 1.4f);
}

void CGenerator::State_Check()
{
	Update_Sound();
	if (m_CurState == m_OldState)
		return;
	m_pMeshCom->Set_AnimationSet(m_CurState);
	m_OldState = m_CurState;
}

void CGenerator::Progress_Check()
{
	if (m_CurState == On)
		return;
	else if (m_fProgressTime >= m_fMaxProgressTime)
	{
		m_CurState = On;
		for (int i = 0; i < 4; ++i)
			m_pCustomLight[i]->SetRender(true);
		m_pCustomSpotLight->SetRender(true);
	}
	else if (m_CurState == Failure && m_pMeshCom->GetTimeAcc() / m_pMeshCom->GetPeriod(m_CurState) > 1.f)
	{
		m_CurState = Idle;
		Progress_Check();
	}
	else if (m_CurState == Failure)
	{

	}
	else if (m_fProgressTime >= 60.f)
		m_CurState = PistonsIdle;
	else if (m_fProgressTime >= 40.f)
		m_CurState = PistonsIdleS3;
	else if (m_fProgressTime >= 20.f)
		m_CurState = PistonsIdleS2;
	else if (m_fProgressTime > 0.05f)
		m_CurState = PistonsIdleS1;
	else if (m_fProgressTime <= 0.05f)
		m_CurState = Idle;
}

void CGenerator::Camper_Check()
{
	if (exPlayerNumber != 5)
		return;

	_int CountCamper = 0;
	_bool PlayerCheck[4] = { false,false,false,false };
	_bool bFail = false;

	for (_int i = 0; i < 4; ++i)
	{
		if (!server_data.Campers[i].bConnect)
			continue;

		if (server_data.Campers[i].iState == AC::GeneratorActivation)
		{
			if (server_data.Campers[i].InterationObject == m_eObjectID)
			{
				PlayerCheck[i] = true;
				++CountCamper;
			}
			//_vec3 vCamperPos = server_data.Campers[i].vPos;
			//_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
			//_float fDist = D3DXVec3Length(&(vCamperPos - vMyPos));

			//if (fDist < 200.f)
			//{
			//	PlayerCheck[i] = true;
			//	++CountCamper;
			//}

		}

	}

	if (server_data.Slasher.Perk & DISSONANCE)
	{
		if (CountCamper > 2)
		{
			m_bPenetration = true;
			m_Color = { 1,1,1,1 };
			m_fDissonanceTime = 3.f;

			GET_INSTANCE_MANAGEMENT;

			for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
			{
				if (PlayerCheck[pCamper->GetID() - CAMPER])
				{
					((CCamper*)pCamper)->Set_Penetraition(6.f);
					((CCamper*)pCamper)->Set_PColor(_vec4(0.6f, 0.f, 0.f, 1.f));
				}
			}

			Safe_Release(pManagement);
		}
	}
}

void CGenerator::ComunicateWithServer()
{
	if (!bStartGameServer)
		return;

	_float ProgressTime = server_data.Game_Data.GRP[m_eObjectID - GENERATOR];
	if (ProgressTime < m_fProgressTime * 0.93f)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Generator/generator_explode_02.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION), 3400.f, 1.5f);

		if (exPlayerNumber == 5)
			GET_INSTANCE(CSoundManager)->PlaySound(string("IngameUI/generator_explode_01.ogg"), *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION),0, 1.5f);
	}
	
	else if (ProgressTime < m_fProgressTime * 0.99f)
	{
		// 달고있다.
	}

	m_fProgressTime = server_data.Game_Data.GRP[m_eObjectID - GENERATOR];
	if (m_fProgressTime >= 79.99f && !m_bDoneRP)
	{
		m_bDoneRP = true;

		if (exPlayerNumber == 5)
		{
			GET_INSTANCE_MANAGEMENT;
			if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").size() == 0)
			{
				Safe_Release(pManagement);
				return;
			}

			for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
			{
				if (pCamper->Get_IsDead())
					continue;

				if (server_data.Slasher.Perk & RESENTMENT)
				{
					((CCamper*)pCamper)->Set_Penetraition(3.f);
					((CCamper*)pCamper)->Set_PColor(_vec4(0.6f, 0.1f, 0.1f, 0.6f));
				}
				if (server_data.Slasher.Perk & DISSONANCE)
				{
					((CCamper*)pCamper)->Set_Penetraition(5.f);
					((CCamper*)pCamper)->Set_PColor(_vec4(0.6f, 0.1f, 0.1f, 0.6f));
				}
			}

			Safe_Release(pManagement);
		}
		else
		{
			if (server_data.Campers[exPlayerNumber - 1].Perk & DARKSENSE)
			{
				GET_INSTANCE_MANAGEMENT;

				if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").size() != 0)
				{
					((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front())->Set_Penetration(5.f);
					((CSlasher*)pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Slasher").front())->Set_PColor(_vec4(0.6f, 0.f, 0.f, 1.f));
				}

				Safe_Release(pManagement);
			}			
		}
	}
}

void CGenerator::Init_Light()
{
	if (m_LateInit)
		return;

	_matrix matLocal;
	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	_vec3 vLocalPos = { 45.f, 470.f, -50.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[0] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider0", m_pLightCollider[0]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { -50.f, 470.f, -50.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[1] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider1", m_pLightCollider[1]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { 40.f, 435.f, -100.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[2] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider2", m_pLightCollider[2]);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { -40.f, 435.f, -100.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pLightCollider[3] = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_LightCollider3", m_pLightCollider[3]);

	for (int i = 0; i < 4; ++i)
	{
		_vec3 vCenter = m_pLightCollider[i]->Get_Center();

		if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight[i])))
			return;

		m_pCustomLight[i]->Add_Light();
		m_pCustomLight[i]->AddRef();

		m_pCustomLight[i]->Set_Position(vCenter);

		m_pCustomLight[i]->Set_Range(80.f);
		m_pCustomLight[i]->Set_Diffuse(D3DXCOLOR(5.f, 5.f, 5.f, 1.f));
		//AddShadowMap
		m_pCustomLight[i]->SetRender(false);
		m_pCustomLight[i]->Set_Shaft(true);
		CLight_Manager*        pLight_Manager = CLight_Manager::GetInstance();
		if (nullptr == pLight_Manager)
			return;

		pLight_Manager->AddRef();

		pLight_Manager->Ready_ShadowMap(m_pCustomLight[i]->GetLight());

		Safe_Release(pLight_Manager);
	}
	m_pCustomLight[0]->Set_Shaft(false);

	D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
	vLocalPos = { 45.f, 380.f, -75.f };
	matLocal.m[3][0] += vLocalPos.x;
	matLocal.m[3][1] += vLocalPos.y;
	matLocal.m[3][2] += vLocalPos.z;

	m_pColliderSpotCom = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_ColliderSpot", m_pColliderSpotCom);

	if (FAILED(GET_INSTANCE(CManagement)->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomSpotLight)))
		return;

	m_pCustomSpotLight->Add_Light();
	m_pCustomSpotLight->AddRef();

	m_pCustomSpotLight->Set_Position(m_pColliderSpotCom->Get_Center());
	m_pCustomSpotLight->Set_Type(D3DLIGHT_SPOT);

	m_pCustomSpotLight->Set_Diffuse(D3DXCOLOR(4.6f, 4.6f, 4.5f, 1.f));
	m_pCustomSpotLight->Set_Range(800.f);
	m_pCustomSpotLight->Set_Theta(D3DXToRadian(21.5f));
	m_pCustomSpotLight->Set_Phi(D3DXToRadian(27.5f));

	m_pCustomSpotLight->Set_Direction(_vec3(0.f, -1.f, 0.f));
	m_pCustomSpotLight->SetRender(false);
	m_pCustomSpotLight->Set_Shaft(true);

	//Add_ShadowCubeGroup
	GET_INSTANCE_MANAGEMENT;

	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static"))
	{
		m_pCustomSpotLight->Add_ShadowCubeGroup(pObj);
	}
	for (auto pObj : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_GameObject"))
	{
		m_pCustomSpotLight->Add_ShadowCubeGroup(pObj);
	}
	Safe_Release(pManagement);

	CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
	if (nullptr == pLight_Manager)
		return;

	pLight_Manager->AddRef();

	pLight_Manager->Ready_ShadowMap(m_pCustomSpotLight->GetLight());
	m_LateInit = true;

	Safe_Release(pLight_Manager);
}

void CGenerator::Add_SoundList(const _uint & m_iState, const _float Ratio, const char * SoundName, const _float & SoundDistance,const _float & SoundValue, const _int & OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundValue = SoundValue;
	Data.SoundDistance = SoundDistance;
	Data.OtherOption = OtherOption;
	m_SoundList.push_back(Data);
}

void CGenerator::Penetration_Check()
{
	if (5 != exPlayerNumber)
		return;

	if (m_fProgressTime >= 80.f)
	{
		m_bPenetration = false;
		return;
	}
		
	GET_INSTANCE_MANAGEMENT;

	CSlasher* pSlasher = (CSlasher*)pManagement->Get_GameObject(SCENE_STAGE, L"Layer_Slasher", 0);
	if (nullptr == pSlasher)
	{
		Safe_Release(pManagement);
		return;
	}

	if (m_Color.w < 0.001f)
	{
		m_bPenetration = false;
		Safe_Release(pManagement);
		return;	
	}

	if (nullptr == pSlasher->Get_CarriedCamper())
	{
		m_bPenetration = true;
	}		
	else
		m_bPenetration = false;

	Safe_Release(pManagement);
}

HRESULT CGenerator::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.CMesh_Static
	m_pMeshCom = (CMesh_Dynamic*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Generator");
	if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		return E_FAIL;

	m_fRad = 200.f;
	_matrix			matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, m_fRad, m_fRad, m_fRad);
	//For.Com_Collider
	m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
	Add_Component(L"Com_Collider", m_pColliderCom);
	if (FAILED(m_pColliderCom->Ready_HullMesh(SCENE_STAGE, L"Mesh_Generator")))
		_MSG_BOX("111");

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CGenerator::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);
	pEffect->SetInt("numBoneInf", 4);

	const SUBSETDESC* pSubSet = &pMeshContainer->pSubSetDesc[iAttributeID];
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);
	pEffect->SetTexture("g_NormalTexture", pSubSet->MeshTexture.pNormalTexture);
	pEffect->SetTexture("g_AOTexture", pSubSet->MeshTexture.pAmbientOcclusionTexture);
	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);
	pEffect->SetTexture("g_LightEmissive", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"T_Generator_EMask.tga")));
	pEffect->SetVector("g_DiffuseColor", (_vec4*)&m_Color);

	return NOERROR;
}


CGenerator * CGenerator::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CGenerator*	pInstance = new CGenerator(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CGenerator Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CGenerator::Clone_GameObject()
{
	CGenerator*	pInstance = new CGenerator(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CGenerator Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	m_eObjectID++;
	return pInstance;
}

void CGenerator::Free()
{
	CColl_Manager::GetInstance()->Erase_Obj(CColl_Manager::C_GENER, this);

	for (int i = 0; i < 4; ++i)
	{
		Safe_Release(m_pLightCollider[i]);
		Safe_Release(m_pCustomLight[i]);
	}
	
	Safe_Release(m_pColliderSpotCom);
	Safe_Release(m_pCustomSpotLight);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pFrustumCom);
	CGameObject::Free();
}
