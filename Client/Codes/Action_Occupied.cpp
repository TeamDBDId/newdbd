#include "stdafx.h"
#include "..\Headers\Action_Occupied.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"

_USING(Client)

CAction_ClosetOccupied::CAction_ClosetOccupied()
{
}

HRESULT CAction_ClosetOccupied::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	CCamper* pCamper = (CCamper*)m_pGameObject;
	m_bIsPlaying = true;

	pCamper->Set_State(AC::ClosetFull_Occupied);
	m_iState = AC::ClosetFull_Occupied;

	return NOERROR;
}

_int CAction_ClosetOccupied::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (m_iState == AC::ClosetFull_Occupied && pMeshCom->IsOverTime(0.3f))
	{
		((CCamper*)m_pGameObject)->Set_State(AC::ClosetIdle);
		return END_ACTION;
	}

	return UPDATE_ACTION;
}

void CAction_ClosetOccupied::End_Action()
{
	m_bIsPlaying = false;
}

void CAction_ClosetOccupied::Send_ServerData()
{
}

void CAction_ClosetOccupied::Free()
{
	CAction::Free();
}
