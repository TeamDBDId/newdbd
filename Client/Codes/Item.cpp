#include "stdafx.h"
#include "..\Headers\Item.h"
#include "Management.h"
#include "Camper.h"
#include "CustomLight.h"
#include "Light_Manager.h"
#include "MeshTexture.h"

_USING(Client)


CItem::CItem(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CItem::CItem(const CItem & rhs)
	: CGameObject(rhs)
	, m_SoundList(rhs.m_SoundList)
{
}

void CItem::Ready_Item(ITEMTYPE eType, _float fDurability, _vec3* vPos)
{
	GET_INSTANCE_MANAGEMENT;

	m_eType = eType;

	switch (m_eType)
	{
	case MEDIKIT:
		m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_MediKit");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		{
			Safe_Release(pManagement);
			return;
		}	
		break;
	case TOOLBOX:
		m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_ToolBox");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		{
			Safe_Release(pManagement);
			return;
		}		
		break;
	case FLASHLIGHT:
	{
		m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_FlashLight");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		{
			Safe_Release(pManagement);
			return;
		}

		_matrix matLocal;
		D3DXMatrixScaling(&matLocal, 30.f, 30.f, 30.f);
		_vec3 vLocalPos = { 12.f, 0.f, 0.f };
		matLocal.m[3][0] += vLocalPos.x;
		matLocal.m[3][1] += vLocalPos.y;
		matLocal.m[3][2] += vLocalPos.z;

		m_pLightCollider = (CCollider*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_OBB, matLocal, nullptr, m_pTransformCom->Get_Matrix_Pointer()));
		Add_Component(L"Com_LightCollider", m_pLightCollider);

		//D3DXVec3TransformCoord(&vLocalPos, &vLocalPos, m_pTransformCom->Get_Matrix_Pointer());

		_vec3 vCenter = m_pLightCollider->Get_Center();

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_CustomLight", SCENE_STAGE, L"Layer_CustomLight", (CGameObject**)&m_pCustomLight)))
			return;

		m_pCustomLight->Add_Light();
		m_pCustomLight->AddRef();

		m_pCustomLight->Set_Position(vCenter);
		m_pCustomLight->Set_Type(D3DLIGHT_SPOT);

		//m_pCustomLight->Set_Diffuse(D3DXCOLOR(1.f, 0.2f, 0.2f, 1.f));
		m_pCustomLight->Set_Range(1200.f);
		m_pCustomLight->Set_Diffuse(D3DXCOLOR(5.6f, 5.6f, 5.5f, 1.f));
		m_pCustomLight->Set_Direction(_vec3(1.f, 0.f, 0.f));
		m_pCustomLight->Set_Theta(D3DXToRadian(55.f));
		m_pCustomLight->Set_Phi(D3DXToRadian(65.f));
		m_pCustomLight->SetMovable(true);
		m_pCustomLight->SetRender(false);

		//AddShadowMap

		m_pCustomLight->Add_ShadowCubeGroup(pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Map_Static").front());

		CLight_Manager*		pLight_Manager = CLight_Manager::GetInstance();
		if (nullptr == pLight_Manager)
			return;

		pLight_Manager->AddRef();

		pLight_Manager->Ready_ShadowMap(m_pCustomLight->GetLight());

		Safe_Release(pLight_Manager);
	}
		break;
	case KEY:
		m_pMeshCom = (CMesh_Static*)pManagement->Clone_Component(SCENE_STAGE, L"Mesh_Key");
		if (FAILED(Add_Component(L"Com_Mesh", m_pMeshCom)))
		{
			Safe_Release(pManagement);
			return;
		}
		break;
	default:
		break;
	}

	m_fMaxDurability = fDurability;
	m_fDurability = fDurability;
	m_pTransformCom->Scaling(1.f, 1.f, 1.f);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, vPos);

	_matrix matLocalTransform;
	D3DXMatrixScaling(&matLocalTransform, 100.f, 100.f, 100.f);

	_float fX = D3DXToRadian(0.f);
	_float fY = D3DXToRadian(0.f);
	_float fZ = D3DXToRadian(0.f);

	m_pTransformCom->SetUp_RotateXYZ(&fX, &fY, &fZ);

	m_pColliderCom = (CCollider*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Collider_Sphere", &CCollider::COLLIDERINFO(CCollider::BOXTYPE_END, matLocalTransform, nullptr, &m_matParent));
	if (FAILED(Add_Component(L"Com_Collider", m_pColliderCom)))
		return;

	Safe_Release(pManagement);
}

void CItem::Drop_Item(_vec3 * vDropPos)
{
	if (nullptr == vDropPos)
		return;

	if (nullptr == m_pTransformCom)
		return;

	D3DXMatrixIdentity(&m_matParent);
	m_pRHandMatrix = nullptr;
	m_pCamperTransform = nullptr;

	_float fX = D3DXToRadian(0.f);
	_float fY = D3DXToRadian(0.f);
	_float fZ = D3DXToRadian(0.f);

	m_pTransformCom->SetUp_RotateXYZ(&fX, &fY, &fZ);
	m_pTransformCom->Set_Parent(nullptr);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, vDropPos);
	m_bIsVisible = true;
}

void CItem::Pick_Item(CTransform * pTransform, const _matrix * pMatRHandFrame)
{
	if (nullptr == pTransform
		|| nullptr == pMatRHandFrame)
		return;

	_vec3 vPos = { 0.f, 0.f, 0.f };
	if (m_eType == FLASHLIGHT)
		vPos = { 0.f, -10.f, 0.f };
	else if(m_eType == KEY)
		vPos = { 0.f, -10.f, 0.f };

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);

	_float fX = D3DXToRadian(90.f);
	_float fY = D3DXToRadian(0.f);
	_float fZ = D3DXToRadian(0.f);

	m_pTransformCom->SetUp_RotateXYZ(&fX, &fY, &fZ);

	m_pRHandMatrix = (_matrix*)pMatRHandFrame;
	m_pCamperTransform = pTransform;
	m_iSoundCheck = 2;
	m_fSoundTimer = 0.f;
	m_pTransformCom->Set_Parent(&m_matParent);

	//GET_INSTANCE_MANAGEMENT;

	//if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").size() == 0)
	//{
	//	Safe_Release(pManagement);
	//	return;
	//}

	//for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
	//{
	//	if (pCamper->Get_IsDead())
	//		continue;

	//	if (((CCamper*)pCamper)->GetColledItem() == this)
	//		((CCamper*)pCamper)->SetColledItem(nullptr);
	//}

	//Safe_Release(pManagement);
	
}

HRESULT CItem::Ready_Prototype()
{
	SetUp_Sound();
	return NOERROR;
}

HRESULT CItem::Ready_GameObject()
{
	if (FAILED(Ready_Component()))
		return E_FAIL;
	m_fSoundTimer = -1.f;
	m_iSoundCheck = 0;

	return NOERROR;
}

_int CItem::Update_GameObject(const _float & fTimeDelta)
{
	_int PlayerIndex = 0;
	if(m_eObjectID >= ITEM)
		PlayerIndex = _int(server_data.Game_Data.Item[m_eObjectID - ITEM] / 10);

	if (PlayerIndex > 0 && m_eType == CItem::FLASHLIGHT && !server_data.Campers[PlayerIndex - 1].bUseItem
		&& nullptr != m_pCustomLight)
	{	
		if (m_iSoundCheck == 1)
		{
			GET_INSTANCE(CSoundManager)->PlaySound(string("Flashlight/flashlight_off_01.ogg"), _vec3(m_matParent._41, m_matParent._42, m_matParent._43), 1700.f, 1.f);
			m_iSoundCheck--;
		}
		m_pCustomLight->SetRender(false);
		m_bUseNow = false;
	}
	if (PlayerIndex == exPlayerNumber && !KEYMGR->MousePressing(1) && nullptr != m_pCustomLight)
	{
		if (m_iSoundCheck == 1)
		{
			GET_INSTANCE(CSoundManager)->PlaySound(string("Flashlight/flashlight_off_01.ogg"), _vec3(m_matParent._41, m_matParent._42, m_matParent._43), 1700.f, 1.f);
			m_iSoundCheck--;
		}
		m_pCustomLight->SetRender(false);
		m_bUseNow = false;
	}
		
	if (m_isDead)
	{
		if (m_eType == CItem::FLASHLIGHT)
			m_pCustomLight->SetRender(false);

		return DEAD_OBJ;
	}	

	if (nullptr != m_pRHandMatrix)
		m_matParent = *m_pRHandMatrix * m_pCamperTransform->Get_Matrix();

	Update_Sound(fTimeDelta);
	Check_Player();
	ComunicateWithServer();
	if (!m_bUseNow)
	{
		if (m_eType == CItem::FLASHLIGHT)
			m_pCustomLight->SetRender(false);
	}

	return _int();
}

_int CItem::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (nullptr == m_pRendererCom)
		return -1;

	if (nullptr == m_pFrustumCom)
		return -1;

	if (!m_bIsVisible)
		return 0;

	//_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	//if (vMyPos.x < 10.f && vMyPos.y < 10.f && vMyPos.z < 10.f)
	//	return 0;
	//_vec3 vPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);

	//D3DXVec3TransformCoord(&vPos, &vPos, &m_matParent);


	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_NONEALPHA, this)))
		return -1;

	return _int();
}

void CItem::Render_GameObject()
{
	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	_uint iPass = 0;
	if (nullptr != m_pRHandMatrix)
	{
		iPass = 9;

		if (m_bUseNow && m_eType == FLASHLIGHT)
		{
			iPass = 3;
			m_pCustomLight->SetRender(true);
			Cal_LightPos();
		}
	}	

	LPD3DXEFFECT		pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect->Begin(nullptr, 0);

	pEffect->BeginPass(iPass);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect, i)))
		{
			Safe_Release(pEffect);
			return;
		}			

		if(3 == iPass)
			pEffect->SetTexture("g_LightEmissive", GET_INSTANCE(CMeshTexture)->Find_Texture(wstring(L"FlashLight_EM.tga")));

		pEffect->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
}

_bool CItem::Use_Item(_float fTimeDelta, CGameObject* pTarget, _vec3 * vDirection)
{
	_float fTime = 0.0f;

	switch (m_eType)
	{
	case MEDIKIT:
		fTime = fTimeDelta * 0.2f;
		break;
	case TOOLBOX:
		fTime = fTimeDelta * 0.2f;
		break;
	case FLASHLIGHT:	
		m_pCustomLight->SetRender(true);
		if(nullptr != vDirection)
			m_vFlashDirection = *vDirection;
		Cal_LightPos();
		fTime = fTimeDelta;
		break;
	case KEY:
		fTime = fTimeDelta;
		break;
	default:
		break;
	}

	m_fDurability -= fTime;
	if (m_fDurability <= 0.f)
		return true;

	return false;
}

void CItem::Update_Sound(const _float& fTimeDelat)
{
	if (m_fSoundTimer < 0.f)
		return;
	m_fSoundTimer += fTimeDelat;
	_int SoundCount = 2;
	for (auto& iter : m_SoundList)
	{
		if (iter.m_iState == m_eType)
		{
			if (m_fSoundTimer >= iter.Ratio && SoundCount == m_iSoundCheck)
			{
				GET_INSTANCE(CSoundManager)->PlaySound(string(iter.SoundName), _vec3(m_matParent._41, m_matParent._42, m_matParent._43), iter.SoundDistance, iter.SoundValue);
				m_iSoundCheck++;
				return;
			}
			SoundCount++;
		}
	}

	if (m_fSoundTimer >= 0.3f)
	{
		m_fSoundTimer = -1.f;
		m_iSoundCheck = 0;
	}
}

void CItem::SetUp_Sound()
{

	Add_SoundList(MEDIKIT, 0.2f, "Medkit/medkit_get_01.ogg", 1700.f, 15.f);
	Add_SoundList(MEDIKIT, 0.23f, "Medkit/medkit_get_02.ogg", 1700.f, 15.f);
	Add_SoundList(MEDIKIT, 0.26f, "Medkit/medkit_get_03.ogg", 1700.f, 15.f);

	Add_SoundList(KEY, 0.2f, "Medkit/medkit_get_01.ogg", 1700.f, 15.f);
	Add_SoundList(KEY, 0.23f, "Medkit/medkit_get_02.ogg", 1700.f, 15.f);
	Add_SoundList(KEY, 0.26f, "Medkit/medkit_get_03.ogg", 1700.f, 15.f);

	Add_SoundList(TOOLBOX, 0.2f, "Toolkit/toolkit_get_01.ogg", 1700.f, 15.f);
	Add_SoundList(TOOLBOX, 0.23f, "Toolkit/toolkit_get_02.ogg", 1700.f, 15.f);
	Add_SoundList(TOOLBOX, 0.26f, "Toolkit/toolkit_get_03.ogg", 1700.f, 15.f);

	Add_SoundList(FLASHLIGHT, 0.2f, "Flashlight/flashlight_get_01.ogg", 1700.f, 15.f);
	Add_SoundList(FLASHLIGHT, 0.23f, "Flashlight/flashlight_get_02.ogg", 1700.f, 15.f);
	Add_SoundList(FLASHLIGHT, 0.26f, "Flashlight/flashlight_get_03.ogg", 1700.f, 15.f);
	Add_SoundList(FLASHLIGHT, 0.29f, "Flashlight/flashlight_get_04.ogg", 1700.f, 15.f);

}

void CItem::Add_SoundList(const _uint & m_iState, const _float Ratio, const char * SoundName, const _float & SoundDistance, const _float & SoundValue, const _int & OtherOption)
{
	SoundData Data;
	Data.m_iState = m_iState;
	Data.Ratio = Ratio;
	strcpy_s(Data.SoundName, SoundName);
	Data.SoundDistance = SoundDistance;
	Data.SoundValue = SoundValue;     
	Data.OtherOption = OtherOption;
	m_SoundList.push_back(Data);
}

void CItem::ComunicateWithServer()
{
	if (server_data.Game_Data.Item[m_eObjectID - ITEM] == 0)
	{
		m_isDead = true;
		return;
	}
		
	if (!bStartGameServer)
		return;
	

	/*GET_INSTANCE_MANAGEMENT;

	if (pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper").size() >= 1000)
	{
		Safe_Release(pManagement);
		return;
	}

	for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
	{
		if (nullptr == pCamper)
			continue;
		if (server_data.Campers[pCamper->GetID() - CAMPER].iItem == 0)
			continue;
		
		if ((pCamper->GetID() - CAMPER) == (exPlayerNumber - 1))
			continue;
		if (nullptr == m_pRHandMatrix &&
			server_data.Campers[pCamper->GetID() - CAMPER].iItem == server_data.Game_Data.Item[m_eObjectID - ITEM])
		{
			if (((CCamper*)pCamper)->GetCurItem() == nullptr)
			{
				_int PlayerIndex = _int(server_data.Game_Data.Item[m_eObjectID - ITEM] / 10);

				CTransform* pTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
				if (nullptr == pTransform)
					continue;

				_matrix* RHandMatrix = nullptr;
				RHandMatrix = (_matrix*)((CCamper*)pCamper)->Get_CalRHandMatrix();
				if (nullptr == RHandMatrix)
					continue;

				((CCamper*)pCamper)->SetItem(this);
				Pick_Item(pTransform, RHandMatrix);
			}
			else if (((CCamper*)pCamper)->GetCurItem()->Get_IsDead())
			{
			
			}
			else
			{
				_int PlayerIndex = _int(server_data.Game_Data.Item[m_eObjectID - ITEM] / 10);

				CTransform* pTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
				if (nullptr == pTransform)
					continue;

				_matrix* RHandMatrix = nullptr;
				RHandMatrix = (_matrix*)((CCamper*)pCamper)->Get_CalRHandMatrix();
				if (nullptr == RHandMatrix)
					continue;

				if (nullptr == ((CCamper*)pCamper)->GetCurItem())
				{
					((CCamper*)pCamper)->GetCurItem()->Drop_Item((_vec3*)m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
					((CCamper*)pCamper)->SetItem(this);
					Pick_Item(pTransform, RHandMatrix);
				}
			}
		}
		else if (nullptr != m_pRHandMatrix
			&& server_data.Campers[pCamper->GetID() - CAMPER].iItem == server_data.Game_Data.Item[m_eObjectID - ITEM])
		{
			if(!server_data.Campers[pCamper->GetID() - CAMPER].bConnect
				&& server_data.Game_Data.Item[m_eObjectID - ITEM] == server_data.Campers[pCamper->GetID() - CAMPER].iItem)
				Drop_Item(&_vec3(0,0,0));

			_int CurItem = server_data.Game_Data.Item[m_eObjectID - ITEM];
			_int PlayerItem = server_data.Campers[pCamper->GetID() - CAMPER].iItem;
			
			if (PlayerItem == 0 && ((CCamper*)pCamper)->GetCurItem() == this)
				m_isDead = true;
			
		}

		if (nullptr != ((CCamper*)pCamper)->GetCurItem() && (((CCamper*)pCamper)->GetCurItem()->GetID()) == (m_eObjectID)
			&& server_data.Campers[pCamper->GetID() - CAMPER].iItem != server_data.Game_Data.Item[m_eObjectID - ITEM])
		{
			Drop_Item(&server_data.Campers[pCamper->GetID() - CAMPER].vPos);
			((CCamper*)pCamper)->SetItem(nullptr);
		}
	}

	Safe_Release(pManagement);*/
}

void CItem::Cal_LightPos()
{
	if (nullptr == m_pRHandMatrix)
		return;
	if (m_eType != FLASHLIGHT)
		return;
	if (!m_bUseNow)
		return;
	if (m_iSoundCheck == 0)
	{
		GET_INSTANCE(CSoundManager)->PlaySound(string("Flashlight/flashlight_on_01.ogg"), _vec3(m_matParent._41, m_matParent._42, m_matParent._43), 1700.f, 1.f);
		m_iSoundCheck++;
	}
	_int PlayerIndex = _int(server_data.Game_Data.Item[m_eObjectID - ITEM] / 10);

	if (PlayerIndex == exPlayerNumber)
		if(!KEYMGR->MousePressing(1))
			return;
	else
		if (!server_data.Campers[PlayerIndex - 1].bUseItem)
			return;

	_vec3 vPosition;
	memcpy(&vPosition, &m_matParent.m[3], sizeof(_vec3));
	m_pCustomLight->Set_Position(vPosition);

	//_matrix matView;
	//_vec3 vRHandPos, vCamLook, vCamPos;

	//m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	//D3DXMatrixInverse(&matView, nullptr, &matView);


	//memcpy(&vCamLook, &matView.m[2], sizeof(_vec3));
	//memcpy(&vCamPos, &matView.m[3], sizeof(_vec3));
	//memcpy(&vRHandPos, &m_matParent.m[3], sizeof(_vec3));

	//vCamLook *= 500.f;
	//vCamPos += vCamLook;

	//_vec3 vFlashDirection = vCamPos - vRHandPos;
	_vec3 vRHandDir;
	memcpy(&vRHandDir, &m_matParent.m[1], sizeof(_vec3));

	D3DXVec3Normalize(&vRHandDir, &vRHandDir);

	m_pCustomLight->Set_Direction(vRHandDir);
}

HRESULT CItem::Ready_Component()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	// For.Com_Transform
	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	// For.Com_Renderer
	m_pRendererCom = (CRenderer*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Renderer");
	if (FAILED(Add_Component(L"Com_Renderer", m_pRendererCom)))
		return E_FAIL;

	// For.Com_Shader
	m_pShaderCom = (CShader*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Shader_Mesh");
	if (FAILED(Add_Component(L"Com_Shader", m_pShaderCom)))
		return E_FAIL;

	// For.Com_Frustum
	m_pFrustumCom = (CFrustum*)pManagement->Clone_Component(SCENE_STAGE, L"Component_Frustum");
	if (FAILED(Add_Component(L"Com_Frustum", m_pFrustumCom)))
		return E_FAIL;

	Safe_Release(pManagement);

	return NOERROR;
}

HRESULT CItem::SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint & iAttributeID)
{
	m_pTransformCom->SetUp_OnShader(pEffect, "g_matWorld");

	_matrix		matView, matProj, matVP;

	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	m_pGraphic_Device->GetTransform(D3DTS_PROJECTION, &matProj);

	matVP = matView * matProj;

	pEffect->SetMatrix("g_matVP", &matVP);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	pEffect->SetTexture("g_DiffuseTexture", pSubSet->MeshTexture.pDiffuseTexture);

	pEffect->SetTexture("g_MetalicTexture", pSubSet->MeshTexture.pMetallicTexture);
	pEffect->SetTexture("g_RoughnessTexture", pSubSet->MeshTexture.pRoughnessTexture);

	return NOERROR;
}

void CItem::Check_Player()
{
	if (nullptr != m_pRHandMatrix)
	{
		return;
	}		
	if (!bStartGameServer)
		return;
	if (5 == exPlayerNumber)
		return;

	GET_INSTANCE_MANAGEMENT;

	for (auto& pCamper : pManagement->Get_ObjectList(SCENE_STAGE, L"Layer_Camper"))
	{
		if ((pCamper->GetID() - CAMPER) != (exPlayerNumber - 1))
			continue;

		CTransform* pTransform = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
		_vec3 vPlayerPos = *pTransform->Get_StateInfo(CTransform::STATE_POSITION);
		_vec3 vMyPos = *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION);
		_float fDist = D3DXVec3Length(&(vPlayerPos - vMyPos));
		if (fDist > 100.f)
		{
			if(((CCamper*)pCamper)->GetColledItem() == this)
				((CCamper*)pCamper)->SetColledItem(nullptr);

			continue;
		}
			
		((CCamper*)pCamper)->SetColledItem(this);
		break;
	}

	Safe_Release(pManagement);
}

CItem * CItem::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CItem*	pInstance = new CItem(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CItem Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CItem::Clone_GameObject()
{
	CItem*	pInstance = new CItem(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CItem Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CItem::Free()
{
	Safe_Release(m_pLightCollider);
	Safe_Release(m_pCustomLight);
	Safe_Release(m_pColliderCom);
	Safe_Release(m_pTransformCom);
	Safe_Release(m_pRendererCom);
	Safe_Release(m_pShaderCom);
	Safe_Release(m_pMeshCom);
	Safe_Release(m_pFrustumCom);

	CGameObject::Free();
}
