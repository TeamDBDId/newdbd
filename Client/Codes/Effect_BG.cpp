#include "stdafx.h"
#include "Effect_BG.h"
#include "Management.h"

#include "Camper.h"

#include "Math_Manager.h"
_USING(Client)

CEffect_BG::CEffect_BG(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_BG::CEffect_BG(const CEffect_BG & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_BG::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.
	GET_INSTANCE(CSoundManager)->PlaySound("IngameUI/GeneratorFin.ogg", _vec3(), 0, 1.f);
	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_BG::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;



	m_pTransformCom->Scaling(300.f, 300.f, 0.f);


	return NOERROR;
}

_int CEffect_BG::Update_GameObject(const _float & _fTick)
{
	if (true == m_isDead)
		return 1;

	m_fTime += _fTick;
	if (8.f < m_fTime)
	{
		m_isDead = true;
	}





	return _int();
}

_int CEffect_BG::LastUpdate_GameObject(const _float & _fTick)
{
	Compute_CameraDistance(m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION));
	if (m_fCameraDistance < 1500.f)
		return NOERROR;


	if (nullptr == m_pRendererCom)
		return -1;


	_matrix		matView;
	m_pGraphic_Device->GetTransform(D3DTS_VIEW, &matView);
	D3DXMatrixInverse(&matView, nullptr, &matView);

	_vec3		vRight, vUp, vLook;


	vRight = *(_vec3*)&matView.m[0][0] * m_pTransformCom->Get_Scale().x;
	vUp = *(_vec3*)&matView.m[1][0] * m_pTransformCom->Get_Scale().y;
	vLook = *(_vec3*)&matView.m[2][0] * m_pTransformCom->Get_Scale().z;

	m_pTransformCom->Set_StateInfo(CTransform::STATE_RIGHT, &vRight);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_UP, &vUp);
	m_pTransformCom->Set_StateInfo(CTransform::STATE_LOOK, &vLook);





	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_UI, this)))
		return -1;



	return _int();
}

void CEffect_BG::Render_GameObject()
{


	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(_E_EXIT1);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);
	/////////////////////////////////////////////////////////////////////////////////////////

	if (nullptr == m_pMeshCom ||
		nullptr == m_pShaderCom)
		return;

	LPD3DXEFFECT		pEffect2 = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect2)
		return;

	pEffect2->AddRef();

	_ulong dwNumMaterials = m_pMeshCom->Get_NumMaterials();

	pEffect2->Begin(nullptr, 0);

	pEffect2->BeginPass(_E_GENON);

	for (size_t i = 0; i < dwNumMaterials; i++)
	{
		if (FAILED(SetUp_ConstantTable(pEffect2, i)))
			return;

		pEffect2->CommitChanges();

		m_pMeshCom->Render_Mesh(i);
	}

	pEffect2->EndPass();
	pEffect2->End();

	Safe_Release(pEffect2);

	if(BG_HOOK==m_eKind && nullptr != m_pCamper)
		m_pCamper->Render_UIImage();
}

void CEffect_BG::Set_Param(const _vec3& _vPos, const void* _pVoid)
{

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &_vPos);

	m_pGenWorld = *(_matrix*)_pVoid;

	_matrix matRot;
	D3DXMatrixRotationY(&matRot,D3DXToRadian(90.f));


	_vec3 vR;
	_vec3 vU;
	_vec3 vL;

	D3DXVec3TransformNormal(&vR,(_vec3*)&m_pGenWorld._11,&matRot);
	D3DXVec3TransformNormal(&vU, (_vec3*)&m_pGenWorld._21, &matRot);
	D3DXVec3TransformNormal(&vL, (_vec3*)&m_pGenWorld._31, &matRot);

	memcpy(&m_pGenWorld.m[0], &vR, sizeof(_vec3));
	memcpy(&m_pGenWorld.m[1], &vU, sizeof(_vec3));
	memcpy(&m_pGenWorld.m[2], &vL, sizeof(_vec3));


	/*_vec3 vR = *(_vec3*)&m_pGenWorld._11;
	_vec3 vL = *(_vec3*)&m_pGenWorld._31;

	_float fTemp = vR.x;
	vR.x = vR.z;
	vR.z = fTemp;

	fTemp = vL.x;
	vL.x = vL.z;
	vL.z = fTemp;*/

/*
	memcpy(&m_pGenWorld.m[0], &vR, sizeof(_vec3));
	memcpy(&m_pGenWorld.m[2], &vL, sizeof(_vec3));*/

	
	////d3dxmatrota

	//m_pGenWorld *= matRot;
	//int a = 0;

	///*D3DXMatrixIdentity(&m_pGenWorld);
	//memcpy(&m_pGenWorld.m[3], &_vPos, sizeof(_vec3));*/
//	m_pGenWorld = *(_matrix*)_pVoid;

	

/*
	m_pGenWorld = *(_matrix*)_pVoid;
	_vec3 vUp = _vec3(0.f, 1.f, 0.f);
	_vec3 vLook = *(_vec3*)&m_pGenWorld._31;
	D3DXVec3Normalize(&vLook,&vLook);
	_vec3 vRight;
	D3DXVec3Cross(&vRight,&vUp,&vLook);

	_vec3 vPos = _vPos - vLook*50.f;
	vPos.y = 0.f;

	memcpy(&m_pGenWorld.m[0], &vRight, sizeof(_vec3));
	memcpy(&m_pGenWorld.m[1], &vUp, sizeof(_vec3));
	memcpy(&m_pGenWorld.m[2], &vLook, sizeof(_vec3));
	memcpy(&m_pGenWorld.m[3], &vPos, sizeof(_vec3));*/


	const _tchar* meshName;
	switch (m_eKind)
	{
	case BG_GENON:
		meshName = L"Com_Mesh_GenOn";
		break;
	case BG_HOOK:
		meshName = L"Com_Mesh_HookOn";
		break;
	}

	m_pMeshCom = (CMesh_Static*)GET_INSTANCE(CManagement)->Clone_Component(SCENE_STAGE, meshName);
	Add_Component(L"Com_Mesh", m_pMeshCom);
}

void CEffect_BG::Set_Kind(const eKind & _eKind, void * _pCamper)
{
	m_eKind = _eKind;

	if (BG_HOOK == m_eKind)
		m_pCamper = (CCamper*)_pCamper;
}



HRESULT CEffect_BG::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Effect");


	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Exit_Round");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;


	//m_pTextureCom[3] = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Fail_Noise");
	//if (FAILED(Add_Component(L"Com_Texture3", m_pTextureCom[3])))
	//	return E_FAIL;


	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_BG::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);


	_float m_fSize = 0.f;
	if (m_fTime < 0.75f)
	{
		m_fBGSize = sqrtf(m_fTime*1.33334f);
		m_fSize = (0.75f - m_fTime)*1.33334f;
	}
	else if (7.25f < m_fTime)
	{
		m_fBGSize = sqrtf((8 - m_fTime)*1.33334f);
		m_fSize = (m_fTime - 7.25f)*1.33334f;
	}
	else
		m_fSize = 0.f;


	_pEffect->SetFloat("g_fSize", m_fSize);
	//_float m_fSin = (sinf(m_fTime*5.f)*0.1f);
	//_pEffect->SetFloat("g_fSin", m_fSin);

	_float fDist = m_fCameraDistance*0.001f;
	fDist *= m_fBGSize;
	m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");


	_matrix matWorld = m_pTransformCom->Get_Matrix();
	matWorld._11 *= fDist;
	matWorld._12 *= fDist;
	matWorld._13 *= fDist;
	matWorld._21 *= fDist;
	matWorld._22 *= fDist;
	matWorld._23 *= fDist;
	_pEffect->SetMatrix("g_matWorld", &matWorld);



	return NOERROR;
}

HRESULT CEffect_BG::SetUp_ConstantTable(LPD3DXEFFECT _pEffect, const _uint & iAttributeID)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);


	//m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");

	_pEffect->SetMatrix("g_matWorld", &m_pGenWorld);

	const SUBSETDESC* pSubSet = m_pMeshCom->Get_SubSetDesc(iAttributeID);
	if (nullptr == pSubSet)
		return E_FAIL;

	//m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex0");

	return NOERROR;

}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_BG * CEffect_BG::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_BG*	pInst = new CEffect_BG(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_BG::Clone_GameObject()
{
	CEffect_BG*	pInst = new CEffect_BG(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("CEffect_BG Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_BG::Free()
{
	Safe_Release(m_pBufferCom);


	Safe_Release(m_pTransformCom);

	Safe_Release(m_pTextureCom);

	Safe_Release(m_pMeshCom);

	CBaseEffect::Free();
}
