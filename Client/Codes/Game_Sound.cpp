#include "stdafx.h"
#include "Game_Sound.h"
#include "Management.h"
#include "Math_Manager.h"
#include <string>
#include "Slasher.h"
#include "Camper.h"
_USING(Client);

CGame_Sound::CGame_Sound(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CGame_Sound::CGame_Sound(const CGame_Sound & rhs)
	: CGameObject(rhs)
	, m_pSoundManager(rhs.m_pSoundManager)
{
	m_pSoundManager->AddRef();
}


HRESULT CGame_Sound::Ready_Prototype()
{
	m_pSoundManager = GET_INSTANCE(CSoundManager);
	m_pSoundManager->AddRef();
	return NOERROR;
}

HRESULT CGame_Sound::Ready_GameObject()
{
	m_AngerTimer = 0.f;
	m_IsSlasherAnger = false;
	m_fHearthTimer = 0.f;
	m_fChangeTimer = 0.f;
	return NOERROR;
}

_int CGame_Sound::Update_GameObject(const _float & fTimeDelta)
{
	if (m_pSlasher == nullptr)
		Set_Player();
	Check_Observe();
	SlasherAngerCheck(fTimeDelta);
	Camper_Check(fTimeDelta);
	Slasher_Check(fTimeDelta);
	Change_State(fTimeDelta);
	Update_BGM();
	return _int();
}

_int CGame_Sound::LastUpdate_GameObject(const _float & fTimeDelta)
{
	return _int();
}

void CGame_Sound::SlasherAngerCheck(const _float& fTimeDelta)
{
	_bool IsLook;
	if (m_iPlayerNum != 5)
	{
		if (m_pCamper == nullptr)
			return;

		IsLook = m_pCamper->IsLook();
		m_pCamper->Set_IsLook(false);
	}
	else
	{
		IsLook = m_pSlasher->IsLook();
		m_pSlasher->Set_IsLook(false);
	}
	
	if (m_IsSlasherAnger)
	{
		if (IsLook)
			m_AngerTimer = 5.f;
		else if (m_AngerTimer <= 0.f)
		{
			m_IsSlasherAnger = false;
			m_AngerTimer = 0.f;
		}
		else
			m_AngerTimer -= fTimeDelta;
	}
	else
	{
		if (IsLook && m_AngerTimer >= 2.f)
		{
			m_IsSlasherAnger = true;
			m_AngerTimer = 5.f;
		}
		else if (IsLook)
			m_AngerTimer += fTimeDelta;
		else
			m_AngerTimer = 0.f;
	}

	if ((server_data.Slasher.iCharacter == 0 &&	server_data.Slasher.iSkill == WRAITH_HIDEINGSKILL) ||
	(server_data.Slasher.iCharacter == 1 && server_data.Slasher.iSkill == SPIRIT_SKILLUSE))
	{
		m_IsSlasherAnger = false;
		m_AngerTimer = 0.f;
	}
}

void CGame_Sound::Set_Player(_int PlayerexNum)
{
	m_iPlayerNum = PlayerexNum;
	m_iSlasherCharictorNum = server_data.Slasher.iCharacter;
	SetUp_Player();
	SetUp_SoundMap();
	SetUp_BaseSound();
	m_pSoundManager->PlaySound(string("BGM"), string("Ingame_Music/Farm_SlaughterHouse_Bounce.ogg"), _vec3(), 0, 0.3f);
}

void CGame_Sound::Set_Player()
{
	m_iSlasherCharictorNum = server_data.Slasher.iCharacter;
	SetUp_Player();
	SetUp_SoundMap();
}
     
void CGame_Sound::SetUp_Player()
{
	GET_INSTANCE_MANAGEMENT;
	m_pSlasher = (CSlasher*)pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Slasher", SLASHER);
	if(m_iPlayerNum != 5)
		m_pCamper = (CCamper*)pManagement->Get_GameObjectFromObjectID(SCENE_STAGE, L"Layer_Camper", CAMPER + m_iPlayerNum-1);
	Safe_Release(pManagement);
}

void CGame_Sound::SetUp_BaseSound()
{
	
	string SoundName;
	if (m_iPlayerNum != 5)
	{
		m_CurSituation = C_Normal;
		m_OldSituation = C_Normal;
		auto& iter = m_MapSound.find(C_Normal);
		_int BaseSound = Math_Manager::CalRandIntFromTo(1, iter->second.FileNum);
		SoundName = iter->second.FileName + to_string(BaseSound) + string(".ogg");
		m_pSoundManager->PlaySound(string("Situation"), SoundName, _vec3(),0, 0.3f);
	}
	else
	{
		m_CurSituation = S_Normal;
		m_OldSituation = S_Normal;
		auto& iter = m_MapSound.find(S_Normal);
		_int BaseSound = Math_Manager::CalRandIntFromTo(1, iter->second.FileNum);
		SoundName = iter->second.FileName + to_string(BaseSound) + string(".ogg");
		m_pSoundManager->PlaySound(string("Situation"), SoundName, _vec3(), 0, 0.3f);
	}
}

void CGame_Sound::Change_State(const _float& fDeltaTime)
{
	if (m_fChangeTimer > 0.f)
	{
		auto& iter = m_MapSound.find(m_CurSituation);
		m_pSoundManager->ChannelVolume(string("Situation"),  iter->second.fVolume* Math_Manager::ProgressToPersent(0.f, 1.f, m_fChangeTimer / 3.f));
		m_fChangeTimer -= fDeltaTime;

		if (m_fChangeTimer <= 0.f)
		{
			string SoundName = iter->second.FileName + to_string(iter->second.FileNum) + string(".ogg");
			m_pSoundManager->PlaySound(string("Situation"), SoundName, _vec3(), 0, iter->second.fVolume);
			m_fChangeTimer = 0.f;
		}
	}
	if (m_CurSituation != m_OldSituation)
	{
		m_OldSituation = m_CurSituation;
		m_fChangeTimer = 3.f;
	}

}

void CGame_Sound::SetUp_SoundMap()
{
	if (m_MapSound.size() == 0)
	{
		m_MapSound.insert({ S_Carrying,{"Situation/mu_killer_carrying_0", 3, 0.4f } });
		m_MapSound.insert({ S_Exit, {"Situation/mu_killer_exit_0", 3, 0.4f } });
		m_MapSound.insert({ S_Normal, {"Situation/mu_killer_normal_0", 3, 0.3f } });
		m_MapSound.insert({ S_TallyScreen, {"Situation/mu_killer_tallyscreen_0", 1,0.3f } });
		m_MapSound.insert({ S_Chase,{ "Situation/mu_killer_chase_0",3, 0.4f } });
		m_MapSound.insert({ C_Captured,{"Situation/mu_survivor_captured_0", 2, 0.4f } });
		m_MapSound.insert({ C_Dead,{"Situation/mu_survivor_dead_0", 2, 0.6f } });
		m_MapSound.insert({ C_Dying,{"Situation/mu_survivor_dying_0", 4, 0.5f } });
		m_MapSound.insert({ C_Hooked_1,{"Situation/mu_survivor_hooked_part1_0", 3, 0.5f } });
		m_MapSound.insert({ C_Hooked_2,{"Situation/mu_survivor_hooked_part2_0", 2, 0.6f } });
		m_MapSound.insert({ C_Injured,{"Situation/mu_survivor_injured_0", 3, 0.3f } });
		m_MapSound.insert({ C_Normal,{"Situation/mu_survivor_normal_0", 3, 0.3f } });
		m_MapSound.insert({ C_TallyScreen,{"Situation/mu_survivor_tallyscreen_0", 1 } });
	}
}

void CGame_Sound::Camper_Check(const _float & fDeltaTime)
{
	if (m_iPlayerNum == 5 || m_pCamper == nullptr)
		return;

	if (m_pCamper->GetCurCondition() == CCamper::BLOODDEAD || m_pCamper->GetCurCondition() == CCamper::HOOKDEAD)
		m_CurSituation = C_Dead;
	else if (m_pCamper->GetCurCondition() == CCamper::HOOKED)
	{
		if (m_pCamper->GetEnergy() >= m_pCamper->GetMaxEnergy() * 0.5f)
			m_CurSituation = C_Hooked_2;
		else
			m_CurSituation = C_Hooked_1;
	}
	else if (m_pCamper->GetCurCondition() == CCamper::DYING)
		m_CurSituation = C_Dying;
	else if (m_IsSlasherAnger)
		m_CurSituation = C_Captured;
	else if (m_pCamper->GetCurCondition() == CCamper::INJURED)
		m_CurSituation = C_Injured;
	else if (m_pCamper->GetCurCondition() == CCamper::SPECIAL || m_pCamper->GetCurCondition() == CCamper::OBSTACLE);
	else
		m_CurSituation = C_Normal;

	if (m_pSlasher != nullptr)
		Heart_Sound(fDeltaTime);
	
}

void CGame_Sound::Slasher_Check(const _float& fDeltaTime)
{
	if (m_iPlayerNum != 5 || m_pSlasher == nullptr)
		return;

	if (m_pSlasher->IsCarried())
		m_CurSituation = S_Carrying;
	else if (m_IsSlasherAnger)
		m_CurSituation = S_Chase;
	else if (GET_INSTANCE(CUIManager)->Get_GeneratorNum() == 0)
		m_CurSituation = S_Exit;
	else
		m_CurSituation = S_Normal;
}

void CGame_Sound::Heart_Sound(const _float& fDeltaTime)
{
	if (m_pSlasher->Get_IsDead())
	{
		m_pSlasher = nullptr;
		return;
	}
	_vec3 SlasherPos = m_pSlasher->Get_Pos();
	_vec3 CamperPos = m_pCamper->Get_Pos();
	_vec3 MinusPos = SlasherPos - CamperPos;
	_float fLength = D3DXVec3Length(&MinusPos);
	_float fMaxLength = WRAITH_TERROR_RADIUS;
	if (m_iSlasherCharictorNum == 1)
	{
		fMaxLength = SPIRIT_TERROR_RADIUS;
		if (server_data.Slasher.iSkill == SPIRIT_SKILLUSE)
			return;
	}
	else
	{
		if (server_data.Slasher.iSkill == WRAITH_HIDEINGSKILL)
			return;
	}
	if (fMaxLength < fLength)
		return;

	m_fHearthTimer += fDeltaTime;
	_float Volume = Math_Manager::ProgressToPersent(1.5f, 0.f, fLength / fMaxLength);
	_float Timer = Math_Manager::ProgressToPersent(0.3f, 1.8f, fLength / fMaxLength);

	if (m_fHearthTimer >= Timer)
	{
		m_pSoundManager->PlaySound(string("Lobby/Heart_Bounce.ogg"),_vec3(),0,Volume);
		m_fHearthTimer = 0.f;
	}
}

void CGame_Sound::Update_BGM()
{
		
}

void CGame_Sound::Check_Observe()
{
	if (!bObserver)
		return;

	if (m_iPlayerNum != exPlayerNumber)
		Set_Player(exPlayerNumber);
}

CGame_Sound * CGame_Sound::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CGame_Sound*	pInstance = new CGame_Sound(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CGame_Sound Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CGame_Sound::Clone_GameObject()
{
	CGame_Sound*	pInstance = new CGame_Sound(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CGame_Sound Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CGame_Sound::Free()
{
	Safe_Release(m_pSoundManager);
	CGameObject::Free();
}
