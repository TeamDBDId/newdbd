#include "stdafx.h"
#include "..\Headers\UI_GameState.h"
#include "Management.h"
#include "UI_Texture.h"
#include "Camper.h"
#include <string>

_USING(Client);




CUI_GameState::CUI_GameState(LPDIRECT3DDEVICE9 pGraphic_Device)
	: CGameObject(pGraphic_Device)
{
}

CUI_GameState::CUI_GameState(const CUI_GameState & rhs)
	: CGameObject(rhs)
{
}


HRESULT CUI_GameState::Ready_Prototype()
{
	return NOERROR;
}

HRESULT CUI_GameState::Ready_GameObject()
{
	for (int i = 0; i < 4; i++)
	{
		m_fChangeState[i] = 0.f;
		m_CurState[i] = HEALTHY;
		m_OldState[i] = HEALTHY;
	}
	m_fChangeExit = 0.f;
	Set_CamperStateUI();
	Set_BasicUI();
	Set_ExitState();
	m_bUI = true;
	return NOERROR;
}

_int CUI_GameState::Update_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	if(!m_bUI)
		return _int();
	for (_int i = 0; i < 4; i++)
	{
		if (m_fChangeState[i] > 0.f)
			m_fChangeState[i] -= fTimeDelta;
		else
			m_fChangeState[i] = 0.f;
	}
	if (m_fChangeExit  > 0.f)
		m_fChangeExit -= fTimeDelta;
	else
		m_fChangeExit = 0.f;
	Update_Generator();

	Check_CamperState();
	Update_CamperState();
	Update_ChangeState();
	return _int();
}

_int CUI_GameState::LastUpdate_GameObject(const _float & fTimeDelta)
{
	if (m_isDead)
		return 1;
	return _int();
}

void CUI_GameState::Set_Rendering(_bool IsRendering)
{
	if (m_BaseUI != nullptr)
	{
		m_BaseUI->IsRendering(IsRendering);
	}
	for (_int i = 0; i < 4; i++)
	{
		if(m_pCamperBlood[i] != nullptr)
			m_pCamperBlood[i]->IsRendering(IsRendering);
		if (m_pCamperSmoke[i] != nullptr)
			m_pCamperSmoke[i]->IsRendering(IsRendering);
		if (m_pCamperState[i] != nullptr)
			m_pCamperState[i]->IsRendering(IsRendering);
		if (m_pCamperHpBase[i] != nullptr)
			m_pCamperHpBase[i]->IsRendering(IsRendering);
		if (m_pCamperHpBar[i] != nullptr)
			m_pCamperHpBar[i]->IsRendering(IsRendering);
		if (m_pCamperName[i] != nullptr)
			m_pCamperName[i]->IsRendering(IsRendering);
		if (i < 2)
		{
			if (m_pExitState[i] != nullptr)
				m_pExitState[i]->IsRendering(IsRendering);
		}
	}
}

void CUI_GameState::Set_Delete()
{
	if (m_BaseUI != nullptr)
	{
		m_BaseUI->SetDead();
		m_BaseUI = nullptr;
	}
	for (_int i = 0; i < 4; i++)
	{
		m_pCamperBlood[i]->SetDead();
		m_pCamperBlood[i] = nullptr;
		m_pCamperSmoke[i]->SetDead();
		m_pCamperSmoke[i] = nullptr;
		m_pCamperState[i]->SetDead();
		m_pCamperState[i] = nullptr;
		m_pCamperHpBase[i]->SetDead();
		m_pCamperHpBase[i] = nullptr;
		m_pCamperHpBar[i]->SetDead();
		m_pCamperHpBar[i] = nullptr;
		m_pCamperName[i]->SetDead();
		m_pCamperName[i] = nullptr;
		if (i < 2)
		{
			m_pExitState[i]->SetDead();
			m_pExitState[i] = nullptr;
		}
	}
	m_bUI = false;
}

HRESULT CUI_GameState::Set_CamperStateUI()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_vec2 vPos = { 0.07f, 0.91f };

	for (int i = 0; i < m_iCamperNum; i++)
	{
		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pCamperBlood[i])))
			return E_FAIL;
		m_pCamperBlood[i]->Set_TexName(wstring(L"PlayerStatus_Blood1.tga"));
		m_pCamperBlood[i]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_pCamperBlood[i]->Set_Scale(_vec2(0.6f, 0.6f));
		m_pCamperBlood[i]->Set_Alpha(0.5f);
		m_pCamperBlood[i]->IsRendering(false);

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pCamperSmoke[i])))
			return E_FAIL;
		m_pCamperSmoke[i]->Set_TexName(wstring(L"PlayerStatus_BloodSmoke5.tga"));
		m_pCamperSmoke[i]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_pCamperSmoke[i]->Set_Scale(_vec2(0.6f, 0.6f));
		m_pCamperSmoke[i]->Set_Alpha(1.f);
		m_pCamperSmoke[i]->IsRendering(false);


		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pCamperState[i])))
			return E_FAIL;
		m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Idle.tga"));
		m_pCamperState[i]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
		m_pCamperState[i]->Set_Alpha(0.5f);
		m_pCamperState[i]->Set_Scale(_vec2(0.6f, 0.6f));

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pCamperHpBase[i])))
			return E_FAIL;
		m_pCamperHpBase[i]->Set_TexName(wstring(L"PlayerHp_Base.tga"));
		m_pCamperHpBase[i]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*(vPos.y+0.04f)));
		m_pCamperHpBase[i]->Set_Scale(_vec2(0.55f, 0.7f));
		m_pCamperHpBase[i]->Set_Alpha(0.5f);
		m_pCamperHpBase[i]->IsRendering(false);

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pCamperHpBar[i])))
			return E_FAIL;
		m_pCamperHpBar[i]->Set_TexName(wstring(L"PlayerHp_Bar.tga"));
		m_pCamperHpBar[i]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*(vPos.y + 0.04f)));
		m_pCamperHpBar[i]->Set_Scale(_vec2(0.55f, 0.7f));
		m_pCamperHpBar[i]->Set_Alpha(0.5f);
		m_pCamperHpBar[i]->IsRendering(false);

		if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pCamperName[i])))
			return E_FAIL;
		m_pCamperName[i]->Set_Font(wstring(L"생존자")+to_wstring(i+1), _vec2(12.f, 15.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f), 1);
		m_pCamperName[i]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*(vPos.y + 0.07f)));
		m_pCamperName[i]->Set_Alpha(1.f);

		vPos.x += 0.055f;
	}

	Safe_Release(pManagement);
	
	return NOERROR;
}

HRESULT CUI_GameState::Set_ExitState()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_vec2 vPos = { 0.09f, 0.808f };

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pExitState[0])))
		return E_FAIL;
	m_pExitState[0]->Set_TexName(wstring(L"Hud_GeneratorIcon.tga"));
	m_pExitState[0]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_pExitState[0]->Set_Scale(_vec2(0.6f, 0.6f));
	m_pExitState[0]->Set_Alpha(0.5f);
	vPos = { 0.0546f, 0.816f };

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_pExitState[1])))
		return E_FAIL;
	m_pExitState[1]->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_pExitState[1]->Set_Font(L"5", _vec2(25.f,35.f), D3DXCOLOR(1.f,1.f,1.f,1.f));
	m_pExitState[1]->Set_Alpha(0.5f);
	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CUI_GameState::Set_BasicUI()
{
	GET_INSTANCE_MANAGEMENTR(E_FAIL);

	_vec2 vPos = { 0.15f, 0.86f };

	if (FAILED(pManagement->Add_GameObjectToLayer(L"GameObject_UI_Texture", SCENE_STAGE, L"Layer_UI_GameState", (CGameObject**)&m_BaseUI)))
		return E_FAIL;
	m_BaseUI->Set_TexName(wstring(L"Hud_Line.tga"));
	m_BaseUI->Set_Pos(_vec2(g_iBackCX*vPos.x, g_iBackCY*vPos.y));
	m_BaseUI->Set_Scale(_vec2(0.6f, 0.6f));

	Safe_Release(pManagement);
	return NOERROR;
}

void CUI_GameState::Update_Generator()
{
	_int LeaveGenerator = 5;
	for (size_t i = 0; i < 7; i++)
	{
		if (server_data.Game_Data.GRP[i] >= 80.f)
			LeaveGenerator--; 
	}
	if (m_iLeaveGenerator == LeaveGenerator)
		return;
	GET_INSTANCE(CUIManager)->Set_GeneratorNum(LeaveGenerator);

	if (LeaveGenerator > 0)
	{
		_tchar Num[10] = L"";
		swprintf_s(Num, L"%d", LeaveGenerator);
		m_pExitState[1]->Set_Font(Num, _vec2(25.f, 35.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
	}
	else
	{
		m_pExitState[0]->Set_Font(L"나가기", _vec2(25.f, 23.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f),1);
		m_pExitState[0]->IsColor(true);
		m_pExitState[0]->Set_Color(D3DXCOLOR(1.f,0.f,0.f,1.f));
		m_pExitState[1]->Set_Font(L"출구를 찾아", _vec2(10.f, 12.f), D3DXCOLOR(1.f, 1.f, 1.f, 1.f));
		m_pExitState[0]->Set_Pos(_vec2(0.0743385f* g_iBackCX, 0.839946f* g_iBackCY));
		m_pExitState[1]->Set_Pos(_vec2(0.0698925f* g_iBackCX, 0.815174f* g_iBackCY));
		bCanGoOut = true;
	}

	m_iLeaveGenerator = LeaveGenerator;
}

void CUI_GameState::Check_CamperState()
{
	for (size_t i = 0; i < MAX_Client; i++)
	{
		if (!server_data.Campers[i].bConnect)
			m_CurState[i] = UNCONNECTED;
		else if (server_data.Campers[i].iCondition == CCamper::HEALTHY)
			m_CurState[i] = HEALTHY;
		else if (server_data.Campers[i].iCondition == CCamper::INJURED)
			m_CurState[i] = INJURED;
		else if (server_data.Campers[i].iCondition == CCamper::DYING)
			m_CurState[i] = DYING;
		else if (server_data.Campers[i].iCondition == CCamper::HOOKED)
			m_CurState[i] = HOOKED;
		else if (server_data.Campers[i].iCondition == CCamper::HOOKDEAD)
			m_CurState[i] = HOOKDEAD;
		else if (server_data.Campers[i].iCondition == CCamper::BLOODDEAD)
			m_CurState[i] = BLOODDEAD;
		else if (server_data.Campers[i].iCondition == CCamper::ESCAPE)
			m_CurState[i] = ESCAPE;
	}
}

void CUI_GameState::Update_CamperState()
{
	for (size_t i = 0; i < MAX_Client; i++)
	{
		m_pCamperSmoke[i]->IsRendering(true);
		m_pCamperBlood[i]->IsRendering(false);
		m_pCamperHpBase[i]->IsRendering(false);
		m_pCamperHpBar[i]->IsRendering(false);
		m_pCamperState[i]->Set_Alpha(1.f);
		m_pCamperBlood[i]->Set_Alpha(1.f);
		m_pCamperSmoke[i]->Set_Alpha(1.f);
		if (m_CurState[i] == UNCONNECTED)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_UnConnected.tga"));
		else if (m_CurState[i] == HEALTHY)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Idle.tga"));
		else if (m_CurState[i] == INJURED)
		{
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Hurted.tga"));
			m_pCamperBlood[i]->Set_TexName(wstring(L"PlayerStatus_Blood1.tga"));
			m_pCamperBlood[i]->IsRendering(true);
			m_pCamperBlood[i]->Set_Scale(_vec2(0.6f,0.6f));
		}
		else if (m_CurState[i] == DYING)
		{
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Dying.tga"));
			m_pCamperBlood[i]->Set_TexName(wstring(L"PlayerStatus_Blood2.tga"));
			m_pCamperBlood[i]->Set_Scale(_vec2(0.6f, 0.6f));
			m_pCamperBlood[i]->IsRendering(true);
			m_pCamperHpBase[i]->IsRendering(true);
			m_pCamperHpBase[i]->Set_Alpha(1.f);
			m_pCamperHpBar[i]->IsRendering(true);
			m_pCamperHpBar[i]->Set_Alpha(1.f);
			m_pCamperHpBar[i]->Set_UV(_vec2(server_data.Campers[i].fEnergy / 240.f, 1.f));
		}
		else if (m_CurState[i] == HOOKED)
		{
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Hooked.tga"));
			m_pCamperBlood[i]->Set_TexName(wstring(L"PlayerStatus_Blood3.tga"));
			m_pCamperBlood[i]->Set_Scale(_vec2(0.6f, 0.6f));
			m_pCamperBlood[i]->IsRendering(true);
			m_pCamperHpBase[i]->IsRendering(true);
			m_pCamperHpBase[i]->Set_Alpha(1.f);
			m_pCamperHpBar[i]->IsRendering(true);
			m_pCamperHpBar[i]->Set_Alpha(1.f);
			m_pCamperHpBar[i]->Set_UV(_vec2(server_data.Campers[i].fEnergy / 120.f, 1.f));
		}
		else if (m_CurState[i] == HOOKDEAD)
		{
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Dead1.tga"));
			m_pCamperBlood[i]->Set_TexName(wstring(L"PlayerStatus_Blood4.tga"));
			m_pCamperBlood[i]->Set_Scale(_vec2(0.6f, 0.6f));
			m_pCamperBlood[i]->IsRendering(true);
		}
		else if (m_CurState[i] == BLOODDEAD)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Dead2.tga"));
		else if (m_CurState[i] == ESCAPE)
			m_pCamperState[i]->Set_TexName(wstring(L"PlayerStatus_Escape.tga"));
		m_pCamperState[i]->Set_Scale(_vec2(0.6f, 0.6f));
		if(m_CurState[i] == HOOKDEAD)
			m_pCamperState[i]->Set_Scale(_vec2(1.f, 1.f));
		m_fChangeState[i] = 5.f;
		m_OldState[i] = m_CurState[i];
	}
}

void CUI_GameState::Update_ChangeState()
{
	for (_int i = 0; i < 4; i++)
	{
		if (m_fChangeState[i] == 0.f)
			continue;
		if (m_fChangeState[i] >= 4.5f)
			m_pCamperSmoke[i]->Set_Alpha((5.f- m_fChangeState[i])*2.f);
		else if(m_fChangeState[i] >= 0.5f)
			m_pCamperSmoke[i]->Set_Alpha((m_fChangeState[i]-0.5f)/4.f);
		else if (m_fChangeState[i] > 0.f)
		{
			m_pCamperSmoke[i]->IsRendering(false);
			m_pCamperBlood[i]->Set_Alpha(m_fChangeState[i] + 0.5f);
			m_pCamperState[i]->Set_Alpha(m_fChangeState[i] + 0.5f);
			m_pCamperHpBase[i]->Set_Alpha(m_fChangeState[i] + 0.5f);
			m_pCamperHpBar[i]->Set_Alpha(m_fChangeState[i] +0.5f);
		}
	}

	if (m_fChangeExit == 0.f)
		return;

	if (m_fChangeExit >= 4.5f)
	{
		m_pExitState[0]->Set_Alpha((5.f - m_fChangeExit)*2.f);
		m_pExitState[1]->Set_Alpha((5.f - m_fChangeExit)*2.f);
	}
	else if(m_fChangeExit >= 0.5f)
	{
		m_pExitState[0]->Set_Alpha(1.f);
		m_pExitState[1]->Set_Alpha(1.f);
	}
	else if (m_fChangeExit > 0.f)
	{
		m_pExitState[0]->Set_Alpha(m_fChangeExit + 0.5f);
		m_pExitState[1]->Set_Alpha(m_fChangeExit + 0.5f);
	}
}

CUI_GameState * CUI_GameState::Create(LPDIRECT3DDEVICE9 pGraphic_Device)
{
	CUI_GameState*	pInstance = new CUI_GameState(pGraphic_Device);

	if (FAILED(pInstance->Ready_Prototype()))
	{
		MessageBox(0, L"CUI_GameState Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

CGameObject * CUI_GameState::Clone_GameObject()
{
	CUI_GameState*	pInstance = new CUI_GameState(*this);

	if (FAILED(pInstance->Ready_GameObject()))
	{
		MessageBox(0, L"CUI_GameState Created Failed", L"System Error", MB_OK);
		Safe_Release(pInstance);
	}
	return pInstance;
}

void CUI_GameState::Free()
{
	CGameObject::Free();
}
      