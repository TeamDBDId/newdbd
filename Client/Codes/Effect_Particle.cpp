#include "stdafx.h"
#include "Effect_Particle.h"
#include "Management.h"



#include "Math_Manager.h"
_USING(Client)

CEffect_Particle::CEffect_Particle(LPDIRECT3DDEVICE9 _pGDevice)
	: CBaseEffect(_pGDevice)
{
}

CEffect_Particle::CEffect_Particle(const CEffect_Particle & _rhs)
	: CBaseEffect(_rhs)

{

}


// 원형객체 생성될 때 호출.
HRESULT CEffect_Particle::Ready_Prototype()
{
	CBaseEffect::Ready_Prototype();

	// 파일 입출력 등, 초기화에 시간이 걸리는 ㄱ데이터들 셋.

	return NOERROR;
}

// 복사본객체 생성될 때 호출.
HRESULT CEffect_Particle::Ready_GameObject()
{
	CBaseEffect::Ready_GameObject();


	if (FAILED(Ready_Component()))
		return E_FAIL;

	

	m_pTransformCom->SetUp_Speed(300.f,D3DXToRadian(90.f));
	

	return NOERROR;
}

_int CEffect_Particle::Update_GameObject(const _float & _fTick)
{
	if (m_isDead)
		return 1;


	m_fTime += _fTick;
	if (1.5f < m_fTime)
		m_isDead = true;

	//m_pTransformCom->Move_V3Time(m_vMove,_fTick);
	m_pTransformCom->Move_Gravity(_fTick*2.f);
	m_pTransformCom->Go_Up(-_fTick);
	m_pTransformCom->Rotation_Axis(_fTick, m_pTransformCom->Get_StateInfo(CTransform::STATE_RIGHT));
	//m_pTransformCom->Rotation_Axis(-_fTick, m_pTransformCom->Get_StateInfo(CTransform::STATE_UP));

	

	return _int();
}

_int CEffect_Particle::LastUpdate_GameObject(const _float & _fTick)
{
	if (nullptr == m_pRendererCom)
		return -1;


	


	if (FAILED(m_pRendererCom->Add_RenderGroup(CRenderer::RENDER_ALPHA, this)))
		return -1;



	return _int();
}

void CEffect_Particle::Render_GameObject()
{

	if (nullptr == m_pBufferCom)
		return;

	// 셰이더를 이용해서 그려. 
	LPD3DXEFFECT	pEffect = m_pShaderCom->Get_EffectHandle();
	if (nullptr == pEffect)
		return;

	pEffect->AddRef();


	if (FAILED(SetUp_ContantTable(pEffect)))
		return;

	pEffect->Begin(nullptr, 0);
	pEffect->BeginPass(_E_PARTICLE);

	m_pBufferCom->Render_VIBuffer();

	pEffect->EndPass();
	pEffect->End();

	Safe_Release(pEffect);

}

void CEffect_Particle::Set_Param(const _vec3 & _vPos, const void * _pVoid)
{
	_vec3 vPos = _vPos;
	vPos.x += Math_Manager::CalRandFloatFromTo(-15.f, 15.f);
	vPos.y += Math_Manager::CalRandFloatFromTo(-15.f, 15.f);
	vPos.z += Math_Manager::CalRandFloatFromTo(-15.f, 15.f);


	//m_pTransformCom->SetUp_RotateXYZ(&pDir.x, &pDir.y, &pDir.z);

	m_pTransformCom->Set_StateInfo(CTransform::STATE_POSITION, &vPos);
	m_pTransformCom->Scaling(Math_Manager::CalRandFloatFromTo(1.f,1.5f), Math_Manager::CalRandFloatFromTo(6.f,9.f), Math_Manager::CalRandFloatFromTo(1.f, 1.5f));
	
	
	m_pTransformCom->SetUp_RotationX(D3DXToRadian(Math_Manager::CalRandFloatFromTo(-120.f, -30.f)));//위아래
	m_pTransformCom->SetUp_Rotation_Axis(Math_Manager::CalRandFloatFromTo(0.f, 360.f), m_pTransformCom->Get_StateInfo(CTransform::STATE_LOOK));
	

	//m_pTransformCom->Scaling(Math_Manager::CalRandFloatFromTo(2.f, 3.f),Math_Manager::CalRandFloatFromTo(10.f, 20.f) , 0.f);
	
}




HRESULT CEffect_Particle::Ready_Component()
{
	CBaseEffect::Ready_Component(L"Component_Shader_Effect");


	CManagement* pManagement = CManagement::GetInstance();
	if (nullptr == pManagement)
		return E_FAIL;
	pManagement->AddRef();


	m_pBufferCom = (CBuffer_RcTex*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Buffer_RcTex");
	if (FAILED(Add_Component(L"Com_Buffer", m_pBufferCom)))
		return E_FAIL;


	m_pTransformCom = (CTransform*)pManagement->Clone_Component(SCENE_STATIC, L"Component_Transform");
	if (FAILED(Add_Component(L"Com_Transform", m_pTransformCom)))
		return E_FAIL;

	m_pTextureCom = (CTexture*)pManagement->Clone_Component(SCENE_STAGE, L"Com_Tex_Particle");
	if (FAILED(Add_Component(L"Com_Texture", m_pTextureCom)))
		return E_FAIL;

	Safe_Release(pManagement);
	return NOERROR;
}

HRESULT CEffect_Particle::SetUp_ContantTable(LPD3DXEFFECT _pEffect)
{
	CBaseEffect::SetUp_ContantTable(_pEffect);



	m_pTransformCom->SetUp_OnShader(_pEffect, "g_matWorld");
	//_pEffect->SetMatrix("g_matWorld", &m_matWorld);

	m_pTextureCom->SetUp_OnShader(_pEffect, "g_Tex4");



	return NOERROR;
}

// 원형객체를 생성하기위해 만들어진 함수.
CEffect_Particle * CEffect_Particle::Create(LPDIRECT3DDEVICE9 _pGDevice)
{
	CEffect_Particle*	pInst = new CEffect_Particle(_pGDevice);

	if (FAILED(pInst->Ready_Prototype()))
	{
		Safe_Release(pInst);
	}
	return pInst;
}

// 원형객체가 호출해주는 함수(원형주소->Clone_GameObject())
// 복사본객첼르 생성하기위해.
CGameObject * CEffect_Particle::Clone_GameObject()
{
	CEffect_Particle*	pInst = new CEffect_Particle(*this);

	if (FAILED(pInst->Ready_GameObject()))
	{
		_MSG_BOX("Particle Created Failed");
		Safe_Release(pInst);
	}
	return pInst;
}

void CEffect_Particle::Free()
{
	Safe_Release(m_pBufferCom);
	Safe_Release(m_pTransformCom);

	Safe_Release(m_pTextureCom);


	CBaseEffect::Free();
}
