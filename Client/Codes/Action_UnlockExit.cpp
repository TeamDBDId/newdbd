#include "stdafx.h"
#include "..\Headers\Action_UnlockExit.h"
#include "Management.h"
#include "AnimationKey.h"
#include "Camper.h"
#include "ExitDoor.h"

_USING(Client)

CAction_UnlockExit::CAction_UnlockExit()
{
}

HRESULT CAction_UnlockExit::Ready_Action()
{
	if (m_bIsPlaying == true)
		return NOERROR;

	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_ExitDoor);

	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetOldCondition(pCamper->GetCurCondition());
	pCamper->SetCurCondition(CCamper::SPECIAL);
	m_bIsPlaying = true;
	pCamper->IsLockKey(true);
	m_UnlockTime = 0.f;
	pCamper->Set_State(AC::UnlockExit);
	m_iState = AC::UnlockExit;
	//cout << 1 << endl;
	if (m_pExitDoor != nullptr)
		m_pExitDoor->SetState(CExitDoor::SwitchActivation);
	

	return NOERROR;
}

_int CAction_UnlockExit::Update_Action(const _float & fTimeDelta)
{
	STATE eState = Check_Action(fTimeDelta);
	if (eState != UPDATE_ACTION)
		return eState;

	CMesh_Dynamic* pMeshCom = (CMesh_Dynamic*)m_pGameObject->Get_ComponentPointer(L"Com_Mesh");

	if (!KEYMGR->MousePressing(0))
	{
		m_pExitDoor->SetState(CExitDoor::Closed);
		return END_ACTION;
	}
	m_UnlockTime += fTimeDelta;
	if (m_pExitDoor->Get_ProgressTime() >= m_pExitDoor->Get_MaxProgressTime())
		return END_ACTION;



	if (m_iState == AC::UnlockExit && pMeshCom->IsOverTime(0.25f))
	{
		((CCamper*)m_pGameObject)->Set_State(AC::UnlockExitIdle);
		m_iState = AC::UnlockExitIdle;

		CTransform* pTrans = (CTransform*)(CCamper*)m_pGameObject->Get_ComponentPointer(L"Com_Transform");
		_vec3 vPos = *pTrans->Get_StateInfo(CTransform::STATE_POSITION) + *pTrans->Get_StateInfo(CTransform::STATE_LOOK)*60.f;
		vPos.y += 130.f;
		EFFMGR->Make_Effect(CEffectManager::E_ExitOpen, vPos);
	}
	Send_ServerData();
	 return UPDATE_ACTION;
}

void CAction_UnlockExit::End_Action()
{
	GET_INSTANCE(CUIManager)->Add_Score(CUI_Score::Objective, wstring(L"����"), (_int)(m_UnlockTime / m_pExitDoor->Get_MaxProgressTime()* 1250.f));
	GET_INSTANCE(CUIManager)->Set_ProgressBar(CUIManager::PG_None);
	m_bIsPlaying = false;
	camper_data.InterationObject = 0;
	CCamper* pCamper = (CCamper*)m_pGameObject;
	pCamper->SetCurCondition(pCamper->GetOldCondition());
	pCamper->Set_State(AC::Idle);
	pCamper->IsLockKey(false);

	CTransform* pTrans = (CTransform*)pCamper->Get_ComponentPointer(L"Com_Transform");
	_vec3 vPos = *pTrans->Get_StateInfo(CTransform::STATE_POSITION) + *pTrans->Get_StateInfo(CTransform::STATE_LOOK)*60.f;
	vPos.y += 120.f;
	EFFMGR->Make_Effect(CEffectManager::E_ExitOpen, vPos);
}

void CAction_UnlockExit::Send_ServerData()
{
	if (nullptr != m_pExitDoor)
	{
		camper_data.InterationObject = m_pExitDoor->GetID();
		camper_data.InterationObjAnimation = m_pExitDoor->Get_CurAnimation();
	}
	else
		camper_data.InterationObject = 0;
}

void CAction_UnlockExit::Free()
{
	CAction::Free();
}
