
matrix		g_matWorld, g_matView, g_matProj, g_matWVInv;// , g_matVInv, g_matPInv;// , g_matViewInv, g_matWldInv;

float		g_fAlpha;

texture		g_DecalTextures;
texture		g_DecalAlphaTextures;
texture		g_Gradient;
//texture		g_Color;

float		g_fUVX;
float		g_fUVY;

float		g_fVal;

float		g_fGradient;

//float		g_fUV;

sampler DecalSampler = sampler_state
{
	texture = g_DecalTextures;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
sampler DecalAlphaSampler = sampler_state
{
	texture = g_DecalAlphaTextures;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
sampler GradientSampler = sampler_state
{
	texture = g_Gradient;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
//sampler ColorSampler = sampler_state
//{
//	texture = g_Color;
//	minfilter = linear;
//	magfilter = linear;
//	mipfilter = linear;
//};

texture		g_DepthTexture;
sampler DepthSampler = sampler_state
{
	texture = g_DepthTexture;
};


struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;	
	vector	vProjPos : TEXCOORD1;
	vector	vViewPos : TEXCOORD2;
	float2	vGradietUV :TEXCOORD3;
};


VS_OUT VS_Default(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matrix matProj = g_matProj;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vProjPos = Out.vPosition;
	Out.vViewPos = mul(vector(In.vPosition, 1.f), matWV);
	Out.vGradietUV = float2(g_fGradient,0.5f);
	return Out;
}

 
struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	vector	vProjPos : TEXCOORD1;
	vector	vViewPos : TEXCOORD2;
	float2	vGradietUV :TEXCOORD3;
};

struct PS_OUT
{    
	vector	vColor : COLOR;
};

PS_OUT PS_Default(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vGradient = tex2D(GradientSampler, In.vGradietUV);
	/*vector	vColorTex = vector(1, 0.3f, 0.1f, 1);
	vColorTex.xyz = max(6.10352e-5, vColorTex.xyz);
	vColorTex.xyz = vColorTex.xyz > 0.04045 ? pow(vColorTex.xyz * (1.0 / 1.055) + 0.0521327, 2.4) : vColorTex.xyz * (1.0 / 12.92);
	vColorTex.xyz *= 2.f;*/

	float2	vProjPos = In.vProjPos.xy/ In.vProjPos.w;
	float2	vUV;
	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;
	vector	vDepthInfo = tex2D(DepthSampler, vUV);
	


	float	fUVx = 1.f + abs(vUV.x - 0.5f);
	float	fUVy = 1.f + abs(vUV.y - 0.5f);

	float	Depth=vDepthInfo.g;



	float3 ViewRay = In.vViewPos.xyz*(30000.f / In.vViewPos.z);

	float4 ViewPos =  float4(ViewRay*Depth,1.f);

	float4 ObjPos = mul(ViewPos, g_matWVInv);
	float3 ObjAbs = abs(ObjPos.xyz);


	clip(0.5f - ObjAbs.xyz);


	float2 vDecalUV = (ObjPos.xy + ObjPos.xz + ObjPos.yz) / 3.f;//ObjPos.xz + 0.5f;


	vector	vDecal = tex2D(DecalSampler, vDecalUV);

	vector	vDecalAlpha = tex2D(DecalAlphaSampler, float2(vDecalUV.x+ g_fUVX, vDecalUV.y + g_fUVY));

	float	fAlpha = g_fAlpha *pow(vDecalAlpha.r, 5)*2.f*vDepthInfo.b;
	
	//Out.vColor = vector(vDecal.r*2.5f, vDecal.r*1.5f, vDecal.r*2.5f, vDecal.r*fAlpha);
	//Out.vColor = vector(vDecal.r*1.5f, vDecal.r*1.2f, vDecal.r*2.5f, vDecal.r*fAlpha);

	//Out.vColor = vector(vGradient.g, vGradient.b, vGradient.r, vDecal.r*fAlpha);
	Out.vColor = vector(vGradient.r, vGradient.g, vGradient.b, vDecal.r*fAlpha);

	//Out.vColor *= 2.f;
		//vGradient*2.f;
	Out.vColor.a = vDecal.r*fAlpha;

	//Out.vColor = vector(vDecal.r*2.f, 0.f, 0.f, vDecal.r*vDecalAlpha.r);

	return Out;
	
}

PS_OUT PS_Blood(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	float2	vProjPos = In.vProjPos.xy / In.vProjPos.w;
	float2	vUV;
	vUV.x = vProjPos.x * 0.5f + 0.5f;
	vUV.y = vProjPos.y * -0.5f + 0.5f;
	vector	vDepthInfo = tex2D(DepthSampler, vUV);

	float	fUVx = 1.f + abs(vUV.x - 0.5f);
	float	fUVy = 1.f + abs(vUV.y - 0.5f);

	float	Depth = vDepthInfo.g;


	float3 ViewRay = In.vViewPos.xyz*(30000.f / In.vViewPos.z);

	float4 ViewPos = float4(ViewRay*Depth, 1.f);

	float4 ObjPos = mul(ViewPos, g_matWVInv);
	float3 ObjAbs = abs(ObjPos.xyz);



	clip(0.5f - ObjAbs.xyz);

	float2 vDecalUV = ObjPos.xz+0.5f;


	vector	vDecal = tex2D(DecalSampler, vDecalUV);




	clip(vDecal.g-(1.f- pow(g_fVal,2)));

	Out.vColor = vector(vDecal.g, 0.f, 0.f, vDecal.g*g_fAlpha*vDepthInfo.b);
	//Out.vColor = vector(vDecal.r*2.f, 0.f, 0.f, vDecal.r*vDecalAlpha.r);

	return Out;

}


// technique : 장치 지원여부에 따른 ㅅㅖ이더 선택을 가능하게 하기위해. 
technique	DefaultDevice
{
	pass FOOTPRINT
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = none;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_Default();
	}

	pass BLOOD
	{
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = none;


		VertexShader = compile vs_3_0 VS_Default();
		PixelShader = compile ps_3_0 PS_Blood();
	}
}

