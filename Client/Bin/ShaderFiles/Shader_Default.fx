matrix		g_matWorld, g_matView, g_matProj;
float2		g_fUV;
vector		g_vColor;

texture		g_DiffuseTexture;
texture		g_BlendTexture;

float		g_fTime;
float		g_fTime2;
bool		g_bAlpha;

float2		g_fAngle;

float		g_fRandX, g_fRandY;


sampler DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler BlendTexture = sampler_state
{
	texture = g_BlendTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_DistortionTexture;
sampler DistortionSampler = sampler_state
{
	texture = g_DistortionTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_OffsetTexture;
sampler OffsetSampler = sampler_state
{
	texture = g_OffsetTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_FlowTexture;
sampler FlowSampler = sampler_state
{
	texture = g_FlowTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_BorderTexture;
sampler BorderSampler = sampler_state
{
	texture = g_BorderTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};
texture		g_BorderOffsetTexture;
sampler BorderOffsetSampler = sampler_state
{
	texture = g_BorderOffsetTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_MaskTexture;
sampler MaskSampler = sampler_state
{
	texture = g_MaskTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};

VS_OUT VS_MAIN(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;
	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;

	return Out;
}

struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector	vColor : COLOR;
};

PS_OUT PS_MAIN(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	// 픽셀의 색을 결정하는 작업.	
	// tex2D : 텍스쳐의 정보를 담은 샘플러로부터 색을 얻어온다.
	// 1 : 텍스쳐의 정보를 담은 샘플러, 2 : 유브이좌표.
	In.vTexUV = In.vTexUV*g_fUV;
	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.a *= g_fTime;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_Fade_Normal(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_UI(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	if (g_bAlpha)
		vDiffuseColor.a = 0.5f;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_UIFRAME(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.a *= 1.f;

	if (In.vTexUV.x <= (0.5f + g_fTime) && In.vTexUV.x >= (0.5f - g_fTime))
		vDiffuseColor.a *= 0.f;
	if(In.vTexUV.y <= (0.5f + g_fTime) && In.vTexUV.y >= (0.5f - g_fTime))
		vDiffuseColor.a *= 0.f;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_Check(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.gb = 0.f;
	vDiffuseColor.r = 1.f;
	
	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_Fade(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.a = vDiffuseColor.a * g_fTime;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_CUSTOMCOLOR(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.rgb = g_vColor.rgb;
	vDiffuseColor.a *= g_vColor.a;
	vDiffuseColor.a *= g_fTime;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_ANGLE(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);


	float3  Line = { In.vTexUV.x- 0.5f, In.vTexUV.y - 0.5f, 0.f};
	float3	BaseLine = { 0.f,-1.f,0.f };
	
	float Angle = (acos(dot(normalize(Line), normalize(BaseLine))));
	
	if (In.vTexUV.x < 0.5f)
		Angle = radians(360) - Angle;

	
	if (Angle > g_fAngle.x && Angle < g_fAngle.y)
	{
		Out.vColor = vDiffuseColor;
		Out.vColor.a *= g_fTime;
	}
	else
		Out.vColor.a = 0.f;

	return Out;
}

PS_OUT PS_ANGLERUIN(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);


	float3  Line = { In.vTexUV.x - 0.5f, In.vTexUV.y - 0.5f, 0.f };
	float3	BaseLine = { 0.f,-1.f,0.f };

	float Angle = (acos(dot(normalize(Line), normalize(BaseLine))));

	if (In.vTexUV.x < 0.5f)
		Angle = radians(360) - Angle;


	if (Angle > g_fAngle.x && Angle < g_fAngle.y)
	{
		Out.vColor = vDiffuseColor;
		Out.vColor.gb *= 0.1f;
		Out.vColor.a *= g_fTime;
	}
	else
		Out.vColor.a = 0.f;

	return Out;
}

PS_OUT PS_CoolTime(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);


	float3  Line = { In.vTexUV.x - 0.5f, In.vTexUV.y - 0.5f, 0.f };
	float3	BaseLine = { 0.f,-1.f,0.f };

	float Angle = (acos(dot(normalize(Line), normalize(BaseLine))));

	if (In.vTexUV.x < 0.5f)
		Angle = radians(360) - Angle;


	if (Angle > g_fAngle.x && Angle < g_fAngle.y)
	{
		Out.vColor = vDiffuseColor;
		Out.vColor.rgb *= 0.2f;
		Out.vColor.a *= g_fTime;
	}
	else
	{
		Out.vColor = vDiffuseColor;
		Out.vColor.a *= g_fTime;
	}	

	return Out;
}

PS_OUT PS_ColorCoolTime(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	vDiffuseColor.rgb = g_vColor.rgb;
	vDiffuseColor.a *= g_vColor.a;
	vDiffuseColor.a *= g_fTime;

	Out.vColor = vDiffuseColor;

	float3  Line = { In.vTexUV.x - 0.5f, In.vTexUV.y - 0.5f, 0.f };
	float3	BaseLine = { 0.f,-1.f,0.f };

	float Angle = (acos(dot(normalize(Line), normalize(BaseLine))));

	if (In.vTexUV.x < 0.5f)
		Angle = radians(360) - Angle;


	if (Angle > g_fAngle.x && Angle < g_fAngle.y)
	{
		Out.vColor.rgb *= 0.2f;
		Out.vColor.a *= g_fTime;
	}
	else
	{
		Out.vColor.a *= g_fTime;
	}

	return Out;
}

PS_OUT PS_MAIN_BASEUI(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);
	vDiffuseColor.a *= g_fTime;
	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_UI_FRAME(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);
	float TexAlpha = 0.f;

	if (In.vTexUV.x >= (0.5f - g_fTime*0.5f) && In.vTexUV.x <= (0.5f + g_fTime*0.5f))
		TexAlpha = 1.f;
	if (In.vTexUV.y >= (0.5f - g_fTime*0.5f) && In.vTexUV.y <= (0.5f + g_fTime*0.5f))
		TexAlpha = 1.f;
	vDiffuseColor.a *= TexAlpha;

	Out.vColor = vDiffuseColor;

	return Out;
}

PS_OUT PS_MAIN_DISTORTION(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	float Mask = tex2D(MaskSampler, In.vTexUV).r;
	if (Mask > 0.9)
	{
		Out.vColor = vector(0, 0, 0, 0);
		return Out;
	}
	else if (Mask > 0.4f)
	{
		float fTime = g_fTime;
		float fTime2 = g_fTime - 0.5f;
		if (fTime2 < 0.0f)
			fTime2 += 1.f;
		else if (fTime2 > 1.0f)
			fTime2 -= 1.f;
		float2 flowmap = tex2D(BorderSampler, In.vTexUV).rg * 2.0f - 1.0f;
		float cycleOffset = tex2D(BorderOffsetSampler, In.vTexUV).b;

		float phase0 = cycleOffset * 0.5f + fTime;
		float phase1 = cycleOffset * 0.5f + fTime2 - 0.5f;

		float3 normalT0 = tex2D(DiffuseSampler, (In.vTexUV * 0.95f) + flowmap * phase0);
		float3 normalT1 = tex2D(DiffuseSampler, (In.vTexUV * 0.95f) + flowmap * phase1);

		float flowLerp = (abs(0.5f - g_fTime) / 0.5f);
		float3 offset = lerp(normalT0, normalT1, flowLerp);

		Out.vColor.xyz = offset;
		Out.vColor.a = 0.5f;

		return Out;
	}

	//
	//float fTime = (g_fTime * 0.5f);
	//float2 vUV = saturate(In.vTexUV + g_fTime * 0.4f);
	//float2 vDis = tex2D(DistortionSampler, vUV).xy * 0.05f;
	//vector Color = tex2D(DiffuseSampler, saturate(In.vTexUV + vDis - 0.1f));

	//Color.xyz *= 0.8f;

	//Out.vColor = Color;

	float fTime = g_fTime;
	float fTime2 = g_fTime - 0.5f;
	if (fTime2 < 0.0f)
		fTime2 += 1.f;
	else if (fTime2 > 1.0f)
		fTime2 -= 1.f;
	float2 flowmap = tex2D(FlowSampler, In.vTexUV).rg * 2.0f - 1.0f;
	float cycleOffset = tex2D(OffsetSampler, In.vTexUV).r;

	float phase0 = cycleOffset * 0.5f + fTime;
	float phase1 = cycleOffset * 0.5f + fTime2 - 0.5f;

	float3 normalT0 = tex2D(DiffuseSampler, (In.vTexUV * 0.8f) + flowmap * phase0);
	float3 normalT1 = tex2D(DiffuseSampler, (In.vTexUV * 0.8f) + flowmap * phase1);

	float flowLerp = (abs(0.5f - g_fTime) / 0.5f);
	float3 offset = lerp(normalT0, normalT1, flowLerp);

	Out.vColor.xyz = offset;
	Out.vColor.a = 0.3f;

	return Out;
}

PS_OUT PS_MAIN_AFTERIMAGE(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	vector	vDiffuseColor = tex2D(DiffuseSampler, In.vTexUV);

	float2 vCenter = float2(0.5f, 0.5f);
	float2 vDir = vCenter - In.vTexUV;

	float4 col;
	for (int x = -6; x <= 6; ++x)
	{
		float2 T = In.vTexUV;
		T.x += (2.f * x) / 1024.f;
		col += tex2D(DiffuseSampler, T) * exp((-x*x) / 12.f);
	}
	col /= 12.f;

	float factor = (length(vDir) * 1.9f);
	Out.vColor = lerp(vDiffuseColor, col, factor);
	Out.vColor *= 1.4f;


	float4 t0;
	if (g_fRandX >= 0.0000001f)
	{
		float2 t = 0;

		t = In.vTexUV;
		t.x *= g_fRandX;
		t.y *= g_fRandY;
		t0 = tex2D(DistortionSampler, t) - 0.5;
		t0 *= 0.1f;
		t = In.vTexUV + float2(t0.x, t0.y);

		t0 = tex2D(DiffuseSampler, t);
		Out.vColor = lerp(Out.vColor, t0, 0.5f);
	}

	return Out;
}

PS_OUT PS_MAIN_DISTORTIONSEC(PS_IN In)
{
	PS_OUT		Out = (PS_OUT)0;

	float fTime = g_fTime;
	float fTime2 = g_fTime - 0.5f;
	if (fTime2 < 0.0f)
		fTime2 += 1.f;
	else if (fTime2 > 1.0f)
		fTime2 -= 1.f;
	float2 flowmap = tex2D(FlowSampler, In.vTexUV).rg * 2.0f - 1.0f;
	float cycleOffset = tex2D(OffsetSampler, In.vTexUV).r;

	float phase0 = cycleOffset * 0.5f + fTime;
	float phase1 = cycleOffset * 0.5f + fTime2 - 0.5f;

	float3 normalT0 = tex2D(DiffuseSampler, (In.vTexUV * 0.8f) + flowmap * phase0);
	float3 normalT1 = tex2D(DiffuseSampler, (In.vTexUV * 0.8f) + flowmap * phase1);

	float flowLerp = (abs(0.5f - g_fTime) / 0.5f);
	float3 offset = lerp(normalT0, normalT1, flowLerp);

	Out.vColor.xyz = offset;

	float2 vCenter = float2(0.5f, 0.5f);

	Out.vColor.a = length(vCenter - In.vTexUV) * 0.5f * g_fTime2;

	return Out;
}

technique	DefaultDevice
{
	pass Default_Rendering	// 0
	{
		ZEnable = true;
		ZWriteEnable = true;
		AlphaTestEnable = false;
		AlphaBlendEnable = false;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass Color_Rendering	// 1
	{	
		ZEnable = true;
		ZWriteEnable = true;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass UI_Rendering	// 2
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;
		//AlphaRef = 0x01;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_UI();
	}

	pass UIFrame_Rendering	// 3
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		
		AlphaTestEnable = false;

		//AlphaRef = 0x01f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_UIFRAME();
	}

	pass Check_Rendering	// 4
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		//AlphaRef = 0x0f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_Check();
	}

	pass Fade_Rendering	// 5
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		//AlphaTestEnable = true;

		//AlphaRef = 0x0f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_Fade();
	}

	pass Normal_Rendering	// 6
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		//AlphaRef = 0x01f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_Fade_Normal();
	}

	pass CustomColor_Rendering	// 7
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		//AlphaRef = 0x0f;
		//AlphaFunc = Greater;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;


		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_CUSTOMCOLOR();
	}

	pass UI_UV_Rendering	// 8
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN();
	}

	pass UI_Angle_Rendering	// 9
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_ANGLE();

	}

	pass UI_Base_Rendering	// 10
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_BASEUI();
	}
	pass UI_Base_Rendering	// 11
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_UI_FRAME();
	}
	
	pass Distortion // 12
	{
		CullMode = ccw;
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_DISTORTION();
	}

	pass AfterImage // 13
	{
		FillMode = solid;
		Cullmode = none;
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;

		blendop = add;
		SrcBlend = srcalpha;
		DestBlend = one;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_AFTERIMAGE();
	}

	pass DistortionSec // 14
	{
		CullMode = ccw;
		ZEnable = false;
		ZWriteEnable = false;

		AlphaBlendEnable = true;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_MAIN_DISTORTIONSEC();
	}

	pass UI_CoolTime_Rendering	// 15
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_CoolTime();

	}

	pass UI_ColorCoolTime_Rendering	// 16
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_ColorCoolTime();

	}

	pass UI_ColorCoolTime_Rendering	// 17
	{
		ZEnable = false;
		ZWriteEnable = false;
		AlphaBlendEnable = true;
		AlphaTestEnable = false;

		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		VertexShader = compile vs_3_0 VS_MAIN();
		PixelShader = compile ps_3_0 PS_ANGLERUIN();

	}
	
}


//
//셰이더 비긴
//
//패스 시작(0)
//객체 렌더링
//패스 끝
//
//패스 시작(1)
//객체 렌더링
//패스 끝
//
//
//셰이더 엔드