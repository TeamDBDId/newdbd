

// 컨스턴트 테이블
matrix		g_matWorld, g_matView, g_matProj;
vector		g_vColor;

int			g_iColumn;
int			g_iRow;


float		g_fAlpha;

float		g_fDot;

float		g_fTime;

texture		g_Tex0;
sampler Tex0Sampler = sampler_state
{
	texture = g_Tex0;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

texture		g_Tex1;
sampler Tex1Sampler = sampler_state
{
	texture = g_Tex1;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};


struct VS_IN
{
	float3	vPosition : POSITION;
	float2	vTexUV : TEXCOORD;	
};


struct VS_OUT
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	float2	vSubUV : TEXCOORD1;
};


VS_OUT VS_BG(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matrix matProj = g_matProj;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, matProj);

	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	
	Out.vTexUV = float2(In.vTexUV);
	Out.vSubUV = float2(In.vTexUV.x*0.2f + g_fTime, In.vTexUV.y);
		


	return Out;
}

VS_OUT VS_SubUV(VS_IN In)
{
	VS_OUT			Out = (VS_OUT)0;

	matrix		matWV, matWVP;

	matWV = mul(g_matWorld, g_matView);
	matWVP = mul(matWV, g_matProj);
	
	Out.vPosition = mul(vector(In.vPosition, 1.f), matWVP);
	Out.vTexUV = In.vTexUV;
	Out.vSubUV.x = (In.vTexUV.x + g_iRow)*g_fDot;
	Out.vSubUV.y = (In.vTexUV.y + g_iColumn)*g_fDot;
	return Out;
}


struct PS_IN
{
	vector	vPosition : POSITION;
	float2	vTexUV : TEXCOORD0;
	float2	vSubUV : TEXCOORD1;
};


struct PS_OUT
{
	vector	vColor : COLOR;
};


PS_OUT PS_BG(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;//초기화


	vector	vDiffuseColor = tex2D(Tex0Sampler, In.vSubUV);
	vector	vMaskColor = tex2D(Tex1Sampler, In.vTexUV);

	vector vColor = vector(vMaskColor.r, vMaskColor.r*0.9f, vMaskColor.r*0.6f, 0.f);
	Out.vColor = vColor +vDiffuseColor.r*0.1f;// *vColor;
	Out.vColor.a = vMaskColor.r*0.3f*g_fAlpha;
//	Out.vColor += ;
	return Out;
}


PS_OUT PS_Spark(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;//초기화


	vector	vDiffuseColor = tex2D(Tex0Sampler, In.vTexUV);
	vector	vGradient = tex2D(Tex1Sampler, float2(vDiffuseColor.r*0.95f,0.5f));

	Out.vColor = vGradient;
	Out.vColor.a = vDiffuseColor.r*g_fAlpha;
	//Out.vColor = vector(1.f, 0.9f, 0.f, vDiffuseColor.r*g_fAlpha);// +vDiffuseColor.r;
 

	return Out;
}

PS_OUT PS_SubUV(VS_OUT In)
{
	PS_OUT		Out = (PS_OUT)0;//초기화

	vector	vDiffuseColor = tex2D(Tex0Sampler, In.vSubUV);
	Out.vColor = vector(vDiffuseColor.r, vDiffuseColor.r*0.9f, vDiffuseColor.r*0.6f,vDiffuseColor.a*g_fAlpha);
	//Out.vColor = vector(1.f,1.f,1.f, vDiffuseColor.a);



	return Out;
}


technique	DefaultDevice
{

	pass BG//0
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;
		
		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_BG();
		PixelShader = compile ps_3_0 PS_BG();
	}

	pass Spark//1
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_BG();
		PixelShader = compile ps_3_0 PS_Spark();
	}

	pass Smoke//2
	{
		ZEnable = true;
		ZWriteEnable = false;

		AlphaBlendEnable = true;
		SrcBlend = SrcAlpha;
		DestBlend = InvSrcAlpha;

		CullMode = ccw;


		VertexShader = compile vs_3_0 VS_SubUV();
		PixelShader = compile ps_3_0 PS_SubUV();
	}
}

