texture			g_DiffuseTexture;
texture			g_LightInfoTex;
int				g_iNumLight;

sampler	DiffuseSampler = sampler_state
{
	texture = g_DiffuseTexture;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

sampler	LightInfoSampler = sampler_state
{
	texture = g_LightInfoTex;
	minfilter = linear;
	magfilter = linear;
	mipfilter = linear;
};

vector LightShaft(float2 texCoord, vector LightInfo)
{
	float decay = 0.97815;
	float exposure = 1.f;
	float density = 0.966;
	float weight = 0.58767;
	int NUM_SAMPLES = 50;
	float2 tc = texCoord;
	float2 deltaTexCoord = (tc - LightInfo.xy);
	deltaTexCoord *= 1.0f / NUM_SAMPLES * density;
	float illuminationDecay = 1.0;
	vector color = tex2D(DiffuseSampler, tc)*0.4f;
	color.w = 1.f;

	for (int i = 0; i < NUM_SAMPLES; i++)
	{
		tc -= deltaTexCoord;
		vector samp = tex2D(DiffuseSampler, tc)*0.4f;
		samp.w = 1.f;
		samp *= illuminationDecay * weight;
		color += samp;
		illuminationDecay *= decay;
	}

	return vector((float3(color.r, color.g, color.b) * exposure), 1);
}

struct PS_IN
{
	vector		vPosition : POSITION;
	float2		vTexUV : TEXCOORD0;
};

struct PS_OUT
{
	vector		vColor : COLOR0;
};

PS_OUT PS_MAIN(PS_IN In)
{
	PS_OUT			Out = (PS_OUT)0;

	vector vDiff = tex2D(DiffuseSampler, In.vTexUV);
	if (g_iNumLight == 0)
	{
		Out.vColor = vDiff;
		return Out;
	}

	vector vLInfo;
	vector vColor;
	for (int i = 0; i < g_iNumLight; ++i)
	{
		vector vLightInfo = tex2D(LightInfoSampler, float2((i + 0.5f) / 64.f, 0.5f));
		vLInfo = vLightInfo;
		float fDist = length(In.vTexUV - vLightInfo.rg);
		float fRange = (vLightInfo.a * vLightInfo.b) * 0.5f;
		if (fRange < fDist)
			continue;
		vColor.xyz += LightShaft(In.vTexUV, vLightInfo).xyz;
		vColor.a = 1.f - (fDist * 2.f);
	}
	vColor.xyz /= g_iNumLight;

	Out.vColor = vColor;

	return Out;
}

technique	DefaultDevice
{
	pass Render_Blend
	{
		AlphaTestEnable = true;
		AlphaFunc = Greater;
		AlphaRef = 0;

		ZEnable = false;
		ZWriteEnable = false;

		VertexShader = NULL;
		PixelShader = compile ps_3_0 PS_MAIN();
	}
}


