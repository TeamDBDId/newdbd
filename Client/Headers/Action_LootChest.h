#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CChest;
class CAction_LootChest final : public CAction
{
public:
	explicit CAction_LootChest();
	virtual ~CAction_LootChest() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetChest(CGameObject* pChest) { m_pChest = (CChest*)pChest; }
private:
	_float m_fIndex = 0.f;
	_float m_LootTime = 0.f;
	_float m_fOverTime = 0.f;
	_int m_iState = 0;
	CChest* m_pChest = nullptr;
protected:
	virtual void Free();
};

_END