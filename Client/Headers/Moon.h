#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CBuffer_RcTex;
class CTexture;
_END


_BEGIN(Client)
//class CCustomLight;
class CMoon final : public CGameObject
{
private:
	explicit CMoon(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CMoon(const CMoon& rhs);
	virtual ~CMoon() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTexture*			m_pTexCom = nullptr;
	CTexture*			m_pLightTexture = nullptr;
private:
	_vec3	m_vProjPos;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, _uint i);
public:
	static CMoon* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
};

_END
