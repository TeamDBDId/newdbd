#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)

class CSpider final : public CGameObject
{
public:
	enum STATE { Struggle_MIN, Struggle_MAX, StruggleToSacrifice, Stuggle, StabOut, StabLoop, StabIN, Reaction_Out, Reaction_Loop, Reaction_In, Invisible };
private:
	explicit CSpider(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CSpider(const CSpider& rhs);
	virtual ~CSpider() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_Stemp();
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
public:
	void SetState(STATE eState) { m_CurState = eState; m_iCurAnimation = eState; }
	void Set_RotationY();
private:
	void Camper_Check();
	void State_Check();
	void ComunicateWithServer();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
private:
	wstring m_Key;
	_int m_iOtherOption = 0;
	STATE m_OldState = Invisible;
	STATE m_CurState = Invisible;
	_float m_fDeltaTime = 0.f;
	_float m_fDissolveTime = 0.f;
	_bool  m_bCamper = false;
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CSpider* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END