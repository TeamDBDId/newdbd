#pragma once
#include "GameObject.h"
_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Static;
class CCollider;
class CFrustum;
_END


_BEGIN(Client)
class CCustomLight;
class CExitDoorLight : public CGameObject
{
private:
	explicit CExitDoorLight(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CExitDoorLight(const CExitDoorLight& rhs);
	virtual ~CExitDoorLight() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();


public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	void	Set_LightRender(_uint i) { m_iLightRender = i; }
private:
	void Init_Light();

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
	CCollider*			m_pLightCollider[3] = { nullptr, nullptr, nullptr };
	CCustomLight*		m_pCustomLight[3] = { nullptr, nullptr, nullptr };
private:	
	_matrix m_matLocalInv;
	_vec3	m_vLocalPos;
	_float	m_Max = 0.f;
	wstring m_Key = L"";
	_uint	m_iLightRender = 0;
	_bool	m_LateInit = false;
private:
	void Check_Light();
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
public:
	static CExitDoorLight* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END
