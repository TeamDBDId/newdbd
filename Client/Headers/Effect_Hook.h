#pragma once

#include "Defines.h"
#include "BaseEffect.h"


_BEGIN(Client)
class CEffect_Hook final : public CGameObject
{
private:
	explicit CEffect_Hook(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_Hook(const CEffect_Hook& _rhs);
	virtual ~CEffect_Hook() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();
public:
	void Set_Param(const _vec3& _vPos);
private:
	void Make_BG(const _float& _fTick);
	void Make_Paticle(const _float& _fTick);
	void Make_EX(const _float& _fTick);
	void Make_Dust(const _float& _fTick);
public:
	static CEffect_Hook* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

private:
	const _float		BGINIT = 3.f;
	_bool				m_isBGInit = false;

	const _float		PARICLESTART = 2.f;
	_float				PARTICLEDELAY = 0.12f;
	_float				m_fParticle = 0.f;

	const _float		EXINIT = 7.f;
	_bool				m_isEXInit = false;




	//_float			m_fBloodTime = 0.f;

	_float				m_fLifeTime = 0.f;

	_vec3				m_vPos = _vec3(0.f, 0.f, 0.f);

};

_END