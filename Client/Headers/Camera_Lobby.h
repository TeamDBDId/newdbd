#pragma once

#include "Defines.h"
#include "Camera.h"

_BEGIN(Engine)
class CFrustum;
_END

_BEGIN(Client)
class CCamera_Lobby final : public CCamera
{
public:
	enum LobbyState{ L_Start, L_RoleSelection, L_Camper, L_Slasher, L_End};
private:
	explicit CCamera_Lobby(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamera_Lobby(const CCamera_Lobby& rhs);
	virtual ~CCamera_Lobby() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	void Set_State(LobbyState State);

private:
	void Move_StateToState(const LobbyState& _From, const LobbyState& _To);
	void State_Check(const _float& fTimeDelta);
private:
	CFrustum*		m_pFrustumCom = nullptr;
	HRESULT			Ready_Component();
private:
	POINT		m_ptMouse;
	_int		m_iSoundNum = 0;
	LobbyState	m_OldLobbyState = L_Start;
	LobbyState	m_CurLobbyState = L_RoleSelection;
	_float		m_fTime = 0.f;

	_vec3		m_CurPos;
	_vec3		m_CurLook;

	_vec3		m_StatePos[L_End];
	_vec3		m_StateLook[L_End];
public:
	static CCamera_Lobby* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
protected:
	virtual void Free();
};

_END