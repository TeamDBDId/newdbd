#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamper;
class CAction_HealCamper final : public CAction
{
public:
	explicit CAction_HealCamper();
	virtual ~CAction_HealCamper() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetCamper(CGameObject* pCamper);
	CCamper* GetCamper() { return m_pCamper; }
private:
	_float		m_fOldHealCount = 0;
	_int		m_iState = 0;
	_float		m_fDelay = -1.f;
	_float		m_fEndDelay = 0.f;
	_float		m_fHealTime = 0.f;
	CCamper*	m_pCamper = nullptr;
protected:
	virtual void Free();
};

_END