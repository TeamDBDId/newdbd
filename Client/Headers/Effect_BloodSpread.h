#pragma once

#include "Defines.h"
#include "BaseEffect.h"


_BEGIN(Client)
class CEffect_BloodSpread final : public CGameObject
{
private:
	explicit CEffect_BloodSpread(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_BloodSpread(const CEffect_BloodSpread& _rhs);
	virtual ~CEffect_BloodSpread() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();
public:
	void Set_Param(const _vec3& _vPos, const _vec3& _vDir);
private:
	void Make_BloodBall(const _float& _fTick);


public:
	static CEffect_BloodSpread* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

private:
	_float			BALLDELAY = 0.06f;
	_float			m_fBloodTime = 0.f;

	_float			m_fLifeTime = 0.f;

	_vec3			m_vPos = _vec3(0.f,0.f,0.f);
	_vec3			m_vDir = _vec3(0.f,0.f,0.f);

};

_END