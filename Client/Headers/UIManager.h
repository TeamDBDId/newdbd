#pragma once
#include "Base.h"
#include "GameObject.h"
#include "UI_Score.h"

_BEGIN(Client)

class CUI_Interaction;
class CUIManager : public CBase
{
public:
	enum ProgressState { PG_None, PG_Generator, PG_Totem, PG_Chest, PG_HealCamper, PG_HealSelf, PG_BeingHeal, PG_HookStruggle, PG_HookFree, PG_HookFreed,PG_Wiggle, PG_ExitDoor, PG_Sabotage, PG_Hide, PG_UnHide, PG_SpiritPower, PG_DyingHeal, PG_Dying };
	enum KeyEvent {EV_None, EV_SetResist, EV_IdleResist, EV_ClickResist, EV_ResistEnd, EV_SetWiggle, EV_IdleWiggle, EV_KeyOnWiggle, EV_LeftWiggle, EV_RightWiggle, EV_WiggleEnd};
private:
	_DECLARE_SINGLETON(CUIManager)
	explicit CUIManager();
	virtual ~CUIManager() = default;

public:
	void Set_UI(CUI_Interaction* pUIInteraction, CUI_Score* pUIScore) { m_pUIInteraction = pUIInteraction; m_pUIScore = pUIScore; }
	void Add_Button(const wstring& TextureName, const wstring& TextName);
	void Set_GeneratorNum(_uint Num) { m_iGeneratorNum = Num; }
	_uint& Get_GeneratorNum() { return m_iGeneratorNum; }
	void Set_SkillCheckState(_int SkillCheck) { m_iSkillCheckState = SkillCheck; }
	_int Get_SkillCheckState();
	void Set_ProgressBar(ProgressState State, _int iItem = 0);
	void Set_ProgressPoint(const _float& fProgress, const _float& fMaxProgress);
	void Set_KeyEvent(KeyEvent KeyEvent);
	void Add_Score(const _uint& Type, const wstring& Text, const _int& Point);
	void Set_CalZZi();
	KeyEvent Get_KeyEvent();
private:
	CUI_Interaction* m_pUIInteraction = nullptr;
	CUI_Score* m_pUIScore = nullptr;
	_uint m_iGeneratorNum = 5;
	_int m_iSkillCheckState = 0;
private:
	virtual void Free() override;

};
_END