#pragma once

#include "Defines.h"
#include "BaseEffect.h"


_BEGIN(Client)
class CEffect_Fire final : public CGameObject
{
private:
	explicit CEffect_Fire(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_Fire(const CEffect_Fire& _rhs);
	virtual ~CEffect_Fire() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	void Set_Param(const _vec3& _vPos, const _uint& _iType);
private:
	void Make_Core(const _float& _fTick);
	void Make_Smoke(const _float& _fTick);
public:
	static CEffect_Fire* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

private:
	_float			COREDELAY = 0.f;
	_float			SMOKEDELAY = 0.f;

	_vec3			m_vPos = _vec3(0.f, 0.f, 0.f);
	_float			m_fCoreDelay = 0.f;
	_float			m_fSmokeDelay = 0.f;

	_uint			m_iType = 0;
};

_END