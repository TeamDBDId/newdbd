#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CGenerator;
class CAction_Generator final : public CAction
{
public:
	explicit CAction_Generator();
	virtual ~CAction_Generator() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetGenerator(CGameObject* pGenerator) { m_pGenerator = (CGenerator*)pGenerator; }
private:
	void Coop_Check(const _float& fTimeDelta);
private:
	_float m_fFailCoolTime = 0.f;
	_float m_fRepairTime = 0.f;
	_float m_fCoopTime = 0.f;
	_int m_iState = 0;
	CGenerator* m_pGenerator = nullptr;
protected:
	virtual void Free();
};

_END