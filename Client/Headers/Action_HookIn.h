#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCamper;
class CSlasher;
class CMeatHook;
class CAction_HookIn final : public CAction
{
public:
	explicit CAction_HookIn();
	virtual ~CAction_HookIn() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetHook(CGameObject* pHook) { m_pHook = (CMeatHook*)pHook; }
private:
	_float	m_fDelay = 0.f;
	_bool m_bIsInit = false;
	_vec3 m_vOriginPos;
	_int m_iState = 0;
	_float	m_fIndex = 0.f;
	vector<_vec3>	m_vecCamperPos;
	vector<_vec3>	m_vecSlasherPos;
	CCamper*		m_pCamper = nullptr;
	CSlasher*		m_pSlasher = nullptr;
	CMeatHook*		m_pHook = nullptr;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END