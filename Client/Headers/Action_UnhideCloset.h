#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCloset;
class CAction_UnhideCloset final : public CAction
{
public:
	explicit CAction_UnhideCloset();
	virtual ~CAction_UnhideCloset() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetCloset(CGameObject* pCloset) { m_pCloset = (CCloset*)pCloset; }
private:
	_bool m_bIsInit = false;
	_vec3 m_vOriginPos;
	vector<_vec3> m_vecPos;
	vector<_vec3> m_vecPosFast;
	_float m_fIndex = 0.f;
	_int m_iState = 0;
	CCloset* m_pCloset = nullptr;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END