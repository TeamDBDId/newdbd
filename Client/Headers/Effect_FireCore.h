#pragma once

#include "Defines.h"
#include "BaseEffect.h"

_BEGIN(Engine)
class CTransform;
class CTexture;
class CBuffer_RcTex;
_END

_BEGIN(Client)
class CEffect_FireCore final : public CBaseEffect
{
private:
	explicit CEffect_FireCore(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_FireCore(const CEffect_FireCore& _rhs);
	virtual ~CEffect_FireCore() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	virtual void Set_Param(const _vec3& _vPos, const void* _pVoid);

private:
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CTransform*			m_pTransformCom = nullptr;
	CTexture*			m_pTextureCom = nullptr;

private:
	_vec3				m_vDir = _vec3(0.f, 0.f, 0.f);

	_uint				m_iType = 0;
private:
	virtual HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
	virtual HRESULT SetUp_ContantTable(LPD3DXEFFECT _pEffect);
public:
	static CEffect_FireCore* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END