#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CPlank;
class CAction_Destroy_Pallet final : public CAction
{
public:
	explicit CAction_Destroy_Pallet();
	virtual ~CAction_Destroy_Pallet() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void Send_ServerData() override;
public:
	void SetPalnk(CGameObject* pPlank) { m_pPlank = (CPlank*)pPlank; }
private:
	_int			m_iState = 0;
	_float			m_fIndex = 0.f;
	CPlank*			m_pPlank = nullptr;
protected:
	virtual void Free();
};

_END