#pragma once

#include "Defines.h"
#include "BaseEffect.h"

_BEGIN(Engine)
class CBuffer_CubeTex;
class CTexture;
//class CMesh_Static;
_END

_BEGIN(Client)
class CEffect_Blood final : public CBaseEffect
{
private:
	explicit CEffect_Blood(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_Blood(const CEffect_Blood& _rhs);
	virtual ~CEffect_Blood() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();
public:
	virtual void Set_Param(const _vec3& _vPos, const void* _pVoid = nullptr);
public:
	static CEffect_Blood* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
private:
	virtual HRESULT SetUp_ContantTable(LPD3DXEFFECT _pEffect);
protected:
	virtual void Free();
private:
	CTransform*			m_pTransformCom = nullptr;
	CBuffer_CubeTex*	m_pDecalBox = nullptr;
	CTexture*			m_pTextureCom = nullptr;
	//CMesh_Static*		m_pMeshCom = nullptr;
private:
	_vec3			m_vPos = _vec3(0.f, 0.f, 0.f);
	_vec3			m_vDir = _vec3(0.f, 0.f, 0.f);


	_vec2			m_fUV = _vec2(0.f, 0.f);
	

	_uint			m_iTexNum = 0;
};

_END