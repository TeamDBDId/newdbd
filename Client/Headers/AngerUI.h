#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CBuffer_RcTex;
class CShader;
_END

_BEGIN(Client)

class CUI_Texture;
class CAngerUI : public CGameObject
{
private:
	explicit CAngerUI(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CAngerUI(const CAngerUI& rhs);
	virtual ~CAngerUI() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
private:
	_float m_fTime = 0.f;
	_float m_fAnger = 0.f;
	_bool  m_bDecr = false;
private:
	void Check_Anger(const _float& fTimeDelta);
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect);
public:
	static CAngerUI* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END