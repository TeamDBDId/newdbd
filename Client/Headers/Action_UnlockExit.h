#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CExitDoor;
class CAction_UnlockExit final : public CAction
{
public:
	explicit CAction_UnlockExit();
	virtual ~CAction_UnlockExit() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetExitDoor(CGameObject* pExitDoor) { m_pExitDoor = (CExitDoor*)pExitDoor; }
private:
	_float m_UnlockTime = 0.f;
	_int m_iState = 0;
	CExitDoor* m_pExitDoor = nullptr;
protected:
	virtual void Free();
};

_END