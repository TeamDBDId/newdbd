#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_Score : public CGameObject
{
public:
	enum ScoreType { Objective, Survival, Altruism, Boldness, Brutality, Deviousness, Hunter, Sacrifice };
	typedef struct tagScore
	{
		CUI_Texture*	pProgress_Base = nullptr;
		CUI_Texture*	pProgress_Bar = nullptr;
		CUI_Texture*	pTypeIcon = nullptr;
		CUI_Texture*	pText = nullptr;
		CUI_Texture*	pScore = nullptr;
	}SCOREINFO;
private:
	explicit CUI_Score(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Score(const CUI_Score& rhs);
	virtual ~CUI_Score() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	void Camper_Position_Check(const _float & fTimeDelta);
	void Add_Score(const _uint& Type, const wstring& Text, const _int& Point);
	void Score_Check();
	void FadeOut_Score();
private:
	void Update_Score(const _float& fTimeDelta);
private:
	CGameObject*		m_pPlayer = nullptr;
	vector<SCOREINFO>	m_vecScore;
	queue<SCOREINFO>	m_WaitingScore;
	_float				m_ProgressTime = 0.f;
	_uint				m_PlayerNum = 0;
	_float				m_fWaitingTime = 0.f;

	_float				m_fBasementTime = 0.f;
	_float				m_fBoldTime = 0.f;

public:
	static CUI_Score* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END