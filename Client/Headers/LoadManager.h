#pragma once
#include "Base.h"
#include "GameObject.h"
#include "Renderer.h"

_BEGIN(Client)
class CLoadManager : public CBase
{
	_DECLARE_SINGLETON(CLoadManager)
public:
	enum DATANUM{Lobby, OnlyLobby, Stage, DataEnd};
private:
	explicit CLoadManager();
	virtual ~CLoadManager() = default;

public:
	void	Set_GraphicDevice(LPDIRECT3DDEVICE9 pGraphic_Device) { m_pGraphic_Device = pGraphic_Device; m_pGraphic_Device->AddRef(); }
	void	LoadMapData(const _uint& Stage, const _uint& SceneID = SCENE_STAGE);
	void	Setup_Map_Index(CGameObject* pGameObject, const _matrix& matWorld);
	_int	Compute_Map_Index(const _vec3& vPosition);
	CRenderer* Get_Renderer() { CRenderer* pRenderer = m_pRenderer; return pRenderer; }

	HRESULT Add_ReplacedName();
	HRESULT Prototype_S1_GameObject();
	HRESULT Prototype_S1_StaticMesh();
	HRESULT Prototype_S1_DynamicMesh();
	HRESULT Prototype_S1_Component();
	HRESULT Prototype_S1_Texture();
	HRESULT Prototype_S1_CollMesh();

	HRESULT Prototype_L_Component();
	HRESULT Prototype_L_StaticMesh();
	HRESULT Prototype_L_DynamicMesh();
	HRESULT Prototype_L_GameObject();
	HRESULT Prototype_L_Texture();
	HRESULT Prototype_OL_Texture();
	_float	Get_Progress() { return m_fProgressTime; }
	void	Set_Progress(_float Progress) {	m_fProgressTime = Progress;	}
	HRESULT S1_LightInfo();
	HRESULT S1_Camera();
	HRESULT S1_BackGround();
	_bool	Get_IsLoading() { return m_isLoading; }
	void	Set_IsLoding(_bool IsLoading) { m_isLoading = IsLoading; }
	void	Set_MouseVisible(_bool IsVisible) { m_IsMouseVisible = IsVisible; }
	_bool	Get_MouseVisible() { return m_IsMouseVisible; }

	HRESULT	AddSceneTexture(_uint SceneNum, const _tchar* FilePath, const _tchar* FileName);
	HRESULT	DeleteSceneTexture(_uint SceneNum);
private:
	_int Find_ObjectName(wstring Key);
private:
	LPDIRECT3DDEVICE9	m_pGraphic_Device = nullptr;
	CRenderer*			m_pRenderer = nullptr;
	vector<wstring*>	m_vecObjectName;
	_bool				m_isLoading = true;
	_bool				m_IsMouseVisible = true;
	_float				m_fProgressTime = 0.f;

	vector<const _tchar*>		m_Texture[DataEnd];
private:
	virtual void Free() override;

};
_END