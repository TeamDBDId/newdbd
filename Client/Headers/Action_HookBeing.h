#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CMeatHook;
class CCamper;
class CAction_HookBeing final : public CAction
{
public:
	explicit CAction_HookBeing();
	virtual ~CAction_HookBeing() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetCamper(CGameObject* pCamper) { m_pCamper = (CCamper*)pCamper; }
	void SetHook(CGameObject* pHook) { m_pMeatHook = (CMeatHook*)pHook; }
private:
	_float		m_fHookBeingTime = 0.f;
	_float		m_fTime = 0.0f;
	_int		m_iState = 0;
	_int		m_iCountOfFrame = 0;
	CCamper*	m_pCamper = nullptr;
	CMeatHook*	m_pMeatHook = nullptr;
	_bool		m_bIsRescued = false;
	_bool		m_bIsInit = false;
	_int		m_iID_HealCamper = 0;
	_float		m_fDelay = 0.f;
private:
	//_bool Check_HealCamperState();
	//void Init_HealCamper();
	_bool	Check_ResecueCamper();
protected:
	virtual void Free();
};

_END