#pragma once

#include "Defines.h"
#include "BaseEffect.h"


_BEGIN(Client)
class CEffect_ExitOpen final : public CGameObject
{
private:
	explicit CEffect_ExitOpen(LPDIRECT3DDEVICE9 _pGDevice);
	explicit CEffect_ExitOpen(const CEffect_ExitOpen& _rhs);
	virtual ~CEffect_ExitOpen() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& _fTick);
	virtual _int LastUpdate_GameObject(const _float& _fTick);
	virtual void Render_GameObject();

public:
	void Set_Param(const _vec3* _vPos);

private:
	void Make_Particle();
public:
	static CEffect_ExitOpen* Create(LPDIRECT3DDEVICE9 _pGDevice);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

private:
	const _vec3*			m_pPos = nullptr;


	_float			m_fTick = 0.f;
};

_END