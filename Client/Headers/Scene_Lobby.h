#pragma once

#include "Defines.h"
#include "Scene.h"

_BEGIN(Client)
class CCamper;
class CSlasher;
class CCamera_Lobby;
class CUI_Texture;
class CScene_Lobby final : public CScene
{
public:
	enum LobbyState { RoleSelection, OverlayMenu };
private:
	explicit CScene_Lobby(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual ~CScene_Lobby() = default;
public:
	virtual HRESULT Ready_Scene() override;
	virtual _int Update_Scene(const _float& fTimeDelta) override;
	virtual _int LastUpdate_Scene(const _float& fTimeDelta) override;
	virtual void Render_Scene() override;
public:
	static size_t CALLBACK Lobby_Thread(LPVOID lpParam);
	static size_t CALLBACK LoadingThreadFunc(LPVOID lpParam);
private:
	HANDLE hThread;

	CRITICAL_SECTION	m_CritSec;
	HANDLE				m_hLoadingThread;
private:
	HRESULT Ready_Prototype_GameObject();
	HRESULT Ready_Prototype_Component();
	HRESULT Ready_Layer_Camera(const _tchar* pLayerTag);
	HRESULT Ready_LightInfo();
	HRESULT Ready_Layer_Camper(const _tchar* pLayerTag, _uint Index);
	HRESULT Ready_Layer_Camper(const _uint& PosIndex,const _uint& CamperNum);
	HRESULT Ready_Layer_Slasher(const _tchar* pLayerTag, _uint Character);
	HRESULT Ready_Layer_Slasher(const _int& Character);
	void	SetUp_Sound();
private:
	void	Update_Server();
	void	LobbyState_Check();
	_int	Change_To_Stage();
	void	Clear_LayerObjList();
	void	Check_Connedtion_Players();
	void	Check_Connection_OtherPlayers();
	CGameObject* Find_Camper(_uint idx);
	void Delete_Camper(_uint idx);
	CGameObject* Find_Slasher();
	void Delete_AllCharictor();
	void	Set_GameOn(_bool IsGameOn) { m_IsGameOn = IsGameOn; }
	_bool	IsGameOn() { return m_IsGameOn; }
	
public:
	static CScene_Lobby* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
private:
	LobbyState m_eCurLobbyState = RoleSelection;
	LobbyState m_eOldLobbyState = RoleSelection;
	CGameObject*	m_pCurUI = nullptr;
	CCamera_Lobby*	m_pCamera = nullptr;

	_bool	m_IsStart = false;
	_bool	m_IsLoading = false;

	CUI_Texture* m_pCamperName[4] = { nullptr, nullptr, nullptr, nullptr };
	_int	m_PlayerCharictor[5] = { -1, -1, -1, -1, -1 };
	CCamper* m_pCamper[4] = { nullptr, nullptr, nullptr, nullptr };
	CSlasher* m_pSlasher = nullptr;
	_bool	m_IsGameOn = true;
protected:
	virtual void Free();
};

_END