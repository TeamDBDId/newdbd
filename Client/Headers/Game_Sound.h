#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CSlasher;
class CCamper;
class CGame_Sound : public CGameObject
{
public:
	enum Situation { C_Normal, C_Captured, C_Dead, C_Dying, C_Injured, C_Hooked_1, C_Hooked_2,C_TallyScreen, S_Normal, S_Chase, S_Carrying, S_Exit, S_TallyScreen, Victory};
	enum BGM{B_House, B_Basement,B_Else,B_End};
	typedef struct SituationSound{
		string FileName;
		_int FileNum;
		_float fVolume;
	}SITUSOUND;
private:
	explicit CGame_Sound(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CGame_Sound(const CGame_Sound& rhs);
	virtual ~CGame_Sound() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	void SlasherAngerCheck(const _float& fTimeDelta);
	void Set_Player(_int PlayerexNum);
	void Set_Player();
private:
	void SetUp_Player();
	void SetUp_BaseSound();
	void Change_State(const _float& fDeltaTime);
	void SetUp_SoundMap();
	void Camper_Check(const _float& fDeltaTime);
	void Slasher_Check(const _float& fDeltaTime);
	void Heart_Sound(const _float& fDeltaTime);
	void Update_BGM();
	void Check_Observe();
private:
	_int			m_iPlayerNum = 0;
	_int			m_iSlasherCharictorNum = 0;
	CSoundManager*	m_pSoundManager = nullptr;

	CCamper*	m_pCamper = nullptr;
	CSlasher*	m_pSlasher= nullptr;

	_uint		m_CurSituation;
	_uint		m_OldSituation;
	_float		m_fChangeTimer = 0.f;
	_float		m_fHearthTimer = 0.f;
	map<_uint, SITUSOUND>	m_MapSound;

	_float		m_AngerTimer = 0.f;
	_bool		m_IsSlasherAnger = false;
	_int		m_CurBGM = 0;
	_int		m_OldBGM = 0;
public:
	static CGame_Sound* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END