#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CMachete;
class CAction_Hit final : public CAction
{
public:
	explicit CAction_Hit();
	virtual ~CAction_Hit() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void Send_ServerData() override;
private:
	_int			m_iState = 0;
	_float			m_fDelay = 0.f;
	_bool			m_bIsInit = false;
	_float			m_fIndex = 0.f;
	_float			m_fAccTimeDelta = 1.f;
	_float			m_fAcc_Attack_Intime = 0.f;
	CMachete*		m_pMachete = nullptr;
protected:
	virtual void Free();
};

_END