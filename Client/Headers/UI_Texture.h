#pragma once
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CBuffer_RcTex;
class CShader;
_END

_BEGIN(Client)

class CUI_Texture : public CGameObject
{
public:
	enum FadeState { NONE, FADEIN, FADEOUT };
private:
	explicit CUI_Texture(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Texture(const CUI_Texture& rhs);
	virtual ~CUI_Texture() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();

public:
	void	Set_TexName(wstring TexName, _int Num = 0);
	wstring	Get_TexName() { return m_TexName; }
	void	Set_Alpha(_float Alpha) { m_fAlpha = Alpha; }
	_float	Get_Alpha() { return m_fAlpha; }
	void	Set_Pos(const _vec2& Pos);
	void	Set_Pos(const _vec3& Pos);
	void	Set_PosPlus(_vec2 Pos);
	_vec2	Get_Pos();
	void	Set_Scale(_vec2 Scale, _bool IsOriginScale = true);
	_vec2	Get_Scale();
	void	Set_Font(wstring Font, _vec2 Size, D3DXCOLOR Color, _uint FontNum = 0);
	void	Set_FontScale(_vec2 Size);
	void	Set_Test() { m_isTest = true; }
	void	Set_UV(_vec2 vUV);
	void	IsRendering(_bool isRender) { m_isRender = isRender; }
	void	Set_Angle(_vec2 Radian) { m_vAngle.x = Radian.x; m_vAngle.y = Radian.y; }
	void	Set_Rotation(_float Radian);
	_float	Get_Rotation() { return m_fRotation; }
	void	Set_DeadTime(_float DeadTime) { m_fDeadTime = DeadTime; }
	_vec2	Get_Angle() { return m_vAngle; }
	void	Set_OneTimeRendering() { m_iOneTimeRendring = 1; }
	void	Set_BlendTexture(_bool IsBlend) { m_IsBlend = true; }
	void	Set_FadeIn(_float fFadeTime) { m_fFadeTime = m_fMaxFadeTime = fFadeTime; m_eFadeState = FADEIN; }
	void	Set_FadeOut(_float fFadeTime) { m_fFadeTime = m_fMaxFadeTime = fFadeTime; m_eFadeState = FADEOUT; }
	_float	Get_FadeTime(_uint* FadeState = nullptr) { if (FadeState != nullptr)*FadeState = m_eFadeState;  return m_fFadeTime; }
	void	IsButtonFrame(_bool IsButtonFrame) { m_IsButtonFrame = IsButtonFrame; }
	void	IsCoolTime(_bool IsButtonFrame) { m_IsCoolTime = IsButtonFrame; }
	_bool	IsTouch();
	void	IsColor(_bool IsColor) { m_IsShadeColor = IsColor; }
	void	IsColorCoolTime(_bool IsColor) { m_IsColorCoolTime = IsColor; }
	void	Set_Color(D3DXCOLOR Color) { m_Color = Color; }

	void	Set_IsBillboard(_bool IsBillboard) { m_IsBillboard = IsBillboard; }
	void	Update_Billboard();
private:
	void Test(const _float& fDeltaTime);
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CBuffer_RcTex*		m_pBufferCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
private:
	LPDIRECT3DTEXTURE9	m_Texture = nullptr;
	LPDIRECT3DTEXTURE9	m_Texture2 = nullptr;
	wstring				m_TexName = L"";
	wstring				m_TexName2 = L"";

	FadeState			m_eFadeState = NONE;
	_float				m_fFadeTime = 0.f;
	_float				m_fMaxFadeTime = 0.f;

	_int				m_iOneTimeRendring = 0;
	_float				m_fDeadTime = 0.f;
	_bool				m_isRender = true;
	_bool				m_isTest = false;
	_vec2				m_vUV = { 1.f,1.f };
	_vec2				m_vAngle = { 0.f, 0.f };
	_float				m_fRotation = 0.f;
	_float				m_fAlpha = 1.f;
	_bool				m_IsBlend = false;
	_bool				m_IsButtonFrame = false;
	_bool				m_IsCoolTime = false;
	_bool				m_IsColorCoolTime = false;
	_bool				m_IsBillboard = false;
	_bool				m_IsShadeColor = false;
	D3DXCOLOR			m_Color = {1.f, 1.f, 1.f, 1.f};
private:
	HRESULT Ready_Component(); // 이 객체안에서 사용할 컴포넌트들을 원형객체로부터 복사(공유) 해서 온다.
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect);
	HRESULT	SetUp_ConstantTable_Color(LPD3DXEFFECT pEffect);
	HRESULT SetUp_ConstantTable_UV(LPD3DXEFFECT pEffect);
	HRESULT SetUp_ConstantTable_Angle(LPD3DXEFFECT pEffect);
	HRESULT SetUp_ConstantTable_Billboard(LPD3DXEFFECT pEffect);
	HRESULT SetUp_ConstantTable_CoolTime(LPD3DXEFFECT pEffect);
	HRESULT SetUp_ConstantTable_ColorCoolTime(LPD3DXEFFECT pEffect);
public:
	static CUI_Texture* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END