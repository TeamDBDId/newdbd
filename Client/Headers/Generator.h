#pragma once

#include "Defines.h"
#include "GameObject.h"

_BEGIN(Engine)
class CTransform;
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CFrustum;
_END

_BEGIN(Client)
class CCustomLight;
class CGenerator final : public CGameObject
{
public:
	enum STATE { PulleyAdd, PistonsIdleS3, PistonsIdleS2, PistonsIdleS1, PistonsIdle, On, Idle, Failure };
private:
	explicit CGenerator(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CGenerator(const CGenerator& rhs);
	virtual ~CGenerator() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
	virtual void Render_Stencil();
	virtual void Render_ShadowCubeMap(_matrix * VP, LPD3DXEFFECT pEffect, _vec4 vLightPos);
	virtual void Render_CubeMap(_matrix* View, _matrix* Proj);
	virtual void Render_Stemp();
	void	Set_State(_uint State) { m_CurState = (STATE)State; }
public:
	virtual void Set_Base(const _tchar* Key, const _matrix& matWorld, const _int& OtherOption = 0);
	virtual _matrix	Get_Matrix();
	virtual const _tchar* Get_Key();
	virtual _int	Get_OtherOption();
public:
	void Set_CurProgressTimeForSlasher(_float ProgressTime) { m_fCurProgressTimeForSlasher = ProgressTime; }
	_float Get_CurProgressTimeForSlasher() { return m_fCurProgressTimeForSlasher; }

private:
	void Update_Sound();
	void SetUp_Sound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float & SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);
private:
	void State_Check();
	void Progress_Check();
	void Camper_Check();
	void ComunicateWithServer();
	void Init_Light();
private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Dynamic*		m_pMeshCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
	CCollider*			m_pLightCollider[4] = { nullptr, nullptr, nullptr, nullptr };
	CCustomLight*		m_pCustomLight[4] = { nullptr, nullptr, nullptr, nullptr };
	CCollider*			m_pColliderSpotCom = nullptr;
	CCustomLight*		m_pCustomSpotLight = nullptr;
private:
	_bool	m_bDoneRP = false;
	_vec4	m_Color;
	_float	m_fDissonanceTime = 0.f;;
	wstring m_Key;
	_int m_iOtherOption = 0;
	STATE m_OldState = Idle;
	STATE m_CurState = Idle;
	_float	m_fCurProgressTimeForSlasher = 0.f;
	_float	m_fDeltaTime = 0.f;
	_tchar sztt[200] = L"";
	_bool	m_LateInit = false;

	_float	m_fFailTime = 0.f;
	_int	m_LoopSound = 0;
	_float	m_fRatio = 0.f;
	_int	m_SoundCheck = 0;
	list<SoundData>	m_SoundList;
private:
	void Penetration_Check();
private:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const D3DXMESHCONTAINER_DERIVED* pMeshContainer, const _uint& iAttributeID);
public:
	static CGenerator* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END