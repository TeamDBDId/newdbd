#pragma once

#include "Defines.h"
#include "Camera.h"

_BEGIN(Engine)
class CFrustum;
_END

_BEGIN(Client)
class CSlasher;
class CCamera_Slasher final : public CCamera
{
private:
	explicit CCamera_Slasher(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CCamera_Slasher(const CCamera_Slasher& rhs);
	virtual ~CCamera_Slasher() = default;
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
public:
	_bool GetLock() { return m_bIsLockCam; }
public:
	void SetLock(_bool bIsLock) { m_bIsLockCam = bIsLock; }
	void SetCamFrame(const _matrix* pMatCam) { m_pMatCamFrameOfSlasher = pMatCam; }
	_bool	m_bIsControl = false;
private:
	CFrustum*		m_pFrustumCom = nullptr;
private:
	const _matrix*	m_pMatCamFrameOfSlasher = nullptr;
	const _matrix*	m_pMatSlasher = nullptr;
	POINT			m_ptMouse;
	_float			m_fMoveY = 0.f;
	CSlasher*		m_pSlasher = nullptr;
	_bool			m_bIsLockCam = false;
private:
	HRESULT Ready_Component();
	void ComunicateWithServer();
public:
	static CCamera_Slasher* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject();
protected:
	virtual void Free();
};

_END