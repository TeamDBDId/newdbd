#pragma once
#include "GameObject.h"

_BEGIN(Client)

class CUI_Texture;
class CUI_Interaction : public CGameObject
{
public:
	enum ProgressBar { Base, Bar, Texture, Text, ProgressBarEnd };
	enum SkillCheck { S_Base, S_Full, S_NoneFull, S_End, S_GridLine, S_SpaceBar, S_Text, SkillCheckEnd };
	enum SkillCheckState { SC_NONE, SC_Ready, SC_Check };
	struct ButtonInfo
	{
		CUI_Texture* pButtonTexture = nullptr;
		CUI_Texture* pButtonText = nullptr;
		CUI_Texture* pMotionText = nullptr;
	};
private:
	explicit CUI_Interaction(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CUI_Interaction(const CUI_Interaction& rhs);
	virtual ~CUI_Interaction() = default;

public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	void	Add_Button(wstring TextureName, wstring TextName);
	void	Set_ProgressBar(CUIManager::ProgressState State, _int iItem = 0);
	void	Set_ProgressPoint(const _float& fProgress, const _float& fMaxProgress) { m_fProgress = fProgress; m_fMaxProgress = fMaxProgress; }
	void	Set_KeyEvent(CUIManager::KeyEvent KeyEvnet) { m_eKeyEvent = KeyEvnet; }
	_uint	Get_SkillCheckState() { return m_eSkillCheckState; }
	CUIManager::KeyEvent Get_KeyEvent() { return m_eKeyEvent; }
	void	Delete_AllTexture();
	_bool	IsSkillCheck();
private:
	void	SetUp_Spirit();
	void	Interact_Check();
	void	Update_Spirit();
	void	Update_SkillCheck(const _float& fTimeDelta);
	void	Update_ButtonPos();
	void	Update_AddButton_Camper();
	void	Update_AddButton_Slasher();
	void	Check_CollisionObject();
	void	Update_ProgressBar();
	HRESULT SetUp_ProgressBar();
	HRESULT Set_SkillCheck();
	void	KeyEvent_Check(const _float& fDeltaTime);
	void	ColItem_Check();
	void	CurItem_Check();
	void	Set_Resistance();
	void	Set_Wiggle();
	void	Delete_Wiggle();
	void	Update_Observe();
	void	Delete_Resist();

	CUI_Texture* Find_UITexture(wstring UIName);
	void Delete_UI_Texture(wstring UIName);
private:
	_bool			m_IsObserve = false;

	CGameObject*	m_pPlayer = nullptr;
	CGameObject*	m_pCollisionObjtect = nullptr;
	CUI_Texture*	m_ProgressBar[ProgressBarEnd];
	CUI_Texture*	m_SkillCheck[SkillCheckEnd];
	map<wstring, ButtonInfo> m_mapButton;
	map < wstring, CUI_Texture*> m_mapUITexture;

	_float	m_fProgress = 0.f;
	_float	m_fMaxProgress = 0.f;

	_uint	m_PlayerNum = 0;
	_int	m_InteractionObject = 0;
	_float	m_fSkillCheckTime = 0.f;
	_float	m_fClickButton = 0.f;
	_uint	m_eSkillCheckState = SC_NONE;
	_bool	m_isWiggleA = false;

	_uint	m_OldItem = 0;
	_uint	m_OldColItem = 0;
	CUIManager::KeyEvent m_eKeyEvent = CUIManager::EV_None;
	CUIManager::ProgressState m_ProgressState = CUIManager::PG_None;
public:
	static CUI_Interaction* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();

};

_END

