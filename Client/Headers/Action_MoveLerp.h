#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CAction_MoveLerp final : public CAction
{
public:
	explicit CAction_MoveLerp();
	virtual ~CAction_MoveLerp() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void	Send_ServerData() override;
public:
	void SetActionName(_tchar* pName) { m_ActionName = pName; }
	void SetPos(_vec3 vPos) { m_vDistPos = vPos; }
	void SetDuration(_float fDuration) { m_fDuration_TargetAction = fDuration; }
	void SetTargetOfInteraction(CGameObject* pTargetInteraction) { m_pTargetOfInteraction = pTargetInteraction; }
private:
	_int m_iState = 0;
	_vec3 m_vDistPos;
	_vec3 m_vRight;
	_vec3 m_vLook;
	_tchar* m_ActionName = nullptr;
	CGameObject* m_pTargetOfInteraction = nullptr;
	_float	m_fDuration_TargetAction = 0.f;
	_float	m_fDelay = 0.f;
protected:
	virtual void Free();
};

_END