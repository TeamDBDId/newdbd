#pragma once

#include "Defines.h"
#include "GameObject.h"
#include "Transform.h"

_BEGIN(Engine)
class CRenderer;
class CShader;
class CMesh_Dynamic;
class CCollider;
class CMesh_Static;
class CFrustum;
_END

_BEGIN(Client)
class CCustomLight;
class CItem : public CGameObject
{
public:
	enum ITEMTYPE { I_NULL, MEDIKIT, TOOLBOX, FLASHLIGHT, KEY, ITEMEND };
private:
	explicit CItem(LPDIRECT3DDEVICE9 pGraphic_Device);
	explicit CItem(const CItem& rhs);
	virtual ~CItem() = default;
public:
	void Ready_Item(ITEMTYPE eType, _float fDurability, _vec3* vPos);
	void Drop_Item(_vec3* vDropPos);
	void Pick_Item(CTransform* pTransform, const _matrix* pMatRHandFrame);
public:
	virtual HRESULT Ready_Prototype();
	virtual HRESULT Ready_GameObject();
	virtual _int Update_GameObject(const _float& fTimeDelta);
	virtual _int LastUpdate_GameObject(const _float& fTimeDelta);
	virtual void Render_GameObject();
public:
	_bool Use_Item(_float fTimeDelta, CGameObject* pTarget = nullptr, _vec3* vDirection = nullptr);
	_float	Get_Durability() { return m_fDurability; }
	_float	Get_MaxDurability() { return m_fMaxDurability; }
	ITEMTYPE Get_ItemType() { return m_eType; }
	_vec3	Get_Position() { return *m_pTransformCom->Get_StateInfo(CTransform::STATE_POSITION); }
	void SetVisible(_bool Visible) { m_bIsVisible = Visible; }
	void Set_UseNow(_bool bUse) { m_bUseNow = bUse; }
	_matrix* Get_RHandMatrix() { return m_pRHandMatrix; }

private:
	void Update_Sound(const _float& fTimeDelat);
	void SetUp_Sound();
	void Add_SoundList(const _uint& m_iState, const _float Ratio, const char* SoundName, const _float& SoundDistance = 1700.f, const _float& SoundValue = 1.f, const _int& OtherOption = 0);

private:
	CTransform*			m_pTransformCom = nullptr;
	CRenderer*			m_pRendererCom = nullptr;
	CShader*			m_pShaderCom = nullptr;
	CMesh_Static*		m_pMeshCom = nullptr;
	CFrustum*			m_pFrustumCom = nullptr;
	CCollider*			m_pColliderCom = nullptr;
	CCustomLight*		m_pCustomLight = nullptr;
	CCollider*			m_pLightCollider = nullptr;
private:
	_bool				m_bUseNow = false;
	_matrix*			m_pRHandMatrix = nullptr;
	_matrix				m_matParent;
	CTransform*			m_pCamperTransform = nullptr;
	_uint				m_iPlayerNum = 0;
	ITEMTYPE			m_eType = ITEMEND;
	_float				m_fDurability = 0.f;
	_float				m_fMaxDurability = 0.f;
	_vec3				m_vFlashDirection = _vec3();
	_bool				m_bIsVisible = true;
	_float				m_fSoundTimer = -1.f;
	_int				m_iSoundCheck = 0;
	list<SoundData>		m_SoundList;
private:
	void	ComunicateWithServer();
	void	Cal_LightPos();
public:
	HRESULT Ready_Component();
	HRESULT SetUp_ConstantTable(LPD3DXEFFECT pEffect, const _uint& iAttributeID);
	void Check_Player();
public:
	static CItem* Create(LPDIRECT3DDEVICE9 pGraphic_Device);
	virtual CGameObject* Clone_GameObject() override;
protected:
	virtual void Free();
};

_END
