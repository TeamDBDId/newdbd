#pragma once

#include "Defines.h"
#include "Action.h"

_BEGIN(Client)

class CCloset;
class CCamper;
class CSlasher;
class CAction_SearchLocker final : public CAction
{
public:
	explicit CAction_SearchLocker();
	virtual ~CAction_SearchLocker() = default;
public:
	virtual HRESULT Ready_Action() override;
	virtual _int Update_Action(const _float & fTimeDelta) override;
	virtual void End_Action() override;
	virtual void Send_ServerData() override;
public:
	void SetCloset(CGameObject* pCloset) { m_pCloset = (CCloset*)pCloset; }
private:
	_bool m_bIsInit = false;
	_vec3 m_vOriginPos;
	_float	m_fDelay = -1.f;
	_int m_iState = 0;
	_float m_fIndex = 0.f;
	vector<_vec3>	m_vecCamperPos;
	vector<_vec3>	m_vecCamperSpiritPos;
	vector<_vec3>	m_vecWraithPos;
	vector<_vec3>	m_vecSpiritPos;
	CCloset*		m_pCloset = nullptr;
	CCamper*		m_pCamper = nullptr;
	CSlasher*		m_pSlasher = nullptr;
private:
	void SetVectorPos();
protected:
	virtual void Free();
};

_END